//
//  MyEvents.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MyEvents: UIViewController {

    //MARK:- Global declaration
    var pageMenu : CAPSPageMenu?
    
    //MARK:- IBOutlets
    @IBOutlet weak var pagerParent: UIView!
    @IBOutlet weak var viewPage: UIView!
    
    //MARK:- View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        self.configurePager()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    //MARK:- Configure pager
    func configurePager(){
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)

        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let controllerUpcomingEvents = self.storyboard?.instantiateViewController(withIdentifier: "MyEventsListType") as! MyEventsListType
        let controllerHistory = self.storyboard?.instantiateViewController(withIdentifier: "MyEventsListType") as! MyEventsListType
        
        controllerUpcomingEvents.parentNavigationController = self.navigationController
        controllerHistory.parentNavigationController = self.navigationController
        
        controllerUpcomingEvents.title = "Upcoming Events"
        controllerHistory.title = "History"
    
        controllerArray.append(controllerUpcomingEvents)
        controllerArray.append(controllerHistory)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        
        //let bgColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(20),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.black),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(UIColor.white),
            .BottomMenuHairlineColor(UIColor.white),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(20),
            .MenuHeight(40)
        ]
        self.pagerParent.frame.size.height = 0
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.height - 80), pageMenuOptions: parameters)
        
        pageMenu?.view.backgroundColor = UIColor.white
        self.viewPage.addSubview(pageMenu!.view)
    }
    //MARK:- IBActions
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
