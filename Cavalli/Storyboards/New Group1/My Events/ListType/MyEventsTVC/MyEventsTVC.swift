//
//  MyEventsTVC.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
protocol SelectEvent {
    func didTapEvent(selectedEvent: MyEventsModel, isFromEventHistory:Bool)
}
class MyEventsTVC: UITableViewCell {
    
    var arrUpcomingEvents = [MyEventsModel]()
    var arrRecentEvents = [MyEventsModel]()
    var arrViewsOnCarousel : [Any] = []
    var flag = true
    var isUpcoming = false
    var selectedEvent: MyEventsModel?
    var delegate : SelectEvent?
    
    @IBOutlet weak var iCarousel: iCarousel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.iCarousel.scrollSpeed = 0.10

        self.iCarousel.dataSource = self
        self.iCarousel.delegate = self
        
        // Initialization code
    }
}
extension MyEventsTVC : iCarouselDataSource{
    func numberOfItems(in carousel: iCarousel) -> Int {
        if self.isUpcoming{
            return self.arrUpcomingEvents.count
        }
        else{
            return self.arrRecentEvents.count
        }
    }
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let packageView = CavaliNightsPackagesView.instanceFromNib()
        if self.isUpcoming{
            self.setData(view: packageView , index : index)
            //packageView.lblReserved.isHidden = true
            packageView.frame =  CGRect(x: 0, y: 0, width: self.iCarousel.frame.width * 0.73 , height: self.iCarousel.frame.height * 0.94)
            self.arrViewsOnCarousel[index] = packageView
            self.iCarousel.scrollToItem(at: index, duration: 0.1)
            if index == self.self.arrUpcomingEvents.count - 1 && self.flag{
                if self.arrUpcomingEvents.count > 0{
                    self.iCarousel.scrollToItem(at: 1, duration: 0.1)
                } else {
                    self.iCarousel.scrollToItem(at: 0, duration: 0.1)
                }
                self.flag = false
            }
        } else {
            packageView.frame =  CGRect(x: 0, y: 0, width: self.iCarousel.frame.width * 0.73 , height: self.iCarousel.frame.height * 0.94)
            self.setData(view: packageView , index : index)
            self.arrViewsOnCarousel[index] = packageView
            self.iCarousel.scrollToItem(at: index, duration: 0.1)
            if index == self.arrRecentEvents.count - 1 && self.flag {
                if self.arrRecentEvents.count > 0 {
                    self.iCarousel.scrollToItem(at: 1, duration: 0.1)
                } else {
                    self.iCarousel.scrollToItem(at: 0, duration: 0.1)
                }
                self.flag = false
            }
        }
//        if isUpcoming{
//            packageView.lblReserved.isHidden = false
//        }
//        else{
            packageView.lblReserved.isHidden = true
//        }
        return packageView
    }
    func setData(view :CavaliNightsPackagesView , index : Int){
        if isUpcoming {
            if let imageURL = self.arrUpcomingEvents[index].imageUrl{
                view.imageView.sd_setShowActivityIndicatorView(true)
                view.imageView.sd_setIndicatorStyle(.gray)
                view.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
            }
            view.lblTitle.text = self.arrUpcomingEvents[index].title ?? "None"
            let eventDate = Utility.customDateFormatter(dateStr: self.arrUpcomingEvents[index].eventDate ?? "2018-05-01 16:51:00", dateFormat: Constants.serverDateFormat, formatteddate: "EEEE, MMM d")
            view.lblDescription.text = eventDate.uppercased()
        } else {
            if let imageURL = self.arrRecentEvents[index].imageUrl {
                view.imageView.sd_setShowActivityIndicatorView(true)
                view.imageView.sd_setIndicatorStyle(.gray)
                view.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
            }
            view.lblTitle.text = self.arrRecentEvents[index].title ?? "None"
            let eventDate = Utility.customDateFormatter(dateStr: self.arrRecentEvents[index].eventDate ?? "2018-05-01 16:51:00", dateFormat: Constants.serverDateFormat, formatteddate: "EEEE, MMM d")
            view.lblDescription.text = eventDate.uppercased()
        }
    }
}
extension MyEventsTVC : iCarouselDelegate{
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int)
    {
        if self.isUpcoming{
            self.delegate?.didTapEvent(selectedEvent: self.arrUpcomingEvents[index], isFromEventHistory: false)
        }
        else{
            self.delegate?.didTapEvent(selectedEvent: self.arrRecentEvents[index], isFromEventHistory: true)
        }
    }
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.125
        }
        return value
    }
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        self.setCardToLarge(with: carousel.currentItemIndex)
    }
}
extension MyEventsTVC{
    func getCaroselItems(totalEvents : Int){
        let packageView = UIView()
        self.arrViewsOnCarousel.removeAll()
        for _ in 0..<totalEvents {
            self.arrViewsOnCarousel.append(packageView)
        }
    }
    func setCardToLarge(with index: Int) {
        for i in self.arrViewsOnCarousel.indices { //0..<self.viewsOnCarousel.count {
            if i == index {
                let view_carosel = self.arrViewsOnCarousel[i] as! UIView
                view_carosel.frame = CGRect(x: 0, y: 0, width: self.iCarousel.frame.width * 0.73, height: self.iCarousel.frame.size.height - 16)
            }
            else {
                let view_carosel = self.arrViewsOnCarousel[i] as! UIView
                view_carosel.frame = CGRect(x: 0, y: 20, width: self.iCarousel.frame.width * 0.73, height: self.iCarousel.frame.size.height - 64)
            }
        }
    }
}
