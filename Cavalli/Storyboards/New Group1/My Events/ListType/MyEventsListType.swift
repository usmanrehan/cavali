//
//  MyEventsListType.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class MyEventsListType: UIViewController {
    var arrUpcomingEvents = [MyEventsModel]()
    var arrRecentEvents = [MyEventsModel]()
    var parentNavigationController: UINavigationController?
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.processGetMyEvents()
        
        NotificationCenter.default.addObserver(self, selector: #selector(processGetMyEvents), name: NSNotification.Name(rawValue: "myEvents"), object: nil)

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
}

extension MyEventsListType: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = Bundle.main.loadNibNamed("MyEventsTVC", owner: self, options: nil)?.first as! MyEventsTVC
        if let title = self.title{
            if title == "Upcoming Events"{
                cell.isUpcoming = true
            }
        }
        cell.delegate = self
        cell.arrUpcomingEvents = self.arrUpcomingEvents
        cell.arrRecentEvents = self.arrRecentEvents
        if cell.isUpcoming{
            cell.getCaroselItems(totalEvents: self.arrUpcomingEvents.count)
        } else {
            cell.getCaroselItems(totalEvents: self.arrRecentEvents.count)
        }
        cell.iCarousel.reloadData()
        return cell
    }
    
    @objc func processGetMyEvents() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getMyEventsWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response_object = responseObject as NSDictionary
            let RE = response_object.value(forKey: "recent_events") as! [[String : Any]]
            let UE = response_object.value(forKey: "upcoming_events") as! [[String : Any]]
            
            let responseRE = Mapper<MyEventsModel>().mapArray(JSONArray: RE)
            let responseUE = Mapper<MyEventsModel>().mapArray(JSONArray: UE)
            
            self.arrRecentEvents = responseRE
            self.arrUpcomingEvents = responseUE
            if self.arrUpcomingEvents.count == 0 {
                Utility.showToast(message: "No data found.")
            }
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}
extension MyEventsListType: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.frame.height - 40 //* 0.975
    }
}
//MARK:- Service
extension MyEventsListType{
    //MARK: - Service
   
}
extension MyEventsListType: SelectEvent{
    func didTapEvent(selectedEvent: MyEventsModel, isFromEventHistory: Bool) {
        self.pushToCavalliNightEventDetail(selectedEvent, isFromEventHistory: isFromEventHistory)
    }
    
//    func didTapEvent(selectedEvent: MyEventsModel) {
//        self.pushToCavalliNightEventDetail(selectedEvent, isFromEventHistory: <#Bool#>)
//    }
    func pushToCavalliNightEventDetail(_ selectedEvent: MyEventsModel, isFromEventHistory : Bool){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "CNEventDetail") as? CNEventDetail {
            dvc.selectedCavaliEvent = selectedEvent
            print(selectedEvent)
            dvc.isFromReservedEvent = true
            dvc.isFromEventHistory = isFromEventHistory
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
}

