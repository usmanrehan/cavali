//
//  CavalliNights.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class CavalliNights: UIViewController {
    
    var parentNavigationController: UINavigationController?
    //MARK:- Global declaration
    
    var arrViewsOnCarousel : [Any] = []
    var flag = true
    var arrEvents = [MyEventsModel]()
    
    //MARK:- IBOutlets
    @IBOutlet weak var iCarouselView: iCarousel!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.iCarouselView.delegate = self
        self.iCarouselView.dataSource = self
        iCarouselView.scrollSpeed = 0.10
        iCarouselView.type = .linear
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if self.arrEvents.count > 0{
//            self.iCarouselView.scrollToItem(at: 0, duration: 0.2)
//        }
        self.processGetEvents()
    }
    
    //MARK:- IBActions
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.iCarouselView.scrollToItem(at: 0, duration: 0.1)
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- Helper Methods
extension CavalliNights{
    func getCaroselItems(totalEvents : Int){
        let packageView = UIView()
        self.arrViewsOnCarousel.removeAll()
        for _ in 0..<totalEvents {
            self.arrViewsOnCarousel.append(packageView)
        }
    }
    func setCardToLarge(with index: Int) {
        for i in self.arrViewsOnCarousel.indices { //0..<self.viewsOnCarousel.count {
            if i == index {
                let view_carosel = self.arrViewsOnCarousel[i] as! UIView
                view_carosel.frame = CGRect(x: 0, y: 0, width: self.iCarouselView.frame.width * 0.73, height: self.iCarouselView.frame.size.height - 16)
            }
            else {
                let view_carosel = self.arrViewsOnCarousel[i] as! UIView
                view_carosel.frame = CGRect(x: 0, y: 20, width: self.iCarouselView.frame.width * 0.73, height: self.iCarouselView.frame.size.height - 64)
            }
        }
    }
    func pushToCavalliNightEventDetail(_ index: Int){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "CNEventDetail") as? CNEventDetail {
            dvc.selectedCavaliEvent = self.arrEvents[index]
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
extension CavalliNights: iCarouselDataSource{
    func numberOfItems(in carousel: iCarousel) -> Int {
        return self.arrEvents.count
    }
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let packageView = CavaliNightsPackagesView.instanceFromNib()
        packageView.frame =  CGRect(x: 0, y: 0, width: self.iCarouselView.frame.width * 0.73 , height: self.iCarouselView.frame.height * 0.94)
        
        self.setData(view: packageView , index : index)
        
        self.arrViewsOnCarousel[index] = packageView
        //self.iCarouselView.scrollToItem(at: index, duration: 0.1)
        if index == self.arrEvents.count - 1 && self.flag{
//            if self.arrEvents.count > 0 {
//                self.iCarouselView.scrollToItem(at: 1, duration: 0.1)
//            }
//            else{
////                self.iCarouselView.scrollToItem(at: 0, duration: 0.1)
//            }
            self.flag = false
        }
        return packageView
    }
    func setData(view :CavaliNightsPackagesView , index : Int){
        if let imageURL = self.arrEvents[index].imageUrl{
            //view.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"))
            
            view.imageView.sd_setShowActivityIndicatorView(true)
            view.imageView.sd_setIndicatorStyle(.gray)
            view.imageView.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
            
        }
        view.lblTitle.text = self.arrEvents[index].title ?? "None" 
        let eventDate = Utility.customDateFormatter(dateStr: self.arrEvents[index].eventDate ?? "2018-05-01 16:51:00", dateFormat: Constants.serverDateFormat, formatteddate: "EEEE, MMM d")
        view.lblDescription.text = eventDate.uppercased()
    }
}
extension CavalliNights: iCarouselDelegate{
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        self.pushToCavalliNightEventDetail(index)
    }
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.125
        }
        return value
    }
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        self.setCardToLarge(with: carousel.currentItemIndex)
    }
}
//MARK:- Services
extension CavalliNights{
    //MARK: - Service
    func processGetEvents() {
        Utility.showLoader()
        self.flag = true
        let params: [String: Any] = ["event_type":"2"]
        APIManager.sharedInstance.homeAPIManager.getEventsWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MyEventsModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            print(response)
            self.arrEvents = response
            if self.arrEvents.count == 0 {
                Utility.showToast(message: "No Data Found.")
                return
            }
            self.getCaroselItems(totalEvents: self.arrEvents.count)
            
            self.iCarouselView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
        
    }
}
