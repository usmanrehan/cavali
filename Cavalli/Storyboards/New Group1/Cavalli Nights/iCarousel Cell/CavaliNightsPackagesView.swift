//
//  PackagesView.swift
//  TreatzAsia
//
//  Created by Ahmed Shahid on 10/7/17.
//  Copyright © 2017 Ahmed Shahid. All rights reserved.
//

import UIKit

class CavaliNightsPackagesView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    @IBOutlet weak var lblReserved: UILabel!
    
    
    class func instanceFromNib() -> CavaliNightsPackagesView {
        return UINib(nibName: "CavaliNightsPackagesView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CavaliNightsPackagesView
    }

}
