//
//  EventDetailTVC.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class EventDetailTVC: UITableViewCell {

    @IBOutlet weak var lblLocation: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnCalendarMark: UIButton!
    @IBOutlet weak var btnAddInquiry: UIButton!
    @IBOutlet weak var btnReservation: UIButton!

    func setData(event: MarkedEventsModel?){
        self.lblLocation.text = event?.location ?? "None"
        self.lblDate.text = Utility.customDateFormatter(dateStr: event?.eventDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "MMMM dd, YYYY")
        self.lblTime.text = Utility.customDateFormatter(dateStr: event?.eventDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "h:mm a")
        if let eventIsMarked = event?.isMarked{
            if eventIsMarked == 1{
                self.btnCalendarMark.setTitle("Unmark from my Calendar", for: .normal)
            }
            else{
                self.btnCalendarMark.setTitle("Mark to my Calendar", for: .normal)
            }
        }
    }
    func setDataOfCavaliNightEvent(event: MyEventsModel?){
        self.lblLocation.text = event?.location ?? "None"
        self.lblDate.text = Utility.customDateFormatter(dateStr: event?.eventDate ?? "2018-05-01 16:51:00", dateFormat: Constants.serverDateFormat, formatteddate: "EEEE, MMM d")

//        self.lblDate.text = Utility.customDateFormatter(dateStr: event?.eventDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "MMMM dd, YYYY")
        self.lblTime.text = Utility.customDateFormatter(dateStr: event?.eventDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "h:mm a")
        if let eventIsMarked = event?.isMarked{
            if eventIsMarked == 1{
                self.btnCalendarMark.setTitle("Unmark from my Calendar", for: .normal)
            }
            else{
                self.btnCalendarMark.setTitle("Mark to my Calendar", for: .normal)
            }
        }
    }
}

