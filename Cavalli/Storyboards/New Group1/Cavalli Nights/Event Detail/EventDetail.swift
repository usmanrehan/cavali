//
//  DishDetail.swift
//  Cavalli
//
//  Created by shardha on 3/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SDWebImage
class CNEventDetail: UIViewController{
    
    //MARK:- Global declarations
    var product = Products()
    var selectedEvent: MarkedEventsModel?
    var selectedCavaliEvent: MyEventsModel?
    var event_id = 0
    var btnCalendarMark = UIButton()
    var isFromReservedEvent : Bool? = false
    
    var isFromEventHistory : Bool? = false

    //MARK:- IBOutlets
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

//        print(selectedEvent!)
//        print(selectedCavaliEvent!)
        
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.register(UINib(nibName: "DDTitleTVC", bundle: nil), forCellReuseIdentifier: "DDTitleTVC")
        self.tableView.register(UINib(nibName: "DDDescriptionTVC", bundle: nil), forCellReuseIdentifier: "DDDescriptionTVC")
        self.tableView.register(UINib(nibName: "EventDetailTVC", bundle: nil), forCellReuseIdentifier: "EventDetailTVC")
      

        if let imageURL = self.selectedEvent?.eventImages.first?.imageUrl {
            //self.imageBanner.sd_setImage(with: URL(string: imageURL))
            
            self.imageBanner.sd_setShowActivityIndicatorView(true)
            self.imageBanner.sd_setIndicatorStyle(.gray)
            self.imageBanner.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        }
        else{
            if let imageURL = self.selectedEvent?.imageUrl{
                //self.imageBanner.sd_setImage(with: URL(string: imageURL))
                
                self.imageBanner.sd_setShowActivityIndicatorView(true)
                self.imageBanner.sd_setIndicatorStyle(.gray)
                self.imageBanner.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
            }
        }
        if let image_URL = self.selectedCavaliEvent?.eventImages.first?.imageUrl{
            //self.imageBanner.sd_setImage(with: URL(string: image_URL))
            self.imageBanner.sd_setShowActivityIndicatorView(true)
            self.imageBanner.sd_setIndicatorStyle(.gray)
            self.imageBanner.sd_setImage(with: URL(string: image_URL), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        }
        else{
            if let image_URL = self.selectedCavaliEvent?.imageUrl{
                //self.imageBanner.sd_setImage(with: URL(string: image_URL))
                self.imageBanner.sd_setShowActivityIndicatorView(true)
                self.imageBanner.sd_setIndicatorStyle(.gray)
                self.imageBanner.sd_setImage(with: URL(string: image_URL), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
            }
        }
        if self.event_id != 0{
            self.checkMarkStatusWith()
        }        
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "myEvents"), object: nil, userInfo: nil)

        self.navigationController?.popViewController(animated: true)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.processGetMyEvents()), name: NSNotification.Name(rawValue: "myEvents"), object: nil)

    }
}
extension CNEventDetail : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDTitleTVC") as? DDTitleTVC else { return UITableViewCell() }
            cell.lableTitle.text = self.selectedEvent?.title ?? self.selectedCavaliEvent?.title
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDDescriptionTVC") as? DDDescriptionTVC else { return UITableViewCell() }
            let plainText = self.selectedEvent?.descriptionValue ?? self.selectedCavaliEvent?.descriptionValue
            if let descr = plainText{
                cell.lableDesc.attributedText = Utility.getLightDarkGrayString(plainText: descr)
            }
            else{
                cell.lableDesc.attributedText = Utility.getLightDarkGrayString(plainText: "None")
            }
            //https://stackoverflow.com/a/35087471
//            OperationQueue.main.addOperation({() -> Void in
//                cell.lableDesc.attributedText = Utility.getBoldDarkGrayString(boldText: "Description: ", plainText: plainText)
//            })
            
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "EventDetailTVC") as? EventDetailTVC else { return UITableViewCell() }
            cell.selectionStyle = .none
          //  print(selectedCavaliEvent!)
            
            if self.selectedEvent != nil{
                cell.setData(event: self.selectedEvent)
            }
            if self.selectedCavaliEvent != nil{
            cell.setDataOfCavaliNightEvent(event: self.selectedCavaliEvent)
            }
            cell.btnCalendarMark.addTarget(self, action: #selector(onBtnCalendarMark(sender:)), for: .touchUpInside)
            self.btnCalendarMark = cell.btnCalendarMark
            cell.btnAddInquiry.addTarget(self, action: #selector(onBtnAddInquiry), for: .touchUpInside)
            
            cell.btnReservation.addTarget(self, action: #selector(addReservation), for: .touchUpInside)
            
            if isFromReservedEvent == true {
                cell.btnReservation.isHidden = true
            }
            if isFromEventHistory == true {
                cell.btnCalendarMark.isHidden = true
            }
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    //MARK:- Helper Methods
    @objc func onBtnCalendarMark(sender: UIButton) {
        self.markUnMarkEvent(sender: sender)
    }
    @objc func onBtnAddInquiry(sender: UIButton) {
         self.pushToAddInquiry()
    }
    
    @objc func addReservation(sender: UIButton) {
//        print(selectedCavaliEvent)
        
        let storyboard = UIStoryboard(name: "HomeModule", bundle: nil)
        guard let reservationFilter = storyboard.instantiateViewController(withIdentifier: "ReservationFilter") as? ReservationFilter else { return }
        guard let reservation = storyboard.instantiateViewController(withIdentifier: "Reservations") as? Reservations else { return }
        
        if let eventId  = self.selectedCavaliEvent?.id{
            reservation.selectedEvent = "\(eventId)"
            reservation.selectedEventTitle = selectedCavaliEvent?.title
            reservation.isFromEvents = true
            reservation.selectedEventDate = selectedCavaliEvent?.eventDate
        } else {
            if let eventId = self.selectedEvent?.id{
                reservation.selectedEvent = "\(eventId)"
                reservation.selectedEventTitle = selectedEvent?.title
                reservation.isFromEvents = true
                reservation.selectedEventDate = selectedEvent?.eventDate
            }
        }
        
        
//        reservation.selectedEvent = "\((selectedCavaliEvent?.id)!)"
//        reservation.selectedEventTitle = selectedCavaliEvent?.title
//        reservation.isFromEvents = true
//        reservation.selectedEventDate = selectedCavaliEvent?.eventDate//Utility.customDateFormatter(dateStr: selectedCavaliEvent?.eventDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "dd MMM yyyy")
//        reservation.selectedEventTime = Utility.customDateFormatter(dateStr: selectedCavaliEvent?.eventDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "hh:mm a")
        
        let nav = ReservationNavigation(menuViewController: UIViewController(), contentViewController: reservation)
        nav.navigationBar.isHidden = true
        nav.sideMenu = ENSideMenu(sourceView: nav.view, menuViewController: reservationFilter, menuPosition:.right)
        nav.sideMenu?.menuWidth = self.view.frame.size.width * 0.60
//            self.navigationController?.pushViewController(nav, animated: true)
        self.present(nav, animated: true, completion: nil)
    }
    
    func pushToAddInquiry(){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "Inquiry") as? Inquiry {
            dvc.selectedEvent = self.selectedEvent
            dvc.selectedCavaliEvent = self.selectedCavaliEvent
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}

extension CNEventDetail: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.item {
        case 0:
            return  self.view.frame.size.height * 0.075
        case 1:
            return  UITableViewAutomaticDimension
        case 2:
            return  UITableViewAutomaticDimension
        default:
            return  self.view.frame.size.height * 0.080
        }
    }
}

//MARK:- Services
extension CNEventDetail{
    func markUnMarkEvent(sender: UIButton) {
        Utility.showLoader()
        var event_id = 0
        if self.selectedEvent != nil{
            event_id = self.selectedEvent?.parentId ?? 0
        }
        if self.selectedCavaliEvent != nil{
            event_id = self.selectedCavaliEvent?.parentId ?? 0
        }
        let params: [String: Any] = ["event_id": event_id]
        print(params)
        APIManager.sharedInstance.homeAPIManager.markUnMarkEventWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            let response = responseObject as NSDictionary
            if let eventIsMarked = response.value(forKey: "is_marked") as? Int{
                if eventIsMarked == 1{
                    sender.setTitle("Unmark from my Calendar", for: .normal)
                }
                else{
                    sender.setTitle("Mark to my Calendar", for: .normal)
                }
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
    
    func checkMarkStatusWith() {
        Utility.showLoader()
        let params: [String: Any] = ["event_id": self.event_id]
        print(params)
        APIManager.sharedInstance.homeAPIManager.checkMarkStatusWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            print(responseObject)
            let response = responseObject as NSDictionary
            print(response)
            if let eventIsMarked = response.value(forKey: "is_marked") as? Int{
                if eventIsMarked == 1{
                    self.btnCalendarMark.setTitle("Unmark from my Calendar", for: .normal)
                }
                else{
                    self.btnCalendarMark.setTitle("Mark to my Calendar", for: .normal)
                }
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}
