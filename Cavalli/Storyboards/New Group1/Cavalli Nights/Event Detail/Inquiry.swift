//
//  Inquiry.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/11/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class Inquiry: UIViewController {

    var selectedEvent: MarkedEventsModel?
    var selectedCavaliEvent: MyEventsModel?
    let placeholderIntro = "Your Message"
    
    @IBOutlet weak var tfFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var tvYourMessage: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tfFullName.text = AppStateManager.sharedInstance.loggedInUser.userName
        self.tvYourMessage.delegate = self
        self.tvYourMessage.textColor = UIColor.lightGray
        self.tvYourMessage.text = self.placeholderIntro
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnSubmit(_ sender: Any) {
        validate()
    }
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    func validate(){
        let message = self.tvYourMessage.text.trimmingCharacters(in: .whitespacesAndNewlines)
        guard Validation.validateStringLength(message) else{
            Utility.showToast(message: "Please provide message.")
            return
        }
        if message == placeholderIntro{
            Utility.showToast(message: "Please provide message.")
            return
        }
        self.addInquiry()
    }
}
extension Inquiry: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.placeholderIntro
            textView.textColor = UIColor.lightGray
        }
    }
}
extension Inquiry{
    //MARK:- Services
    func addInquiry () {
        Utility.showLoader()
        var event_id = 0
        if self.selectedEvent != nil{
            event_id = self.selectedEvent?.id ?? 0
        }
        if self.selectedCavaliEvent != nil{
            event_id = self.selectedCavaliEvent?.id ?? 0
        }
        print(event_id)
        let params: [String: Any] = ["event_id": event_id,
                                     "message": self.tvYourMessage.text ?? ""]
        APIManager.sharedInstance.userAPIManager.addInquiryWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.navigationController?.popViewController(animated: true)
            Utility.showToast(message: "Inquiry successfully submitted.")
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }

}
