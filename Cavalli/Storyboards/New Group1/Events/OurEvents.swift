//
//  OurEvents.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class OurEvents: UIViewController {
    
    var arrLatestUpdates = [MyEventsModel]()
    
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "MenuCategoryTVC", bundle: nil), forCellReuseIdentifier: "MenuCategoryTVC")
    }
    //MARK:- IBActions
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension OurEvents: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrLatestUpdates.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCategoryTVC") as? MenuCategoryTVC else { return UITableViewCell() }
        if let imageURL = self.arrLatestUpdates[indexPath.row].imageUrl{
            //cell.imageProduct.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"))
            cell.imageProduct.sd_setShowActivityIndicatorView(true)
            cell.imageProduct.sd_setIndicatorStyle(.gray)
            cell.imageProduct.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
        }
        cell.labelTitle.text = self.arrLatestUpdates[indexPath.row].title ?? "None"
        cell.labelDesc.text = self.arrLatestUpdates[indexPath.row].descriptionValue ?? "None"
        return cell
    }
}

extension OurEvents: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  self.tableView.frame.size.height * 0.25
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushToOurEventsDetail(indexPath.row)
    }
    //MARK:- Helper Methods
    func pushToOurEventsDetail(_ index: Int){
        // var selectedLatestUpdates : MyEventsModel?
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "OurEventDetail") as? OurEventDetail {
            dvc.selectedLatestUpdates = self.arrLatestUpdates[index]
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
