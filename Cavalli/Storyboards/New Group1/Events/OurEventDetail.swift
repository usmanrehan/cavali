//
//  EventDetail.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class OurEventDetail: UIViewController {
    
    var selectedLatestUpdates : MyEventsModel?
    
    @IBOutlet weak var imageEvent: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.register(UINib(nibName: "DDTitleTVC", bundle: nil), forCellReuseIdentifier: "DDTitleTVC")
        self.tableView.register(UINib(nibName: "DDDescriptionTVC", bundle: nil), forCellReuseIdentifier: "DDDescriptionTVC")
        self.tableView.register(UINib(nibName: "Date_TimeTVC", bundle: nil), forCellReuseIdentifier: "Date_TimeTVC")
        self.tableView.register(UINib(nibName: "BarOrderingAddToCart", bundle: nil), forCellReuseIdentifier: "BarOrderingAddToCart")
        self.tableView.register(UINib(nibName: "OurEventsShareTVC", bundle: nil), forCellReuseIdentifier: "OurEventsShareTVC")
        
        if let imageURL = self.selectedLatestUpdates?.eventImages.first?.imageUrl{
            //self.imageEvent.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"))
            self.imageEvent.sd_setShowActivityIndicatorView(true)
            self.imageEvent.sd_setIndicatorStyle(.gray)
            self.imageEvent.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
        }
        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension OurEventDetail : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDTitleTVC") as? DDTitleTVC else { return UITableViewCell() }
            cell.lableTitle.text = self.selectedLatestUpdates?.title ?? "None"
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDDescriptionTVC") as? DDDescriptionTVC else { return UITableViewCell() }
            let desc = self.selectedLatestUpdates?.descriptionValue ?? "None"
            cell.lableDesc.attributedText = Utility.getLightDarkGrayString(plainText: desc)
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "Date_TimeTVC") as? Date_TimeTVC else { return UITableViewCell() }
            let eventDate = self.selectedLatestUpdates?.eventDate ?? "2018-12-12 12:24:00"
            
            if eventDate.count != 0 {
                cell.lblDate.text = Utility.customDateFormatter(dateStr: eventDate, dateFormat: Constants.serverDateFormat, formatteddate: "EEEE, MMM d")
            }
            
            cell.selectionStyle = .none
            cell.timeView.isHidden = true
            cell.btnAddInquiry.isHidden = true
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "OurEventsShareTVC") as? OurEventsShareTVC else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.btnShare.addTarget(self, action: #selector(self.onBtnShare), for: .touchUpInside)
            return cell
        default:
            return UITableViewCell()
        }
    }
    
    //MARK:- Helper Methods
    @objc func onBtnShare(){
        self.shareData()
    }
    
    func shareData(){
        let shareTextTitle = "\(selectedLatestUpdates?.title ?? "")"
        let shareTextDesc = "\(selectedLatestUpdates?.descriptionValue ?? "")"
        let shareText = shareTextTitle + " \n" + shareTextDesc
        let shareImage = self.imageEvent.image ?? UIImage(named: "placeHolder")
        let shareAll = [shareImage! , shareText] as [Any]
        let shareVC: UIActivityViewController = UIActivityViewController(activityItems: shareAll, applicationActivities: nil)
        self.present(shareVC, animated: true, completion: nil)
    }
}

extension OurEventDetail: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.item {
        case 0:
            return  self.view.frame.size.height * 0.07
        case 1:
            return  UITableViewAutomaticDimension
        case 2:
            return  self.tableView.frame.height * 0.2
        case 3:
            return  UITableViewAutomaticDimension
        default:
            return  self.view.frame.size.height * 0.080
        }
    }
}

