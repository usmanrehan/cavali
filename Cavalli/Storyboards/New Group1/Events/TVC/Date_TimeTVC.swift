//
//  Date_TimeTVC.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Date_TimeTVC: UITableViewCell {

    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var btnAddInquiry: UIButton!
    @IBOutlet weak var timeView: UIStackView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
