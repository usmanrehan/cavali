//
//  MusicPlayer.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Jukebox
import MediaPlayer
import ObjectMapper
class MusicPlayer: UIViewController {

    @IBOutlet weak var imageImageBackground: UIImageView!
    @IBOutlet weak var imageMusicWall: UIImageView!
    @IBOutlet weak var btnPlayPause: UIButton!
    @IBOutlet weak var sliderVolume: UISlider!
    @IBOutlet weak var lblTitle: MarqueeLabel!
    
    var jukebox: Jukebox?
    let volumeView = MPVolumeView(frame: .zero)
    var musicInfo : LiveMusicInfo?
    let musicURL = "http://edge.mixlr.com/channel/dqgpt" //"https://www.soundhelix.com/examples/mp3/SoundHelix-Song-1.mp3"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        volumeView.isHidden = true
        self.view.addSubview(volumeView)
        NotificationCenter.default.addObserver(self, selector: #selector(volumeChanged), name: NSNotification.Name(rawValue: "AVSystemController_SystemVolumeDidChangeNotification"), object: nil)
        
        getMusicInfo()
        // Do any additional setup after loading the view.
    }
    
    
    @objc func volumeChanged(notification: NSNotification) {
        if let userInfo = notification.userInfo {
            if let volumeValue = userInfo["AVSystemController_AudioVolumeNotificationParameter"] as? Float {
                self.sliderVolume.value = volumeValue
            }
        }
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.jukebox?.stop()
    }
  
    func setData(){
        self.lblTitle.text = self.musicInfo?.title ?? "None"
        if let imageURL = self.musicInfo?.imageUrl{
            self.imageMusicWall.sd_setShowActivityIndicatorView(true)
            self.imageMusicWall.sd_setIndicatorStyle(.gray)
            self.imageMusicWall.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
            
            self.imageImageBackground.sd_setShowActivityIndicatorView(true)
            self.imageImageBackground.sd_setIndicatorStyle(.gray)
            self.imageImageBackground.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
        }
        self.jukebox = Jukebox(delegate: self, items: [
            JukeboxItem(URL: URL(string : musicURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!)!)
            ])
        self.btnPlayPause.isSelected = true
        
        self.jukebox?.play()
        
        Utility.hideLoader()
    }
    //MARK:- IBActions
    @IBAction func onSliderValueChange(_ sender: UISlider) {
        self.jukebox?.volume = sender.value
        
    }
    @IBAction func onBtnPlay(_ sender: UIButton) {
        self.btnPlayPause.isSelected = !self.btnPlayPause.isSelected
        if sender.isSelected{
            self.jukebox?.play()
        }
        else{
            self.jukebox?.pause()
        }
    }
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
//MARK:- Services
extension MusicPlayer{
    func getMusicInfo() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getLiveMusicInfo(params: params, success: {
            (responseObject) in
            print(responseObject)
            let response = Mapper<LiveMusicInfo>().map(JSON: responseObject as [String : Any])
            self.musicInfo = response
            self.setData()

//            self.tableView.reloadSections([0], with: .none)
        }, failure: {
            (error) in
            Utility.hideLoader()
            print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}
// MARK: - JUKEBOX DELEGATE
extension MusicPlayer: JukeboxDelegate
{
    func jukeboxStateDidChange(_ jukebox: Jukebox) {
        
    }
    
    func jukeboxPlaybackProgressDidChange(_ jukebox: Jukebox) {
        
    }
    
    func jukeboxDidLoadItem(_ jukebox: Jukebox, item: JukeboxItem) {
        
    }
    
    func jukeboxDidUpdateMetadata(_ jukebox: Jukebox, forItem: JukeboxItem) {
        
    }
    
    
}
