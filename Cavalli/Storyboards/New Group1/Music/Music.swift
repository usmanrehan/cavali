//
//  Music.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class Music: UIViewController {
    
    //MARK:- Global declaration
    var arrLatestUpdates = [MyEventsModel]()
    var musicInfo : LiveMusicInfo?
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.getMusicInfo()
        self.processlatestUpdates()
        self.tableView.register(UINib(nibName: "HomeCollectionTVC", bundle: nil), forCellReuseIdentifier: "HomeCollectionTVC")
        // Do any additional setup after loading the view.
    }
    
    //MARK:- IBActions
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension Music: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = Bundle.main.loadNibNamed("iCarousel_TVC", owner: self, options: nil)?.first as! iCarousel_TVC
            cell.selectionStyle = .none
            
            cell.delegate = self
            cell.getCaroselItems()
            cell.musicInfo = self.musicInfo
            cell.btnSelectMusic.addTarget(self, action: #selector(onBtnMusicPress), for: .touchUpInside)
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionTVC") as? HomeCollectionTVC else { return UITableViewCell() }
            cell.gridView.isHidden = true
            cell.collectionView.isHidden = false
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.selectionStyle = .none
            let layout = cell.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.scrollDirection = .horizontal
            cell.collectionView.register(UINib(nibName: "HomeLatestUpdatesCVC", bundle: nil), forCellWithReuseIdentifier: "HomeLatestUpdatesCVC")
            cell.collectionView.reloadData()
            return cell
        default:
            return UITableViewCell()
        }
    }
    @objc func onBtnMusicPress(){
        self.pushToMusicPlayer()
    }
}
extension Music: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return self.tableView.frame.height * 0.85
        default:
            return self.tableView.frame.size.width/2
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: HomeHeader = .fromNib()
        header.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        switch section {
        case 1:
            header.labelTitle.text = "LATEST UPDATES"
            header.buttonSeeAll.isHidden = false
            header.buttonSeeAll.addTarget(self, action: #selector(self.actionSeeAllLatestUpdates), for: .touchUpInside)
            return header
        default:
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        switch section {
        case 1:
            return 30
        default:
            return 0
        }
    }
    @objc func actionSeeAllLatestUpdates(){
        self.pushToOurEvents()
    }
    func pushToOurEvents(){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "OurEvents") as? OurEvents {
            dvc.arrLatestUpdates = self.arrLatestUpdates
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    func pushToMusicPlayer(){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "MusicPlayer") as? MusicPlayer {
            dvc.musicInfo = self.musicInfo
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }

}
extension Music: SelectMusicEvent{
    func didSelectMusicEvent(_ index: Int) {
        self.pushToMusicPlayer(index)
    }
    func pushToMusicPlayer(_ index: Int){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "MusicPlayer") as? MusicPlayer {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
//MARK:- Services
extension Music{
    func getMusicInfo() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getLiveMusicInfo(params: params, success: {
            (responseObject) in
            print(responseObject)
            let response = Mapper<LiveMusicInfo>().map(JSON: responseObject as [String : Any])
            self.musicInfo = response
            self.tableView.reloadSections([0], with: .none)
        }, failure: {
            (error) in
            Utility.hideLoader()
            print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
    func processlatestUpdates() {
        Utility.showLoader()
        let params: [String: Any] = ["event_type":"1"]
        APIManager.sharedInstance.homeAPIManager.getEventsWith(params: params, success: {
            (responseObject) in
            print(responseObject)
            Utility.hideLoader()
            let response = Mapper<MyEventsModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            print(response)
            self.arrLatestUpdates = response
            self.tableView.reloadSections([1], with: .none)
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}
