//
//  PackagesView.swift
//  TreatzAsia
//
//  Created by Ahmed Shahid on 10/7/17.
//  Copyright © 2017 Ahmed Shahid. All rights reserved.
//

import UIKit

class PackagesView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDescription: UILabel!
    
    
    class func instanceFromNib() -> PackagesView {
        return UINib(nibName: "PackagesView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! PackagesView
    }

}
