//
//  iCarousel_TVC.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

protocol SelectMusicEvent {
    func didSelectMusicEvent(_ index: Int)
}

class iCarousel_TVC: UITableViewCell {
    
    var musicInfo : LiveMusicInfo?
    let iCarouselItems = 1
    var arrViewsOnCarousel : [Any] = []
    var flag = true
    var delegate : SelectMusicEvent?
    
    @IBOutlet weak var btnSelectMusic: UIButton!
    @IBOutlet weak var iCarouselContentView: iCarousel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.iCarouselContentView.delegate = self
        self.iCarouselContentView.dataSource = self
        self.iCarouselContentView.scrollSpeed = 0.10

        iCarouselContentView.type = .linear
        
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}


extension iCarousel_TVC: iCarouselDataSource{
    
    func numberOfItems(in carousel: iCarousel) -> Int {
        return iCarouselItems
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        let packageView = PackagesView.instanceFromNib()
        packageView.frame =  CGRect(x: 0, y: 0, width: self.iCarouselContentView.frame.width * 0.73 , height: self.iCarouselContentView.frame.height * 0.94)
    
        if let imageURL = self.musicInfo?.imageUrl{
            //packageView.imageView.sd_setImage(with: URL(string : imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"))
            
            packageView.imageView.sd_setShowActivityIndicatorView(true)
            packageView.imageView.sd_setIndicatorStyle(.gray)
            packageView.imageView.sd_setImage(with: URL(string : imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
        }
        packageView.lblTitle.text = self.musicInfo?.title ?? "None"
        self.arrViewsOnCarousel[index] = packageView
        self.iCarouselContentView.scrollToItem(at: index, duration: 0.1)
        
        if index == iCarouselItems - 1 && self.flag{
            self.iCarouselContentView.scrollToItem(at: 1, duration: 0.1)
            self.flag = false
        }
        return packageView
    }
}
extension iCarousel_TVC: iCarouselDelegate{
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        if (option == .spacing) {
            return value * 1.125
        }
        return value
    }
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        self.setCardToLarge(with: carousel.currentItemIndex)
    }
    func carousel(_ carousel: iCarousel, didSelectItemAt index: Int) {
        if self.delegate != nil{
            self.delegate?.didSelectMusicEvent(index)
        }
    }
}
extension iCarousel_TVC{
    //MARK:- Helper Methods
    func getCaroselItems(){
        let packageView = UIView()
        self.arrViewsOnCarousel.removeAll()
        for _ in 0..<iCarouselItems {
            self.arrViewsOnCarousel.append(packageView)
        }
    }
    func setCardToLarge(with index: Int) {
        for i in self.arrViewsOnCarousel.indices { //0..<self.viewsOnCarousel.count {
            if i == index {
                let view_carosel = self.arrViewsOnCarousel[i] as! UIView
                view_carosel.frame = CGRect(x: 0, y: 0, width: self.iCarouselContentView.frame.width * 0.73, height: self.iCarouselContentView.frame.size.height - 16)
            }
            else {
                let view_carosel = self.arrViewsOnCarousel[i] as! UIView
                view_carosel.frame = CGRect(x: 0, y: 20, width: self.iCarouselContentView.frame.width * 0.73, height: self.iCarouselContentView.frame.size.height - 64)
            }
        }
    }
}
