//
//  Music_ExtensionCV.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
extension Music: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrLatestUpdates.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeLatestUpdatesCVC", for: indexPath) as? HomeLatestUpdatesCVC else { return UICollectionViewCell() }
        if let imageURL = self.arrLatestUpdates[indexPath.row].imageUrl{
            //cell.imageBanner.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"))
            cell.imageBanner.sd_setShowActivityIndicatorView(true)
            cell.imageBanner.sd_setIndicatorStyle(.gray)
            cell.imageBanner.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "placeHolder"), options: .refreshCached, progress: nil, completed: nil)
        }
        cell.lableName.text = self.arrLatestUpdates[indexPath.row].title ?? "None"
        return cell
    }
}
extension Music: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.pushToOurEventsDetails(indexPath.row)
    }
    func pushToOurEventsDetails(_ Index: Int){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "OurEventDetail") as? OurEventDetail {
            dvc.selectedLatestUpdates = self.arrLatestUpdates[Index]
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
extension Music: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         //latest updates
        return CGSize(width: collectionView.frame.size.width/2.5 , height: collectionView.frame.size.width/2.5)
    
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
         //latest updates
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        //latest updates
        return 0
    }
}
