//
//  GalleryCell.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/21/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class GalleryCell: UICollectionViewCell {

    @IBOutlet weak var imageGallery: UIImageView!
    
    func configureCell(_ data : GalleryModel){
        self.imageGallery.sd_setShowActivityIndicatorView(true)
        self.imageGallery.sd_setIndicatorStyle(.gray)
        guard let imageURL = data.imageUrl else { return }
        self.imageGallery.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
    }
}
