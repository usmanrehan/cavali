//
//  Gallery.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/21/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import ImageSlideshow

class Gallery: UIViewController {
    
    var arrLatestPhotos = [GalleryModel]()
    var arrAllPhotos = [GalleryModel]()
    var arrCategories = [MenuCategoryModel]()
    var arrImagesLatestPhotos = [UIImage]()
    var arrImagesAllPhotos = [UIImage]()
    
    var selectedItem : String? = ""
    var flag = true
    
    @IBOutlet weak var daysSelectionCollectionView: UICollectionView!

    @IBOutlet weak var latestPhotosCollectionView: UICollectionView!
    @IBOutlet weak var photosCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.daysSelectionCollectionView.register(UINib(nibName: "DaysSelectionCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "DaysSelectionCollectionViewCell")
        self.latestPhotosCollectionView.register(UINib(nibName: "GalleryCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCell")
        self.photosCollectionView.register(UINib(nibName: "GalleryCell", bundle: nil), forCellWithReuseIdentifier: "GalleryCell")
        self.daysSelectionCollectionView.dataSource = self
        self.latestPhotosCollectionView.dataSource = self
        self.photosCollectionView.dataSource = self
        self.daysSelectionCollectionView.delegate = self
        self.latestPhotosCollectionView.delegate = self
        self.photosCollectionView.delegate = self
        // Do any additional setup after loading the view.
        setDaySelection()
        processGetCategory()
    }
    
    func setDaySelection() {
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        selectedItem = formatter.string(from: date)
        selectedItem = selectedItem?.lowercased()
        daysSelectionCollectionView.reloadData()
    }

    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension Gallery: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == daysSelectionCollectionView {
            return self.arrCategories.count
        }
        if collectionView.tag == 0{
            return self.arrLatestPhotos.count
        } else{
            return self.arrAllPhotos.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == daysSelectionCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "DaysSelectionCollectionViewCell", for: indexPath) as! DaysSelectionCollectionViewCell
            cell.lblDaysSelection.text = arrCategories[indexPath.row].name
            cell.lblDaysSelection.textColor = UIColor.darkGray
            if selectedItem! == arrCategories[indexPath.row].name {
                cell.lblDaysSelection.textColor = Global.APP_COLOR
            }
            return cell
        } else {
            let item = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryCell
            if collectionView.tag == 0{
                item.configureCell(self.arrLatestPhotos[indexPath.row])
                self.arrImagesLatestPhotos[indexPath.row] = item.imageGallery.image ?? #imageLiteral(resourceName: "image-placeholder")
                return item
            } else{
                item.configureCell(self.arrAllPhotos[indexPath.row])
                self.arrImagesAllPhotos[indexPath.row] = item.imageGallery.image ?? #imageLiteral(resourceName: "image-placeholder")
                return item
            }
        }
    }
}

extension Gallery: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == daysSelectionCollectionView {
            selectedItem = arrCategories[indexPath.row].name
            daysSelectionCollectionView.reloadData()
            processGetGallery(category_id: "\(arrCategories[indexPath.row].id)")
        } else {
            var imageSource: [AlamofireSource] = []
            if collectionView.tag == 0{
                self.flag = true
                for objImage in arrLatestPhotos {
                    imageSource.append(AlamofireSource(urlString: objImage.imageUrl!)!)
                }
            } else {
                self.flag = false
                for objImage in arrAllPhotos {
                    imageSource.append(AlamofireSource(urlString: objImage.imageUrl!)!)
                }
            }
            let slideshow = ImageSlideshow()
            slideshow.setImageInputs(imageSource)
            slideshow.backgroundColor = UIColor.clear

            slideshow.slideshowInterval = 5.0
            slideshow.pageControlPosition = PageControlPosition.insideScrollView
            slideshow.pageControl.currentPageIndicatorTintColor = UIColor(displayP3Red: 161/255.0, green: 145/255.0, blue: 97/255.0, alpha: 1.0)
            slideshow.zoomEnabled = true
            slideshow.setCurrentPage(indexPath.row, animated: true)
            
            slideshow.presentFullScreenController(from: self)
        }
    }
}

extension Gallery: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == daysSelectionCollectionView {
            return CGSize(width: daysSelectionCollectionView.frame.size.width / 3 , height: 40)
        } else {
            if collectionView.tag == 0{
                var lengthOfItem = CGFloat((collectionView.frame.width))
                lengthOfItem = CGFloat((collectionView.frame.height))
                return CGSize(width: lengthOfItem , height: lengthOfItem)
            } else{
                let lengthOfItem = CGFloat((collectionView.frame.width)) / 2.075
                return CGSize(width: lengthOfItem , height: lengthOfItem)
            }
        }
    }
}

//MARK: Service
extension Gallery{
    func processGetCategory() {
        Utility.showLoader()
        let params: [String: Any] = [:]
        APIManager.sharedInstance.homeAPIManager.getCategoryWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let menuCategory = Mapper<MenuCategoryModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrCategories = menuCategory
            self.selectedItem = self.arrCategories.first?.name
            self.daysSelectionCollectionView.reloadData()
            self.processGetGallery(category_id: "\(self.arrCategories[0].id)")
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    
    func processGetGallery(category_id : String) {
        Utility.showLoader()
        let params: [String: Any] = ["category_id": category_id]
        APIManager.sharedInstance.homeAPIManager.getGalleryWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response_object = responseObject as NSDictionary
            print(response_object)
            let LP = response_object.value(forKey: "latest_photo") as! [[String : Any]]
            let AP = response_object.value(forKey: "generall_photo") as! [[String : Any]]

            let responseLP = Mapper<GalleryModel>().mapArray(JSONArray: LP)
            let responseAP = Mapper<GalleryModel>().mapArray(JSONArray: AP)
            
            self.arrLatestPhotos = responseLP
            self.arrAllPhotos    = responseAP
            
            self.latestPhotosCollectionView.reloadData()
            self.photosCollectionView.reloadData()
            
            if self.arrLatestPhotos.count == 0 && self.arrAllPhotos.count == 0 {
                Utility.showToast(message: "No data found.")
            }
            self.arrImagesLatestPhotos.removeAll()
            self.arrImagesAllPhotos.removeAll()
            for _ in self.arrLatestPhotos{
                self.arrImagesLatestPhotos.append(UIImage(named: "image-placeholder")!)
            }
            for _ in self.arrAllPhotos{
                self.arrImagesAllPhotos.append(UIImage(named: "image-placeholder")!)
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}
