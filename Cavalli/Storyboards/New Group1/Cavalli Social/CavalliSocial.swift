//
//  CavalliSocial.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/21/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper_Realm
import ObjectMapper

class CavalliSocial: UIViewController {
    
    let FB_Link = "https://www.facebook.com/CavalliClub"
    let Twitter_Link = "https://twitter.com/CavalliClubDxb"
    let Insta_Link = "https://www.instagram.com/cavalliclubdxb/"
    let Gmail_Link = "https://www.google.com/"
    let YT_Link = "https://www.youtube.com/channel/UCS4CezefvogrDhvBP03KBZg"
    
    var arrSocialLinks = [CavalliSocialModel]()

    @IBOutlet weak var lblContent : UILabel?

    @IBOutlet weak var socialCollectionView: UICollectionView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
         super.viewWillAppear(animated)
        self.socialCollectionView?.register(UINib(nibName: "SocialCollectionCell", bundle: nil), forCellWithReuseIdentifier: "socialCell")

//        socialCollectionView?.register(HomeCVC.self, forCellWithReuseIdentifier: "socailCell") as SocialCollectionCell
//        self.socialCollectionView?.delegate = self
//        self.socialCollectionView?.dataSource = self
        processGetSocialLinks()
    }
    
    @IBAction func onBtnFB(_ sender: UIButton) {
        self.openURL(link: FB_Link)
    }
    @IBAction func onBtnTwitter(_ sender: UIButton) {
        self.openURL(link: Twitter_Link)
    }
    @IBAction func onBtnInsta(_ sender: UIButton) {
        self.openURL(link: Insta_Link)
    }
    @IBAction func onBtnGoogle(_ sender: UIButton) {
        self.openURL(link: Gmail_Link)
    }
    @IBAction func onBtnYoutube(_ sender: UIButton) {
        self.openURL(link: YT_Link)
    }
    
    @IBAction func whatsappShareText(_ sender: AnyObject) {
        let originalString = "Cavalli Whatsapp Share"
        let escapedString = originalString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
        let url  = URL(string: "whatsapp://send?phone=+971509910400&abid=12354&text=\(escapedString!)")
        if UIApplication.shared.canOpenURL(url! as URL) {
            UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
        }
    }
    
    @IBAction func onCall(_ sender: UIButton) {
        let phoneNumber = "tel://+971509910400"
        call(phoneNumber: phoneNumber)
    }
    
    @IBAction func onWebsite(_ sender: UIButton) {
        guard let url = URL(string: "http://dubai.cavalliclub.com/") else { return }
        UIApplication.shared.open(url)
    }
    
    func call(phoneNumber: String) {
        if let url = URL(string: phoneNumber) {
            if #available(iOS 10, *) {
                UIApplication.shared.open(url, options: [:],
                                          completionHandler: {
                                            (success) in
                                            print("Open \(phoneNumber): \(success)")
                })
            } else {
                let success = UIApplication.shared.openURL(url)
                print("Open \(phoneNumber): \(success)")
            }
        }
    }
    
    @IBAction func onBtnback(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func openURL(link: String){
//        if Utility.validateUrl(urlString: link){
            Utility.openBrowser(link: link)
//            return
//        }
    }
    
    func processGetSocialLinks() {
        Utility.showLoader()
        APIManager.sharedInstance.homeAPIManager.getCavalliSocialOptions(success: {
            (responseObject) in
            Utility.hideLoader()
            var socialArray = Array<CavalliSocialModel>()
            let response = Mapper<CavalliSocialModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            socialArray = response
            self.arrSocialLinks.removeAll()
            for objSocial in socialArray {
                if objSocial.key == "social_content" {
                    self.lblContent?.text = objSocial.value
                } else {
                    self.arrSocialLinks.append(objSocial)
                }
            }
            self.socialCollectionView?.delegate = self
            self.socialCollectionView?.dataSource = self
            self.socialCollectionView?.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrSocialLinks.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "socialCell", for: indexPath) as? SocialCollectionCell else { return UICollectionViewCell() }
        if let imageURL = arrSocialLinks[indexPath.row].image_url {
            cell.imgSocial?.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        }
        return cell
    }
}

extension CavalliSocial: UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: socialCollectionView!.frame.size.width/3.5, height: socialCollectionView!.frame.size.width/3.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if arrSocialLinks[indexPath.row].key == "whatsapp" {
//            +971509910400
            let originalString = "Cavalli Whatsapp Share"
            let escapedString = originalString.addingPercentEncoding(withAllowedCharacters:CharacterSet.urlQueryAllowed)
            let whNumber : String = arrSocialLinks[indexPath.row].value!
            let url  = URL(string: "whatsapp://send?phone=\(whNumber)&abid=12354&text=\(escapedString!)")
            if UIApplication.shared.canOpenURL(url! as URL) {
                UIApplication.shared.open(url! as URL, options: [:], completionHandler: nil)
            }
        }
        else if arrSocialLinks[indexPath.row].key == "phone_no" {
            let number = arrSocialLinks[indexPath.row].value
            let phoneNumber = "tel://+971" + (number?.stripped)! //509910400"
            call(phoneNumber: phoneNumber)
        } else {
            self.openURL(link: arrSocialLinks[indexPath.row].value!)

        }
    }
}


