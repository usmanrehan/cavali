//
//  LogoutPopUpView.swift
//  Template
//
//  Created by Usman Bin Rehan on 10/30/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import Foundation

import UIKit

class LogoutPopUpView: UIView
{
    @IBOutlet weak var logoutPopUpView: UIView!
    
    @IBOutlet weak var btnYes: UIButton!
    @IBOutlet weak var btnNo: UIButton!
   
    @IBOutlet weak var lableTitle: UILabel!
    class func instanceFromNib() -> UIView
    {
        return UINib(nibName: "LogoutPopUpView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
    }
    
    // popup animation when appears
    func showAnimate()
    {
        
        self.logoutPopUpView.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        self.logoutPopUpView.alpha = 0.0
        
        UIView.animate(withDuration: 0.25, animations: {
            
            self.logoutPopUpView.alpha = 1.0
            self.logoutPopUpView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
    }
    
    // popup animation when dissappears
    func removeAnimate()
    {
        
        self.logoutPopUpView.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.logoutPopUpView.alpha = 0.0
            self.logoutPopUpView.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        }) { (success) in
            self.removeFromSuperview()
        }
        
    }
    
    
    
} // end class LogoutCustomView





