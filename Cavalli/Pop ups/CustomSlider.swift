
//
//  CustomSlider.swift
//  Cavalli
//
//  Created by shardha kawal on 10/12/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ImageSlideshow
class CustomSlider: UIView {

    @IBOutlet weak var btnCancel: UIButton?
    @IBOutlet var slideshow: ImageSlideshow?
    
    class func instanceFromNib() -> UIView
    {
        return UINib(nibName: "CustomSlider", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        slideshow?.backgroundColor = UIColor.clear
        slideshow?.slideshowInterval = 5.0
        slideshow?.pageControlPosition = PageControlPosition.insideScrollView
        slideshow?.pageControl.currentPageIndicatorTintColor = UIColor(displayP3Red: 161/255.0, green: 145/255.0, blue: 97/255.0, alpha: 1.0)
        slideshow?.pageControl.pageIndicatorTintColor = UIColor.white
        slideshow?.zoomEnabled = true
        slideshow?.contentScaleMode = UIViewContentMode.scaleAspectFit
    }
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
