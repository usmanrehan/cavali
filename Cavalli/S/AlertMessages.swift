//
//  AlertMessages.swift
//  Auditix
//
//  Created by Ahmed Shahid on 1/4/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation

enum AlertMessages: String {
    
    // MARK: NEXT MODULE
    case WillImplementInNextMode = "Functionality will be implement in Beta"
    
    // MARK: Login and SignUp
    case emailNotValid = "Please provide a valid Email Address."
    case nameNotValid = "Invalid name"
    
    case passwordNotValid = "Password should contain atlest 6 characters."
    case passwordInValidShort = "At least 6 characters"
    
    case passwordNotMatch = "New password and confirm password does not match."
    case passwordNotMatchShort = "Password does'nt match"
    
    case PhoneNumber = "Invalid number"
    
    case AllFieldsRequired = "All Fields are required!"
    
    // MARK: BOOKS
    case aboutTheBook = "ABOUT THE BOOK"
    case aboutTheNarator = "NARRATOR INTRODUCTION"
    case addToLibrary = "Add to Library"
    case addToLibrarySuccss = "Your Book has been added to library successfully"
    case typeToSearch = "Type somthing to search"
    case noDataFound = "No data found"

    var message: String { return self.rawValue }
}


extension String {
    var stripped: String {
        let okayChars = Set("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890+")//-=().!_ \n\t\r")
        return self.filter {okayChars.contains($0) }
    }
}
