//
//  Alerts.swift
//  Auditix
//
//  Created by Ahmed Shahid on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation

enum AlertTitles: String {
    
    case alert = "Alert"
    case paswordReset = "Password Reset"
    case profileSaved = "Profile Saved"
    case thankYou = "Thank You"
    case success = "Success"
    var message: String { return self.rawValue }
}
