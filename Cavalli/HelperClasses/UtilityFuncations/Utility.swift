import Foundation
import UIKit
import AVFoundation
import Toast_Swift
import NVActivityIndicatorView
import Alamofire
//// MARK: AppHelperUtility setup
@objc class Utility: NSObject
{
    static let main = Utility()
    fileprivate override init() {}
}


extension Utility {
    
    func roundAndFormatFloat(floatToReturn : Float, numDecimalPlaces: Int) -> String{
        let formattedNumber = String(format: "%.\(numDecimalPlaces)f", floatToReturn)
        return formattedNumber
    }
    func printFonts() {
        for familyName in UIFont.familyNames {
            print("\n-- \(familyName) \n")
            for fontName in UIFont.fontNames(forFamilyName: familyName) {
                print(fontName)
            }
        }
    }
    
    func topViewController(base: UIViewController? = (Constants.APP_DELEGATE).window?.rootViewController) -> UIViewController? {
        if let nav = base as? UINavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
    
    
    //     func showAlert(title:String?, message:String?) {
    //        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    //        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in })
    //        Utility().topViewController()!.present(alert, animated: true){}
    //    }
    
    func resizeImage(image: UIImage,  targetSize: CGFloat) -> UIImage {
        guard (image.size.width > 1024 || image.size.height > 1024) else {
            return image;
        }
        
        // Figure out what our orientation is, and use that to form the rectangle
        var newRect: CGRect = CGRect.zero;
        
        if(image.size.width > image.size.height) {
            newRect.size = CGSize(width: targetSize, height: targetSize * (image.size.height / image.size.width))
        } else {
            newRect.size = CGSize(width: targetSize * (image.size.width / image.size.height), height: targetSize)
        }
        
        // Actually do the resizing to the rect using the ImageContext stuff
        UIGraphicsBeginImageContextWithOptions(newRect.size, false, 1.0)
        image.draw(in: newRect)
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    
    func thumbnailForVideoAtURL(url: URL) -> UIImage? {
        let asset = AVAsset(url: url)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform=true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            return UIImage(cgImage: imageRef)
        } catch {
            print("error")
            return nil
        }
    }
    
    
    func delay(delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    func showLabelIfNoData(withtext text: String, controller: UIViewController, tableView: UITableView) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: controller.view.bounds.size.width, height: controller.view.bounds.size.height))
        messageLabel.text = text//"No voucher is currently available. Please pull down to refresh."
        messageLabel.textColor = UIColor.black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: "Palatino-Italic", size: 20) ?? UIFont()
        messageLabel.sizeToFit()
        tableView.backgroundView = messageLabel;
        tableView.separatorStyle = UITableViewCellSeparatorStyle.none;
    }
}

// MARK: Alert related functions
extension Utility
{
    func showAlert(message:String,title:String,controller:UIViewController){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alertController.view.tintColor = Constants.THEME_COLOR_GOLDEN
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlertWithOptionalController(message:String,title:String,controller:UIViewController?) -> UIViewController?{
        if(controller == nil)
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            
            return alertController
        }
        else
        {
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            controller!.present(alertController, animated: true, completion: nil)
            
            return nil
        }
    }
    
    func showAlert(message: String, title: String, controller: UIViewController, usingCompletionHandler handler:@escaping (() -> Swift.Void))
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: {
            
            (action) in
            
            handler()
        }
        ))
        controller.present(alertController, animated: true, completion: nil)
    }
    
    func showAlert(message:String,title:String,controller:UIViewController, completionHandler: @escaping (UIAlertAction?, UIAlertAction?) -> Void){
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
//        alertController.view.tintColor = Constants.THEME_COLOR_GOLDEN
        alertController.addAction(UIAlertAction(title: "YES", style: UIAlertActionStyle.default){ (alertActionYES) in
            completionHandler(alertActionYES, nil)
        })
        
        alertController.addAction(UIAlertAction(title: "NO", style: UIAlertActionStyle.cancel){ (alertActionNO) in
            completionHandler(nil, alertActionNO)
        })
        
        controller.present(alertController, animated: true, completion: nil)
        
    }
    
    func showAlert(message:String,title:String){
        if var topController = UIApplication.shared.keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            topController.present(alertController, animated: true, completion: nil)
            
        }
    }
    
    func convertToArray(text: String) -> Array<Any>? {
        if let data = text.data(using: String.Encoding.utf8) {
            NSLog("Enter here")
            do {
                return try JSONSerialization.jsonObject(with: data, options: [.allowFragments, .mutableContainers, .mutableLeaves]) as? Array
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
        
    }
    
    func showActivityViewController(with sharingText: String)
    {
        let activityViewController = UIActivityViewController(
            activityItems: [sharingText],
            applicationActivities: nil)
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.present(activityViewController, animated: true, completion: nil)
    }
    
    class func showToastOnView(message: String, view: UIView)
        
    {
        
        
        
        view.hideToastActivity()
        
        // create a new style
        
        var style = ToastStyle()
        
        
        
        // this is just one of many style options
        
        style.backgroundColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        style.messageColor = UIColor.white
        
        
        
        // present the toast with the new style
        
        //self.view.makeToast("This is a piece of toast", duration: 3.0, position: .Bottom, style: style)
        
        
        
        // or perhaps you want to use this style for all toasts going forward?
        
        // just set the shared style and there's no need to provide the style again
        
        ToastManager.shared.style = style
        
        
        
        // toggle "tap to dismiss" functionality
        
        ToastManager.shared.tapToDismissEnabled = true
        
        
        
        // toggle queueing behavior
        
        ToastManager.shared.queueEnabled = true
        
        view.makeToast(message, duration: 1.0, position: .center)
        
    }
}

// MARK:- INDICATOR
extension Utility
{
    class func showLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballClipRotate, color: .white , padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
//        NVActivityIndicatorType.s
//        NVActivityIndicatorView.DEFAULT_BLOCKER_MINIMUM_DISPLAY_TIME
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(<#T##data: ActivityData##ActivityData#>)
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
    }

    class func hideLoader() {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
        NVActivityIndicatorPresenter.sharedInstance.stopAnimating()
    }
}
// MARK:- TOAST HELPER UTILITY
extension Utility
{
    class func showToast(message: String, controller: UIViewController)
    {
        controller.view.hideToastActivity()
        // create a new style
        var style = ToastStyle()
        
        // this is just one of many style options
        style.backgroundColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        style.messageColor = UIColor.white
        
        // present the toast with the new style
        //self.view.makeToast("This is a piece of toast", duration: 3.0, position: .Bottom, style: style)
        
        // or perhaps you want to use this style for all toasts going forward?
        // just set the shared style and there's no need to provide the style again
        ToastManager.shared.style = style
        
        // toggle "tap to dismiss" functionality
        ToastManager.shared.tapToDismissEnabled = true
        
        // toggle queueing behavior
        ToastManager.shared.queueEnabled = true
        
        controller.view.makeToast(message, duration: 1.0, position: .center)
    }

    class func showToast(message: String, controller: UIViewController, position: ToastPosition)
    {
        // create a new style
        var style = ToastStyle()
        
        // this is just one of many style options
        style.backgroundColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        style.messageColor = UIColor.white
        
        // present the toast with the new style
        //self.view.makeToast("This is a piece of toast", duration: 3.0, position: .Bottom, style: style)
        
        // or perhaps you want to use this style for all toasts going forward?
        // just set the shared style and there's no need to provide the style again
        ToastManager.shared.style = style
        
        // toggle "tap to dismiss" functionality
        ToastManager.shared.tapToDismissEnabled = true
        
        // toggle queueing behavior
        ToastManager.shared.queueEnabled = true
        controller.view.hideToastActivity()
        controller.view.makeToast(message, duration: 3.0, position: position)
    }
    class func showToast(message: String, controller: UIViewController, duration: TimeInterval)
    {
        controller.view.hideToastActivity()
        // create a new style
        var style = ToastStyle()
        
        // this is just one of many style options
        style.backgroundColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        style.messageColor = UIColor.white
        
        // present the toast with the new style
        //self.view.makeToast("This is a piece of toast", duration: 3.0, position: .Bottom, style: style)
        
        // or perhaps you want to use this style for all toasts going forward?
        // just set the shared style and there's no need to provide the style again
        ToastManager.shared.style = style
        
        // toggle "tap to dismiss" functionality
        ToastManager.shared.tapToDismissEnabled = true
        
        // toggle queueing behavior
        ToastManager.shared.queueEnabled = true
        controller.view.makeToast(message, duration: duration, position: .center)

    }

    class func showToast(message: String)
    {
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.view.hideToastActivity()
        // create a new style
        var style = ToastStyle()
        
        // this is just one of many style options
        style.backgroundColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        style.messageColor = UIColor.white
        
        // present the toast with the new style
        //self.view.makeToast("This is a piece of toast", duration: 3.0, position: .Bottom, style: style)
        
        // or perhaps you want to use this style for all toasts going forward?
        // just set the shared style and there's no need to provide the style again
        ToastManager.shared.style = style
        
        // toggle "tap to dismiss" functionality
        ToastManager.shared.tapToDismissEnabled = true
        
        // toggle queueing behavior
        ToastManager.shared.queueEnabled = true
        topController?.view.makeToast(message, duration: 1.0, position: .center)
    }
    class func showToastBottom(message: String)
    {
        let topController = UIApplication.shared.keyWindow?.rootViewController
        topController?.view.hideToastActivity()
        // create a new style
        var style = ToastStyle()
        
        // this is just one of many style options
        style.backgroundColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        style.messageColor = UIColor.white
        
        // present the toast with the new style
        //self.view.makeToast("This is a piece of toast", duration: 3.0, position: .Bottom, style: style)
        
        // or perhaps you want to use this style for all toasts going forward?
        // just set the shared style and there's no need to provide the style again
        ToastManager.shared.style = style
        
        // toggle "tap to dismiss" functionality
        ToastManager.shared.tapToDismissEnabled = true
        
        // toggle queueing behavior
        ToastManager.shared.queueEnabled = true
        topController?.view.makeToast(message, duration: 1.0, position: .bottom)
    }
    static func getBoldDarkGrayString(boldText: String, plainText : String)-> NSAttributedString{
        //let attributedString = NSMutableAttributedString(string: plainText)
        
        let poppinsBold = [NSAttributedStringKey.font : UIFont.init(name: "Poppins-Bold", size: 14)!]
        let boldString = NSMutableAttributedString(string:boldText, attributes:poppinsBold)
        
        let poppinsLight = [NSAttributedStringKey.font : UIFont.init(name: "Poppins-Light", size: 14)!]
        let lightString = NSMutableAttributedString(string:plainText, attributes:poppinsLight)
        
        boldString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location:0 ,length: boldText.count))
        boldString.append(lightString)
        
        return boldString
    }
    static func getLightDarkGrayString(plainText : String)-> NSAttributedString{
        
        let poppinsLight = [NSAttributedStringKey.font : UIFont.init(name: "Poppins-Light", size: 14)!]
        let lightString = NSMutableAttributedString(string:plainText, attributes:poppinsLight)
        
        lightString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location:0 ,length: plainText.count))
        return lightString
    }
    static func customDateFormatter(dateStr: String , dateFormat : String , formatteddate : String) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat //Your date format
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        let date = dateFormatter.date(from: dateStr)
        dateFormatter.dateFormat = formatteddate
        return dateFormatter.string(from: date!)
    }
    static func getDateFromString(dateString: String, dateFormat: String)-> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat //Your date format
        //dateFormatter.timeZone = TimeZone(abbreviation: "GMT+0:00") //Current time zone
        let date = dateFormatter.date(from: dateString)
        return date ?? Date()
    }
    
    //Validate URL
    static func validateUrl (urlString: String) -> Bool {
        //let urlRegEx = "((?:http|https)://)?(?:www\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        let urlRegEx = "((http|https)://)(?:(www|m)\\.)?[\\w\\d\\-_]+\\.\\w{2,3}(\\.\\w{2})?(/(?<=/)(?:[\\w\\d\\-./_]+)?)?"
        return NSPredicate(format: "SELF MATCHES %@", urlRegEx).evaluate(with: urlString)
    }
    static func openBrowser(link: String) {
        let trimmedString = link.trimmingCharacters(in: .whitespaces)
        let application = UIApplication.shared
        if application.canOpenURL((NSURL(string: trimmedString) as URL?)!) {
            application.open((NSURL(string: trimmedString) as URL?)!)
        }
    }
    
//    class func isConnectedToNetwork() -> Bool {
//        var zeroAddress = sockaddr_in(sin_len: 0, sin_family: 0, sin_port: 0, sin_addr: in_addr(s_addr: 0), sin_zero: (0, 0, 0, 0, 0, 0, 0, 0))
//        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
//        zeroAddress.sin_family = sa_family_t(AF_INET)
//        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
//            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
//                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
//            }
//        }
//        var flags: SCNetworkReachabilityFlags = SCNetworkReachabilityFlags(rawValue: 0)
//        if SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) == false {
//            return false
//        }
//        /* Only Working for WIFI
//         let isReachable = flags == .reachable
//         let needsConnection = flags == .connectionRequired
//
//         return isReachable && !needsConnection
//         */
//
//        // Working for Cellular and WIFI
//        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
//        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
//        let ret = (isReachable && !needsConnection)
//        if !ret {
//            showAlert(title: "Alert", message: "Internet connection seems to be offline")
//        }
//        return ret
//    }

}
//MARK:- Convert UTC to GMT
extension Utility {
    static func UTCToLocal(date:String, oldFormate:String, newFormate: String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = oldFormate
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.dateFormat = newFormate
        
        return dateFormatter.string(from: dt!)
    }
}

class Connectivity {
    class func isConnectedToInternet() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }
}
