//
//  Validation.swift
//  Auditix
//
//  Created by Ahmed Shahid on 1/4/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import UIKit
class Validation {
    
    static func isValidEmail(_ testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    static  func isValidPhone(value: String) -> Bool {
        var result = false
        if value.count >= 12 {
            result = true
        }
        return result
    }
    
    static func isValidePassword(value: String) -> Bool {
        var result = false
        if value.count >= 6 {
            result = true
        }
        return result
    }
    
    static func isConfirmPasswordIsEqualToPassword(password: String, confirm: String) -> Bool {
        if(password == confirm)
        {
            return true
        }
        
        return false
    }
    
    static func validateStringLength(_ text: String) -> Bool {
        let trimmed = text.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return !trimmed.isEmpty
    }
    
    static func validateName (input: String) -> Bool {
        for chr in input {
            if (!(chr >= "a" && chr <= "z") && !(chr >= "A" && chr <= "Z") && !(chr == " ") ) {
                return false
            }
        }
        return true
    }
    
    static func validatePhone(input: String) -> Bool {
        do {
            let detector = try NSDataDetector(types: NSTextCheckingResult.CheckingType.phoneNumber.rawValue)
            let matches = detector.matches(in: input, options: [], range: NSMakeRange(0, input.count))
            if let res = matches.first {
                return res.resultType == .phoneNumber && res.range.location == 0 && res.range.length == input.count 
            } else {
                return false
            }
        } catch {
            return false
        }
    }
}
