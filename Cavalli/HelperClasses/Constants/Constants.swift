//
//  Constants.swift
//  Auditix
//
//  Created by Ahmed Shahid on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Foundation
import RealmSwift

struct Global{
    
    static var APP_MANAGER                   = AppStateManager.sharedInstance
    static var APP_REALM                     = APP_MANAGER.realm
    static var USER                          = APP_MANAGER.loggedInUser
    static var APP_COLOR                     = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
    
}

struct Constants {
    static var device_token : String = "0123456789"
    static let APP_DELEGATE                = UIApplication.shared.delegate as! AppDelegate
    static let UIWINDOW                    = UIApplication.shared.delegate!.window!
    
    static let USER_DEFAULTS               = UserDefaults.standard
    
    static let SINGLETON                   = Singleton.sharedInstance
    
    static let DEFAULTS_USER_KEY           = "User"
    
    static let serverDateFormat = "yyyy-MM-dd HH:mm:ss"
    
    //MARK: - THEME COLORS
    static let THEME_ORANGE_COLOR          = UIColor(red: 0xFF, green: 0x7E, blue: 0x5B) //FF-7E-5B
    static let FIELD_VALIDATION_RED_COLOR       = UIColor(red: 0xC4, green: 0x13, blue: 0x02) //C41302
    
    //MARK: - Base URLs
    static let BaseURL = "http://cavalli.stagingic.com/api/" //live
    //"http://10.1.18.153/cavalli/api/" // Local
    
    static let PAGINATION_PAGE_SIZE        = 100
    
    // MARK: - Google Route
//    static let getGoogleUserInfo = 
    
    // MARK: - Users Route
    static let Login = "login"
    static let Logout = "logout"

    static let SocialLogin = "socialLogin"
    static let Register = "register"
    static let Verification = "codeVerification"
    static let ForgotPassword = "forgotPassword"
    static let PushOnOff = "pushOnOff"
    static let checkPhoneNo = "checkPhoneNo"
    static let updatePhoneNo = "updatePhoneNo"

    
    // MARK: - Home Routs
    static let ReservationSlots = "getReservationSlots"
    static let getSocialLinks = "getSocialLinks"

    static let LatestUpdates = "getLatestUpdate"
    static let AddReservation = "addReservation"
    static let MyReservation = "getReservations"
    static let MenuCategories = "getMenuCategories"
    static let EditReservation = "editReservation"
    static let GetMenuProducts = "getMenuProducts"
    static let CMS = "getCms"
    static let BarData = "getBarData"
    static let CompetitionData = "getCompetitions"
    static let PaticipateCompetition = "participateCompetition"
    static let CompetitionHistory = "getCompetitionHistory"
    static let UpdateProfile = "updateProfile"
    static let AddOrder = "addOrder"
    static let Tax = "getSetting"
    
    static let MarkedEvents = "myMarkedEvent"
    static let MarkUnMarkEvent = "markUnMarkEvent"
    static let GetEvents = "getEvents"
    static let GetMyEvents = "getMyEvents"
    static let GetLiveMusicInfo = "getLiveMusicInfo"
    
    //MARK:- Module3
    
    static let GetThreads = "getMyThreads"
    static let GetChat = "getThreadMessages"
    static let AddMessage = "addMessage"
    static let GetOrderHistoy = "getMyOrders"
    static let GetMembership = "getMemberships"
    static let BuySubscription = "purchaseMembership"
    static let GetMembershipHistory = "getMyMemberships"
    static let GetNotifications = "getNotifications"
    static let DeleteNotifications = "deleteNotification"
    static let AddInternationalRequest = "addInternationalRequest"
    static let AddInquiry = "addInquiry"
    static let ChangePassword = "changePassword"
    static let ResetPassword = "resetPassword"

    static let GetGalleryCategories = "getGalleryCategories"

    static let GetGallery = "getGallery"
    static let HomeSearch = "homeSearch"
    static let GET_CMS = "getCms"

    //MARK:- Module4
    static let GuestListCategories = "guestListCategories"
    static let GuestList = "guestList"
    static let AddGuestList = "createGuestList"
    static let EditGuestList = "editGuestListTitle"
    static let GetMembers = "getGuestMemberList"
    static let AddGuestListMember = "addGuestListMember"
    static let EditGuest = "editGuestListMember"
    static let DeleteGuest = "deleteGuestListMember"
    static let GetGuestHistoy = "getMyGuestList"
    static let HomeSearchDetail = "homeSearchDetail"
    static let CheckMarkStatus = "checkMarkStatus"
    static let UpdateDeviceToken = "updateDeviceToken"
    static let GetCancleReservation = "cancelReservation"
    
    
}
