//
//  ParserHelper.swift
//  TreatzAsia
//
//  Created by Ahmed Shahid on 10/19/17.
//  Copyright © 2017 Ahmed Shahid. All rights reserved.
//

import Foundation

class ParserHelper
{
    static func emptyStringIfNull(value: Any?) -> String
    {
        if ((value) == nil || value is NSNull || !(value is String))
        {
            return ""
        }
        
        if value is String
        {
            return value as! String
        }
        
        return ""
    }
    
    static func replaceMyCharacterIfStringIsNull(value: Any?, character: String) -> String
    {
        if ((value) == nil || value is NSNull || !(value is String))
        {
            return character
        }
        
        if value is String
        {
            if((value as! String).characters.count == 0)
            {
                return character
            }
            
            return value as! String
        }
        
        return character
    }
    
    static func zeroIntIfNull(value: Any?) -> Int
    {
        if ((value) == nil || value is NSNull)
        {
            return 0
        }
        
        if value is Int
        {
            return value as! Int
        }
        
        if value is String
        {
            if(!(value as! String).isEmpty)
            {
                if let n = NumberFormatter().number(from: value as! String)
                {
                    return Int(n)
                }
                else
                {
                    return 0
                }
            }
            else
            {
                return 0
            }
        }
        return 0
    }
    
    static func isArrayValidForUse(value: Any?) -> Bool
    {
        if ((value) == nil || value is NSNull)
        {
            return false
        }
        if let arr = value as? NSArray
        {
            if(arr.count > 0)
            {
                return true
            }
            else
            {
                return false
            }
        }
        return false
    }
    static func convertIntToBool(value: Any?) -> Bool
    {
        
        if ((value) == nil || value is NSNull)
        {
            return false
        }
        
        if value is Int
        {
            return value as! Int == 0 ? false : true
        }
        
        if value is Bool
        {
            return value as! Bool
        }
        
        return false
    }
    
    static func convertIntToString(value: Any?) -> String
    {
        
        if ((value) == nil || value is NSNull)
        {
            return ""
        }
        
        if value is Int
        {
            return "\(value!)"
        }
        
        if value is String
        {
            return value as! String
        }
        
        return ""
    }
    
    static func convertIntToDouble(value: Any?) -> Double
    {
        if ((value) == nil || value is NSNull)
        {
            return 0.0
        }
        
        if value is Double
        {
            return value as! Double
        }
        
        if value is Int
        {
            return value as! Double
        }
        
        if value is String
        {
            if((value as! String).characters.count == 0)
            {
                return 0.0
            }
            return Double(value as! String)!
        }
        
        return 0.0
        
    }
    
    static func convertBoolToInt(value: Any?) -> Int
    {
        
        if ((value) == nil || value is NSNull)
        {
            return 0
        }
        //
        ////        if value is Int
        ////        {
        ////            return value as! Int == 0 ? false : true
        ////        }
        //
        //        if value is Bool
        //        {
        //            return value as! Int
        //        }
        //
        //
        //        return false
        if value is Bool
        {
            return value as! Bool == false ? 0 : 1
        }
        
        return 0
        
    }
    static func convertStringToFloat(value: Any?) -> Float
    {
        if let n = NumberFormatter().number(from: value as! String)
        {
            return Float(n)
        }
        else
        {
            return 0.0
        }
    }
    
    static func convertStringToInt(value: Any?) -> Int
    {
        if let n = NumberFormatter().number(from: value as! String)
        {
            return Int(n)
        }
        else
        {
            return 0
        }
    }
    
    static func convertToDouble(value: Any?) -> Double
    {
        if ((value) == nil || value is NSNull)
        {
            return 0.0
        }
        
        if value is Int
        {
            return Double.init(value as! Int)
        }
        
        if value is Float
        {
            return Double.init(value as! Float)
        }
        
        if value is Double
        {
            return value as! Double
        }
        
        if value is String
        {
            if let n = NumberFormatter().number(from: value as! String)
            {
                return Double(n)
            }
        }
        
        return 0.0
    }
    
    static func findDiscount(on amount: Float, discountPercent: Float) -> Float
    {
        return amount * (discountPercent / 100)
    }
    
    static func formatMyDateTo(formater: String, dateStr: String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd" // server date format
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formater
        
        let date: Date? = dateFormatterGet.date(from: dateStr)
        
        return dateFormatter.string(from: date!)
    }
    
    static func formatMyDateTo(formater: String, fromDate: String, dateStr: String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = fromDate
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = formater
        
        if let date: Date = dateFormatterGet.date(from: dateStr) {
            return dateFormatter.string(from: date)
        }
        
        return ""
       
        
    }
    
    static func changeTimeToLocal(withserverTime serverTimeParam: String, serverFormat: String, desireFormat: String) -> String
    {
        let serverDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = serverFormat
            result.timeZone = NSTimeZone(forSecondsFromGMT: 0) as TimeZone!
            return result
        }()
        
        let serverTime = serverTimeParam
        let localTime = serverDateFormatter.date(from: serverTime)!
        
        let localDateFormatter: DateFormatter = {
            let result = DateFormatter()
            result.dateFormat = desireFormat
            return result
        }()
        
        return localDateFormatter.string(from: localTime)
    }
    
    static func formatTimeFrom24To12Hour(with timeStr: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:mm:ss"
        
        
        if let date: Date? = dateFormatter.date(from: timeStr)
        {
            
            dateFormatter.amSymbol = "AM"
            dateFormatter.pmSymbol = "PM"
            
            dateFormatter.dateFormat = "h a"
            
            return dateFormatter.string(from: date!)
        }
        
        return ""
        
        
    }
    
    static func getCurrentDate(withThis format: String) -> String
    {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = format
        return dateFormatterGet.string(from: Date())
    }
    
    static func removeHtmlTagsFromTerms(with terms: String) -> String
    {
        var termsString = terms
        termsString = termsString.replacingOccurrences(of: "<p>", with: "")
        termsString = termsString.replacingOccurrences(of: "</p>", with: "")
        
        return termsString
    }
    
    static func setParameterAccordingToLanguage(with text: String) -> String
    {
        let perferredLanguge = Locale.preferredLanguages[0] as NSString
        
        switch perferredLanguge {
        case "en":
            return text
        case "id":
            return "in_\(text)"
        case "ms":
            return "ma_\(text)"
        default:
            return text
        }
    }
    
    static func getDuration(with secound: Int) -> String {
        let mins = (secound % 3600) / 60
        let hours = secound / 3600
        
        if hours > 1 {
            return "\(hours) Hr \(mins) mins"
        }
        else if mins > 0 {
            return "\(mins) mins"
        }
        else {
            return "-"
        }
    }
    
    static func returnChaptersString(with noOfChap: Int) -> String {
        if noOfChap == 1 {
            return "\(noOfChap) Chapter"
        }
        else if noOfChap > 1 {
            return "\(noOfChap) Chapters"
        }
        return "-"
    }
    
    static func returnPriceString(with price: Double, isPaid: Bool) -> String {
        if(isPaid) {
            return "$ \(price)"
        }
        else {
            return "FREE"
        }
    }
}

