//
//  Singleton.swift
//  Auditix
//
//  Created by Ahmed Shahid on 1/3/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation

class Singleton {
    static let sharedInstance = Singleton()
}
