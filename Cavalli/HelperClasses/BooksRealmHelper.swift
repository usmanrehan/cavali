//
//  BooksRealmHelper.swift
//  Auditix
//
//  Created by Ahmed Shahid on 1/12/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

//import Foundation
////import RealmSwift
//
//class BooksRealmHelper: NSObject {
//     static let sharedInstance = BooksRealmHelper()
//    
//    var realm: Realm!
//    
//    // MARK: ADD BOOK TO CART
//    func addThisBookToCart(with book: BookModel) {
//        for cartBookItem in self.getAllBooksInCart() {
//            if cartBookItem.CartBook.BookID == book.BookID {
//                return
//            }
//        }
//        
//        let cartBook = BookCartModel()
//        cartBook.AccountID = AppStateManager.sharedInstance.loggedInUser.AccountID
//        cartBook.CartBook = book
//        
//        try! Global.APP_REALM?.write(){
//            Global.APP_REALM?.add([cartBook])
//        }
//    }
//    
//    // MARK: GET ALL CART BOOKS
//    func getAllBooksInCart() -> Results<BookCartModel> {
//        return (Global.APP_REALM?.objects(BookCartModel.self))!
//    }
//    
//    // MARK: DEL THIS BOOK FROM CART
//    func deleteThisBook(cartbook: BookCartModel) {
//        try! Global.APP_REALM?.write({ () -> Void in
//            Global.APP_REALM?.delete(cartbook)
//        })
//    }
//    
//    // MARK: EMPTY CART LIST
//    func emptyCartList() {
//        try! Global.APP_REALM?.write(){
//            Global.APP_REALM?.delete( (Global.APP_REALM?.objects(BookCartModel.self))!)
//        }
//    }
//}

