//
//  InviteGuest.swift
//  Cavalli
//
//  Created by shardha on 2/1/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import SkyFloatingLabelTextField
class InviteGuest: UIViewController {

    //MARK:- IBOutlets
    var titleText : String? = ""
    @IBOutlet weak var txtListName: SkyFloatingLabelTextField?
    @IBOutlet weak var lblNodataFound: UILabel!
    @IBOutlet weak var tableView: UITableView!
    let addNewGuestList = AddNewGuestList.instanceFromNib() as! AddNewGuestList
    var count = 4
    var arrayGuestList = [GuestListModel]()
    var isEditGuestList = false
    var editIndex = 0
    var category_id = 0
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "InviteGuestTVC", bundle: nil), forCellReuseIdentifier: "InviteGuestTVC")
        self.tableView.register(UINib(nibName: "AddNewListTVC", bundle: nil), forCellReuseIdentifier: "AddNewListTVC")
        
        self.addNewGuestList.buttonDismiss.addTarget(self, action: #selector(self.actionDismiss(sender:)), for: .touchUpInside)
        self.addNewGuestList.buttonSubmit.addTarget(self, action: #selector(self.actionSubmit(sender:)), for: .touchUpInside)
        self.count = self.arrayGuestList.count
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.processGetGuestList()
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @objc func actionEdit(sender: UIButton) {
        self.isEditGuestList = true
        self.editIndex = sender.tag
        self.addNewGuestList.frame = self.view.frame
        self.addNewGuestList.showAnimate()
        self.addNewGuestList.textFieldTitle.text = self.arrayGuestList[sender.tag].title
        let window = UIApplication.shared.keyWindow!
        window.addSubview(self.addNewGuestList)
    }
    @objc func actionDismiss(sender: UIButton) {
        self.addNewGuestList.removeAnimate()
    }
    @objc func actionSubmit(sender: UIButton) {
        if self.isEditGuestList {
            if (self.addNewGuestList.textFieldTitle.text?.isEmpty)! {
                Utility.showToastOnView(message: "Guest list name is empty.", view: self.addNewGuestList)
            }else {
                self.addNewGuestList.removeAnimate()
                self.processEditGuest(index: self.editIndex)
            }
        }else {
            if (self.addNewGuestList.textFieldTitle.text?.isEmpty)! {
                Utility.showToastOnView(message: "Guest list name is empty.", view: self.addNewGuestList)
            }else {
                self.addNewGuestList.removeAnimate()
                self.processAddGuest()
            }
        }
        self.isEditGuestList = false
    }
    @objc func actionAddNewList(sender: UIButton)
    {
        self.addNewGuestList.frame = self.view.frame
        self.addNewGuestList.showAnimate()
        let window = UIApplication.shared.keyWindow!
        window.addSubview(self.addNewGuestList)
    }
}
extension InviteGuest: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return count + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.item < count {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "InviteGuestTVC") as? InviteGuestTVC else { return UITableViewCell() }
            cell.lableTitle.text = self.arrayGuestList[indexPath.item].title
            cell.selectionStyle = .none
            cell.buttonEdit.tag = indexPath.item
            cell.buttonEdit.addTarget(self, action: #selector(self.actionEdit(sender:)), for: .touchUpInside)
            return cell
        }else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewListTVC") as? AddNewListTVC else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.buttonAddToCart.tag = indexPath.item
            cell.buttonAddToCart.addTarget(self, action: #selector(self.actionAddNewList(sender:)), for: .touchUpInside)
            return cell
        }
    }
}
extension InviteGuest: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.item < count {
            return 60
        }else {
            return  UITableViewAutomaticDimension
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "GuestListDetail") as? GuestListDetail {
            if (self.arrayGuestList[indexPath.item].id != nil) {
                dvc.id = self.arrayGuestList[indexPath.item].id
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        }
    }
}
extension InviteGuest {
    func processGetGuestList() {
        Utility.showLoader()
        let params: [String: Any] = ["category_id":"\(category_id)"]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getGuestListWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.arrayGuestList.removeAll()
            let response = Mapper<GuestListModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayGuestList = response
            if self.arrayGuestList.count == 0 {
                self.lblNodataFound.isHidden = false
            } else {
                self.lblNodataFound.isHidden = true
                self.count = self.arrayGuestList.count
                self.tableView.reloadData()
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            print(error)
            Utility.showToast(message: error.localizedFailureReason ?? "")        })
        
    }
    func processAddGuest() {
        
        Utility.showLoader()
        let params: [String: Any] = ["title": self.addNewGuestList.textFieldTitle.text!,
                                     "category_id":"\(category_id)"]
        print(params)
        APIManager.sharedInstance.homeAPIManager.addGuestListWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.addNewGuestList.textFieldTitle.text = ""
            Utility.showToast(message: "Guest list successfully added.")
            self.processGetGuestList()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            print(error)
            self.addNewGuestList.textFieldTitle.text = ""
            Utility.showToast(message: error.localizedFailureReason ?? "")})
        
    }
    func processEditGuest(index: Int) {
        Utility.showLoader()
        let params: [String: Any] = ["title": self.addNewGuestList.textFieldTitle.text!,
                                     "guest_list_id": self.arrayGuestList[index].id]
        print(params)
        APIManager.sharedInstance.homeAPIManager.editGuestListWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.addNewGuestList.textFieldTitle.text = ""
            Utility.showToast(message: "Guest list successfully edited.")
            self.processGetGuestList()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            print(error)
            self.addNewGuestList.textFieldTitle.text = ""
            Utility.showToast(message: error.localizedFailureReason ?? "")})
        
    }
}
