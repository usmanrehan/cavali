//
//  GuestListDetailTVC.swift
//  Cavalli
//
//  Created by shardha on 4/18/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class GuestListDetailTVC: UITableViewCell {
    
    @IBOutlet weak var buttonDeleteGuest: UIButton!
    @IBOutlet weak var buttonEditGuest: UIButton!
    @IBOutlet weak var lableTitle: UILabel!
    @IBOutlet weak var buttonEdit: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
