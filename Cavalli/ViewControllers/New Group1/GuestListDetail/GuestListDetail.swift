//
//  GuestListDetail.swift
//  Cavalli
//
//  Created by shardha on 4/18/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftDate
import SkyFloatingLabelTextField
class GuestListDetail: UIViewController {
    
    var isFromHistory : Bool? = false
    
    var dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMM yyyy"
        return formatter
    }()
    
    //MARK:- IBOutlets
   // var titleText : String? = ""
    @IBOutlet weak var txtListName: SkyFloatingLabelTextField?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblDataNotFound: UILabel!
    
    //@IBOutlet weak var btnAddMore: UIButton?
    
    //var cpvTextField: CountryPickerView!

    let addNewGuest = AddNewGuest.instanceFromNib() as! AddNewGuest
    let LogoutAlertView = LogoutPopUpView.instanceFromNib() as! LogoutPopUpView
    var count = 4
    var arrayGuest = [GuestListModel]()
    var arrayGuestList = [GuestListMember]()

    var id = 0
    var isEditGuestList = false
    var editIndex = 0
    var deleteIndex = 0
    var isPresent = false
    
    var strListTitle : String? = ""
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtListName?.text = strListTitle

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.estimatedRowHeight = 60
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "GuestListDetailTVC", bundle: nil), forCellReuseIdentifier: "GuestListDetailTVC")
        self.tableView.register(UINib(nibName: "AddNewListTVC", bundle: nil), forCellReuseIdentifier: "AddNewListTVC")
        self.tableView.register(UINib(nibName: "GuestListEmptyTVC", bundle: nil), forCellReuseIdentifier: "GuestListEmptyTVC")
        
        self.addNewGuest.buttonDismiss.addTarget(self, action: #selector(self.actionDismiss(sender:)), for: .touchUpInside)
        self.addNewGuest.buttonSubmit.addTarget(self, action: #selector(self.actionSubmit(sender:)), for: .touchUpInside)
        
        LogoutAlertView.btnYes.setTitle("Yes", for: .normal)
        LogoutAlertView.btnNo.setTitle("No", for: .normal)
        LogoutAlertView.btnYes.addTarget(self, action: #selector(onBtnYes), for: .touchUpInside)
        LogoutAlertView.btnNo.addTarget(self, action: #selector(onBtnNo), for: .touchUpInside)
        
        self.count = self.arrayGuest.count
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.processGetGuest()
    }
    @IBAction func actionBack(_ sender: UIButton) {
        if self.isPresent {
            self.dismiss(animated: true, completion: nil)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
    }
    @objc func actionDismiss(sender: UIButton) {
        self.addNewGuest.textFieldFullName.text = ""
        self.addNewGuest.textFieldEmailAddress.text = ""
        self.addNewGuest.removeAnimate()
    }
    @objc func actionSubmit(sender: UIButton) {
        if self.isEditGuestList {
            if Validation.isValidEmail(self.addNewGuest.textFieldEmailAddress.text!) && !(self.addNewGuest.textFieldFullName.text?.isEmpty)! && !(self.addNewGuest.textFieldPhoneNumber.text?.isEmpty)! && !(self.addNewGuest.textFieldTitle.text?.isEmpty)! && !(self.addNewGuest.textFieldTitle.text?.isEmpty)! {
                self.addNewGuest.removeAnimate()
                self.processEditGuest(index: self.editIndex)
            }else {
                Utility.showToastOnView(message: "Please enter correct data.", view: self.addNewGuest)
            }
        }else {
            if Validation.isValidEmail(self.addNewGuest.textFieldEmailAddress.text!) && !(self.addNewGuest.textFieldFullName.text?.isEmpty)! && !(self.addNewGuest.textFieldPhoneNumber.text?.isEmpty)! && !(self.addNewGuest.textFieldTitle.text?.isEmpty)! && !(self.addNewGuest.textFieldDate.text?.isEmpty)!{
                self.addNewGuest.removeAnimate()
                self.processAddGuest()
            } else {
                Utility.showToastOnView(message: "Please enter correct data.", view: self.addNewGuest)
            }
        }
        self.isEditGuestList = false
    }
    
    @objc func actionEditGuest(sender: UIButton) {
        
        self.addNewGuest.textFieldFullName.text = ""
        self.addNewGuest.textFieldEmailAddress.text = ""
        self.addNewGuest.textFieldTitle.text = ""
        self.addNewGuest.textFieldPhoneNumber.text = ""
        self.addNewGuest.textFieldDate.text = ""
        self.addNewGuest.strDate = ""
        
        self.editIndex = self.arrayGuestList[sender.tag].id
        self.isEditGuestList = true
        self.addNewGuest.frame = self.view.frame
        self.addNewGuest.showAnimate()
        let window = UIApplication.shared.keyWindow!
        window.addSubview(self.addNewGuest)
        
        self.addNewGuest.textFieldFullName.text = "\(self.arrayGuestList[sender.tag].fullName ?? "")"
        self.addNewGuest.textFieldEmailAddress.text = "\(self.arrayGuestList[sender.tag].emailAddress ?? "")"
        self.addNewGuest.textFieldTitle.text = "\(self.arrayGuestList[sender.tag].nameTitle ?? "")"
        
        if self.arrayGuestList[sender.tag].date != nil {
            let date = DateInRegion(string: self.arrayGuestList[sender.tag].date!, format: .custom("yyyy-MM-dd"))
            self.addNewGuest.textFieldDate.text = date?.string(custom: "dd MMM yyyy")
            self.addNewGuest.strDate = self.arrayGuestList[sender.tag].date ?? ""
        }
        self.addNewGuest.textFieldPhoneNumber.text = "\(self.arrayGuestList[sender.tag].mobileNumber ?? "")"
    }
    
    @objc func actionDeleteGuest(sender: UIButton) {
        self.deleteIndex = sender.tag
        self.LogoutAlertView.lableTitle.text = "Are you sure you want to delete \(self.arrayGuestList[sender.tag].fullName ?? "")?"
        self.LogoutAlertView.frame = self.view.frame
        self.LogoutAlertView.showAnimate()
        let window = UIApplication.shared.keyWindow!
        window.addSubview(self.LogoutAlertView)
    }
    @objc func actionAddNewGuest(sender: UIButton) {
        self.addNewGuest.textFieldFullName.text = ""
        self.addNewGuest.textFieldEmailAddress.text = ""
        self.addNewGuest.textFieldTitle.text = ""
        self.addNewGuest.textFieldPhoneNumber.text = ""
        self.addNewGuest.textFieldDate.text = ""
        self.addNewGuest.strDate = ""
        
        self.addNewGuest.frame = self.view.frame
        self.addNewGuest.showAnimate()
        let window = UIApplication.shared.keyWindow!
        window.addSubview(self.addNewGuest)
    }
    @objc func onBtnYes(){
        self.processDeleteGuest(index: self.deleteIndex)
        LogoutAlertView.removeAnimate()
    }
    @objc func onBtnNo(){
        LogoutAlertView.removeAnimate()
    }
}
extension GuestListDetail: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if count == 0 {
            return 1 + 1
        }else {
            return count + 1
        }
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if count == 0 {
            if indexPath.item == 0 {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GuestListEmptyTVC") as? GuestListEmptyTVC else { return UITableViewCell() }
                cell.selectionStyle = .none
                return cell
            }else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewListTVC") as? AddNewListTVC else { return UITableViewCell() }
                cell.selectionStyle = .none
                if isFromHistory == false {
                    cell.buttonAddToCart.setTitle("Add More Guest", for: .normal)
                    cell.buttonAddToCart.addTarget(self, action: #selector(self.actionAddNewGuest(sender:)), for: .touchUpInside)
                }
                else {
                    cell.buttonAddToCart.isHidden = true
                }
                return cell
            }
        }else {
            if indexPath.item < count {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "GuestListDetailTVC") as? GuestListDetailTVC else { return UITableViewCell() }
                cell.selectionStyle = .none
                cell.lableTitle.text = self.arrayGuestList[indexPath.item].fullName
                cell.buttonEdit.tag = indexPath.item
                cell.buttonEdit.addTarget(self, action: #selector(self.actionEditGuest(sender:)), for: .touchUpInside)
                cell.buttonDeleteGuest.tag = indexPath.item
                cell.buttonDeleteGuest.addTarget(self, action: #selector(self.actionDeleteGuest(sender:)), for: .touchUpInside)

                if isFromHistory == true {
                    cell.buttonEdit.isHidden = true
                }
                
                return cell
            }else {
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "AddNewListTVC") as? AddNewListTVC else { return UITableViewCell() }
                if isFromHistory == false {
                    cell.buttonAddToCart.setTitle("Add More Guests", for: .normal)
                    cell.buttonAddToCart.addTarget(self, action: #selector(self.actionAddNewGuest(sender:)), for: .touchUpInside)
                }
                else {
                    cell.buttonAddToCart.isHidden = true
                }
                cell.selectionStyle = .none

                
                return cell
            }
        }
    }
}
extension GuestListDetail: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if count == 0 {
            if indexPath.item == 0 {
                return self.view.frame.size.height * 0.20
            } else {
                return  UITableViewAutomaticDimension
            }
        } else {
            if indexPath.item < count {
                return 60
            }else {
                return  UITableViewAutomaticDimension
            }
        }
        
    }
}
extension GuestListDetail {
    func processGetGuest() {
        Utility.showLoader()
        let params: [String: Any] = ["category_id":self.id]
        APIManager.sharedInstance.homeAPIManager.getMembersWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.arrayGuest.removeAll()
            self.arrayGuestList.removeAll()
            let response = Mapper<GuestListModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayGuest = response
            if self.arrayGuest.count != 0 {
                if let list = responseObject[0].value(forKey: "guest_list_member") {
                    let po = list as! [[String : Any]]
                    let responseList = Mapper<GuestListMember>().mapArray(JSONArray: po)
                    self.arrayGuestList = responseList
                }
                //self.arrayGuest.first?.guestListMember
                self.count = self.arrayGuestList.count
                self.tableView.reloadData()
            } else {
                self.lblDataNotFound.isHidden = false
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            print(error)
            Utility.showToast(message: error.localizedFailureReason ?? "")        })
    }
    func processAddGuest() {
        Utility.showLoader()
        let params: [String: Any] = ["full_name": self.addNewGuest.textFieldFullName.text!,
                                     "email_address": self.addNewGuest.textFieldEmailAddress.text!,
                                     "category_id": self.id,
                                     "date":self.addNewGuest.strDate!,
                                     "mobile_no":self.addNewGuest.textFieldPhoneNumber.text!,
                                     "name_title":self.addNewGuest.textFieldTitle.text!]
        APIManager.sharedInstance.homeAPIManager.addGuestMemberWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.addNewGuest.textFieldFullName.text = ""
            self.addNewGuest.textFieldEmailAddress.text = ""
            Utility.showToast(message: "Guest successfully added.")
            self.processGetGuest()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            print(error)
            self.addNewGuest.textFieldFullName.text = ""
            self.addNewGuest.textFieldEmailAddress.text = ""
            Utility.showToast(message: error.localizedFailureReason ?? "")})
        
    }
    func processEditGuest(index: Int) {
        Utility.showLoader()
        let params: [String: Any] = ["full_name": self.addNewGuest.textFieldFullName.text!,
                                     "email_address": self.addNewGuest.textFieldEmailAddress.text!,
                                     "member_id": "\(index)",
                                     "date":self.addNewGuest.strDate!,
                                     "mobile_no":self.addNewGuest.textFieldPhoneNumber.text!,
                                     "name_title":self.addNewGuest.textFieldTitle.text!]
        APIManager.sharedInstance.homeAPIManager.editGuestWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.addNewGuest.textFieldFullName.text = ""
            self.addNewGuest.textFieldEmailAddress.text = ""
            Utility.showToast(message: "Guest successfully edited.")
            self.processGetGuest()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            self.addNewGuest.textFieldFullName.text = ""
            self.addNewGuest.textFieldEmailAddress.text = ""
            Utility.showToast(message: error.localizedFailureReason ?? "")})
        
    }
    func processDeleteGuest(index: Int) {
        Utility.showLoader()
        let params: [String: Any] = ["member_id": self.arrayGuestList[index].id]
        print(params)
        APIManager.sharedInstance.homeAPIManager.deleteGuestWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.addNewGuest.textFieldFullName.text = ""
            self.addNewGuest.textFieldEmailAddress.text = ""
            Utility.showToast(message: "Guest successfully deleted from the list.")
            self.processGetGuest()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            print(error)
            self.addNewGuest.textFieldFullName.text = ""
            self.addNewGuest.textFieldEmailAddress.text = ""
            Utility.showToast(message: error.localizedFailureReason ?? "")})
    }
}

