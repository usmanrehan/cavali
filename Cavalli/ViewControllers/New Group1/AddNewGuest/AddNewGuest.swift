//
//  AddNewGuest.swift
//  Cavalli
//
//  Created by shardha on 4/18/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DropDown
import CountryPickerView
import DateTimePicker
import SwiftDate

class AddNewGuest: UIView {
    @IBOutlet weak var textFieldTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPhoneNumber: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldDate: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldFullName: SkyFloatingLabelTextField!
    
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var buttonDismiss: UIButton!
    
    @IBOutlet weak var viewTop: UIView!
    
//    weak var cpvTextField: CountryPickerView?

    private let titleDropDown = DropDown()

    var arrTitle = ["Mr", "Mrs", "Miss"]
    
    var strDate :  String? = ""
    class func instanceFromNib() -> UIView
    {
        return UINib(nibName: "AddNewGuest", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setDatePicker()
        onClickSelectCategory()
    }
    
    // set Gender Dropdown...
    
    func onClickSelectCategory() {
        titleDropDown.anchorView = textFieldTitle
        titleDropDown.direction = .bottom
        titleDropDown.dataSource = arrTitle
        
        // Action triggered on selection
        titleDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.textFieldTitle?.text = self.arrTitle[index]
        }
    }
    
    //MARK: Set Date picker
    func setDatePicker(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: date)
        textFieldDate.text = result
        strDate = date.string(custom: "yyyy-MM-dd HH:mm:ss")
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = date
        textFieldDate.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    
    @objc func handleDatePicker(sender: UIDatePicker) {//2018-05-29
        let date = DateInRegion(absoluteDate: sender.date)
        //        if date.isToday || date.isInFuture {
        textFieldDate.text = date.string(custom: "dd MMM yyyy")
        strDate = date.string(custom: "yyyy-MM-dd")
        //        }
    }
    
    //MARK:- IBActions
    @IBAction func actionGenderDropDown(_ sender: Any) {
        titleDropDown.show()
    }
    
    override func layoutSubviews()
    {
        super.layoutSubviews()
    }
    
    // popup animation when appears
    func showAnimate()
    {
        
        self.viewTop.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        self.viewTop.alpha = 0.0
        
        UIView.animate(withDuration: 0.25, animations: {
            
            self.viewTop.alpha = 1.0
            self.viewTop.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
    }
    
    // popup animation when dissappears
    func removeAnimate(){
        
        self.viewTop.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.viewTop.alpha = 0.0
            self.viewTop.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        }) { (success) in
            self.removeFromSuperview()
        }
        
    }
    
}
