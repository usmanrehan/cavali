//
//  GuestListTypeController.swift
//  Cavalli
//
//  Created by shardha kawal on 6/26/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class GuestListTypeController: UIViewController {

    var arrayGuestCategoriesList = [GuestListCategories]()
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.estimatedRowHeight = 60.0
        self.tableView.rowHeight = UITableViewAutomaticDimension
        
        // Do any additional setup after loading the view.
        processGetGuestListCategories()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionAddGuests(_ sender: UIButton)
    {
        print(sender.tag)
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "GuestListDetail") as? GuestListDetail {
            if (self.arrayGuestCategoriesList[sender.tag].id != nil) {
                dvc.id = arrayGuestCategoriesList[sender.tag].id
                dvc.strListTitle = arrayGuestCategoriesList[sender.tag].title!
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        }
//        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "InviteGuest") as? InviteGuest {
//            dvc.category_id = arrayGuestCategoriesList[sender.tag].id
//            self.navigationController?.pushViewController(dvc, animated: true)
//        }
    }

    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
//MARK: - Service 
extension GuestListTypeController {
    func processGetGuestListCategories() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getGuestListCategoriesWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.arrayGuestCategoriesList.removeAll()
            let response = Mapper<GuestListCategories>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayGuestCategoriesList = response
//            self.count = self.arrayGuestList.count
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            print(error)
            Utility.showToast(message: error.localizedFailureReason ?? "")        })
        
    }
}


// MARK: - UITableView Delegate, DataSource
extension GuestListTypeController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {   
        return arrayGuestCategoriesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellGuestList", for: indexPath) as! InviteGuestTVC
        cell.lableTitle.text = arrayGuestCategoriesList[indexPath.row].title
        cell.lableDetails?.text = arrayGuestCategoriesList[indexPath.row].guestTypeDescription
        cell.buttonEdit.tag = indexPath.row
        cell.buttonEdit?.addTarget(self, action: #selector(actionAddGuests(_:)), for: .touchUpInside)

        cell.buttonEdit.backgroundColor = Global.APP_COLOR
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 110
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
    
}
