//
//  AddNewGuestList.swift
//  Cavalli
//
//  Created by shardha on 4/18/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class AddNewGuestList: UIView {
    
    @IBOutlet weak var buttonSubmit: UIButton!
    @IBOutlet weak var textFieldTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var buttonDismiss: UIButton!
    @IBOutlet weak var viewTop: UIView!
    
    class func instanceFromNib() -> UIView
    {
        return UINib(nibName: "AddNewGuestList", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    override func awakeFromNib()
    {
        super.awakeFromNib()
    }
    override func layoutSubviews()
    {
        super.layoutSubviews()
    }
    
    // popup animation when appears
    func showAnimate()
    {
        
        self.viewTop.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        self.viewTop.alpha = 0.0
        
        UIView.animate(withDuration: 0.25, animations: {
            
            self.viewTop.alpha = 1.0
            self.viewTop.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
        
    }
    
    // popup animation when dissappears
    func removeAnimate()
    {
        
        self.viewTop.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        
        UIView.animate(withDuration: 0.25, animations: {
            self.viewTop.alpha = 0.0
            self.viewTop.transform = CGAffineTransform(scaleX: 1.7, y: 1.7)
        }) { (success) in
            self.removeFromSuperview()
        }
        
    }
    
}
