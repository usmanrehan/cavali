//
//  CMSViewController.swift
//  Cavalli
//
//  Created by shardha kawal on 8/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

import ObjectMapper

class CMSViewController: UIViewController {

    @IBOutlet weak var txtCMS: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        txtCMS.text = ""
        self.getCMSData()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    //MARK:- Service
    private func getCMSData(){
        Utility.showLoader()
        let params: [String: Any] = ["type": "1"]
        APIManager.sharedInstance.homeAPIManager.getCMSDatahWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<CMS_Model>().map(JSON: responseObject as [String : Any])

            let cmsData = CMSModel(value: response!)
            
            self.txtCMS.text = cmsData.descriptionValue
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
