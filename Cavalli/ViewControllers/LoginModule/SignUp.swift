//
//  SignUp.swift
//  Cavalli
//
//  Created by shardha on 1/19/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FBSDKLoginKit
import Validator
import ObjectMapper
import CountryPickerView
import DateTimePicker
import SwiftDate
import DropDown

class SignUp: UIViewController{
    
    //MARK:- Global declarations
    private var socialMediaId = ""
    private var socialMediaType = ""
    
    var strDOB : String = ""
    //MARK:- IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textFieldName: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldEmail: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldDOB: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldGender: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldConfirmPassword: SkyFloatingLabelTextFieldWithIcon!
    
    
    @IBOutlet weak var btnGender: UIButton!

    @IBOutlet weak var btnTermsConditions: UIButton?

    weak var cpvTextField: CountryPickerView!
    
    private let genderDropDown = DropDown()

    var arrGender = ["Male","Female"]
    
//    @IBOutlet weak var cpvMain: CountryPickerView!
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

      // let countryCode = NSLocale.current.regionCode//objectForKey(NSLocaleCountryCode) as String
         //Do any additional setup after loading the view.

//        self.textFieldName.text = "testUser"
//        self.textFieldEmail.text = "diathaeg@fakeinbox.com"
//        self.textFieldPhone.text = "+923557776237"
//        self.textFieldPassword.text = "123456"
//        self.textFieldConfirmPassword.text = "123456"
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear

        self.scrollView.delegate = self
        self.textFieldName.delegate = self
        self.textFieldEmail.delegate = self
        self.textFieldPhone.delegate = self
        self.textFieldPassword.delegate = self
        self.textFieldConfirmPassword.delegate = self
        
        let cp = CountryPickerView(frame: CGRect(x: 0, y: 5, width: 90, height: textFieldPhone.frame.size.height))
        cp.flagImageView.translatesAutoresizingMaskIntoConstraints = true
        cp.countryDetailsLabel.translatesAutoresizingMaskIntoConstraints = true
        cp.flagImageView.frame = CGRect(x: 0, y: 10, width: 30, height: textFieldPhone.frame.size.height - 10)
        cp.countryDetailsLabel.frame = CGRect(x: 35, y: 10, width: 100, height: textFieldPhone.frame.size.height - 10)
        textFieldPhone.leftView = cp
        
        textFieldPhone.textColor = UIColor.white
        textFieldPhone.leftViewMode = .always

        self.cpvTextField = cp
        self.cpvTextField.countryDetailsLabel.textColor = UIColor.white
        self.cpvTextField.flagImageView.image = #imageLiteral(resourceName: "phone")
        textFieldPhone.showDoneButtonOnKeyboard()

        self.cpvTextField.delegate = self
        
        setDatePicker()
        onClickSelectCategory()
    }
   
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        

        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        textFieldName.endEditing(true)
        textFieldEmail.endEditing(true)
        textFieldPhone.endEditing(true)
        textFieldPassword.endEditing(true)
        textFieldConfirmPassword.endEditing(true)
        
    }
    // set Gender Dropdown...
    
    func onClickSelectCategory() {
        genderDropDown.anchorView = btnGender
        genderDropDown.direction = .bottom
        genderDropDown.dataSource = arrGender
        
        // Action triggered on selection
        genderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.textFieldGender?.text = self.arrGender[index]
        }
    }
    
    //MARK: Set Date picker
    func setDatePicker(){
        //let date = Date()
        let MAXIMUM_AGE: Date = Calendar.current.date(byAdding: .year, value: -15, to: Date())!;

        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: MAXIMUM_AGE)
        textFieldDOB.text = result
        strDOB = MAXIMUM_AGE.string(custom: "yyyy-MM-dd HH:mm:ss")
        
        let datePickerView = UIDatePicker()
        // Age of 100.
        datePickerView.datePickerMode = .date
        datePickerView.maximumDate = MAXIMUM_AGE//date
        datePickerView.datePickerMode = .date
//        datePickerView.maximumDate = date
        textFieldDOB.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    
    @objc func handleDatePicker(sender: UIDatePicker) {//2018-05-29
        let date = DateInRegion(absoluteDate: sender.date)
//        if date.isToday || date.isInFuture {
            textFieldDOB.text = date.string(custom: "dd MMM yyyy")
            strDOB = date.string(custom: "yyyy-MM-dd")
//        }
    }
    
   //MARK:- IBActions
    @IBAction func actionGenderDropDown(_ sender: Any) {
        genderDropDown.show()
    }
    
    @IBAction func actionAcceptTerms(_ sender: Any) {
//        (currentQuestion.answer == true) ? "correct" : "Incorrect"
        btnTermsConditions?.isSelected = (btnTermsConditions?.isSelected == true) ? false : true//(btnTermsConditions = false) : true ?? false
    }
    
    @IBAction func actionTermsConditions(_ sender: Any) {
        let story = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = story.instantiateViewController(withIdentifier: "TCandPP") as? TCandPP {
            dvc.isPrivacy = false
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.view.endEditing(true)

        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSignUp(_ sender: UIButton) {
        self.validate()
    }
    @IBAction func actionFB(_ sender: UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    @IBAction func actionLogin(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nameEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validateName(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.nameNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func phoneEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validatePhone(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.PhoneNumber.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func passwordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidePassword(value: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.passwordInValidShort.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func confirmPasswordChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isConfirmPasswordIsEqualToPassword(password: self.textFieldPassword.text!, confirm: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.passwordNotMatchShort.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
   
    //MARK: - HelperMethods
    func checkAllFieldsAreNotEmpty() -> Bool {
        if(!(self.textFieldEmail.text?.isEmpty)! && !(self.textFieldPassword.text?.isEmpty)! && !(self.textFieldPassword.text?.isEmpty)! && !(self.textFieldConfirmPassword.text?.isEmpty)!){
            return true
        }
        return false
    }
    func validateName() -> Bool {
        return Validation.validateName(input: self.textFieldName.text!)
    }
    func validateEmailAddress() -> Bool {
        if(Validation.isValidEmail(self.textFieldEmail.text!)) {
            return true
        }
        return false
    }
    func validationPassword() -> Bool {
        if(Validation.isValidePassword(value: self.textFieldPassword.text!)) {
            return true
        }
        return false
    }
    func validConfirmPassword() -> Bool {
        if(Validation.isConfirmPasswordIsEqualToPassword(password: self.textFieldPassword.text!, confirm: self.textFieldConfirmPassword.text!)) {
            return true
        }
        return false
    }
    func validatePhone() -> Bool {
        return Validation.validatePhone(input: self.textFieldPhone.text!)
    }
    private func validate(){
        if self.textFieldName.text! == "" || self.textFieldEmail.text! == "" || self.textFieldPhone.text! == "" {
            Utility.showToast(message: "Please enter required fields!")
            return
        }
        
        if self.validateEmailAddress() && self.validatePhone() {
            if self.socialMediaId == "" {
                if self.textFieldPassword.text! == "" || self.textFieldConfirmPassword.text! == "" {
                    Utility.showToast(message: "Please enter required fields!")
                    return
                }
                if(self.validationPassword() && self.validConfirmPassword()) {
                    if btnTermsConditions?.isSelected == false {
                        Utility.showToast(message: "Please accept terms and conditions before proceeding with registration.")
                    }
                    else {
                        self.processRegisterUser(isSocialMedia: false)
                    }
                }
            }
            else {
                if btnTermsConditions?.isSelected == false {
                    Utility.showToast(message: "Please accept terms and conditions before proceeding with registration.")
                } else {
                    self.processRegisterUser(isSocialMedia: true)
                }
            }
        }
    }
    
       /* if(self.checkAllFieldsAreNotEmpty() && self.validateEmailAddress() && self.validationPassword() && self.validConfirmPassword() && self.validatePhone()) {
            if btnTermsConditions?.isSelected == false {
                Utility.showToast(message: "Please accept terms and conditions before proceeding with registration.")
            }
            else {
                if self.socialMediaId == "" {
                    self.processRegisterUser(isSocialMedia: false)
                }else {
                    self.processRegisterUser(isSocialMedia: true)
                }
            }
        }*/
//    }
    
    //MARK:- Services
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    self.textFieldPassword.isHidden = true
                    self.textFieldConfirmPassword.isHidden = true
                    
                    var dict = result as! [String : AnyObject]
                    print(dict["id"] as! String)
                    print(dict["name"] as! String)
                    let name = dict["name"] as! String
                    //                    let email = dict["email"] as! String
                    self.socialMediaId = dict["id"] as! String
                    self.socialMediaType = "facebook"
                    
                    self.textFieldName.text = name
                     
                    guard let email = dict["email"] else {
                        self.textFieldEmail.isHidden = false
                        return
                    }
                    print(dict["email"] as! String)
                    self.textFieldEmail.text = email as? String
                    
                    
                }
            })
        }
    }
    func processRegisterUser(isSocialMedia: Bool) {
        Utility.showLoader()
        var params: [String: Any] = ["":""]
        
        if isSocialMedia {
            params = ["user_name": self.textFieldName.text!,
                      "email": self.textFieldEmail.text!,
                      "phone": cpvTextField.countryDetailsLabel.text! + self.textFieldPhone.text!,
                      //"password": self.textFieldPassword.text!,
                      "socialmedia_id": self.socialMediaId,
                      "socialmedia_type": self.socialMediaType,
                      "device_type": "ios",
                      "device_token": Constants.device_token]
        }else {
            params = ["user_name": self.textFieldName.text!,
                      "email": self.textFieldEmail.text!,
                      "phone": cpvTextField.countryDetailsLabel.text! + self.textFieldPhone.text!,
                      "password": self.textFieldPassword.text!,
                      "device_type": "ios",
                      "device_token": Constants.device_token]
        }
        var strGender = ""
        if let gender = textFieldGender.text{
            strGender = gender.lowercased()
        }
        params.updateValue(strDOB, forKey: "dob")
        params.updateValue(strGender, forKey: "gender")
        APIManager.sharedInstance.userAPIManager.registerWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user = User(value: response!)
            AppStateManager.sharedInstance.loggedInUser = user
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "Verification") as? Verification {
                dvc.user = user
                Utility.showToast(message: "Code has been sent to your email address & phone number")
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
        
    }
}

extension SignUp: CountryPickerViewDelegate{
    
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country)
        self.view.endEditing(true)
        self.cpvTextField.countryDetailsLabel.text = country.phoneCode// "(\(country.code)) \(country.phoneCode)"
        self.cpvTextField.flagImageView.image = #imageLiteral(resourceName: "phone")
    }
}

extension SignUp: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension SignUp: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
}


extension UITextField {
    func showDoneButtonOnKeyboard() {
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(resignFirstResponder))
        
        var toolBarItems = [UIBarButtonItem]()
        toolBarItems.append(flexSpace)
        toolBarItems.append(doneButton)
        
        let doneToolbar = UIToolbar()
        doneToolbar.items = toolBarItems
        doneToolbar.sizeToFit()
        
        inputAccessoryView = doneToolbar
    }
}

