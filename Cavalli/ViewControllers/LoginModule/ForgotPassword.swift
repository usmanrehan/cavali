//
//  ForgotPassword.swift
//  Cavalli
//
//  Created by shardha on 1/19/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import ObjectMapper

class ForgotPassword: UIViewController {
    
    //MARK:- IBOutlet
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextFieldWithIcon!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK:- IBActions
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.view.endEditing(true)

        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.view.endEditing(true)

        self.validate()
    }
    
    //MARK:- Helper Methods
    func validate() {
        if !(self.textFieldEmailAddress.text?.isEmpty)! {
            if self.validateEmailAddress() {
                self.processForgotPassword()
            }
        }
    }
    func validateEmailAddress() -> Bool {
        if(Validation.isValidEmail(self.textFieldEmailAddress.text!)) {
            return true
        }
        return false
    }
    
    func pushToVerification(user: User){
        self.view.endEditing(true)

        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "Verification") as? Verification {
            dvc.isHeroEnabled = true
            dvc.isResetPwd = true
            dvc.user = user
            self.navigationController?.heroNavigationAnimationType = .fade
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    //MARK:- Services
    func processForgotPassword() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["email": self.textFieldEmailAddress.text!]
        
        APIManager.sharedInstance.userAPIManager.forgotPasswordWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user = User(value: response)
            self.pushToVerification(user: user)
            Utility.showToast(message: "We have sent you verification code in your email, please check your inbox as well as spam/junk folder.")
            
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}

