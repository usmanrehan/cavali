//
//  Verification.swift
//  Cavalli
//
//  Created by shardha on 3/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import PinCodeTextField
import ObjectMapper

class Verification: UIViewController {
    
    //MARK:- Global Declaration
    var isFromMobileNumVerification : Bool? = false
    var isMobileNumVerified: Bool? = false
    
    var strPhoneNum : String? = ""
    
    var user: User? = nil
    var isResetPwd = false
    //MARK:- IBOutlets
    @IBOutlet weak var bgTop: NSLayoutConstraint!
    @IBOutlet weak var pincode: PinCodeTextField!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.\
        
        pincode.keyboardType = .numberPad
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK:- IBActions
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.validate()
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.view.endEditing(true)

        if self.navigationController?.viewControllers.count == 1{
            let appDelegate = Constants.APP_DELEGATE
            appDelegate.showSplash()
        }
        else{
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    //MARK:- Helper Method
    private func validate()
    {
        if self.pincode.text?.count == 5 {
            if isFromMobileNumVerification == true || isMobileNumVerified == true {
                verifyCodeInApp(pinCode: self.pincode.text!)
            }
            else {
                if !isResetPwd{
                    self.processVerficationCode()
                }
                else{
                    self.processResetPwdVerficationCode()
                }
            }
        }
        else {
            Utility.showToast(message: "Please enter complete 5 digit code")
        }
    }
    
    func verifyCodeInApp(pinCode: String) {
        let code = "\((user?.verificationCode)!)"
        if code == pinCode {
            if isMobileNumVerified == true {
                requestUpdateNumber()
            }
            else {
                if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "ChangeNumber") as? ChangeNumberController {
                    dvc.isFromVerifcation = true
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
            }
        }
        else {
            Utility.showToast(message: "Entered pin code is invaild")
        }
//        if isMobileNumVerified == true {
//            requestUpdateNumber()
//        }
//        else {
//
//        }
    }
    
    func requestUpdateNumber() {
        Utility.showLoader()
        let params: [String: Any] = ["phone": strPhoneNum!]
        APIManager.sharedInstance.homeAPIManager.updatePhoneNumberWith(params: params, success: {
            (responseObject) in
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user = User(value: response!)
            user.token = (AppStateManager.sharedInstance.loggedInUser.token)!
            AppStateManager.sharedInstance.loggedInUser = user
            try! Global.APP_REALM?.write {
                Global.APP_REALM?.add(user, update: true)
            }
            if self.isMobileNumVerified == true {
                let viewControllers = self.navigationController?.viewControllers
                let count = viewControllers?.count
                if count! > 1 {
                    if let setVC = viewControllers?[count! - 3] as? MainContainer {
                        //Set the value
                        self.navigationController?.popToViewController(setVC, animated: true)
                    }
                }
            }
            Utility.main.showAlert(message: "Number verified", title: "Message")
            Utility.hideLoader()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
    }
    
    //MARK:- @objc
    @objc func keyboardWillShow(notification: NSNotification) {
        self.bgTop.constant = -50
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.bgTop.constant = 0
    }
    
    func pushToResetPassword(user: User){
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "ResetPassword") as? ResetPassword {
            dvc.isHeroEnabled = true
            dvc.user = user
            self.navigationController?.heroNavigationAnimationType = .fade
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    //MARK:- Services
    
    
    func processVerficationCode() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["user_id": (user?.id)!,
                                     "verification_code": self.pincode.text!]
        
        APIManager.sharedInstance.userAPIManager.verificationCodeWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
//            let user = AppStateManager.sharedInstance.loggedInUser
//            try! Global.APP_REALM?.write {
//                user?.active = "1"
//            }
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user_obj = User(value: response!)
            try! Global.APP_REALM?.write {
                AppStateManager.sharedInstance.loggedInUser = user_obj
                Global.APP_REALM?.add(user_obj, update: true)
            }
            let appDelegate = Constants.APP_DELEGATE
            appDelegate.showHome()
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
    func processResetPwdVerficationCode() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["user_id": user?.id ?? 0,
                                     "verification_code": self.pincode.text!]
        
        APIManager.sharedInstance.userAPIManager.verificationCodeWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user_obj = User(value: response)
            try! Global.APP_REALM?.write {
                AppStateManager.sharedInstance.loggedInUser = user_obj
                Global.APP_REALM?.add(user_obj, update: true)
            }
            self.pushToResetPassword(user: user_obj)
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
    
}

