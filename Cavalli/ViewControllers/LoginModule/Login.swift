

//
//  Login.swift
//  Cavalli
//
//  Created by shardha on 1/17/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Toast_Swift
import ObjectMapper
import FBSDKLoginKit

class Login: UIViewController{

    //MARK:- Global declarations
    private var socialMediaId = ""
    private var socialMediaType = ""
    
    //MARK:- IBOutlets
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldPassword: SkyFloatingLabelTextFieldWithIcon!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.view.endEditing(true)
        UIApplication.shared.statusBarView?.backgroundColor = UIColor.clear

        // Do any additional setup after loading the view.
//        self.textFieldEmailAddress.text = "tefriara@fakeinbox.com"
//        self.textFieldPassword.text = "123456"
        
        self.textFieldEmailAddress.delegate = self
        self.textFieldPassword.delegate = self
    }

   //MARK:- IBActions
    @IBAction func actionLogin(_ sender: UIButton) {
       self.validate()
    }
    
    @IBAction func actionSignup(_ sender: UIButton) {
        self.view.endEditing(true)

        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "SignUp") as? SignUp {
            dvc.isHeroEnabled = true
            self.navigationController?.heroNavigationAnimationType = .fade
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @IBAction func actionForgotPassword(_ sender: UIButton) {
        self.view.endEditing(true)
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassword") as? ForgotPassword {
            dvc.isHeroEnabled = true
            self.navigationController?.heroNavigationAnimationType = .fade
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func passwordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidePassword(value: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.passwordInValidShort.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func actionFB(_ sender: UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    //MARK:- Helper Methods
    //MARK:- Services
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    var dict = result as! [String : AnyObject]
                    print(dict["id"] as! String)
                    print(dict["name"] as! String)
                    self.socialMediaId = dict["id"] as! String
                    self.socialMediaType = "facebook"
                    self.processLoginSocialMedia()
                }
            })
        }
    }
    func checkAllFieldsAreNotEmpty() -> Bool {
        if(!((self.textFieldEmailAddress.text?.isEmpty)!) && !((self.textFieldPassword.text?.isEmpty)!)){
            return true
        }
        return false
    }
    private func validate()                      {
        if self.textFieldEmailAddress.text! == "" || self.textFieldPassword.text! == ""{
            Utility.showToast(message: "Please enter required fields!")
            return
        }
        if(self.checkAllFieldsAreNotEmpty() && self.validateEmailAddress() && self.validationPassword()) {
             self.processLoginUser()
        }
    }
    func validateEmailAddress() -> Bool {
        if(Validation.isValidEmail(self.textFieldEmailAddress.text!)) {
            return true
        }
        return false
    }
    
    func validationPassword() -> Bool {
        if(Validation.isValidePassword(value: self.textFieldPassword.text!)) {
            return true
        }
        return false
    }
    //MARK:- Services
    func processLoginUser() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["email": self.textFieldEmailAddress.text!,
                                     "password": self.textFieldPassword.text!,
                                     "device_type": "ios",
                                     "device_token": Constants.device_token]
        APIManager.sharedInstance.userAPIManager.loginWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user = User(value: response!)
            if user.active == "0" {
                let appDelegate = Constants.APP_DELEGATE
                appDelegate.showVerification(user: user)
            }else {
                try! Global.APP_REALM?.write {
                    AppStateManager.sharedInstance.loggedInUser = user
                    Global.APP_REALM?.add(user, update: true)
                }
                let appDelegate = Constants.APP_DELEGATE
                appDelegate.showHome()
            }
            print(user)
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
    
    func processLoginSocialMedia() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["socialmedia_type": socialMediaType,
                                     "socialmedia_id": socialMediaId,
                                     "device_type": "ios",
                                     "device_token": Constants.device_token]
        APIManager.sharedInstance.userAPIManager.socialLoginWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user = User(value: response!)
            if user.active == "0" {
                let appDelegate = Constants.APP_DELEGATE
                appDelegate.showVerification(user: user)
            }else {
                try! Global.APP_REALM?.write {
                    AppStateManager.sharedInstance.loggedInUser = user
                    Global.APP_REALM?.add(user, update: true)
                }
                let appDelegate = Constants.APP_DELEGATE
                appDelegate.showHome()
            }
            print(user)
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}

extension Login: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
