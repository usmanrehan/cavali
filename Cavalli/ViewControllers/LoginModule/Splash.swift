//
//  ViewController.swift
//  Cavalli
//
//  Created by shardha on 1/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Hero
class Splash: UIViewController {
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let when = DispatchTime.now() + 0.5
        DispatchQueue.main.asyncAfter(deadline: when) {
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "Login") as? Login {
                //dvc.isHeroEnabled = true
                //self.navigationController?.heroNavigationAnimationType = .fade
                //self.hero_replaceViewController(with: dvc)
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        }
    }
}

