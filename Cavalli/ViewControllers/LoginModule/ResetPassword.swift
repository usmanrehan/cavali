//
//  ResetPassword.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/26/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ResetPassword: UIViewController {

    @IBOutlet weak var tfNewPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var tfConfirmPassword: SkyFloatingLabelTextFieldWithIcon!
    
    var user : User?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func onBtnUpdate(_ sender: UIButton) {
        self.validate()
    }
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func validate(){
        if !Validation.isValidePassword(value: self.tfNewPassword.text!){
            self.view.endEditing(true)
            Utility.showToastBottom(message: AlertMessages.passwordInValidShort.rawValue)
            return
        }
        guard Validation.validateStringLength(self.tfNewPassword.text!) else{
            self.view.endEditing(true)
            Utility.showToastBottom(message: "Please enter your new password.")
            return
        }
        guard Validation.validateStringLength(self.tfConfirmPassword.text!) else{
            self.view.endEditing(true)
            Utility.showToastBottom(message: "Please confirm your password.")
            return
        }
        if self.tfNewPassword.text != self.tfConfirmPassword.text{
            self.view.endEditing(true)
            Utility.showToastBottom(message: "Password does not match the confirm password.")
            return
        }
        else{
            self.setNewPassword()
        }
    }
    func popToLogin(){
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: Login.self) {
                AppStateManager.sharedInstance.logOutUser()
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
    //MARK:- Service
    //resetPassword
    func setNewPassword() {
        Utility.showLoader()
        let password = self.tfNewPassword.text ?? ""
        
        let params: [String: Any] = ["user_id": self.user?.id ?? 0,
                                     "password": password]
        APIManager.sharedInstance.userAPIManager.setNewPasswordWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()

            self.popToLogin()
            Utility.showToast(message: "Password successfully changed.")
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}

