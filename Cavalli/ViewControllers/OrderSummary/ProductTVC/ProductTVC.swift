//
//  ProductTVC.swift
//  Cavalli
//
//  Created by shardha on 3/27/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
protocol ProductTVCDelegate {
    func didTappedMinus(sender: UIButton, count: Int)
    func didTappedPlus(sender: UIButton, count: Int)
}

class ProductTVC: UITableViewCell {

    @IBOutlet weak var buttonMinus: UIButton!
    @IBOutlet weak var buttonPlus: UIButton!
    @IBOutlet weak var labelPrice: UILabel!
    @IBOutlet weak var labelProductName: UILabel!
    @IBOutlet weak var lableCount: UILabel!
    
    var delegate: ProductTVCDelegate? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func actionPlus(_ sender: UIButton) {
        var count = Int(self.lableCount.text!)!
        count = count + 1
        self.lableCount.text = "\(count)"
        delegate?.didTappedPlus(sender: sender, count: count)
    }
    
    @IBAction func actionMinus(_ sender: UIButton) {
        var count = Int(self.lableCount.text!)!
        if count >= 1 {
            count = count - 1
            self.lableCount.text = "\(count)"
        }
        delegate?.didTappedMinus(sender: sender, count: count)
    }
    
}
