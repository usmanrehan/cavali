//
//  OrderToken.swift
//  Cavalli
//
//  Created by shardha on 3/31/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class OrderToken: UIViewController {
    
    //MARK:- Global declarations
    var token = ""
    
    //MARK:- IBOutlets
    @IBOutlet weak var labelTokenNum: UILabel!
    @IBOutlet weak var labelName: UILabel!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let user = AppStateManager.sharedInstance.loggedInUser
        self.labelName.text = (user?.userName ?? "") + "!"
        self.labelTokenNum.text = self.token
    }

    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}
