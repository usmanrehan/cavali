

//
//  OrderSummary.swift
//  Cavalli
//
//  Created by shardha on 3/27/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import RealmSwift
import ObjectMapper

class OrderSummary: UIViewController{

    //MARK:- Global declarations
    var count = 2
    var cartList : Results<Cart>!
    var tip = 0
    var tax = 0.0
    var taxValue = 0.0
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblCartEmpty: UILabel!
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "TipTVC", bundle: nil), forCellReuseIdentifier: "TipTVC")
        self.tableView.register(UINib(nibName: "PriceTVC", bundle: nil), forCellReuseIdentifier: "PriceTVC")
        self.tableView.register(UINib(nibName: "ProductTVC", bundle: nil), forCellReuseIdentifier: "ProductTVC")
        self.tableView.register(UINib(nibName: "PayNowTVC", bundle: nil), forCellReuseIdentifier: "PayNowTVC")
        
        self.processGetTax()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.initData()
    }
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Helper Methods
    func initData() {
        
        self.cartList = CartRealmHelper.sharedInstance.getAllInCart()
        if(self.cartList.count <= 0) {
            self.tableView.isHidden = true
            self.lblCartEmpty.isHidden = false
            //self.btnProceedCheckout.isHidden = true
        }
        else {
            self.tableView.isHidden = false
            self.lblCartEmpty.isHidden = true

        }
        self.count = self.cartList.count
        self.tableView.reloadData()
    }
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    @objc func processOrderNow() {
        var arrayOrderDetail = [[String: Any]]()
        var subTotal: Double = 0
        for item in CartRealmHelper.sharedInstance.getAllInCart() {
            var dic = [String: Any]()
            dic["product_id"] = String(item.product.id)
            dic["quantity"] = String(item.count)
            dic["price"] = item.product.price ?? "0"
            subTotal = subTotal + (Double(item.product.price ?? "0.0")! * Double(item.count))
            arrayOrderDetail.append(dic)
        }
        
        let tip = Double(subTotal) * (Double(self.tip)/100.0)
//        let tax = self.taxValue
        let grandTotal = subTotal + tip // + tax
        let json = self.json(from: arrayOrderDetail)
        Utility.showLoader()
        var params: [String: Any] = ["":""]
        params = ["order_detail": json!,
                  "sub_total": subTotal,
                  "tax": tax,
                  "total_amount": grandTotal,
                  "tip":tip]
        
        APIManager.sharedInstance.homeAPIManager.addOrderWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            if let res = responseObject as? [String: Any] {
                if let tok = res["order_no"] as? Int {
                    Utility.showToast(message: "Order placed successfully.")
                    
                    if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "PaymentMethode") as? PaymentMethodeViewController {
                        dvc.token = "\(tok)"
                        dvc.order_Id = (res["id"] as? Int)!
                        dvc.idTitle = "o_id"
                        dvc.strModel = "payment"
                        self.navigationController?.pushViewController(dvc, animated: true)
                    }
                   /* if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "OrderToken") as? OrderToken {
                        dvc.token = "\(tok)"
                        self.navigationController?.pushViewController(dvc, animated: true)
                    }*/
                }
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
    func processGetTax() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        APIManager.sharedInstance.homeAPIManager.getTaxWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<TaxModel>().map(JSON: responseObject as [String : Any])
            self.tax = Double(response?.value ?? "0.0")!
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}

extension OrderSummary: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.count + 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case self.count:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PriceTVC") as? PriceTVC else {
                return UITableViewCell()
            }
            cell.labelTitle.text = "Sub Total"
            var total = 0.0
            for item in CartRealmHelper.sharedInstance.getAllInCart() {
                total = total + (Double(item.product.price ?? "0.0")! * Double(item.count))
            }
            let strTotal = String(total)
            cell.lablePrice.text = "AED \(strTotal.currencyFormatter())"
            return cell
        case self.count + 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "TipTVC") as? TipTVC else {
                return UITableViewCell()
            }
            cell.delegate = self
            return cell
        case self.count + 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PriceTVC") as? PriceTVC else {
                return UITableViewCell()
            }
            var subTotal = 0.0
            for item in CartRealmHelper.sharedInstance.getAllInCart() {
                subTotal = subTotal + (Double(item.product.price ?? "0")! * Double(item.count))
            }
            cell.labelTitle.text = "inclusive of VAT and municipality fee"
            let priceCurrency = "\(subTotal * (self.tax/100))"
            cell.lablePrice.text = priceCurrency.currencyFormatter()//"\(subTotal * (self.tax/100))"
            self.taxValue = subTotal * (self.tax/100)
            return cell
        case self.count + 3:
            
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PriceTVC") as? PriceTVC else {
                return UITableViewCell()
            }
            var total: Double = 0
            for item in CartRealmHelper.sharedInstance.getAllInCart() {
                total = total + (Double(item.product.price ?? "0.0")! * Double(item.count))
            }
            let tip = Double(total) * (Double(self.tip)/100.0)
            //let tax = self.taxValue
            total = total + tip// + tax
            cell.labelTitle.text = "Total"
            let strTotal = String(total)
            cell.lablePrice.text = "AED \(strTotal.currencyFormatter())"
            cell.lablePrice.font = cell.lablePrice.font.withSize(18)
            cell.lablePrice.textColor = Global.APP_COLOR
            return cell
        case self.count + 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "PayNowTVC") as? PayNowTVC else {
                return UITableViewCell()
            }
            cell.buttonPayNow.addTarget(self, action: #selector(self.processOrderNow), for: .touchUpInside)
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "ProductTVC") as? ProductTVC else {
                return UITableViewCell()
            }
            cell.buttonPlus.tag = indexPath.item
            cell.buttonMinus.tag = indexPath.item
            cell.delegate = self
            cell.labelProductName.text = self.cartList[indexPath.item].product.title ?? ""
            let initialPrice = Double(self.cartList[indexPath.item].product.price ?? "0.0")!
            let price  = Double(self.cartList[indexPath.item].count) * initialPrice
            let strprice = String(price)
            cell.labelPrice.text = "AED \(strprice.currencyFormatter())"
            cell.lableCount.text = "\(self.cartList[indexPath.item].count)"
            return cell
            
        }
    }
}
extension OrderSummary: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.item > count - 1 {
            return  self.view.frame.size.height * 0.09
        }else {
            return  self.view.frame.size.height * 0.07
        }
    }
}
extension OrderSummary: ProductTVCDelegate, Tip {
    func didTappedMinus(sender: UIButton, count: Int) {
        if count == 0 {
            CartRealmHelper.sharedInstance.deleteThis(cart: self.cartList[sender.tag])
            self.initData()
            self.tableView.reloadData()
            if self.cartList.count == 0 {
                self.navigationController?.popViewController(animated: true)
            }
        }else {
            CartRealmHelper.sharedInstance.decrementThis(cart: self.cartList[sender.tag])
            self.tableView.reloadData()
        }
    }
    
    
    func didTappedPlus(sender: UIButton, count: Int) {
        CartRealmHelper.sharedInstance.incrementThis(cart: self.cartList[sender.tag])
        self.tableView.reloadData()
    }
    func tipConstant(x: Int) {
        self.tip = x
        self.tableView.reloadData()
    }
}
