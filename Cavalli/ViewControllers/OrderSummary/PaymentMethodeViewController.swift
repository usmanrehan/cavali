//
//  PaymentMethodeViewController.swift
//  Cavalli
//
//  Created by shardha kawal on 7/19/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import NVActivityIndicatorView
class PaymentMethodeViewController: UIViewController {

    var token : String? = ""
    var strModel = ""
    var order_Id = 0
    var idTitle = ""
    var isFromMemberShip : Bool? = false
    
    @IBOutlet weak var webPaymentMethod: UIWebView?

    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotification(notification:)), name: Notification.Name("NotificationIdentifier"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.methodOfReceivedNotificationFail(notification:)), name: Notification.Name("NotificationIdentifierFail"), object: nil)
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
        let size = CGSize(width: 50, height: 50)
        let bgColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        let activityData = ActivityData(size: size, message: "", messageFont: UIFont.systemFont(ofSize: 12), type: .ballClipRotate, color: Global.APP_COLOR , padding: 0, displayTimeThreshold: 0, minimumDisplayTime: 1, backgroundColor: bgColor, textColor: UIColor.black)
        
        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
        
//        webPaymentMethod?.backgroundColor = UIColor.red
        let url = URL (string: "http://cavalli.stagingic.com/\(strModel)?\(idTitle)=\(order_Id)")
        let request = URLRequest(url: url!)
//            NSURLRequest(URL: url! as URL)
        print(request)
        webPaymentMethod?.loadRequest(request)
        
        if let request = webPaymentMethod?.request {
            if let resp = URLCache.shared.cachedResponse(for: request) {
                if let response = resp.response as? HTTPURLResponse {
                    print(response.allHeaderFields)
                }
            }
        }
        
        // Do any additional setup after loading the view.
    }

    @objc func methodOfReceivedNotification(notification: Notification) {
        CartRealmHelper.sharedInstance.emptyCartList()
        if isFromMemberShip == true {
            self.navigationController?.popViewController(animated: true)

        } else {
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "OrderToken") as? OrderToken {
                dvc.token = token!
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        }
        
    }
    
    @objc func methodOfReceivedNotificationFail(notification: Notification) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PaymentMethodeViewController: UIWebViewDelegate {
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebViewNavigationType) -> Bool {        
        return true
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        Utility.hideLoader()
        let headers = webPaymentMethod?.request?.allHTTPHeaderFields
        for (key,value) in headers! {
            print("key \(key) value \(value)")
        }
    }
    
}
