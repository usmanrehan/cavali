//
//  HomeCollectionTVC.swift
//  Cavalli
//
//  Created by shardha on 1/22/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class HomeCollectionTVC: UITableViewCell {

    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
}
