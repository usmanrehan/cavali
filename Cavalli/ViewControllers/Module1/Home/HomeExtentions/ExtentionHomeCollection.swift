//
//  ExtentionHomeCollection.swift
//  Cavalli
//
//  Created by shardha on 1/23/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SDWebImage
extension Home: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 0 { //categories
            return 8
        }else { //latest updates
            return self.arrayLatestUpdates.count
        }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 0 { //categories
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVC", for: indexPath) as? HomeCVC else { return UICollectionViewCell() }
            switch self.arrayHomeCategory[indexPath.item] {
            case .reservations(let name, let image):
                cell.label.text = name
                cell.image.image = image
                break
            case .menu(let name, let image):
                cell.label.text = name
                cell.image.image = image
                break
            case .guestList(let name, let image):
                cell.label.text = name
                cell.image.image = image
                break
            case .competition(let name, let image):
                cell.label.text = name
                cell.image.image = image
                break
            case .music(let name, let image):
                cell.label.text = name
                cell.image.image = image
                break
            case .barOrdering(let name, let image):
                cell.label.text = name
                cell.image.image = image
                break
            case .cavalliNights(let name, let image):
                cell.label.text = name
                cell.image.image = image
                break
            case .membership(let name, let image):
                cell.label.text = name
                cell.image.image = image
                break
            }
            return cell
        }else { //latest updates
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeLatestUpdatesCVC", for: indexPath) as? HomeLatestUpdatesCVC else { return UICollectionViewCell() }
            let lu = self.arrayLatestUpdates[indexPath.item]
            
            cell.imageBanner.sd_setImage(with: URL(string: lu.imageUrl!))
            cell.lableName.text = lu.title
//            if indexPath.item == 0 {
//                cell.imageBanner.image = #imageLiteral(resourceName: "HomeLU3")
//                cell.lableName.text = "Modern & Contemporary Americ...."
//            }else if indexPath.item == 1 {
//                cell.imageBanner.image = #imageLiteral(resourceName: "HomeLU3")
//                cell.lableName.text = "Bar Moga Opens...."
//            }else {
//                cell.imageBanner.image = #imageLiteral(resourceName: "HomeLU1")
//                cell.lableName.text = "DJ Narcos Perfor"
//            }
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView.tag == 0 { //categories
            return CGSize(width: collectionView.frame.size.width/4, height: collectionView.frame.size.width/4 + 10)
        }else { //latest updates
            return CGSize(width: collectionView.frame.size.width/2.5, height: collectionView.frame.size.width/2)
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 0 { //categories
            return 0
        }else { //latest updates
            return 10
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView.tag == 0 { //categories
            return 0
        }else { //latest updates
            return 0
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView.tag == 0 { //categories
            switch self.arrayHomeCategory[indexPath.item] {
            case .reservations:
                
                guard let reservationFilter = self.storyboard?.instantiateViewController(withIdentifier: "ReservationFilter") as? ReservationFilter else { return }
                guard let reservation = self.storyboard?.instantiateViewController(withIdentifier: "Reservations") as? Reservations else { return }
                let nav = ReservationNavigation(menuViewController: UIViewController(), contentViewController: reservation)
                nav.navigationBar.isHidden = true
                nav.sideMenu = ENSideMenu(sourceView: nav.view, menuViewController: reservationFilter, menuPosition:.right)
                nav.sideMenu?.menuWidth = self.view.frame.size.width * 0.60
                self.present(nav, animated: true, completion: nil)
                break
            case .menu:
                if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "Menu") as? Menu {
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
                break
            case .guestList:
//                if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "Guest") as? Guest {
//                    self.navigationController?.pushViewController(dvc, animated: true)
//                }
                break
            case .competition:
                break
            case .music:
                break
            case .barOrdering:
                break
            case .cavalliNights:
                break
            case .membership:
                break
            }
        }else { //latest updates
        }
    }
    
}
