//
//  MenuTVC.swift
//  Cavalli
//
//  Created by shardha on 2/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MenuTVC: UITableViewCell {

    @IBOutlet weak var imageMenu: UIImageView!
    @IBOutlet weak var labelMenuName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
