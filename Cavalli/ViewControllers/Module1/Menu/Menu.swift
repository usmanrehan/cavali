//
//  Menu.swift
//  Cavalli
//
//  Created by shardha on 2/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
class Menu: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var arrayMenuCategories = [MenuCategoryModel]()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "MenuTVC", bundle: nil), forCellReuseIdentifier: "MenuTVC")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
        
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        self.processGetMenuCategories()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  self.view.frame.size.height * 0.22
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayMenuCategories.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTVC") as? MenuTVC else { return UITableViewCell() }
        let obj = self.arrayMenuCategories[indexPath.item]
//        switch indexPath.item {
//        case 0:
//            cell.imageMenu.image = #imageLiteral(resourceName: "lounge")
//            cell.labelMenuName.text = "Lounge Menu"
//            break
//        case 1:
//            cell.imageMenu.image = #imageLiteral(resourceName: "resturant")
//            cell.labelMenuName.text = "Resturant Menu"
//            break
//        case 2:
//            cell.imageMenu.image = #imageLiteral(resourceName: "cigar")
//            cell.labelMenuName.text = "Cigar Menu"
//            break
//        case 3:
//            cell.imageMenu.image = #imageLiteral(resourceName: "Wine")
//            cell.labelMenuName.text = "Wine Menu"
//            break
//        default:
//            break
//        }
        cell.imageMenu.setImageWith(URL(string: obj.categoryImage!)!)
        cell.labelMenuName.text = obj.name
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "MenuDetail") as? MenuDetail {
            dvc.menuCategory = self.arrayMenuCategories[indexPath.item]
            self.navigationController?.pushViewController(dvc, animated: true)
            
        }
    }
    func processGetMenuCategories() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getMenuCategoriesWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MenuCategoryModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayMenuCategories = response
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "")
            self.navigationController?.popViewController(animated: true)
        })
        
    }

}
