//
//  MyReservation.swift
//  Cavalli
//
//  Created by shardha on 1/31/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
protocol MyReservationProtocol {
    func shouldShowFilter()
    func shouldHideFilter()
}
class MyReservation: UIViewController, UITableViewDataSource, UITableViewDelegate, FilledReservation {
    
    @IBOutlet weak var tableView: UITableView!
    var arrayMyReservation = [MyReservationModel]()
    var arrayFilteredMyReservation = [MyReservationModel]()
    var parentNavigationController: UINavigationController?
    var filterData = false
    var delegate: MyReservationProtocol? = nil
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "ReservationTVC", bundle: nil), forCellReuseIdentifier: "ReservationTVC")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.filterData(_:)), name: NSNotification.Name(rawValue: "updateMyReservation"), object: nil)

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate?.shouldShowFilter()
        self.processGetReservation()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegate?.shouldHideFilter()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc func filterData(_ notification: NSNotification) {
        guard let reserved = notification.userInfo?["reserved"] as? Bool else {return}
        guard let pending = notification.userInfo?["pending"] as? Bool else {return}
        guard let rejected = notification.userInfo?["rejected"] as? Bool else {return}
        guard let cancled = notification.userInfo?["cancled"] as? Bool else {return}
        
        if !reserved && !pending && !rejected && !cancled {
            self.filterData = false
            self.tableView.reloadData()
            return
        }
        var arr = [MyReservationModel]()
        for item in self.arrayMyReservation {
            if item.status == "Reserved " && reserved == true {
                arr.append(item)
            }
            if item.status == "Pending" && pending == true {
                arr.append(item)
            }
            if item.status == "Rejected" && rejected == true {
                arr.append(item)
            }
            if item.status == "Cancelled " && cancled == true {
                arr.append(item)
            }
        }
        self.arrayFilteredMyReservation = arr
        if self.arrayFilteredMyReservation.count == 0 {
            Utility.showToast(message: "No data found!", controller: self)
        }
        self.filterData = true
        self.tableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.filterData {
            return self.arrayFilteredMyReservation.count
        }else {
            return self.arrayMyReservation.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReservationTVC") as? ReservationTVC else { return UITableViewCell() }
        var obj = MyReservationModel()
        if filterData {
            obj = self.arrayFilteredMyReservation[indexPath.item]
        }else {
            obj = self.arrayMyReservation[indexPath.item]
        }
        
        cell.configureCell(title: obj.title, date: obj.date, status: obj.status, number: obj.noPeople)
        cell.buttonViewDetails.tag = indexPath.item
        cell.buttonViewDetails.addTarget(self, action: #selector(self.actionViewDetails(sender:)), for: .touchUpInside)
        return cell
    }
    
    @objc func actionViewDetails(sender: UIButton) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "FilledReservationForm") as? FilledReservationForm {
            if self.filterData {
                dvc.reservationData = self.arrayFilteredMyReservation[sender.tag]
            }else {
                dvc.reservationData = self.arrayMyReservation[sender.tag]
            }
            dvc.delegate = self
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
    func didCompletedEditing() {
        self.processGetReservation()
        
    }
    func didEndEditing() {
        self.delegate?.shouldShowFilter()
    }
    func processGetReservation() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getMyReservationWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()

            
            var Obj : [MyReservationModel] = [MyReservationModel]()
            
            for item in responseObject {
                if let newItem = Mapper<MyReservationModel>().map(JSONObject: item){
                    Obj.append(newItem)
                }
            }
            
            
            if Obj.count == 0 {
                Utility.showToast(message: "No data found!", controller: self)
            }else {
                self.arrayMyReservation = Obj
                self.tableView.reloadData()
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
        
    }
}
