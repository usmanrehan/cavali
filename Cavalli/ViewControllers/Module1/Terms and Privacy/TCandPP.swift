//
//  TCandPP.swift
//  Cavalli
//
//  Created by shardha on 3/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
class TCandPP: UIViewController {

    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var textView: UITextView!
    var isPrivacy = false
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isPrivacy {
            self.titleLable.text = "Privacy Policy"
            self.processGetCMS(id: 2)
        }else {
            self.titleLable.text = "Terms & Conditions"
            self.processGetCMS(id: 1)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func processGetCMS(id: Int) {
        Utility.showLoader()
        let params: [String: Any] = ["type": id]
        APIManager.sharedInstance.homeAPIManager.getCMSWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<CMSModel>().map(JSON: responseObject as! [String : Any])
            self.textView.text = response?.descriptionValue
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
        
    }
}
