//
//  LatestUpdates.swift
//  Cavalli
//
//  Created by shardha on 3/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SDWebImage
class LatestUpdates: UIViewController, UITableViewDataSource, UITableViewDelegate {
    var arrayLatestUpdate = [LatestUpdatesModel]()
    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "LatestUpdateTVC", bundle: nil), forCellReuseIdentifier: "LatestUpdateTVC")
        self.tableView.estimatedRowHeight = 100
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayLatestUpdate.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LatestUpdateTVC") as? LatestUpdateTVC else { return UITableViewCell() }
        cell.imageL.sd_setImage(with: URL(string: self.arrayLatestUpdate[indexPath.item].imageUrl!))
        cell.labelTitle.text = self.arrayLatestUpdate[indexPath.item].title
        cell.labelDescription.text = self.arrayLatestUpdate[indexPath.item].descriptionValue
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
    }

    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
