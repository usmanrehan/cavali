
//
//  MenuDetail.swift
//  Cavalli
//
//  Created by shardha on 3/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
class MenuDetail: UIViewController {
    var pageMenu : CAPSPageMenu?
    var menuCategory = MenuCategoryModel()
    var arrayMenuProduct = [MenuProductModel]()
    @IBOutlet weak var viewPage: UIView!
    @IBOutlet weak var pagerParent: UIView!
    @IBOutlet weak var imageNavBg: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.processGetMenuProducts()
        //self.setNumberOfItemsinPager(number: 2)
//        let when = DispatchTime.now() + 5
//        DispatchQueue.main.asyncAfter(deadline: when) {
//            //self.removeItemsFromPager()
//            self.setNumberOfItemsinPager(number: 10)
//        }
//        let when2 = DispatchTime.now() + 15
//        DispatchQueue.main.asyncAfter(deadline: when2) {
//            //self.removeItemsFromPager()
//            self.setNumberOfItemsinPager(number: 2)
//        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    private func removeItemsFromPager() {
        for item in self.viewPage.subviews {
            if item.tag == 101 {
                item.removeFromSuperview()
            }
        }
        self.pageMenu?.removeFromParentViewController()
        self.pageMenu?.controllerArray.removeAll()
        self.pageMenu = nil
        
    }
    private func setNumberOfItemsinPager(number: Int) {
        self.removeItemsFromPager()
        
        var controllerArray : [UIViewController] = []
        
        for i in 0..<number {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MenuCategoryList") as! MenuCategoryList
            controller.parentNavigationController = self.navigationController
            controller.title = "\(i) Number"
            controllerArray.append(controller)
        }
//        let controllerReservationForm = self.storyboard?.instantiateViewController(withIdentifier: "ReservationForm") as! ReservationForm
//        let controllerMyReservation = self.storyboard?.instantiateViewController(withIdentifier: "MyReservation") as! MyReservation
//
//        controllerReservationForm.parentNavigationController = self.navigationController
//        controllerMyReservation.parentNavigationController = self.navigationController
//
//        controllerReservationForm.title = "Reserve Now"
//        controllerMyReservation.title = "My Reservation"
//
//        controllerArray.append(controllerReservationForm)
//        controllerArray.append(controllerMyReservation)
    
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let height = self.pagerParent.frame.size.height//30//self.imageNavBg.frame.size.width * 0.086
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(0),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.clear),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(goldenColor),
            .BottomMenuHairlineColor(UIColor.clear),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(0),
            .MenuHeight(height)
            
        ]
        
        let frame = CGRect(x: 0, y: 0, width: Int(self.pagerParent.frame.size.width), height:Int(self.viewPage.frame.size.height))
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: frame, pageMenuOptions: parameters)
        pageMenu?.view.tag = 101
        self.viewPage.addSubview(pageMenu!.view)
        
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func processGetMenuProducts() {
        Utility.showLoader()
        let params: [String: Any] = ["category_id":self.menuCategory.id]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getMenuProductWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MenuProductModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayMenuProduct = response
            self.popIfNoItems()
            self.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: , title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "")
            self.navigationController?.popViewController(animated: true)
        })
        
    }
    func popIfNoItems() {
        if self.arrayMenuProduct.count == 0 {
            Utility.showToast(message: "No Product found!")
            self.navigationController?.popViewController(animated: true)
        }
    }
    func reloadData() {
        self.removeItemsFromPager()
        var controllerArray : [UIViewController] = []
        for i in 0..<self.arrayMenuProduct.count {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MenuCategoryList") as! MenuCategoryList
            controller.parentNavigationController = self.navigationController
            controller.title = self.arrayMenuProduct[i].name ?? ""
            controller.products = self.arrayMenuProduct[i].products
            controllerArray.append(controller)
        }
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let height = self.pagerParent.frame.size.height//30//self.imageNavBg.frame.size.width * 0.086
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(0),
            .UseMenuLikeSegmentedControl(false),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.clear),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(goldenColor),
            .BottomMenuHairlineColor(UIColor.clear),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(0),
            .MenuHeight(height)
            
        ]
        
        let frame = CGRect(x: 0, y: 0, width: Int(self.pagerParent.frame.size.width), height:Int(self.viewPage.frame.size.height))
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: frame, pageMenuOptions: parameters)
        pageMenu?.view.tag = 101
        self.viewPage.addSubview(pageMenu!.view)
    }

}
