//
//  SideMenu.swift
//  Cavalli
//
//  Created by shardha on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class SideMenu: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    let arrayData = [("Event Calendar", #imageLiteral(resourceName: "event")), ("Cavalli Social", #imageLiteral(resourceName: "social")), ("Package Information", #imageLiteral(resourceName: "package")), ("International Request", #imageLiteral(resourceName: "international")), ("Gallery", #imageLiteral(resourceName: "gallery")), ("About Us", #imageLiteral(resourceName: "about")), ("Logout", #imageLiteral(resourceName: "logout"))]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.isScrollEnabled = false
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC") as? SideMenuTVC else { return UITableViewCell() }
        let (name, image) = self.arrayData[indexPath.item]
        cell.imageIcon.image = image
        cell.labelTitle.text = name
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.074
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.item == 6  {
            let appD = Constants.APP_DELEGATE
            appD.showSplash()
        }
    }
}
