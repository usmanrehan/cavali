//
//  ForgotPassword.swift
//  Cavalli
//
//  Created by shardha on 1/19/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
class ForgotPassword: UIViewController {

    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextFieldWithIcon!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.validate()
    }
    func validate() {
        if !(self.textFieldEmailAddress.text?.isEmpty)! {
            if self.validateEmailAddress() {
                self.processForgotPassword()
            }
        }
    }
    func validateEmailAddress() -> Bool {
        if(Validation.isValidEmail(self.textFieldEmailAddress.text!)) {
            return true
        }
        return false
    }
    func processForgotPassword() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["email": self.textFieldEmailAddress.text!]
        
        APIManager.sharedInstance.userAPIManager.forgotPasswordWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            Utility.showToast(message: "We have sent you new password in your email, please check your inbox as well as spam/junk folder.")
            self.navigationController?.popViewController(animated: true)
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}
