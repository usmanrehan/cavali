//
//  Login.swift
//  Cavalli
//
//  Created by shardha on 1/17/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import Toast_Swift
import ObjectMapper
class Login: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldPassword: SkyFloatingLabelTextFieldWithIcon!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        self.textFieldEmailAddress.text = "user21@gmail.com"
//        self.textFieldPassword.text = "123456"
        
        self.textFieldEmailAddress.delegate = self
        self.textFieldPassword.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func actionLogin(_ sender: UIButton) {
       self.validate()
    }
    
    @IBAction func actionSignup(_ sender: UIButton) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "SignUp") as? SignUp {
            dvc.isHeroEnabled = true
            self.navigationController?.heroNavigationAnimationType = .fade
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @IBAction func actionForgotPassword(_ sender: UIButton) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassword") as? ForgotPassword {
            dvc.isHeroEnabled = true
            self.navigationController?.heroNavigationAnimationType = .fade
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func passwordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidePassword(value: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.passwordInValidShort.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    func checkAllFieldsAreNotEmpty() -> Bool {
        if(!((self.textFieldEmailAddress.text?.isEmpty)!) && !((self.textFieldPassword.text?.isEmpty)!)){
            return true
        }
        
        return false
        
    }
    private func validate()                      {
        if(self.checkAllFieldsAreNotEmpty() && self.validateEmailAddress() && self.validationPassword()) {
             self.processLoginUser()
        }
    }
    func validateEmailAddress() -> Bool {
        if(Validation.isValidEmail(self.textFieldEmailAddress.text!)) {
            return true
        }
        return false
    }
    
    func validationPassword() -> Bool {
        if(Validation.isValidePassword(value: self.textFieldPassword.text!)) {
            return true
        }
        return false
    }
    func processLoginUser() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["email": self.textFieldEmailAddress.text!,
                                     "password": self.textFieldPassword.text!,
                                     "device_type": "ios",
                                     "device_token": "123456789"]
        APIManager.sharedInstance.userAPIManager.loginWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<User>().map(JSON: responseObject as! [String : Any])
            let user = User(value: response)
            AppStateManager.sharedInstance.loggedInUser = user
            if user.active == "0" {
                let appDelegate = Constants.APP_DELEGATE
                appDelegate.showVerification()
            }else {
                try! Global.APP_REALM?.write {
                    Global.APP_REALM?.add(user, update: true)
                }
                let appDelegate = Constants.APP_DELEGATE
                appDelegate.showHome()
            }
            print(user)
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
        
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}

