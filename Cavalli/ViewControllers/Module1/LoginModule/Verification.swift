//
//  Verification.swift
//  Cavalli
//
//  Created by shardha on 3/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import PinCodeTextField
class Verification: UIViewController {

    var user: User? = nil
    @IBOutlet weak var bgTop: NSLayoutConstraint!
    
    @IBOutlet weak var pincode: PinCodeTextField!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.\
        
        pincode.keyboardType = .numberPad
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    private func validate() {
        if self.pincode.text?.characters.count == 5 {
            self.processVerficationCode()
        }
    }
    func processVerficationCode() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["user_id": AppStateManager.sharedInstance.loggedInUser.id,
            "verification_code": self.pincode.text!]
        
        APIManager.sharedInstance.userAPIManager.verificationCodeWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let user = AppStateManager.sharedInstance.loggedInUser
            try! Global.APP_REALM?.write {
                user?.active = "1"
            }
            let appDelegate = Constants.APP_DELEGATE
            appDelegate.showHome()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.validate()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
            self.bgTop.constant = -50
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
            self.bgTop.constant = 0
    }
}
