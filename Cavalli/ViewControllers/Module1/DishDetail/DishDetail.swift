//
//  DishDetail.swift
//  Cavalli
//
//  Created by shardha on 3/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SDWebImage
class DishDetail: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var buttonPrice: UIButton!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    var product = Products()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.register(UINib(nibName: "DDTitleTVC", bundle: nil), forCellReuseIdentifier: "DDTitleTVC")
        self.tableView.register(UINib(nibName: "DDDescriptionTVC", bundle: nil), forCellReuseIdentifier: "DDDescriptionTVC")
        self.tableView.register(UINib(nibName: "DDPriceTVC", bundle: nil), forCellReuseIdentifier: "DDPriceTVC")
        
        if self.product.productImages.count > 0 {
            if let stringName = self.product.productImages[0].imageUrl {
                self.imageBanner.sd_setImage(with: URL(string: stringName))
            }
        }
        
        if self.product.productAttributes.count > 0 {
            if let price = self.product.productAttributes[0].value {
                if price != "" {
                    self.buttonPrice.setTitle("AED \(price)", for: .normal)
                }
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.item {
        case 0:
            return  self.view.frame.size.height * 0.080
        case 1:
            return  UITableViewAutomaticDimension
        case 2:
            return  UITableViewAutomaticDimension
        case 3:
            return  UITableViewAutomaticDimension
        case 4:
            return  UITableViewAutomaticDimension
        default:
            return  self.view.frame.size.height * 0.080
        }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + self.product.productAttributes.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDTitleTVC") as? DDTitleTVC else { return UITableViewCell() }
            cell.lableTitle.text = self.product.title ?? ""
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDDescriptionTVC") as? DDDescriptionTVC else { return UITableViewCell() }
            cell.lableDesc.text = self.product.descriptionValue ?? ""
            return cell
//        case 2:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDPriceTVC") as? DDPriceTVC else { return UITableViewCell() }
//            cell.catTitle.text = "Small"
//            return cell
//        case 3:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDPriceTVC") as? DDPriceTVC else { return UITableViewCell() }
//            cell.catTitle.text = "Medium"
//            return cell
//        case 4:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDPriceTVC") as? DDPriceTVC else { return UITableViewCell() }
//            cell.catTitle.text = "Large"
//            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDPriceTVC") as? DDPriceTVC else { return UITableViewCell() }
            cell.catTitle.text = self.product.productAttributes[indexPath.item - 2].key ?? ""
            if let p = self.product.productAttributes[indexPath.item - 2].value {
                if p != "" {
                    cell.price.text = "AED \(p)"
                }
            }
            return cell
            
        }
    }
}
