//
//  ChangePassword.swift
//  Cavalli
//
//  Created by shardha on 3/31/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePassword: UIViewController {

    @IBOutlet weak var tfOldPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var tfNewPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var tfConfirmPassword: SkyFloatingLabelTextFieldWithIcon!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func acitonBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.validate()
    }
    //MARK:- Helper Method
    func validate()
    {
        guard Validation.validateStringLength(self.tfOldPassword.text!) else{
            self.view.endEditing(true)
            Utility.showToastBottom(message: "Please enter your password.")
            return
        }
        guard Validation.validateStringLength(self.tfNewPassword.text!) else{
            self.view.endEditing(true)
            Utility.showToastBottom(message: "Please enter your new password.")
            return
        }
        guard Validation.isValidePassword(value:self.tfConfirmPassword.text!) else{
            self.view.endEditing(true)
            Utility.showToastBottom(message: "Password should have least 6 characters.")
            return
        }
        if self.tfNewPassword.text != self.tfConfirmPassword.text{
            self.view.endEditing(true)
            Utility.showToastBottom(message: "Password does not match the confirm password.")
            return
        }
        else{
            self.changePassword()
        }
    }
    
    func changePassword() {
        Utility.showLoader()
        let oldPassword = self.tfOldPassword.text ?? ""
        let newPassword = self.tfNewPassword.text ?? ""
        
        let params: [String: Any] = ["password": newPassword,
                                     "old_password": oldPassword]
        APIManager.sharedInstance.userAPIManager.changePasswordWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.navigationController?.popViewController(animated: true)
            Utility.showToast(message: "Password successfully changed.")
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }

}
