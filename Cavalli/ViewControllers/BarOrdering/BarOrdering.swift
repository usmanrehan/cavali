//
//  BarOrdering.swift
//  Cavalli
//
//  Created by shardha on 3/20/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
class BarOrdering: UIViewController{

    //MARK:- Global declaration
    var arrayBarData = [BarOrderingModel]()
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.backgroundColor = UIColor.white
        self.tableView.register(UINib(nibName: "BarOrderingTVC", bundle: nil), forCellReuseIdentifier: "BarOrderingTVC")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.processGetBarData()
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionCart(_ sender: UIButton)
    {
        let cartItem = CartRealmHelper.sharedInstance.getAllInCart()
        if cartItem.count > 0 {
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "OrderSummary") as? OrderSummary {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        } else {
            Utility.showToast(message: "Cart is empty.")
        }
    }
    
    //MARK:- Helper Methods
    @objc func actionSeeAll(sender: UIButton) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "BarOrderingList") as? BarOrderingList {
            dvc.products = self.arrayBarData[sender.tag].products
            dvc.navTitle = self.arrayBarData[sender.tag].name ?? ""
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    //MARK: - Service
    func processGetBarData() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getBarDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<BarOrderingModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayBarData = response
            try! Global.APP_REALM?.write {
                Global.APP_REALM?.add(response, update: true)
            }   
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "")
            self.navigationController?.popViewController(animated: true)
        })
        
    }
}
extension BarOrdering: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.arrayBarData.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "BarOrderingTVC") as? BarOrderingTVC else { return UITableViewCell() }
        cell.collectionView.delegate = self
        cell.collectionView.dataSource = self
        //cell.backgroundColor = UIColor.blue
        
        cell.collectionView.tag = indexPath.section
        cell.selectionStyle = .none
        let layout = cell.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        layout.scrollDirection = .horizontal
        cell.collectionView.register(UINib(nibName: "BarOrderingCVC", bundle: nil), forCellWithReuseIdentifier: "BarOrderingCVC")
        cell.collectionView.reloadData()
        return cell
    }
}
extension BarOrdering: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height/4
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40//self.view.frame.size.height * 0.08
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0//0.00001
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: BarOrderingHeader = .fromNib()
        header.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 40)
        header.labelTitle.text = self.arrayBarData[section].name
        header.buttonSeeAll.isHidden = false
        header.buttonSeeAll.tag = section
        header.buttonSeeAll.addTarget(self, action: #selector(self.actionSeeAll(sender:)), for: .touchUpInside)
        header.backgroundColor = UIColor.clear
        return header
    }
}
extension BarOrdering: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayBarData[collectionView.tag].products.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BarOrderingCVC", for: indexPath) as? BarOrderingCVC else {
            return UICollectionViewCell()
            
        }
        //cell.contentView.backgroundColor = UIColor.red
        cell.lableName.text = self.arrayBarData[collectionView.tag].products[indexPath.item].title
        //cell.imageBanner.sd_setImage(with: URL(string: self.arrayBarData[collectionView.tag].products[indexPath.item].imageUrl!))
        
        cell.imageBanner.sd_setShowActivityIndicatorView(true)
        cell.imageBanner.sd_setIndicatorStyle(.gray)
        cell.imageBanner.sd_setImage(with: URL(string: self.arrayBarData[collectionView.tag].products[indexPath.item].imageUrl!), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        return cell
    }
}
extension BarOrdering: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "BarOrderingDetail") as? BarOrderingDetail {
            dvc.product = self.arrayBarData[collectionView.tag].products[indexPath.item]
            dvc.category = self.arrayBarData[indexPath.section].name ?? ""
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
extension BarOrdering: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        //return CGSize(width: collectionView.frame.size.width/2.5, height: collectionView.frame.size.width/2)
        return CGSize(width: self.view.frame.size.width/2.5, height: self.view.frame.size.width/2.3)//190)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
