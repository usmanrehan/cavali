//
//  BarOrderingHeader.swift
//  Cavalli
//
//  Created by shardha on 3/20/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BarOrderingHeader: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonSeeAll: UIButton!
}
