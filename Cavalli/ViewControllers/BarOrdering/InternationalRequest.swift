//
//  InternationalRequest.swift
//  Cavalli
//
//  Created by shardha on 3/31/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class InternationalRequest: UIViewController {
    
    let placeholderIntro = "Your Request"
    
    @IBOutlet weak var tfFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var tfSubject: SkyFloatingLabelTextField!
    @IBOutlet weak var tvYourRequest: UITextView!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.tfFullName.text = AppStateManager.sharedInstance.loggedInUser.userName
        self.tvYourRequest.delegate = self
        self.tvYourRequest.textColor = UIColor.lightGray
        self.tvYourRequest.text = self.placeholderIntro
        // Do any additional setup after loading the view.
    }
    @IBAction func onBtnBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func onBtnSubmit(_ sender: Any) {
        self.validate()
    }
    //MARK:- Helper Method
    func validate(){
        let subject = tfSubject.text
        let message = tvYourRequest.text
        guard Validation.validateStringLength(subject ?? "") else{
            Utility.showToast(message: "Please provide subject.")
            return
        }
        guard Validation.validateStringLength(message ?? "") else{
            Utility.showToast(message: "Please provide message.")
            return
        }
        if message == placeholderIntro{
            Utility.showToast(message: "Please provide message.")
            return
        }
        self.addInternationalRequest()
    }
}
extension InternationalRequest: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = self.placeholderIntro
            textView.textColor = UIColor.lightGray
        }
    }
}
//MARK:- Services
extension InternationalRequest{
    //title
    //message
    //MARK:- Services
    func addInternationalRequest () {
        Utility.showLoader()
        let params: [String: Any] = ["title": self.tfSubject.text ?? "",
                                     "message": self.tvYourRequest.text ?? ""]
        APIManager.sharedInstance.userAPIManager.addInternationalRequestWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.navigationController?.popViewController(animated: true)
            Utility.showToast(message: "Request successfully submitted.")
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}
