//
//  BarOrderingDetail.swift
//  Cavalli
//
//  Created by shardha on 3/27/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

import RealmSwift
import ObjectMapper

class BarOrderingDetail: UIViewController{
    
    //MARK:- Global declaration
    var product = Products()
    var category = ""
    var cat = NSMutableAttributedString()
    var desc = NSMutableAttributedString()
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonPrice: UIButton!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self

        self.tableView.register(UINib(nibName: "DDTitleTVC", bundle: nil), forCellReuseIdentifier: "DDTitleTVC")
        self.tableView.register(UINib(nibName: "BarOrderingDetailsTVC", bundle: nil), forCellReuseIdentifier: "BarOrderingDetailsTVC")
        self.tableView.register(UINib(nibName: "BarOrderingAddToCart", bundle: nil), forCellReuseIdentifier: "BarOrderingAddToCart")
        
        if self.product.productImages.count > 0 {
            if let stringName = self.product.productImages[0].imageUrl {
                //self.imageBanner.sd_setImage(with: URL(string: stringName))
                self.imageBanner.sd_setShowActivityIndicatorView(true)
                self.imageBanner.sd_setIndicatorStyle(.gray)
                self.imageBanner.sd_setImage(with: URL(string: stringName), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
            }
        }
        
        if let price = self.product.price {
            if price != "" {
                self.buttonPrice.setTitle("AED \(price.currencyFormatter())", for: .normal)
            }
        }
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 300
//        self.tableView.estimatedRowHeight = 300
//        self.tableView?.rowHeight = UITableViewAutomaticDimension
    }
    
    //MARK:- IBActions
    
    @IBAction func actionCart(_ sender: UIButton)
    {
        let cartItem = CartRealmHelper.sharedInstance.getAllInCart()
        if cartItem.count > 0 {
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "OrderSummary") as? OrderSummary {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        } else {
            Utility.showToast(message: "Cart is empty.")
        }
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
  
    //MARK:- Helper Methods
    @objc func actionAddToCart(sender: UIButton) {
        var alreadyAdded = false
        for item in CartRealmHelper.sharedInstance.getAllInCart() {
            if item.product.id == self.product.id {
                alreadyAdded = true
                break
            }
        }
        if alreadyAdded {
           

            let refreshAlert = UIAlertController(title: "Message", message: "Product is already added in your cart, \nAre you sure you want to increase quantity of the product?", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
                self.increaseProductQuantity(product: self.product)
//                CartRealmHelper.sharedInstance.incrementThis(cart: self.product)//addThisToCart(with: self.product)
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
                //self.dismiss(animated: true, completion: nil)
            }))
            
            self.present(refreshAlert, animated: true, completion: nil)
           // Utility.showToast(message: "Item is already present in the cart.")
        } else {
            CartRealmHelper.sharedInstance.addThisToCart(with: self.product)
            Utility.showToast(message: "Item successfully added to cart.")
        }
    }
    
    func increaseProductQuantity(product: Products) {
        var cartList : Results<Cart>!
        cartList = CartRealmHelper.sharedInstance.getAllInCart()
        print(cartList)
        
        for cartObject in cartList {
            print(cartObject)
            if cartObject.id == product.id {
                CartRealmHelper.sharedInstance.incrementThis(cart: cartObject)//addThisToCart(with: self.product)
            }
        }
    }
}
extension BarOrderingDetail: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDTitleTVC") as? DDTitleTVC else { return UITableViewCell() }
            cell.lableTitle.text = self.product.title ?? ""
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BarOrderingDetailsTVC") as? BarOrderingDetailsTVC else { return UITableViewCell() }
            cell.selectionStyle = .none
            
           // cell.labelCategory.text = self.category
           // cell.lableDesc.text = self.product.descriptionValue ?? ""
            //https://stackoverflow.com/a/35087471
            OperationQueue.main.addOperation({() -> Void in
                cell.labelCategory.attributedText = Utility.getBoldDarkGrayString(boldText: "Category: ", plainText: self.category)
                cell.lableDesc.attributedText = Utility.getBoldDarkGrayString(boldText: "Description: ", plainText: self.product.descriptionValue ?? "")
            })
            return cell
//        case 2:
//            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDDescriptionTVC") as? DDDescriptionTVC else { return UITableViewCell() }
//            //DispatchQueue.main.async {
//            //
//            //             let stringAttributesBold = [NSAttributedStringKey.font: UIFont.init(name: "Poppins-Bold", size: 14)!]
//            //             let stringAttributesLight = [NSAttributedStringKey.font: UIFont.init(name: "Poppins", size: 14)!]
//            //             var attrString1: NSMutableAttributedString = NSMutableAttributedString(string: "Category: ")
//            //             attrString1.addAttributes(stringAttributesBold, range:  NSMakeRange(0, attrString1.length))
//            //             var attrString2: NSMutableAttributedString = NSMutableAttributedString(string: "Shots")
//            //             attrString2.addAttributes(stringAttributesLight, range:  NSMakeRange(0, attrString2.length))
//            //            var attrString3: NSMutableAttributedString = NSMutableAttributedString(string: "Description: ")
//            //            attrString3.addAttributes(stringAttributesBold, range:  NSMakeRange(0, attrString3.length))
//            //            var attrString4: NSMutableAttributedString = NSMutableAttributedString(string: self.product.descriptionValue ?? "")
//            //            attrString4.addAttributes(stringAttributesLight, range:  NSMakeRange(0, attrString4.length))
//            //
//
//
//            //
//            //            let attributedString1 = NSMutableAttributedString(string:"Category: ", attributes:stringAttributesBold)
//            //
//            //            let attributedString2 = NSMutableAttributedString(string:"Shots", attributes:stringAttributesLight)
//            //
//            //            let attributedString3 = NSMutableAttributedString(string:"Description: ", attributes:stringAttributesBold)
//            //
//            //            let attributedString4 = NSMutableAttributedString(string: self.product.descriptionValue ?? "", attributes:stringAttributesLight)
//            //
//            //
//            //            let cat = NSMutableAttributedString()
//            //            cat.append(attributedString1)
//            //            cat.append(attributedString2)
//            //
//            //            attributedString1.append(attributedString2)
//            //            attributedString3.append(attributedString4)
//            //            self.cat = cat
//            //            self.desc = attributedString3
//            //
//            //            let cat = NSMutableAttributedString()
//            //            cat.append(attrString1)
//            //            cat.append(attrString2)
//            //            let desc = NSMutableAttributedString()
//            //            desc.append(attrString3)
//            //            desc.append(attrString4)
//            //                cell.labelCategory.attributedText = cat//attrString//self.cat
//            //                cell.lableDesc.attributedText = desc//self.desc
//            //                //cell.layoutSubviews()
//            //            //}
//            //
//            cell.lableDesc.text = self.product.descriptionValue
//            cell.selectionStyle = .none
//            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BarOrderingAddToCart") as? BarOrderingAddToCart else { return UITableViewCell() }
            cell.buttonAddToCart.tag = indexPath.item
            cell.selectionStyle = .none
            cell.buttonAddToCart.addTarget(self, action: #selector(self.actionAddToCart(sender:)), for: .touchUpInside)
            return cell
            
        }
    }
   
   
}
extension BarOrderingDetail: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.item {
        case 0:
            return UITableViewAutomaticDimension //self.view.frame.size.height * 0.080
        case 1:
            return  UITableViewAutomaticDimension
        case 2:
            return  UITableViewAutomaticDimension
        default:
            return  100//self.view.frame.size.height * 0.080
        }
    }
}
