//
//  GridViewCell.swift
//  Cavalli
//
//  Created by shardha kawal on 10/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class GridViewCell: UICollectionViewCell {
    
    //MARK:- Global declarations
    var isBarList = false
    
    //MARK:- IBOutlets
    @IBOutlet weak var labelTitle: UILabel?
    @IBOutlet weak var imageProduct: UIImageView?
    
    //MARK:- Nib lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK:- Helper Methods
    func configureCell(pro: Products) {
        self.imageProduct?.sd_setShowActivityIndicatorView(true)
        self.imageProduct?.sd_setIndicatorStyle(.gray)
        self.imageProduct?.sd_setImage(with: URL(string: pro.imageUrl!)!, placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        self.labelTitle?.text = pro.title ?? ""
    }
}
