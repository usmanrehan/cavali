//
//  MenuCategoryList.swift
//  Cavalli
//
//  Created by shardha on 3/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm
import SwiftPhotoGallery
import ImageSlideshow

class MenuCategoryList: UIViewController{
    
   
    //MARK:- GLobal declarations
    var products = List<Products>()
    var productImages = List<ProductImages>()
    var parentNavigationController: UINavigationController?
    
    @IBOutlet var imgSlideShow: ImageSlideshow?
    
    var viewType = ""
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView?
    @IBOutlet weak var gridView: UICollectionView?
    @IBOutlet weak var lblNodataFound: UILabel!
    
    var selectedItem : String? = ""
    var flag = true
    var arrImagesLatestPhotos = [UIImage]()
    var arrImagesAllPhotos = [UIImage]()
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(products.count)
        // Do any additional setup after loading the view.
        if self.products.count == 0 {
            self.lblNodataFound.isHidden = false
            self.gridView?.isHidden = true
            self.tableView?.isHidden = true
            imgSlideShow?.isHidden = true
        } else {
            if viewType == "image" {
                self.gridView?.dataSource = self
                self.gridView?.delegate = self
                self.gridView?.register(UINib(nibName: "GridCollectionCell", bundle: nil), forCellWithReuseIdentifier: "gridCell")
                self.gridView?.isHidden = true
                imgSlideShow?.isHidden = false
                self.lblNodataFound.isHidden = true
                self.tableView?.isHidden = true
                self.gridView?.reloadData()
            } else {
                self.tableView?.dataSource = self
                self.tableView?.delegate = self
                self.tableView?.register(UINib(nibName: "MenuCategoryTVC", bundle: nil), forCellReuseIdentifier: "MenuCategoryTVC")
                self.tableView?.isHidden = false
                self.lblNodataFound.isHidden = true
                self.gridView?.isHidden = true
                imgSlideShow?.isHidden = true

                self.tableView?.reloadData()
            }
        }
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.didTap))
        imgSlideShow?.addGestureRecognizer(gestureRecognizer)
    }
    
    @objc func didTap() {
        
        var imageSource: [AlamofireSource] = []
        if let proImage = products[(imgSlideShow?.currentPage)!].imageUrl {
            imageSource.append(AlamofireSource(urlString: proImage)!)
            
            let window = UIApplication.shared.keyWindow!
            let LogoutAlertView = CustomSlider.instanceFromNib() as! CustomSlider
            LogoutAlertView.btnCancel?.addTarget(self, action: #selector(removeSlider), for: .touchUpInside)
            LogoutAlertView.frame = window.frame
            LogoutAlertView.tag = 201
            LogoutAlertView.slideshow?.setImageInputs(imageSource)
            window.addSubview(LogoutAlertView)
        } else {
            Utility.main.showAlert(message: "This product does not contain image", title: "Message")
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setImageSlideShow()
    }
    
    func setImageSlideShow() {
        
        imgSlideShow?.backgroundColor = UIColor.clear
        imgSlideShow?.zoomEnabled = true
//        imgSlideShow?.slideshowInterval = 5.0
        imgSlideShow?.pageControlPosition = PageControlPosition.insideScrollView
        imgSlideShow?.pageControl.currentPageIndicatorTintColor = UIColor(displayP3Red: 161/255.0, green: 145/255.0, blue: 97/255.0, alpha: 1.0)
        imgSlideShow?.pageControl.pageIndicatorTintColor = UIColor.white
        imgSlideShow?.contentScaleMode = UIViewContentMode.scaleAspectFit
//        imgSlideShow?.setCurrentPage(2, animated: false)
        var imageSource: [AlamofireSource] = []
        for productObj in products {
            print((products.first?.imageUrl)!)
            imageSource.append(AlamofireSource(urlString: (productObj.imageUrl)!)!)
        }
        
        imgSlideShow?.setImageInputs(imageSource)
        
    }
    @objc func removeSlider() {
        let window = UIApplication.shared.keyWindow!
        for item in window.subviews{
            if item.tag == 201 {
                item.removeFromSuperview()
            }
        }
    }
}

extension MenuCategoryList: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCategoryTVC") as? MenuCategoryTVC else { return UITableViewCell() }
        cell.configureCell(pro: self.products[indexPath.item])
        return cell
    }
}
extension MenuCategoryList: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  120//self.view.frame.size.height * 0.25
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "DishDetail") as? DishDetail {
            dvc.product = self.products[indexPath.item]
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
}

extension MenuCategoryList: UICollectionViewDelegate {
    
}

extension MenuCategoryList: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: gridView!.frame.size.width / 3.2 , height: gridView!.frame.size.width / 3.2)
    }
}
extension MenuCategoryList: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gridCell", for: indexPath) as! GridCollectionCell
        cell.backgroundColor = UIColor.gray
        cell.configureCell(pro: products[indexPath.row])
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
       // arrImages = AppStateManager.sharedInstance.arrRoomDetails["Images"] as! [String]
        var imageSource: [AlamofireSource] = []
        for image in products[indexPath.row].productImages {
            let img = image.imageUrl
            imageSource.append(AlamofireSource(urlString: img!)!)
        }
        
//        self.gridView?.isHidden = true
        let window = UIApplication.shared.keyWindow!
        let LogoutAlertView = CustomSlider.instanceFromNib() as! CustomSlider
        LogoutAlertView.btnCancel?.addTarget(self, action: #selector(removeSlider), for: .touchUpInside)
        LogoutAlertView.frame = window.frame
        LogoutAlertView.tag = 201
        LogoutAlertView.slideshow?.setImageInputs(imageSource)
        window.addSubview(LogoutAlertView)
        
        
        
      /*  let appDelegate = UIApplication.shared.delegate as! AppDelegate
        var customReviewPopup = LogoutPopUpView.init(nibName: "LogoutPopUpView", bundle: Bundle.main)
        
        self.appDelegate.window?.addSubview((customReviewPopup.view)!)
        self.customReviewPopup.view.frame = (self.appDelegate.window?.bounds)!
        self.customReviewPopup.view.alpha = 0
        self.customReviewPopup.view.isHidden = true
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .transitionCrossDissolve, animations: {
            self.customReviewPopup.view.isHidden = false
            self.customReviewPopup.view.alpha = 1
        }, completion: nil)*/
        
        
//        roomSlideshow.backgroundColor = UIColor.clear
//        roomSlideshow.slideshowInterval = 5.0
//        roomSlideshow.pageControlPosition = PageControlPosition.insideScrollView
//        roomSlideshow.pageControl.currentPageIndicatorTintColor = AppConstants.redBarColor
//        roomSlideshow.pageControl.pageIndicatorTintColor = UIColor.white
//        roomSlideshow.contentScaleMode = UIViewContentMode.scaleAspectFill
//
//        arrImages = AppStateManager.sharedInstance.arrRoomDetails["Images"] as! [String]
//        var imageSource: [AlamofireSource] = []
//        for image in products[indexPath.row].productImages {
//            let img = image.imageUrl
//            imageSource.append(AlamofireSource(urlString: img)!)
//        }
//        self.roomSlideshow.setImageInputs(imageSource)
        
//        self.arrImagesAllPhotos.removeAll()
//        for _ in products[indexPath.row].productImages{
//            self.arrImagesAllPhotos.append(UIImage(named: "placeHolder")!)
//        }
//        productImages = products[indexPath.row].productImages
//
//        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pager"), object: nil, userInfo: ["isShow": 0])
//
//        let gallery = SwiftPhotoGallery(delegate: self, dataSource: self)
//        gallery.backgroundColor = .black
//        gallery.pageIndicatorTintColor = .lightGray
//        gallery.currentPageIndicatorTintColor = .white
//        gallery.hidePageControl = false
//
//        present(gallery, animated: true, completion: { () -> Void in
//            gallery.currentPage = 1
//        })
    }
}
extension MenuCategoryList: SwiftPhotoGalleryDataSource{
    func numberOfImagesInGallery(gallery: SwiftPhotoGallery) -> Int {
        return self.arrImagesAllPhotos.count
    }
    
    func imageInGallery(gallery: SwiftPhotoGallery, forIndex: Int) -> UIImage? {
//        if let 
        let imageView = UIImageView()
        imageView.sd_setImage(with: URL(string: (productImages[forIndex].imageUrl)!)!, placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        return imageView.image
        
//        self.imageProduct?.sd_setImage(with: URL(string: pro.imageUrl!)!, placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
//        if flag{
//            return self.arrImagesLatestPhotos[forIndex]
//        } else {
//            return self.arrImagesAllPhotos[forIndex]
//        }
    }
}
extension MenuCategoryList: SwiftPhotoGalleryDelegate{
    func galleryDidTapToClose(gallery: SwiftPhotoGallery) {
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "pager"), object: nil, userInfo: ["isShow": 1])
        self.dismiss(animated: true, completion: nil)
        
        self.gridView?.isHidden = false
        
    }
}
