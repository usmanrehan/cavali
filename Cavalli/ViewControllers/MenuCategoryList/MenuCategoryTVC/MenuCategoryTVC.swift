//
//  MenuCategoryTVC.swift
//  Cavalli
//
//  Created by shardha on 3/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MenuCategoryTVC: UITableViewCell {

    //MARK:- Global declarations
    var isBarList = false
    
    //MARK:- IBOutlets
    @IBOutlet weak var lablePrice: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    
    //MARK:- Nib lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    //MARK:- Helper Methods
    func configureCell(pro: Products) {
        //self.imageProduct.sd_setImage(with: URL(string: pro.imageUrl!)!)
        self.imageProduct.sd_setShowActivityIndicatorView(true)
        self.imageProduct.sd_setIndicatorStyle(.gray)
        self.imageProduct.sd_setImage(with: URL(string: pro.imageUrl!)!, placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        
        self.labelTitle.text = pro.title ?? ""
        self.labelDesc.text = pro.descriptionValue ?? ""
        if self.isBarList {
            if let p = pro.price as? String {
                 self.lablePrice.text = "AED \(p.currencyFormatter())"
            }else {
                self.lablePrice.text = ""
            }
        }
        if pro.productAttributes.count > 0 {
            if let p = pro.productAttributes[0].value as? String {
                if p != "" {
                    self.lablePrice.text = "AED \(p.currencyFormatter())"
                }else {
                    self.lablePrice.text = ""
                }
            }else {
                self.lablePrice.text = ""
            }
        }
    }
}
