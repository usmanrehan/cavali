//
//  Messages.swift
//  Cavalli
//
//  Created by shardha on 4/4/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
class Messages: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var arrayThread = [ThreadModel]()
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "MessagesTVC", bundle: nil), forCellReuseIdentifier: "MessagesTVC")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        self.processGetThreadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
    }
    
    //MARK: - Service
    func processGetThreadData() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        APIManager.sharedInstance.homeAPIManager.getThreadDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<ThreadModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayThread = response
            if self.arrayThread.count == 0 {
                Utility.showToast(message: "No Data Found.")
            }
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
        
    }
}
extension Messages: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayThread.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesTVC") as? MessagesTVC else { return UITableViewCell() }
        let obj = self.arrayThread[indexPath.item]
        cell.labelDesc.text = obj.message
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let odate = dateformatter.date(from: obj.createdAt ?? "2000-01-01 01:01")
        let dFForDate = DateFormatter()
        dFForDate.dateFormat = "dd:MM:yy"
        cell.labelDate.text = dFForDate.string(from: odate!)
        let tFForDate = DateFormatter()
        tFForDate.dateFormat = "HH:mm"
        cell.labelTime.text = tFForDate.string(from: odate!)
        
        return cell
    }
}
extension Messages: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.22
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "MessagesDetail") as? MessagesDetail {
            dvc.thread = self.arrayThread[indexPath.item]
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
}
