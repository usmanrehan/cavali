//
//  MembershipDetails.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MembershipDetails: UIViewController {
    
    //MARK:- Global declaration
    var cat = NSMutableAttributedString()
    var desc = NSMutableAttributedString()
    var membershipData = MembershipModel()
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonPrice: UIButton!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "MDTitleTVC", bundle: nil), forCellReuseIdentifier: "MDTitleTVC")
        self.tableView.register(UINib(nibName: "MDDescriptionTVC", bundle: nil), forCellReuseIdentifier: "MDDescriptionTVC")
        self.tableView.register(UINib(nibName: "MDBuyNow", bundle: nil), forCellReuseIdentifier: "MDBuyNow")
        
        if self.membershipData.memberShipImages.count > 0 {
            if let stringName = self.membershipData.memberShipImages[0].imageUrl {
                self.imageBanner.sd_setImage(with: URL(string: stringName))
            }
        }
        
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Helper Methods
    @objc func actionAddToCart(sender: UIButton) {
        self.processBuySubscription()
    }
}
extension MembershipDetails: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MDTitleTVC") as? MDTitleTVC else { return UITableViewCell() }
            cell.lableTitle.text = self.membershipData.title
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MDDescriptionTVC") as? MDDescriptionTVC else { return UITableViewCell() }
            cell.lableDesc.text = self.membershipData.descriptionValue
            cell.selectionStyle = .none
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MDBuyNow") as? MDBuyNow else { return UITableViewCell() }
            cell.buttonAddToCart.tag = indexPath.item
            cell.selectionStyle = .none
            cell.buttonAddToCart.addTarget(self, action: #selector(self.actionAddToCart(sender:)), for: .touchUpInside)
            return cell
            
        }
    }
    func processBuySubscription() {
        Utility.showLoader()
        let params: [String: Any] = ["membership_id": self.membershipData.id]
        APIManager.sharedInstance.homeAPIManager.buySubscriptionWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            Utility.showToast(message: "Success")
            self.navigationController?.popToRootViewController(animated: true)
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}
extension MembershipDetails: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.item {
        case 0:
            return  UITableViewAutomaticDimension//self.view.frame.size.height * 0.080
        case 1:
            return  UITableViewAutomaticDimension
        case 2:
            return  UITableViewAutomaticDimension
        default:
            return  10//self.view.frame.size.height * 0.080
        }
    }
}

