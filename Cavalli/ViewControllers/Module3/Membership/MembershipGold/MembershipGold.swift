//
//  MembershipGold.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MembershipGold: UIViewController {

    var parentNavigationController: UINavigationController?
    
    @IBOutlet weak var imageMembershipCard: UIImageView!
    @IBOutlet weak var lableTitle: UILabel!
    @IBOutlet weak var lableDesc: UILabel!
    var membership = MembershipModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.updateData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateData() {
        self.lableTitle.text = membership.title
        self.lableDesc.text = membership.descriptionValue
        self.imageMembershipCard.sd_setImage(with: URL(string: membership.imageUrl ?? ""))
    }
    
    @IBAction func actionBuynow(_ sender: UIButton) {
        self.processBuySubscription()
    }
    
    @IBAction func actionMemberShipCard(_ sender: UIButton) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "MembershipDetails") as? MembershipDetails {
            dvc.membershipData = self.membership
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
    func processBuySubscription() {
        Utility.showLoader()
        let params: [String: Any] = ["membership_id": membership.id]
        APIManager.sharedInstance.homeAPIManager.buySubscriptionWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            Utility.showToast(message: "Success", controller: self)
            self.navigationController?.popToRootViewController(animated: true)
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }

}
