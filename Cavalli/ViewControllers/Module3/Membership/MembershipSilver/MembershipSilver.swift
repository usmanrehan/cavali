//
//  MembershipSilver.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
protocol MembershipSilverDelegate : NSObjectProtocol {
    func willMoveToMG(controllerMS: MembershipGold)
    func willMoveToMP(controllerMS: MembershipPlatinum)
    func didMoveToMS()
}
class MembershipSilver: UIViewController {

    var parentNavigationController: UINavigationController?
    var arrayMembership = [MembershipModel]()
    weak var delegate: MembershipSilverDelegate? = nil
    
    @IBOutlet weak var imageMembershipCard: UIImageView!
    @IBOutlet weak var lableTitle: UILabel!
    @IBOutlet weak var lableDesc: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.processGetMembershipData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionMemberShipCard(_ sender: UIButton) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "MembershipDetails") as? MembershipDetails {
            dvc.membershipData = self.arrayMembership[0]
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
    @IBAction func actionBuynow(_ sender: UIButton) {
        self.processBuySubscription()
    }
    
    func updateData() {
        self.lableTitle.text = self.arrayMembership[0].title
        self.lableDesc.text = self.arrayMembership[0].descriptionValue
        self.imageMembershipCard.sd_setImage(with: URL(string: self.arrayMembership[0].imageUrl ?? ""))
    }
    
    //MARK: - Service
    func processGetMembershipData() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getMembershipDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MembershipModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayMembership = response
            self.updateData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    func processBuySubscription() {
        Utility.showLoader()
        let params: [String: Any] = ["membership_id": self.arrayMembership[0].id]
        APIManager.sharedInstance.homeAPIManager.buySubscriptionWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            Utility.showToast(message: "Success", controller: self)
            self.navigationController?.popToRootViewController(animated: true)
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }

}

extension MembershipSilver: MembershipSilverDelegate {
    func willMoveToMG(controllerMS: MembershipGold) {
        print("willMoveToMG")
        controllerMS.membership = self.arrayMembership[1]
    }
    func willMoveToMP(controllerMS: MembershipPlatinum) {
         print("willMoveToMP")
        controllerMS.membership = self.arrayMembership[2]
    }
    func didMoveToMS() {
         print("didMoveToMS")
        self.processGetMembershipData()
    }
}
