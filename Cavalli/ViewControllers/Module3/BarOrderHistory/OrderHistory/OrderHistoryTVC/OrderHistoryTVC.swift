//
//  OrderHistoryTVC.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class OrderHistoryTVC: UITableViewCell {

    //MARK:- IBOutlets
    @IBOutlet weak var lableOrderNumber: UILabel!
    @IBOutlet weak var lablePrice: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var lableTime: UILabel!
    @IBOutlet weak var lableStatus: UILabel!
    @IBOutlet weak var imageStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
