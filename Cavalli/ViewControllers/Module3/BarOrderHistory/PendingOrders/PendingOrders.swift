//
//  PendingOrders.swift
//  Cavalli
//
//  Created by shardha on 4/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
protocol PendingOrderDelegate: NSObjectProtocol {
    func willMoveToOH(controllerOH: OrderHistory)
    func didMoveToPO()
}

class PendingOrders: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var parentNavigationController: UINavigationController?
    var arrayPendingOrder = [PendingOrder]()
    var arrayOrderHistory = [OrrderHistory]()
    
    weak var delegate: PendingOrderDelegate? = nil
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        self.tableView.register(UINib(nibName: "PendingOrderTVC", bundle: nil), forCellReuseIdentifier: "PendingOrderTVC")
        self.processGetOrderHistoryData()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.changeStatusBarColor(clear: true)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
        
    }
    
    //MARK: - Service
    func processGetOrderHistoryData() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getOrderHistoryDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response_object = responseObject as NSDictionary
            let po = response_object.value(forKey: "pending_order") as! [[String : Any]]
            let oh = response_object.value(forKey: "order_history") as! [[String : Any]]
            let responsePO = Mapper<PendingOrder>().mapArray(JSONArray: po)
            let responseOH = Mapper<OrrderHistory>().mapArray(JSONArray: oh)
            self.arrayPendingOrder = responsePO
            self.arrayOrderHistory = responseOH
            if self.arrayPendingOrder.count == 0 {
                Utility.showToast(message: "No Data Found.")
            }
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    
}
extension PendingOrders: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayPendingOrder.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "PendingOrderTVC") as? PendingOrderTVC else { return UITableViewCell() }
        let obj = self.arrayPendingOrder[indexPath.item]
        cell.lableOrderNumber.text = obj.orderNo
        cell.lablePrice.text = "AED \(obj.totalAmount ?? "0.0")"
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let odate = dateformatter.date(from: obj.createdAt ?? "2000-01-01 01:01")
        let dFForDate = DateFormatter()
        dFForDate.dateFormat = "dd:MM:yy"
        cell.labelDate.text = dFForDate.string(from: odate!)
        let tFForDate = DateFormatter()
        tFForDate.dateFormat = "HH:mm"
        cell.lableTime.text = tFForDate.string(from: odate!)
        
        return cell
    }
}
extension PendingOrders: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.20
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryDetail") as? OrderHistoryDetail {
            dvc.parentNavigationController = self.parentNavigationController
            dvc.arrayOrderProducts = self.arrayPendingOrder[indexPath.item].orderProduct
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
}
extension PendingOrders: PendingOrderDelegate {
    func didMoveToPO() {
        self.processGetOrderHistoryData()
    }
    func willMoveToOH(controllerOH: OrderHistory) {
        controllerOH.arrayOrderHistory = self.arrayOrderHistory
    }
    
}

