//
//  OrderHistoryDetailTVC.swift
//  Cavalli
//
//  Created by shardha on 4/10/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class OrderHistoryDetailTVC: UITableViewCell {

    
    @IBOutlet weak var labelQty: UILabel!
    @IBOutlet weak var lablePrice: UILabel!
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
