//
//  MessagesDetail.swift
//  Cavalli
//
//  Created by shardha on 4/4/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
class MessagesDetail: UIViewController {

    @IBOutlet weak var textFieldMessage: UITextView!
    @IBOutlet weak var tableView: UITableView!
    var thread = ThreadModel()
    var chatArray = [ChatModel]()
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        
        self.textFieldMessage.text = "Write Something..."
        self.textFieldMessage.textColor = UIColor.lightGray
        self.textFieldMessage.delegate = self
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "MessageBubbleTVCSent", bundle: nil), forCellReuseIdentifier: "MessageBubbleTVCSent")
        self.tableView.register(UINib(nibName: "MessageBubbleTVCRecieved", bundle: nil), forCellReuseIdentifier: "MessageBubbleTVCRecieved")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        self.processGetChatData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSend(_ sender: UIButton) {
        if self.textFieldMessage.text?.isEmpty == false {
            self.processAddMessage()
        }
    }
    
    //MARK: - Service
    func processGetChatData() {
        Utility.showLoader()
        let params: [String: Any] = ["thread_id": self.thread.id]
        APIManager.sharedInstance.homeAPIManager.getChatDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<ChatModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.chatArray = response
            if self.chatArray.count == 0 {
                Utility.showToast(message: "No Data Found.")
            }
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath(row: self.chatArray.count - 1, section: 0), at: .bottom, animated: true)
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    func processAddMessage() {
        Utility.showLoader()
        let params: [String: Any] = ["request_id": self.chatArray[0].requestId!,
                                     "receiver_id": "1",
                                     "message": self.textFieldMessage.text!]
        APIManager.sharedInstance.homeAPIManager.addMessageWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.view.endEditing(true)
            self.textFieldMessage.text = "Write Something..."
            self.textFieldMessage.textColor = UIColor.lightGray
            self.processGetChatData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}
extension MessagesDetail: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = self.chatArray[indexPath.item]
        if obj.senderDetail?.id == 1 { //admin
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageBubbleTVCSent") as? MessageBubbleTVCSent else { return UITableViewCell() }
            cell.lableBody.text = obj.message
            cell.imageProfile.sd_setImage(with: URL(string: obj.senderDetail?.imageUrl ?? ""))
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let odate = dateFormatter.date(from: obj.createdAt ?? "2000-01-01 01:01:01")
            let dFForDate = DateFormatter()
            dFForDate.dateFormat = "dd:MM:yy HH:mm:ss"
            cell.lableTimeStamp.text = dFForDate.string(from: odate!)
            
            return cell
        }else { //user
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageBubbleTVCRecieved") as? MessageBubbleTVCRecieved else { return UITableViewCell() }
            cell.lableBody.text = obj.message
            cell.imageProfile.sd_setImage(with: URL(string: obj.senderDetail?.imageUrl ?? ""))
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let odate = dateFormatter.date(from: obj.createdAt ?? "2000-01-01 01:01:01")
            let dFForDate = DateFormatter()
            dFForDate.dateFormat = "dd:MM:yy HH:mm:ss"
            cell.lableTimeStamp.text = dFForDate.string(from: odate!)
            return cell
        }
        
    }
}
extension MessagesDetail: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
extension MessagesDetail: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write Something..."
            textView.textColor = UIColor.lightGray
        }
    }
}
