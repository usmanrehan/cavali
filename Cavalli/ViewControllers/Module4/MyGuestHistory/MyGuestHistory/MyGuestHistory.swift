//
//  MyGuestHistory.swift
//  Cavalli
//
//  Created by shardha on 4/21/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MyGuestHistory: UIViewController {
    let colorPending = UIColor(displayP3Red: 255/255.0, green: 152/255.0, blue: 44/255.0, alpha: 1.0)
    let colorReserved = UIColor(displayP3Red: 48/255.0, green: 137/255.0, blue: 41/255.0, alpha: 1.0)
    let colorRejected = UIColor(displayP3Red: 193/255.0, green: 28/255.0, blue: 57/255.0, alpha: 1.0)
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoDataAvailble: UILabel!

    var delegate1: MyReservationProtocol? = nil

    var parentNavigationController: UINavigationController?
    var arrayGuestHis = [Upcoming]()
    var arrayFilteredGuestHis = [Upcoming]()
    var filterData = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "MyGuestGeneralTVC", bundle: nil), forCellReuseIdentifier: "MyGuestGeneralTVC")
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.updataData(_:)), name: NSNotification.Name(rawValue: "updateHistory"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.filterData(_:)), name: NSNotification.Name(rawValue: "updateMyGuest"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate1?.shouldShowFilter()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK:- @objc
    @objc func filterData(_ notification: NSNotification) {
        guard let reserved = notification.userInfo?["reserved"] as? Bool else {return}
        guard let pending = notification.userInfo?["pending"] as? Bool else {return}
        guard let rejected = notification.userInfo?["rejected"] as? Bool else {return}
        guard let cancled = notification.userInfo?["cancled"] as? Bool else {return}
        
        if !reserved && !pending && !rejected && !cancled {
            self.filterData = false
            self.tableView.reloadData()
            return
        }
        var arr = [Upcoming]()
        for item in self.arrayGuestHis {
            if item.guestStatusId == 2 && reserved == true {
                arr.append(item)
            }
            if item.guestStatusId == 1 && pending == true {
                arr.append(item)
            }
            if item.guestStatusId == 3 && rejected == true {
                arr.append(item)
            }
            if item.guestStatusId == 4 && cancled == true {
                arr.append(item)
            }
        }
        self.arrayFilteredGuestHis = arr
        if self.arrayFilteredGuestHis.count == 0 {
            Utility.showToast(message: "No data found!", controller: self)
            self.filterData = true
            self.tableView.reloadData()
//            self.lblNoDataAvailble.isHidden = false
        } else {
//            self.lblNoDataAvailble.isHidden = true

            self.filterData = true
            self.tableView.reloadData()
        }
    }
    // handle notification
    @objc func updataData(_ notification: NSNotification) {
        
        if let data = notification.userInfo?["data"] as? [Upcoming] {
            if data.count == 0 {
                lblNoDataAvailble.isHidden = false
            } else {
                self.arrayGuestHis = data
                self.tableView.reloadData()
            }
        }
    }
}
extension MyGuestHistory: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if self.filterData {
            return self.arrayFilteredGuestHis.count
        }else {
            return self.arrayGuestHis.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyGuestGeneralTVC") as? MyGuestGeneralTVC else { return UITableViewCell() }
        var obj = Upcoming()
        if self.filterData {
            obj = self.arrayFilteredGuestHis[indexPath.item]
        }else {
            obj = self.arrayGuestHis[indexPath.item]
        }
        
        cell.labelTitle.text = obj.guestListCategory?.title ?? ""//obj.title ?? ""
        cell.labelDate.text = Utility.customDateFormatter(dateStr: obj.createdAt ?? "2018-12-12 12:24:00", dateFormat: "yyyy-MM-dd HH:mm:ss", formatteddate: "EEEE, MMM d YYYY")
        cell.labelNumberOfPeople.text = "\(obj.guestListMember.count)"
        switch obj.guestStatusId {
        case 1:
            cell.imageStatus.image = #imageLiteral(resourceName: "pending")
            cell.lableStatus.textColor = self.colorPending
            cell.lableStatus.text = "Pending"
            cell.buttonViewDetails.isHidden = false
            break
        case 2:
            cell.imageStatus.image = #imageLiteral(resourceName: "reserved")
            cell.lableStatus.textColor = self.colorReserved
            cell.lableStatus.text = "Reserved"
            cell.buttonViewDetails.isHidden = false
            break
        case 3:
            cell.imageStatus.image = #imageLiteral(resourceName: "rejected")
            cell.lableStatus.textColor = self.colorRejected
            cell.lableStatus.text = "Rejected"
            cell.buttonViewDetails.isHidden = true
            break
        case 4:
            cell.imageStatus.image = #imageLiteral(resourceName: "rejected")
            cell.lableStatus.textColor = self.colorRejected
            cell.lableStatus.text = "Cancled"
            cell.buttonViewDetails.isHidden = true
            break
        default:
            cell.imageStatus.isHidden = true
            cell.lableStatus.isHidden = true
            cell.buttonViewDetails.isHidden = true
            break
        }
        cell.buttonViewDetails.tag = indexPath.item
        cell.buttonViewDetails.addTarget(self, action: #selector(self.actionViewDetails(sender:)), for: .touchUpInside)
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func actionViewDetails(sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = storyboard.instantiateViewController(withIdentifier: "GuestListDetail") as? GuestListDetail {
            if self.filterData {
                dvc.id = self.arrayFilteredGuestHis[sender.tag].id
                dvc.strListTitle = self.arrayFilteredGuestHis[sender.tag].guestListCategory?.title ?? ""
            }else {
                dvc.id = (self.arrayGuestHis[sender.tag].guestListCategory?.id)!
                dvc.strListTitle = self.arrayGuestHis[sender.tag].guestListCategory?.title!
            }
            dvc.isFromHistory = true
            
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
}

