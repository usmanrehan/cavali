//
//  MyGuestContainer.swift
//  Cavalli
//
//  Created by shardha on 4/21/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
//import CAPSPageMenu

class MyGuestContainer: UIViewController {
    
    
    //MARK:- Global declarations
    var pageMenu : CAPSPageMenu?
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonFilter: UIButton!
    
    weak var poDelegate: MyGuestUpcomingDelegate? = nil
    
    var controllerOH = MyGuestHistory()
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.buttonFilter.isHidden = true
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let controllerPendingOrders = self.storyboard?.instantiateViewController(withIdentifier: "MyGuestUpcoming") as! MyGuestUpcoming
        let controllerOrderHistory = self.storyboard?.instantiateViewController(withIdentifier: "MyGuestHistory") as! MyGuestHistory
        
        controllerPendingOrders.delegate1 = self
        
        controllerPendingOrders.delegate = controllerPendingOrders
        self.poDelegate = controllerPendingOrders.delegate
        self.controllerOH = controllerOrderHistory
        
        controllerPendingOrders.parentNavigationController = self.navigationController
        controllerOrderHistory.parentNavigationController = self.navigationController
        
        controllerPendingOrders.title = "Upcoming"
        
        controllerOrderHistory.title = "History"
        
        controllerArray.append(controllerPendingOrders)
        controllerArray.append(controllerOrderHistory)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        
        //let bgColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(20),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.black),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(UIColor.white),
            .BottomMenuHairlineColor(UIColor.white),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(20),
            .MenuHeight(40)
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.height - 80), pageMenuOptions: parameters)
        
        pageMenu?.view.backgroundColor = UIColor.white
        pageMenu?.delegate = self
        self.view.addSubview(pageMenu!.view)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.poDelegate?.didMoveToU()
        if pageMenu?.currentPageIndex == 1 {
            buttonFilter.isHidden = false
        } else {
            buttonFilter.isHidden = true
        }
    }
    @IBAction func actionFilter(_ sender: UIButton) {
        toggleSideMenuView()
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
}
extension MyGuestContainer: CAPSPageMenuDelegate {
    func willMoveToPage(controller: UIViewController, index: Int) {
        hideSideMenuView()
        if index == 1 {
            self.buttonFilter.isHidden = false
            self.poDelegate?.willMoveToH(controllerOH: self.controllerOH)
        }
        else {
            self.buttonFilter.isHidden = true
        }
    }
    func didMoveToPage(controller: UIViewController, index: Int) {
        if index == 0 {
            self.buttonFilter.isHidden = true
            self.poDelegate?.didMoveToU()
        }
        else {
            self.buttonFilter.isHidden = false
        }
    }
}
extension MyGuestContainer: MyReservationProtocol {
    func shouldHideFilter() {
        self.buttonFilter.isHidden = true
    }
    func shouldShowFilter() {
        self.buttonFilter.isHidden = false
    }
}

//extension MyGuestContainer: CAPSPageMenuDelegate {
//    func willMoveToPage(controller: UIViewController, index: Int) {
//        print("\(index) willMoveToPage")
//
//        if index == 0 {
//            self.buttonFilter.isHidden = true
//        }
//    }
//    func didMoveToPage(controller: UIViewController, index: Int) {
//        print("\(index) didMoveToPage")
//        if index == 0 {
//            self.buttonFilter.isHidden = true
//        }
//    }
//}

