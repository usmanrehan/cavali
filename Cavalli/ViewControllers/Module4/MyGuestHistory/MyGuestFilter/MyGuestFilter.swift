//
//  MyGuestFilter.swift
//  Cavalli
//
//  Created by shardha on 4/21/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MyGuestFilter: UIViewController {
    
    //MARK:- Global declarations
    var filterDic: [String: Bool] = ["pending": false,
                                     "reserved": false,
                                     "rejected": false,
                                     "cancled" : false]
    
    //MARK:- IBOutlets
    @IBOutlet weak var imageReserved: UIImageView!
    @IBOutlet weak var imagePending: UIImageView!
    @IBOutlet weak var imageRejected: UIImageView!
    @IBOutlet weak var imageCanceled: UIImageView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        toggleSideMenuView()
    }
    @IBAction func actionReserved(_ sender: UIButton) {
        if self.imageReserved.image == #imageLiteral(resourceName: "Filterradiobtn") {
            self.filterDic["reserved"] = false
            self.imageReserved.image = #imageLiteral(resourceName: "Filterradiobtnblank")
        }else {
            self.filterDic["reserved"] = true
            self.imageReserved.image = #imageLiteral(resourceName: "Filterradiobtn")
        }
    }
    @IBAction func actionPending(_ sender: UIButton) {
        if self.imagePending.image == #imageLiteral(resourceName: "Filterradiobtn") {
            self.filterDic["pending"] = false
            self.imagePending.image = #imageLiteral(resourceName: "Filterradiobtnblank")
        }else {
            self.filterDic["pending"] = true
            self.imagePending.image = #imageLiteral(resourceName: "Filterradiobtn")
        }
    }
    @IBAction func actionRejected(_ sender: UIButton) {
        if self.imageRejected.image == #imageLiteral(resourceName: "Filterradiobtn") {
            self.filterDic["rejected"] = false
            self.imageRejected.image = #imageLiteral(resourceName: "Filterradiobtnblank")
        }else {
            self.filterDic["rejected"] = true
            self.imageRejected.image = #imageLiteral(resourceName: "Filterradiobtn")
        }
    }
    @IBAction func actionCanceled(_ sender: UIButton) {
        if self.imageCanceled.image == #imageLiteral(resourceName: "Filterradiobtn") {
            self.filterDic["cancled"] = false
            self.imageCanceled.image = #imageLiteral(resourceName: "Filterradiobtnblank")
        }else {
            self.filterDic["cancled"] = true
            self.imageCanceled.image = #imageLiteral(resourceName: "Filterradiobtn")
        }
    }
    @IBAction func actionClear(_ sender: UIButton) {
        self.imageReserved.image = #imageLiteral(resourceName: "Filterradiobtnblank")
        //self.imagePending.image = #imageLiteral(resourceName: "Filterradiobtnblank")
        self.imageRejected.image = #imageLiteral(resourceName: "Filterradiobtnblank")
        //self.imageCanceled.image = #imageLiteral(resourceName: "Filterradiobtnblank")
        self.filterDic = ["pending": false,
                          "reserved": false,
                          "rejected": false,
                          "cancled" : false]
    }
    @IBAction func actionDone(_ sender: UIButton) {
        let filter = self.filterDic
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateMyGuest"), object: nil, userInfo: filter)
        toggleSideMenuView()
    }
}

