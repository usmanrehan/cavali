//
//  MyGuestUpcoming.swift
//  Cavalli
//
//  Created by shardha on 4/21/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
protocol MyGuestUpcomingDelegate: NSObjectProtocol {
    func willMoveToH(controllerOH: MyGuestHistory)
    func didMoveToU()
}
class MyGuestUpcoming: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoDataAvailble: UILabel!
    
    let colorPending = UIColor(displayP3Red: 255/255.0, green: 152/255.0, blue: 44/255.0, alpha: 1.0)
    let colorReserved = UIColor(displayP3Red: 48/255.0, green: 137/255.0, blue: 41/255.0, alpha: 1.0)
    let colorRejected = UIColor(displayP3Red: 193/255.0, green: 28/255.0, blue: 57/255.0, alpha: 1.0)
    var parentNavigationController: UINavigationController?
    weak var delegate: MyGuestUpcomingDelegate? = nil
    var delegate1: MyReservationProtocol? = nil
    var arrayGuestUp = [Upcoming]()
    var arrayGuestHis = [Upcoming]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "MyGuestGeneralTVC", bundle: nil), forCellReuseIdentifier: "MyGuestGeneralTVC")
        self.processMyGuestHistoryData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.delegate1?.shouldHideFilter()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Service
    func processMyGuestHistoryData() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getGuestHistoryDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response_object = responseObject as NSDictionary
            if response_object.count == 0{
                self.lblNoDataAvailble.isHidden = false
            } else {
                let po = response_object.value(forKey: "upcoming") as! [[String : Any]]
                let oh = response_object.value(forKey: "history") as! [[String : Any]]
                let responsePO = Mapper<Upcoming>().mapArray(JSONArray: po)
                let responseOH = Mapper<Upcoming>().mapArray(JSONArray: oh)
                self.arrayGuestUp = responsePO
                self.arrayGuestHis = responseOH
                if self.arrayGuestUp.count == 0 {
                    Utility.showToast(message: "No Data Found.", controller: self)
                }
                let dataDict:[String: [Upcoming]] = ["data": responseOH]
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateHistory"), object: nil, userInfo: dataDict)
                self.tableView.reloadData()
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}

extension MyGuestUpcoming: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayGuestUp.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MyGuestGeneralTVC") as? MyGuestGeneralTVC else { return UITableViewCell() }
        let obj = self.arrayGuestUp[indexPath.item]
        cell.labelTitle.text = obj.guestListCategory?.title ?? ""
        cell.labelDate.text = Utility.customDateFormatter(dateStr: obj.createdAt ?? "2018-12-12 12:24:00", dateFormat: "yyyy-MM-dd HH:mm:ss", formatteddate: "EEEE, MMM d YYYY")
        cell.labelNumberOfPeople.text = "\(obj.guestListMember.count)"
        switch obj.guestStatusId {
        case 1:
            cell.imageStatus.image = #imageLiteral(resourceName: "pending")
            cell.lableStatus.textColor = self.colorPending
            cell.lableStatus.text = "Pending"
            cell.buttonViewDetails.isHidden = false
            break
        case 2:
            cell.imageStatus.image = #imageLiteral(resourceName: "reserved")
            cell.lableStatus.textColor = self.colorReserved
            cell.lableStatus.text = "Reserved"
            cell.buttonViewDetails.isHidden = false
            break
        case 3:
            cell.imageStatus.image = #imageLiteral(resourceName: "rejected")
            cell.lableStatus.textColor = self.colorRejected
            cell.lableStatus.text = "Rejected"
            cell.buttonViewDetails.isHidden = true
            break
        case 4:
            cell.imageStatus.image = #imageLiteral(resourceName: "rejected")
            cell.lableStatus.textColor = self.colorRejected
            cell.lableStatus.text = "Cancled"
            cell.buttonViewDetails.isHidden = true
            break
        default:
            cell.imageStatus.isHidden = true
            cell.lableStatus.isHidden = true
            cell.buttonViewDetails.isHidden = true
            break
        }
        cell.buttonViewDetails.tag = indexPath.item
        cell.buttonViewDetails.addTarget(self, action: #selector(self.actionViewDetails(sender:)), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func actionViewDetails(sender: UIButton) {
        let storyboard = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = storyboard.instantiateViewController(withIdentifier: "GuestListDetail") as? GuestListDetail {
            dvc.id = (self.arrayGuestUp[sender.tag].guestListCategory?.id)!
            dvc.strListTitle = self.arrayGuestUp[sender.tag].guestListCategory?.title!
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
}

extension MyGuestUpcoming: MyGuestUpcomingDelegate {
    func didMoveToU() {
        self.processMyGuestHistoryData()
        self.delegate1?.shouldHideFilter()
    }
    func willMoveToH(controllerOH: MyGuestHistory) {
        controllerOH.arrayGuestHis = self.arrayGuestHis
        self.delegate1?.shouldShowFilter()
    }
}

