//
//  MyGuestGeneralTVC.swift
//  Cavalli
//
//  Created by shardha on 4/21/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MyGuestGeneralTVC: UITableViewCell {
    
    //MARK:- IBOutlets
    
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelNumberOfPeople: UILabel!
    @IBOutlet weak var lableStatus: UILabel!
    @IBOutlet weak var buttonViewDetails: UIButton!
    @IBOutlet weak var imageStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
