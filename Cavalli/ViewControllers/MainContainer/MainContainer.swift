//
//  MainContainer.swift
//  Cavalli
//
//  Created by shardha on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MainContainer: UIViewController {
    
    //MARK:- Global declaration
    var homeViewController: UIViewController!
    var eventBookingViewController: UIViewController!
    var settingsViewController: UIViewController!
    var notificationsViewController: UIViewController!
    var viewControllers: [UIViewController]!
    var selectedIndex: Int = 0
    
    //MARK:- IBOutlets
    @IBOutlet weak var box1: UIImageView!
    @IBOutlet weak var box2: UIImageView!
    @IBOutlet weak var box3: UIImageView!
    @IBOutlet weak var box4: UIImageView!
    
    @IBOutlet weak var imageHome: UIImageView!
    @IBOutlet weak var imageCalender: UIImageView!
    @IBOutlet weak var imageSettings: UIImageView!
    @IBOutlet weak var imageNotification: UIImageView!
    
    @IBOutlet weak var labelHome: UILabel!
    @IBOutlet weak var labelEventBooking: UILabel!
    @IBOutlet weak var labelSettings: UILabel!
    @IBOutlet weak var labelNotifications: UILabel!
    
    @IBOutlet weak var containerView: UIView!
    @IBOutlet var buttons: [UIButton]!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        let storyboard = UIStoryboard(name: "HomeModule", bundle: nil)
        self.homeViewController = storyboard.instantiateViewController(withIdentifier: "Home") as! Home
        self.eventBookingViewController = storyboard.instantiateViewController(withIdentifier: "EventBooking") as! EventBooking
        self.settingsViewController = storyboard.instantiateViewController(withIdentifier: "Settings") as! Settings
        self.notificationsViewController = storyboard.instantiateViewController(withIdentifier: "NotificationVC") as! NotificationVC
        self.viewControllers = [self.homeViewController, self.eventBookingViewController, self.settingsViewController, self.notificationsViewController]
        didPressTab(buttons[selectedIndex])
    }

    //MARK:- IBActions
    @IBAction func didPressTab(_ sender: UIButton) {
        self.changeSelectedTab(sender.tag)
    }

    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    //MARK:- Helper Methods
    private func changeSelectedTab(_ index: Int){
        let previousIndex = selectedIndex
        self.selectedIndex = index
        let previousVC = viewControllers[previousIndex]
        previousVC.willMove(toParentViewController: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParentViewController()
        let vc = viewControllers[selectedIndex]
        addChildViewController(vc)
        UIApplication.shared.isStatusBarHidden = false
        vc.setNeedsStatusBarAppearanceUpdate()
        vc.view.frame = containerView.bounds//CGRect(x: 0, y: -20, width: containerView.bounds.width, height: containerView.bounds.height)//
        containerView.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
        self.changeUI(index: selectedIndex, oldIndex: previousIndex)
    }
    private func changeUI(index: Int, oldIndex: Int) {
        if index != oldIndex {
            switch index {
            case 0:
                self.box1.image = #imageLiteral(resourceName: "box")
                self.box2.image = nil
                self.box3.image = nil
                self.box4.image = nil
                
                self.imageHome.image = #imageLiteral(resourceName: "home")
                self.imageCalender.image = #imageLiteral(resourceName: "BBcalender")
                self.imageSettings.image = #imageLiteral(resourceName: "BBsettings")
                self.imageNotification.image = #imageLiteral(resourceName: "BBnotification")
                break
            case 1:
                self.box2.image = #imageLiteral(resourceName: "box")
                self.box3.image = nil
                self.box1.image = nil
                self.box4.image = nil
                
                 self.imageHome.image = #imageLiteral(resourceName: "BBhome2")
                self.imageCalender.image = #imageLiteral(resourceName: "BBcalender2")
                self.imageSettings.image = #imageLiteral(resourceName: "BBsettings")
                self.imageNotification.image = #imageLiteral(resourceName: "BBnotification")
                break
            case 2:
                self.box3.image = #imageLiteral(resourceName: "box")
                self.box2.image = nil
                self.box1.image = nil
                self.box4.image = nil
                
                self.imageHome.image = #imageLiteral(resourceName: "BBhome2")
                self.imageCalender.image = #imageLiteral(resourceName: "BBcalender")
                self.imageSettings.image = #imageLiteral(resourceName: "BBsettings2")
                self.imageNotification.image = #imageLiteral(resourceName: "BBnotification")
                break
            case 3:
                self.box4.image = #imageLiteral(resourceName: "box")
                self.box2.image = nil
                self.box3.image = nil
                self.box1.image = nil
                
                self.imageHome.image = #imageLiteral(resourceName: "BBhome2")
                self.imageCalender.image = #imageLiteral(resourceName: "BBcalender")
                self.imageSettings.image = #imageLiteral(resourceName: "BBsettings")
                self.imageNotification.image = #imageLiteral(resourceName: "BBnotification2")
                break
            default:
                break
            }
        }
    }
}
extension MainContainer: ITAirSideMenuDelegate {
    func didTappedTermsAndCondittions() {
        
        let story = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = story.instantiateViewController(withIdentifier: "TCandPP") as? TCandPP {
            dvc.isPrivacy = false
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    func didTapPrivacyPolicy() {
        let story = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = story.instantiateViewController(withIdentifier: "TCandPP") as? TCandPP {
            dvc.isPrivacy = true
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    func didTappedAboutUs() {
        let story = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = story.instantiateViewController(withIdentifier: "TCandPP") as? TCandPP {
            dvc.isAbout = true
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    func didTappedGallery() {
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "Gallery") as? Gallery {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    func didTappedCavalliSocial() {
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "CavalliSocial") as? CavalliSocial {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    func didTappedInternationRequest() {
        print("didTappedInternationRequest")
        let storyBoard = UIStoryboard(name: "BarOrdering", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "InternationalRequest") as? InternationalRequest {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    func didTappedEventCalendar(){
        print("didTappedEventCalendar")
        self.changeSelectedTab(1)
    }
//    func didTapEditProfile() {
//        let storyBoard = UIStoryboard(name: "BarOrdering", bundle: nil)
//        if let dvc = storyBoard.instantiateViewController(withIdentifier: "EditProfile") as? EditProfile {
//            self.navigationController?.pushViewController(dvc, animated: true)
//        }
//    }
//    func didTappedAboutUs() {
//        let story = UIStoryboard(name: "HomeModule", bundle: nil)
//        if let dvc = story.instantiateViewController(withIdentifier: "TCandPP") as? TCandPP {
//            dvc.isAbout = true
//            self.navigationController?.pushViewController(dvc, animated: true)
//        }
//    }
}
