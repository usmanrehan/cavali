//
//  BooksRealmHelper.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/12/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift

class CartRealmHelper: NSObject {
    
    static let sharedInstance = CartRealmHelper()
    
    var realm: Realm!
    
    //MARK:- ADD
    func addThisToCart(with product: Products) {
        for productItem in self.getAllInCart() {
            if productItem.product.id == product.id {
                return
            }
        }
        
        let cart = Cart()
        cart.userId = AppStateManager.sharedInstance.loggedInUser.id
        cart.product = product
        
        try! Global.APP_REALM?.write(){
            Global.APP_REALM?.add([cart])
        }
    }
    
    //MARK: - GET ALL
    func getAllInCart() -> Results<Cart> {
        return (Global.APP_REALM?.objects(Cart.self))!
    }
    
    //MARK: - DELETE
    func deleteThis(cart: Cart) {
        try! Global.APP_REALM?.write({ () -> Void in
            Global.APP_REALM?.delete(cart)
        })
    }
    
    //MARK: - EMPTY CART
    func emptyCartList() {
        try! Global.APP_REALM?.write(){
            Global.APP_REALM?.delete( (Global.APP_REALM?.objects(Cart.self))!)
        }
    }
    
    //MARK: - Increment and Decrement
    func incrementThis(cart:  Cart) {
        for productItem in self.getAllInCart() {
            if productItem.product.id == cart.product.id {
//                self.deleteThis(cart: cart)
                try! Global.APP_REALM?.write(){
                    productItem.count += 1
                }
            }
        }
    }
    func decrementThis(cart:  Cart) {
        for productItem in self.getAllInCart() {
            if productItem.product.id == cart.product.id {
                //                self.deleteThis(cart: cart)
                try! Global.APP_REALM?.write(){
                    productItem.count -= 1
                }
            }
        }
    }
}
