//
//  TipTVC.swift
//  Cavalli
//
//  Created by shardha on 3/27/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

protocol Tip {
    func tipConstant(x: Int)
}
class TipTVC: UITableViewCell {

    @IBOutlet weak var button15: UIButton!
    @IBOutlet weak var button10: UIButton!
    @IBOutlet weak var button5: UIButton!
    @IBOutlet weak var button0: UIButton!
    
    var delegate: Tip? = nil
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.button15.isSelected = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func action15(_ sender: UIButton) {
        sender.isSelected = true
        self.button10.isSelected = false
        self.button5.isSelected = false
        self.button0.isSelected = false
        delegate?.tipConstant(x: 15)
    }
    @IBAction func action10(_ sender: UIButton) {
        sender.isSelected = true
        self.button15.isSelected = false
        self.button5.isSelected = false
        self.button0.isSelected = false
        delegate?.tipConstant(x: 10)
    }
    @IBAction func action5(_ sender: UIButton) {
        sender.isSelected = true
        self.button10.isSelected = false
        self.button15.isSelected = false
        self.button0.isSelected = false
        delegate?.tipConstant(x: 5)
    }
    @IBAction func action0(_ sender: UIButton) {
        sender.isSelected = true
        self.button10.isSelected = false
        self.button5.isSelected = false
        self.button15.isSelected = false
        delegate?.tipConstant(x: 0)
    }
    
}
