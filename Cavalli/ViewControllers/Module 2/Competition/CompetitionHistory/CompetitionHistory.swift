//
//  CompetitionHistory.swift
//  Cavalli
//
//  Created by shardha on 3/28/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class CompetitionHistory: UIViewController{
    
    //MARK:- Global declaration
    var parentNavigationController: UINavigationController?
    var arrayCompetitionModel = [CompetitionHistoryModel]()
    let imagePending = #imageLiteral(resourceName: "pending")
    let imageWin = #imageLiteral(resourceName: "reserved")
    let imageLoss = #imageLiteral(resourceName: "rejected")
    let colorPending = UIColor(displayP3Red: 193/255.0, green: 114/255.0, blue: 39/255.0, alpha: 1.0)
    let colorWin = UIColor(displayP3Red: 24/255.0, green: 132/255.0, blue: 27/255.0, alpha: 1.0)
    let colorLoss = UIColor(displayP3Red: 233/255.0, green: 16/255.0, blue: 57/255.0, alpha: 1.0)
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!

    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 100
        self.tableView.register(UINib(nibName: "CompetitionHistoryTVC", bundle: nil), forCellReuseIdentifier: "CompetitionHistoryTVC")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.processGetCompetitionHistoryData()
    }

    //MARK: - Service
    func processGetCompetitionHistoryData() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        APIManager.sharedInstance.homeAPIManager.getCompetitionHistoryWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<CompetitionHistoryModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayCompetitionModel = response
            if self.arrayCompetitionModel.count == 0 {
                Utility.showToast(message: "No Data Found.")
            }
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
        
    }
}
extension CompetitionHistory: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayCompetitionModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CompetitionHistoryTVC") as? CompetitionHistoryTVC else { return UITableViewCell() }
        cell.labelTitle.text = self.arrayCompetitionModel[indexPath.item].title
        cell.labelDesc.text = self.arrayCompetitionModel[indexPath.item].descriptionValue
        cell.imageProduct.sd_setImage(with: URL(string: self.arrayCompetitionModel[indexPath.item].imageUrl!))
        switch (self.arrayCompetitionModel[indexPath.item].status ?? "") {
        case "Pending":
            cell.lableStatus.textColor = self.colorPending
            cell.lableStatus.text = self.arrayCompetitionModel[indexPath.item].status
            cell.imageStatus.image = self.imagePending
            break
        case "Loss":
            cell.lableStatus.textColor = self.colorLoss
            cell.lableStatus.text = self.arrayCompetitionModel[indexPath.item].status
            cell.imageStatus.image = self.imageLoss
            break
        case "Won":
            cell.lableStatus.textColor = self.colorWin
            cell.lableStatus.text = self.arrayCompetitionModel[indexPath.item].status
            cell.imageStatus.image = self.imageWin
            break
        default:
            break
        }
        return cell
    }
}
extension CompetitionHistory: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
}
