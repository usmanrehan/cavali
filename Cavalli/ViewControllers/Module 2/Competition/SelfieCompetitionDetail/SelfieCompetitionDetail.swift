//
//  SelfieCompetitionDetail.swift
//  Cavalli
//
//  Created by shardha on 3/28/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class SelfieCompetitionDetail: UIViewController{
    
    //MARK:- Global declaration
    var competitionModel = CompetitionModel()
    var pickedImage = #imageLiteral(resourceName: "image-placeholder")
    let imagePicker = UIImagePickerController()
    var product = Products()
    var textName = ""
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonPrice: UIButton!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.imagePicker.delegate = self
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.register(UINib(nibName: "DDTitleTVC", bundle: nil), forCellReuseIdentifier: "DDTitleTVC")
        self.tableView.register(UINib(nibName: "DDDescriptionTVC", bundle: nil), forCellReuseIdentifier: "DDDescriptionTVC")
        self.tableView.register(UINib(nibName: "SelfieCompetitionNameTVC", bundle: nil), forCellReuseIdentifier: "SelfieCompetitionNameTVC")
        self.tableView.register(UINib(nibName: "SelfieCompetitionPictureTVC", bundle: nil), forCellReuseIdentifier: "SelfieCompetitionPictureTVC")
        self.tableView.register(UINib(nibName: "SelfieCompetitionAddToCart", bundle: nil), forCellReuseIdentifier: "SelfieCompetitionAddToCart")
//
        //self.imageBanner.image = #imageLiteral(resourceName: "cigar")
        //if self.product.productImages.count > 0 {
            if let stringName = self.competitionModel.imageUrl {
                self.imageBanner.sd_setImage(with: URL(string: stringName))
            }
        //}
        self.textName = AppStateManager.sharedInstance.loggedInUser.userName ?? ""
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Helper Methods
    @objc func actionSubmit(sender: UIButton) {
        self.processSubmitCompetition()
    }
    @objc func actionCamera(sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    func openCamera() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        imagePicker.allowsEditing = false
        imagePicker.cameraCaptureMode = .photo
        self.present(imagePicker, animated: true, completion: nil)
    }
    func openGallary() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    @objc func textFieldDidChange(_ textField: UITextField) {
        self.textName = textField.text!
    }
    func processSubmitCompetition() {
        if self.pickedImage != #imageLiteral(resourceName: "image-placeholder") {
            if self.textName != "" {
                Utility.showLoader()
                let image = UIImageJPEGRepresentation(self.pickedImage, 0.5)
                var params: [String: Any] = ["":""]
                    params = ["competition_id": self.competitionModel.id,
                              "images": image!]
                print(params)
                APIManager.sharedInstance.homeAPIManager.submitCompetitionWith(params: params, success: {
                    (responseObject) in
                    Utility.hideLoader()
                    Utility.showToast(message: "You have successfully participated in competition.")
                    self.navigationController?.popViewController(animated: true)
                }, failure: {
                    (error) in
                    Utility.hideLoader()
                    Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
                })
            }else {
                Utility.showToast(message: "Invalid Name.", controller: self)
            }
        }else {
            Utility.showToast(message: "Please select an image.", controller: self)
        }
        
    }
}
extension SelfieCompetitionDetail: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDTitleTVC") as? DDTitleTVC else { return UITableViewCell() }
            cell.lableTitle.text = self.competitionModel.title
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDDescriptionTVC") as? DDDescriptionTVC else { return UITableViewCell() }
            cell.selectionStyle = .none
            cell.lableDesc.text = self.competitionModel.descriptionValue
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SelfieCompetitionNameTVC") as? SelfieCompetitionNameTVC else { return UITableViewCell() }
            cell.textFieldName.text = self.textName
            cell.selectionStyle = .none
            cell.textFieldName.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SelfieCompetitionPictureTVC") as? SelfieCompetitionPictureTVC else { return UITableViewCell() }
            cell.imageSelfie.image = self.pickedImage
            cell.selectionStyle = .none
            cell.buttonCamera.tag = indexPath.item
            cell.buttonCamera.addTarget(self, action: #selector(self.actionCamera(sender:)), for: .touchUpInside)
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SelfieCompetitionAddToCart") as? SelfieCompetitionAddToCart else { return UITableViewCell() }
            cell.buttonAddToCart.tag = indexPath.item
            cell.selectionStyle = .none
            cell.buttonAddToCart.addTarget(self, action: #selector(self.actionSubmit(sender:)), for: .touchUpInside)
            return cell
        }
    }
}
extension SelfieCompetitionDetail: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.item {
        case 0:
            return  self.view.frame.size.height * 0.080
        case 1:
            return  UITableViewAutomaticDimension
        case 2:
            return  self.view.frame.size.height * 0.080
        case 3:
            return  self.view.frame.size.height * 0.080
        default:
            return  self.view.frame.size.height * 0.15
        }
    }
}
extension SelfieCompetitionDetail: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.pickedImage = pickedImage
            self.tableView.reloadRows(at: [IndexPath(row: 3, section: 0)], with: .bottom)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
