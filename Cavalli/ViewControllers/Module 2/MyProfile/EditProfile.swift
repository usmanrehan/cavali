//
//  EditProfile.swift
//  Cavalli
//
//  Created by shardha on 3/29/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import ObjectMapper
class EditProfile: UIViewController {

    //MARK:- Global declarations
    var imageChanged = false
    var pickedImage = #imageLiteral(resourceName: "image-placeholder")
    let imagePicker = UIImagePickerController()
    
    //MARK:- IBOUTlets
    @IBOutlet weak var textFieldFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var imageProfile: UIImageView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let user = AppStateManager.sharedInstance.loggedInUser
        self.textFieldFullName.text = user?.userName
        self.textFieldEmailAddress.text = user?.email
        self.textFieldPhone.text = user?.phone
        
        self.imageProfile.sd_setImage(with: URL(string: (user?.imageUrl ?? "")!))
        self.imagePicker.delegate = self
    }
    
    //MARK:- IBActions
    @IBAction func actionImagePicker(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSaveChanges(_ sender: UIButton) {
        if self.validate() {
            self.processSumbitProfileChanges()
        }
    }
    
    //MARK:- Helper Methods
    private func validate() -> Bool {
        if Validation.validateStringLength(self.textFieldFullName.text!) {
            if imageChanged {
                return true
            }else {
                //Utility.showToast(message: "Please update image.")
                return true
            }
        }else {
            Utility.showToast(message: "Invalid fullname.")
            return false
        }
    }
    func openCamera() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        imagePicker.allowsEditing = false
        imagePicker.cameraCaptureMode = .photo
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- Services
    func processSumbitProfileChanges() {
        if self.pickedImage != #imageLiteral(resourceName: "image-placeholder") {
            Utility.showLoader()
            let image = UIImageJPEGRepresentation(self.pickedImage, 0.5)
            var params: [String: Any] = ["":""]
            params = ["user_name": self.textFieldFullName.text!,
                      "phone": self.textFieldPhone.text!,
                      "email": self.textFieldEmailAddress.text!,
                      "profile_image": image!]
            APIManager.sharedInstance.homeAPIManager.updateProfileWith(params: params, success: {
                (responseObject) in
                let response = Mapper<User>().map(JSON: responseObject as! [String : Any])
                let user = User(value: response)
                Utility.hideLoader()
                Utility.showToast(message: "Profile updated successfully.")
                AppStateManager.sharedInstance.loggedInUser = user
                try! Global.APP_REALM?.write {
                    Global.APP_REALM?.add(user, update: true)
                }
                self.navigationController?.popViewController(animated: true)
            }, failure: {
                (error) in
                Utility.hideLoader()
                Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
            })
        }else {
            Utility.showLoader()
            var params: [String: Any] = ["":""]
            params = ["user_name": self.textFieldFullName.text!,
                      "phone": self.textFieldPhone.text!,
                      "email": self.textFieldEmailAddress.text!]
            APIManager.sharedInstance.homeAPIManager.updateProfileWith(params: params, success: {
                (responseObject) in
                let response = Mapper<User>().map(JSON: responseObject as! [String : Any])
                let user = User(value: response)
                Utility.hideLoader()
                Utility.showToast(message: "Profile updated successfully.")
                AppStateManager.sharedInstance.loggedInUser = user
                try! Global.APP_REALM?.write {
                    Global.APP_REALM?.add(user, update: true)
                }
                self.navigationController?.popViewController(animated: true)
            }, failure: {
                (error) in
                Utility.hideLoader()
                Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
            })
        }
        
    }
}
extension EditProfile: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.pickedImage = pickedImage
            self.imageProfile.image = self.pickedImage
            self.imageChanged = true
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
