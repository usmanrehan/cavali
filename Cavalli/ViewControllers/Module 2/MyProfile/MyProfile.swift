//
//  MyProfile.swift
//  Cavalli
//
//  Created by shardha on 3/29/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
enum MyProfileCollectionCategory {
    case barOrders (String, UIImage)
    case myEvents (String, UIImage)
    case myGuest (String, UIImage)
    case profile (String, UIImage)
    case membership (String, UIImage)
    case competition (String, UIImage)
    case messages (String, UIImage)
}
class MyProfile: UIViewController {

    //MARK:- Global declarations
    let arrayHomeCategory: Array<MyProfileCollectionCategory> = [.barOrders("BAR ORDERS", #imageLiteral(resourceName: "barorder")), .myEvents("MY EVENTS", #imageLiteral(resourceName: "myevents2")), .myGuest("MY GUEST", #imageLiteral(resourceName: "guestlist2")), .profile("PORFILE", #imageLiteral(resourceName: "profile2")), .membership("MEMBERSHIP", #imageLiteral(resourceName: "membership2")), .competition("COMPETITION", #imageLiteral(resourceName: "competition2")), .messages("MESSAGES", #imageLiteral(resourceName: "message"))]
    
    //MARK:- IBoutlets
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.collectionView.register(UINib(nibName: "HomeCVC", bundle: nil), forCellWithReuseIdentifier: "HomeCVC")
        self.collectionView.isScrollEnabled = false
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let user = AppStateManager.sharedInstance.loggedInUser
        self.labelName.text = user?.userName
        self.labelEmail.text = user?.email
        self.imageBanner.sd_setImage(with: URL(string: (user?.imageUrl ?? "")!))
        self.imageProfile.sd_setImage(with: URL(string: (user?.imageUrl ?? "")!))
    }
  
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension MyProfile: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 7
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeCVC", for: indexPath) as? HomeCVC else { return UICollectionViewCell() }
        switch self.arrayHomeCategory[indexPath.item] {
        case .barOrders(let name, let image):
            cell.label.text = name
            cell.image.image = image
            break
        case .myEvents(let name, let image):
            cell.label.text = name
            cell.image.image = image
            break
        case .myGuest(let name, let image):
            cell.label.text = name
            cell.image.image = image
            break
        case .profile(let name, let image):
            cell.label.text = name
            cell.image.image = image
            break
        case .membership(let name, let image):
            cell.label.text = name
            cell.image.image = image
            break
        case .competition(let name, let image):
            cell.label.text = name
            cell.image.image = image
            break
        case .messages(let name, let image):
            cell.label.text = name
            cell.image.image = image
            break
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if indexPath.item < 4 {
            return CGSize(width: collectionView.frame.size.width/4, height: collectionView.frame.size.width/4 + 10)
        }else {
            return CGSize(width: (collectionView.frame.size.width/3) - 0.01, height: collectionView.frame.size.width/4 + 10)
        }
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch self.arrayHomeCategory[indexPath.item] {
        case .barOrders:
            break
        case .myEvents:
            break
        case .myGuest:
            break
        case .competition:
            break
        case .profile:
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "EditProfile") as? EditProfile {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
        case .membership:
            break
        case .messages:
            break
        }
    }
    
}

