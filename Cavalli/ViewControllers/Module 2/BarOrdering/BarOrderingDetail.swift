//
//  BarOrderingDetail.swift
//  Cavalli
//
//  Created by shardha on 3/27/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BarOrderingDetail: UIViewController{
    
    //MARK:- Global declaration
    var product = Products()
    var cat = NSMutableAttributedString()
    var desc = NSMutableAttributedString()
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonPrice: UIButton!
    @IBOutlet weak var imageBanner: UIImageView!
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.register(UINib(nibName: "DDTitleTVC", bundle: nil), forCellReuseIdentifier: "DDTitleTVC")
        self.tableView.register(UINib(nibName: "DDDescriptionTVC", bundle: nil), forCellReuseIdentifier: "DDDescriptionTVC")
        self.tableView.register(UINib(nibName: "BarOrderingAddToCart", bundle: nil), forCellReuseIdentifier: "BarOrderingAddToCart")
        
        if self.product.productImages.count > 0 {
            if let stringName = self.product.productImages[0].imageUrl {
                self.imageBanner.sd_setImage(with: URL(string: stringName))
            }
        }
        
        if let price = self.product.price {
            if price != "" {
                self.buttonPrice.setTitle("AED \(price)", for: .normal)
            }
        }
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
  
    //MARK:- Helper Methods
    @objc func actionAddToCart(sender: UIButton) {
        var alreadyAdded = false
        for item in CartRealmHelper.sharedInstance.getAllInCart() {
            if item.product.id == self.product.id {
                alreadyAdded = true
                break
            }
        }
        if alreadyAdded {
            Utility.showToast(message: "Item is already present in the cart.")
        }else {
            CartRealmHelper.sharedInstance.addThisToCart(with: self.product)
            Utility.showToast(message: "Item successfully added to cart.")
        }
    }
}
extension BarOrderingDetail: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2 + 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDTitleTVC") as? DDTitleTVC else { return UITableViewCell() }
            cell.lableTitle.text = self.product.title ?? ""
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDDescriptionTVC") as? DDDescriptionTVC else { return UITableViewCell() }
            //DispatchQueue.main.async {
            //
            //             let stringAttributesBold = [NSAttributedStringKey.font: UIFont.init(name: "Poppins-Bold", size: 14)!]
            //             let stringAttributesLight = [NSAttributedStringKey.font: UIFont.init(name: "Poppins", size: 14)!]
            //             var attrString1: NSMutableAttributedString = NSMutableAttributedString(string: "Category: ")
            //             attrString1.addAttributes(stringAttributesBold, range:  NSMakeRange(0, attrString1.length))
            //             var attrString2: NSMutableAttributedString = NSMutableAttributedString(string: "Shots")
            //             attrString2.addAttributes(stringAttributesLight, range:  NSMakeRange(0, attrString2.length))
            //            var attrString3: NSMutableAttributedString = NSMutableAttributedString(string: "Description: ")
            //            attrString3.addAttributes(stringAttributesBold, range:  NSMakeRange(0, attrString3.length))
            //            var attrString4: NSMutableAttributedString = NSMutableAttributedString(string: self.product.descriptionValue ?? "")
            //            attrString4.addAttributes(stringAttributesLight, range:  NSMakeRange(0, attrString4.length))
            //
            
            
            //
            //            let attributedString1 = NSMutableAttributedString(string:"Category: ", attributes:stringAttributesBold)
            //
            //            let attributedString2 = NSMutableAttributedString(string:"Shots", attributes:stringAttributesLight)
            //
            //            let attributedString3 = NSMutableAttributedString(string:"Description: ", attributes:stringAttributesBold)
            //
            //            let attributedString4 = NSMutableAttributedString(string: self.product.descriptionValue ?? "", attributes:stringAttributesLight)
            //
            //
            //            let cat = NSMutableAttributedString()
            //            cat.append(attributedString1)
            //            cat.append(attributedString2)
            //
            //            attributedString1.append(attributedString2)
            //            attributedString3.append(attributedString4)
            //            self.cat = cat
            //            self.desc = attributedString3
            //
            //            let cat = NSMutableAttributedString()
            //            cat.append(attrString1)
            //            cat.append(attrString2)
            //            let desc = NSMutableAttributedString()
            //            desc.append(attrString3)
            //            desc.append(attrString4)
            //                cell.labelCategory.attributedText = cat//attrString//self.cat
            //                cell.lableDesc.attributedText = desc//self.desc
            //                //cell.layoutSubviews()
            //            //}
            //
            cell.lableDesc.text = self.product.descriptionValue
            cell.selectionStyle = .none
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "BarOrderingAddToCart") as? BarOrderingAddToCart else { return UITableViewCell() }
            cell.buttonAddToCart.tag = indexPath.item
            cell.selectionStyle = .none
            cell.buttonAddToCart.addTarget(self, action: #selector(self.actionAddToCart(sender:)), for: .touchUpInside)
            return cell
            
        }
    }
}
extension BarOrderingDetail: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.item {
        case 0:
            return  self.view.frame.size.height * 0.080
        case 1:
            return  UITableViewAutomaticDimension
        case 2:
            return  UITableViewAutomaticDimension
        default:
            return  100//self.view.frame.size.height * 0.080
        }
    }
}
