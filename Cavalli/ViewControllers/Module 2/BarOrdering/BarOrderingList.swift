//
//  BarOrderingList.swift
//  Cavalli
//
//  Created by shardha on 3/27/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class BarOrderingList: UIViewController{
    
    //MARK:- Global declaration
    var products = List<Products>()
    var navTitle = ""
    var parentNavigationController: UINavigationController?
    
    //MARK:- IBoutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var labelTitle: UILabel!
    
    //MARK:-  View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.labelTitle.text = self.navTitle
        self.tableView.register(UINib(nibName: "MenuCategoryTVC", bundle: nil), forCellReuseIdentifier: "MenuCategoryTVC")
    }
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension BarOrderingList: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCategoryTVC") as? MenuCategoryTVC else { return UITableViewCell() }
        cell.isBarList = true
        cell.configureCell(pro: self.products[indexPath.item])
        cell.selectionStyle = .none
        return cell
    }
}
extension BarOrderingList: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  self.view.frame.size.height * 0.25
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "BarOrderingDetail") as? BarOrderingDetail {
            dvc.product = self.products[indexPath.item]
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
