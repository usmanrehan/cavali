
//
//  MenuDetail.swift
//  Cavalli
//
//  Created by shardha on 3/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class MenuDetail: UIViewController {
    
//    var strTitle : String? = ""
//    var imageHeader : String? = ""

    var viewType = ""

    //MARK:- Global declaration
    var pageMenu : CAPSPageMenu?
    var menuCategory = MenuCategoryModel()
    var arrayMenuProduct = [MenuProductModel]()
    
    //MARK:- IBOutlets
    @IBOutlet weak var viewPage: UIView!
    @IBOutlet weak var pagerParent: UIView!
    @IBOutlet weak var imageNavBg: UIImageView!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var viewConstraints: NSLayoutConstraint!
    @IBOutlet weak var imageConstraints: NSLayoutConstraint!
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.processGetMenuProducts()
        
        if let title = menuCategory.name{
            labelTitle.text = title
        }
        
        if let image = menuCategory.categoryImage {
            imageNavBg.sd_setShowActivityIndicatorView(true)
            imageNavBg.sd_setIndicatorStyle(.gray)
            imageNavBg.sd_setImage(with: URL(string: image)!, placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        }
        NotificationCenter.default.addObserver(self, selector: #selector(self.adjustHeight(_:)), name: Notification.Name("pager"), object: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    @objc func adjustHeight(_ notification : NSNotification) {
        print(notification)
        if let data = notification.userInfo?["isShow"] {
            let i = data as? Int
            if i == 0 {
                viewPage.isHidden = true
                pagerParent.isHidden = true
            } else {
                viewConstraints.constant = 130
                imageConstraints.constant = 190
                viewPage.frame = CGRect(x: 0, y: (self.imageNavBg?.frame.origin.y)!, width: self.view.frame.width, height: self.view.frame.height - (self.imageNavBg?.frame.height)!)
                Utility.showLoader()
                let when = DispatchTime.now() + 2.0
                DispatchQueue.main.asyncAfter(deadline: when) {
                    self.viewPage.isHidden = false
                    self.pagerParent.isHidden = false
                    Utility.hideLoader()
                }
            }
        }
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Helper Methods
    private func removeItemsFromPager() {
        for item in self.viewPage.subviews {
            if item.tag == 101 {
                item.removeFromSuperview()
            }
        }
        self.pageMenu?.removeFromParentViewController()
        self.pageMenu?.controllerArray.removeAll()
        self.pageMenu = nil
    }

    private func setNumberOfItemsinPager(number: Int) {
        self.removeItemsFromPager()
        
        var controllerArray : [UIViewController] = []
        
        for i in 0..<number {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MenuCategoryList") as! MenuCategoryList
            controller.parentNavigationController = self.navigationController
            controller.title = "\(i) Number"
            controller.viewType = viewType
            controllerArray.append(controller)
        }
//        let controllerReservationForm = self.storyboard?.instantiateViewController(withIdentifier: "ReservationForm") as! ReservationForm
//        let controllerMyReservation = self.storyboard?.instantiateViewController(withIdentifier: "MyReservation") as! MyReservation
//
//        controllerReservationForm.parentNavigationController = self.navigationController
//        controllerMyReservation.parentNavigationController = self.navigationController
//
//        controllerReservationForm.title = "Reserve Now"
//        controllerMyReservation.title = "My Reservation"
//
//        controllerArray.append(controllerReservationForm)
//        controllerArray.append(controllerMyReservation)
    
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let height = self.pagerParent.frame.size.height//30//self.imageNavBg.frame.size.width * 0.086
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(0),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.clear),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(goldenColor),
            .BottomMenuHairlineColor(UIColor.clear),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(0),
            .MenuHeight(height)
        ]
        
        let frame = CGRect(x: 0.0, y: self.imageNavBg.frame.maxY, width: self.pagerParent.frame.size.width, height: self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: frame, pageMenuOptions: parameters)
        pageMenu?.view.tag = 101
        self.viewPage.addSubview(pageMenu!.view)
    }
 
    func popIfNoItems() {
        if self.arrayMenuProduct.count == 0 {
            Utility.showToast(message: "No Product found!")
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    @objc func reloadData() {
        self.removeItemsFromPager()
        var controllerArray : [UIViewController] = []
        for i in 0..<self.arrayMenuProduct.count {
            let controller = self.storyboard?.instantiateViewController(withIdentifier: "MenuCategoryList") as! MenuCategoryList
            controller.parentNavigationController = self.navigationController
            controller.viewType = viewType

            controller.title  = self.arrayMenuProduct[i].name ?? ""
            controller.products = self.arrayMenuProduct[i].products
            controllerArray.append(controller)
        }
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let height = self.pagerParent.frame.size.height//30//self.imageNavBg.frame.size.width * 0.086
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(0),
            .UseMenuLikeSegmentedControl(false),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.clear),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(goldenColor),
            .BottomMenuHairlineColor(UIColor.clear),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(0),
            .MenuHeight(height)
        ]
        let frame = CGRect(x: 0, y: (self.imageNavBg?.frame.origin.y)!, width: self.pagerParent.frame.size.width, height:self.viewPage.frame.size.height)
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: frame, pageMenuOptions: parameters)
        pageMenu?.view.tag = 101
        self.viewPage.addSubview(pageMenu!.view)
    }
    
    //MARK:- Services
    func processGetMenuProducts() {
        Utility.showLoader()
        let params: [String: Any] = ["category_id":self.menuCategory.id]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getMenuProductWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MenuProductModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayMenuProduct = response
            self.popIfNoItems()
            self.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: , title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "")
            self.navigationController?.popViewController(animated: true)
        })
        
    }
}
