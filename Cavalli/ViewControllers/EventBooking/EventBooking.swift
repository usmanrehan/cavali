//
//  EventBooking.swift
//  Cavalli
//
//  Created by shardha on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import SwiftDate

class EventBooking: UIViewController {

    var calenderIndex = 0
    var isFromHome : Bool = false
    @IBOutlet weak var calendar: FSCalendar!
    @IBOutlet weak var btnBack: UIButton!
    @IBOutlet weak var btnViewEvents: UIButton?
    var currentDate = NSDate()
    var strSelectedDate : String? = ""

    let week_days_view = weekDaysView.instanceFromNib() as! weekDaysView
    let date = Date()
    var arrEvents = [MarkedEventsModel]()
    let calendarDateFormat = "yyyy-MM-dd HH:mm:ss Z"
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.calendar.delegate = self
        self.calendar.dataSource = self
        calendar.register(DIYCalendarCell.self, forCellReuseIdentifier: "cell")
        calendar.register(CalenderCell.self, forCellReuseIdentifier: "cell12")
        
        // Do any additional setup after loading the view.
        self.updateCalendarUI()
        if  isFromHome == true {
            btnBack.isHidden = false
        }
        else {
            btnBack.isHidden = true
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.btnViewEvents?.isUserInteractionEnabled = false

        if strSelectedDate?.count == 0 {
            self.processGetMenuCategories(firstOfAnyMonth: self.getFirstOfCurMonth())
        } else {
            self.calendar.setCurrentPage(self.calendar.currentPage, animated: true)
        }
    }
    
    @IBAction func onBtnViewEvents(_ sender: UIButton) {
        btnViewEvents?.isUserInteractionEnabled = false

        self.pushToCavaliNightsEvents()
    }
    
    @IBAction func onBtnViewBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getFirstOfCurMonth()-> String {
        let calendar = Calendar.current
        let day :Int = 1//calendar.component(.day, from: self.date)
        let year = calendar.component(.year, from: self.date)
        let month = calendar.component(.month, from: self.date)
        let today = String(day) + "-" + String(month) + "-" + String(year)
        return today
    }
    
    func resetcalendar(){
        for date_ in calendar.selectedDates{
            calendar.deselect(date_)
        }
    }
}
//MARK:- Service

extension EventBooking{
    func processGetMenuCategories(firstOfAnyMonth: String) {
        Utility.showLoader()
        let params: [String: Any] = ["date": firstOfAnyMonth]
        APIManager.sharedInstance.homeAPIManager.getMarkedEventsWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MarkedEventsModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrEvents = response
            for date_ in self.arrEvents{
                let eventDate = Utility.getDateFromString(dateString: date_.eventDate ?? "2018-04-28 14:16:00", dateFormat: "yyyy-MM-dd HH:mm:ss")
                 self.calendar.select(eventDate)
            }
            self.calendar.delegate = self
            self.calendar.dataSource = self
            self.calendar.reloadInputViews()
            self.calendar.reloadData()
            if self.arrEvents.isEmpty{
                Utility.showToast(message: "No events found...")
            }
            self.btnViewEvents?.isUserInteractionEnabled = true
         }, failure: {
            (error) in
            Utility.hideLoader()
            self.btnViewEvents?.isUserInteractionEnabled = true

            Utility.showToast(message: error.localizedFailureReason ?? "")
            self.navigationController?.popViewController(animated: true)
        })
    }
}

extension EventBooking : FSCalendarDataSource {
    func calendar(_ calendar: FSCalendar, cellFor date: Date, at position: FSCalendarMonthPosition) -> FSCalendarCell {
        let cell1 = calendar.dequeueReusableCell(withIdentifier: "cell12", for: date, at: position) as! CalenderCell
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        let CalDate = dateFormatterPrint.string(from: date)
        for date_ in self.arrEvents{
            let eventDate = Utility.customDateFormatter(dateStr: date_.eventDate ?? "2018-04-28 14:16:00", dateFormat: "yyyy-MM-dd HH:mm:ss", formatteddate: "yyyy-MM-dd")
            if eventDate == CalDate {
                if date_.isGrand == 1 {
                    let cell = calendar.dequeueReusableCell(withIdentifier: "cell", for: date, at: position) as! DIYCalendarCell
                    cell.circleImageView?.removeFromSuperview()
                    cell.updateUi(image: true)
                    return cell
                }
            }
        }
        return cell1
    }
}

extension EventBooking: FSCalendarDelegate, FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, borderSelectionColorFor date: Date) -> UIColor? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        let CalDate = dateFormatterPrint.string(from: date)
        for date_ in self.arrEvents {
            print(date_.isMarked)
            let eventDate = Utility.customDateFormatter(dateStr: date_.eventDate ?? "2018-04-28 14:16:00", dateFormat: "yyyy-MM-dd HH:mm:ss", formatteddate: "yyyy-MM-dd")
            if eventDate == CalDate {
                if date_.isMarked == 1 {
                    return Global.APP_COLOR
                }
            }
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleSelectionColorFor date: Date) -> UIColor? {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy-MM-dd"
        let CalDate = dateFormatterPrint.string(from: date)
        
        for date_ in self.arrEvents {
            let eventDate = Utility.customDateFormatter(dateStr: date_.eventDate ?? "2018-04-28 14:16:00", dateFormat: "yyyy-MM-dd HH:mm:ss", formatteddate: "yyyy-MM-dd")
            
            if eventDate == CalDate {
                if date_.isMarked == 1 {
                    return Global.APP_COLOR
                }
                
            }
        }
        return nil
    }
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        
        strSelectedDate = Utility.customDateFormatter(dateStr: String(describing: date), dateFormat: calendarDateFormat, formatteddate: "yyyy-MM-dd")

        switch monthPosition.rawValue {
        case 0:
            calendar.setCurrentPage(date, animated: true)
            let formattedDate = Utility.customDateFormatter(dateStr: String(describing: date), dateFormat: calendarDateFormat, formatteddate: "M-yyyy")
            self.processGetMenuCategories(firstOfAnyMonth: "1-\(formattedDate)")
        case 2:
            calendar.setCurrentPage(date, animated: true)
            let formattedDate = Utility.customDateFormatter(dateStr: String(describing: date), dateFormat: calendarDateFormat, formatteddate: "M-yyyy")
            self.processGetMenuCategories(firstOfAnyMonth: "1-\(formattedDate)")
        default:
            calendar.deselect(date)//deselect all remaining
            let storyboard = UIStoryboard(name: "HomeModule", bundle: nil)
            guard let reservationFilter = storyboard.instantiateViewController(withIdentifier: "ReservationFilter") as? ReservationFilter else { return }
            guard let reservation = storyboard.instantiateViewController(withIdentifier: "Reservations") as? Reservations else { return }
            let nav = ReservationNavigation(menuViewController: UIViewController(), contentViewController: reservation)
            nav.navigationBar.isHidden = true
            nav.sideMenu = ENSideMenu(sourceView: nav.view, menuViewController: reservationFilter, menuPosition:.right)
            nav.sideMenu?.menuWidth = self.view.frame.size.width * 0.60
            self.present(nav, animated: true, completion: nil)
        }
        
    }
    func calendar(_ calendar: FSCalendar, shouldDeselect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        strSelectedDate = Utility.customDateFormatter(dateStr: String(describing: date), dateFormat: calendarDateFormat, formatteddate: "yyyy-MM-dd")

        var flag = false
        switch monthPosition.rawValue {
        case 0:
            calendar.setCurrentPage(date, animated: true)
        case 1:
            let selectedDate = Utility.customDateFormatter(dateStr: String(describing: date), dateFormat: calendarDateFormat, formatteddate: "yyyy-MM-dd")
            for event in self.arrEvents{
                let formattedEventDate = Utility.customDateFormatter(dateStr: event.eventDate ?? "2018-04-28 19:16:00", dateFormat: "yyyy-MM-dd HH:mm:ss", formatteddate: "yyyy-MM-dd")
                if selectedDate == formattedEventDate{
                    self.pushToCavalliNightEventDetail(event)
                    flag = false
                    break
                } else {
                    flag = true
                }
            }
        case 2:
            calendar.setCurrentPage(date, animated: true)
        default:
            break
        }
        return flag
    }
    func calendarCurrentPageDidChange(_ calendar: FSCalendar) {
        let formattedDate = Utility.customDateFormatter(dateStr: String(describing: calendar.currentPage), dateFormat: calendarDateFormat, formatteddate: "M-yyyy")
        self.processGetMenuCategories(firstOfAnyMonth: "1-\(formattedDate)")
        
        calenderIndex = calenderIndex + 1
    }
}

//MARK:- Helper Methods
extension EventBooking{
    func updateCalendarUI(){
        self.calendar.headerHeight = self.view.frame.height * 0.10
        self.calendar.appearance.headerDateFormat = "MMM YYYY"
        self.calendar.appearance.headerTitleFont = UIFont.init(name: "Poppins-Medium", size: 19)
        self.calendar.appearance.headerTitleColor = UIColor.darkGray
        self.week_days_view.frame = self.calendar.calendarWeekdayView.frame
        self.calendar.calendarWeekdayView.addSubview(week_days_view)
        self.calendar.appearance.separators = .none
    }
    func pushToCavaliNightsEvents(){

        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "CavalliNights") as? CavalliNights {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    func pushToCavalliNightEventDetail(_ selectedEvent: MarkedEventsModel){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "CNEventDetail") as? CNEventDetail {
            dvc.selectedEvent = selectedEvent
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
