//
//  TCandPP.swift
//  Cavalli
//
//  Created by shardha on 3/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
class TCandPP: UIViewController {
    
    //MARK:- Global declarations
    var isPrivacy = false
    var isAbout = false
    
    //MARK:- IBOutlets
    @IBOutlet weak var titleLable: UILabel!
    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var webTextView: UIWebView!

    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.isAbout {
            self.titleLable.text = "About Us"
            self.processGetCMS(id: 3)
        }else {
            if isPrivacy {
                self.titleLable.text = "Privacy Policy"
                self.processGetCMS(id: 2)
            }else {
                self.titleLable.text = "Terms & Conditions"
                self.processGetCMS(id: 1)
            }
        }
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    //MARK:- Services
    func processGetCMS(id: Int) {
        Utility.showLoader()
        let params: [String: Any] = ["type": id]
        APIManager.sharedInstance.homeAPIManager.getCMSWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<CMSModel>().map(JSON: responseObject as [String : Any])
            self.webTextView.loadHTMLString(response?.descriptionValue ?? "", baseURL: nil)
//            let myAttribute = [ NSAttributedStringKey.foregroundColor: UIColor.darkGray ]
//            let myAttrString = NSAttributedString(string: (response?.descriptionValue)!, attributes: myAttribute)
//            let text = ÷NSAttributedString(string: (response?.descriptionValue)!, attributes: myAttribute)
//            let strText : NSAttributedString = (response?.descriptionValue)!
//            self.textView.attributedText = strText.htmlToString
            self.textView.text = response?.descriptionValue ?? ""
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}


extension String {
    var
    htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return NSAttributedString() }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return NSAttributedString()
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
