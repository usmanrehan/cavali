//
//  LatestUpdates.swift
//  Cavalli
//
//  Created by shardha on 3/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SDWebImage
class LatestUpdates: UIViewController {
    
    //MARK:- Global Declaration
    var arrayLatestUpdate = [LatestUpdatesModel]()
    var indexToScrol : Int? = 0
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!

    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        print(arrayLatestUpdate)
        if arrayLatestUpdate.count == 0 {
            lblNoData.isHidden = false
            tableView.isHidden = true
        }
        else {
            lblNoData.isHidden = true
            tableView.isHidden = false
            self.tableView.dataSource = self
            self.tableView.delegate = self
            self.tableView.register(UINib(nibName: "LatestUpdateTVC", bundle: nil), forCellReuseIdentifier: "LatestUpdateTVC")
        }
        
        
        
        // Do any additional setup after loading the view.
        
//        self.tableView.estimatedRowHeight = 100
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension LatestUpdates : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayLatestUpdate.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LatestUpdateTVC") as? LatestUpdateTVC else { return UITableViewCell() }
        //cell.imageL.sd_setImage(with: URL(string: self.arrayLatestUpdate[indexPath.item].imageUrl!))
        cell.imageL.sd_setShowActivityIndicatorView(true)
        cell.imageL.sd_setIndicatorStyle(.gray)
        
        if let imageUrl = self.arrayLatestUpdate[indexPath.item].imageUrl {
            cell.imageL.sd_setImage(with: URL(string: imageUrl), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        }
//        cell.imageL.sd_setImage(with: URL(string: (), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        
        cell.labelTitle.text = self.arrayLatestUpdate[indexPath.item].title
        cell.labelTitle.adjustsFontSizeToFitWidth = false
        cell.labelDescription.adjustsFontSizeToFitWidth = false
        cell.labelDescription.text = self.arrayLatestUpdate[indexPath.item].descriptionValue

        if indexToScrol != 0 {
            let indexPath:NSIndexPath = NSIndexPath(row: indexToScrol!, section: 0)//IndexPath(forRow: index, inSection: 0)
            
            self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableViewScrollPosition.top, animated: true)
        }
        return cell
    }
}

extension LatestUpdates : UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  140.0//UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "OurEventDetail") as? OurEventDetail {
            let obj : AnyObject = self.arrayLatestUpdate[indexPath.row]
            print(obj)

            dvc.selectedLatestUpdates = MyEventsModel(value: obj)
            self.navigationController?.pushViewController(dvc, animated: true)
        }*/
        
//        let storyboard = UIStoryboard(name: "BarOrdering", bundle: nil)
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "updateDetails") as? UpdateDetailsViewController {
            let obj : AnyObject = self.arrayLatestUpdate[indexPath.row]
            print(obj)

//            var dict : Dictionary = Dictionary<String, Any>()
//            dict = obj[0]// as! Dictionary<String, Any>// as! Dictionary<String, Any>
//            print(dict)

            dvc.selectedLatestUpdates = self.arrayLatestUpdate[indexPath.row]//LatestUpdatesModel(value: obj)
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
