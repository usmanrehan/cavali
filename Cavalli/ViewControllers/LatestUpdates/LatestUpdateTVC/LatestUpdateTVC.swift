//
//  LatestUpdateTVC.swift
//  Cavalli
//
//  Created by shardha on 3/9/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class LatestUpdateTVC: UITableViewCell {

    @IBOutlet weak var labelDescription: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageL: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
