//
//  Verification.swift
//  Cavalli
//
//  Created by shardha on 3/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import PinCodeTextField
class Verification: UIViewController {
    
    //MARK:- Global Declaration
    var user: User? = nil
    
    //MARK:- IBOutlets
    @IBOutlet weak var bgTop: NSLayoutConstraint!
    @IBOutlet weak var pincode: PinCodeTextField!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.\
        
        pincode.keyboardType = .numberPad
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)

    }

    //MARK:- IBActions
    @IBAction func actionSubmit(_ sender: UIButton) {
        self.validate()
    }
    
    //MARK:- Helper Method
    private func validate() {
        if self.pincode.text?.count == 5 {
            self.processVerficationCode()
        }
    }
   
    //MARK:- @objc
    @objc func keyboardWillShow(notification: NSNotification) {
            self.bgTop.constant = -50
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
            self.bgTop.constant = 0
    }
    
    //MARK:- Services
    func processVerficationCode() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["user_id": AppStateManager.sharedInstance.loggedInUser.id,
                                     "verification_code": self.pincode.text!]
        
        APIManager.sharedInstance.userAPIManager.verificationCodeWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let user = AppStateManager.sharedInstance.loggedInUser
            try! Global.APP_REALM?.write {
                user?.active = "1"
            }
            let appDelegate = Constants.APP_DELEGATE
            appDelegate.showHome()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}
