//
//  Home.swift
//  Cavalli
//
//  Created by shardha on 1/20/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import Realm

enum HomeCollectionCategory {
    case reservations (String, UIImage)
    case menu (String, UIImage)
    case guestList (String, UIImage)
    case competition (String, UIImage)
    case music (String, UIImage)
    case barOrdering (String, UIImage)
    case cavalliNights (String, UIImage)
    case membership (String, UIImage)
}

class Home: UIViewController, ITRAirSideMenuDelegate {

    //MARK:- Global Variables
    var arrayLatestUpdates = [LatestUpdatesModel]()
    let arrayHomeCategory: Array<HomeCollectionCategory> = [.reservations("RESERVATIONS", #imageLiteral(resourceName: "reservation2")), .menu("MENU", #imageLiteral(resourceName: "menu2")), .guestList("GUESTS LIST", #imageLiteral(resourceName: "guestlist2")), .competition("COMPETITION", #imageLiteral(resourceName: "competition2")), .music("MUSIC", #imageLiteral(resourceName: "music2")), .barOrdering("BAR ORDERING", #imageLiteral(resourceName: "barordering2")), .cavalliNights("CAVALLI NIGHTS", #imageLiteral(resourceName: "cavlinights2")), .membership("MEMBERSHIP", #imageLiteral(resourceName: "membership2"))]
    
    //MARK:- IBOutlets
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
   
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "HomeCollectionTVC", bundle: nil), forCellReuseIdentifier: "HomeCollectionTVC")
        
        self.processGetLatestUpdates()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textFieldSearch.text = ""
    }

    //MARK:- IBActions
    @IBAction func actionSideMenu(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.itAirSideMenu?.presentLeftMenuViewController()
    }
    @IBAction func actionProfile(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "BarOrdering", bundle: nil)
        if let dvc = storyboard.instantiateViewController(withIdentifier: "MyProfile") as? MyProfile {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    //MARK:- Side Menu Controls
    func sideMenu(_ sideMenu: ITRAirSideMenu!, didRecognizePanGesture recognizer: UIPanGestureRecognizer!) {
        //print("didRecognizePanGesture")
    }
    func sideMenu(_ sideMenu: ITRAirSideMenu!, willShowMenuViewController menuViewController: UIViewController!) {
        //print("willShowMenuViewController")
    }
    func sideMenu(_ sideMenu: ITRAirSideMenu!, didShowMenuViewController menuViewController: UIViewController!) {
        //print("didShowMenuViewController")
    }
    func sideMenu(_ sideMenu: ITRAirSideMenu!, willHideMenuViewController menuViewController: UIViewController!) {
        //print("willHideMenuViewController")
    }
    func sideMenu(_ sideMenu: ITRAirSideMenu!, didHideMenuViewController menuViewController: UIViewController!) {
        //print("didHideMenuViewController")
    }
    
    //MARK:- Service
    private func processGetLatestUpdates() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getLatestUpdatesWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<LatestUpdatesModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayLatestUpdates = response
            self.tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}


