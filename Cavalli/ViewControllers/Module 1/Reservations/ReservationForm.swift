//
//  ReservationForm.swift
//  Cavalli
//
//  Created by shardha on 1/30/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DateTimePicker
class ReservationForm: UIViewController {

    //MARK:- Global declarations
    var parentNavigationController: UINavigationController?
    var selectedDate = ""
    var selectedPeople = ""
    
    //MARK:- IBOutlets
    @IBOutlet weak var textFieldReservationTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldSecondaryPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var labelNumberOfPeople: UILabel!
    @IBOutlet weak var lableDate: UILabel!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let user = AppStateManager.sharedInstance.loggedInUser
        self.textFieldEmailAddress.text = user?.email
        self.textFieldPhone.text = user?.phone
        self.textFieldFullName.text = user?.userName
    }
   
    //MARK:- IBActions
    @IBAction func actionDropDown(_ sender: UIButton) {
        
        let actionSheetController = UIAlertController(title: "Number Of People", message: "Please select number of people", preferredStyle: .actionSheet)
        let ten = UIAlertAction(title: "10", style: .default) { action -> Void in
            self.selectedPeople = "10"
            self.labelNumberOfPeople.text = "10"
        }
        actionSheetController.addAction(ten)
        
        let twenty = UIAlertAction(title: "20", style: .default) { action -> Void in
            self.selectedPeople = "20"
            self.labelNumberOfPeople.text = "20"
        }
        actionSheetController.addAction(twenty)
        
        let thirty = UIAlertAction(title: "30", style: .default) { action -> Void in
            self.selectedPeople = "30"
            self.labelNumberOfPeople.text = "30"
        }
        actionSheetController.addAction(thirty)
        
        let fifty = UIAlertAction(title: "50", style: .default) { action -> Void in
            self.selectedPeople = "50"
            self.labelNumberOfPeople.text = "50"
        }
        actionSheetController.addAction(fifty)
        
        let cancle = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancle)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    @IBAction func actionDate(_ sender: Any) {
        let min = Date()
        //let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
        
        
        let picker = DateTimePicker.show(minimumDate: min, maximumDate: nil)
        picker.highlightColor =  Global.APP_COLOR
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "Done"
        picker.todayButtonTitle = "Today"
        picker.is12HourFormat = true
        picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        //        picker.isDatePickerOnly = true
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            //formatter.dateFormat = "dd-MM-YY"
            formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.selectedDate = formatter.string(from: date)
            self.lableDate.text = self.selectedDate
            
        }
    }
    @IBAction func acitonSubmit(_ sender: UIButton) {
        self.validate()
    }
    
    //MARK:- Helper Methods
    func validateName() -> Bool {
        return Validation.validateName(input: self.textFieldFullName.text!)
    }
    func validateEmailAddress() -> Bool {
        if(Validation.isValidEmail(self.textFieldEmailAddress.text!)) {
            return true
        }
        return false
    }
    func validatePhone() -> Bool {
        return Validation.validatePhone(input: self.textFieldPhone.text!)
    }
    func validate() {
        if(self.checkAllFieldsAreNotEmpty() && self.validateEmailAddress() && self.validatePhone()) {
            if self.selectedDate != "" {
                if self.selectedPeople != "" {
                    //-
                    self.processAddReservation()
                }else {
                    Utility.showToast(message: "Please select number of people.", controller: self, duration: 1.0)
                }
            }else {
                Utility.showToast(message: "Please select date.", controller: self, duration: 1.0)
            }
            
        }
    }
    func checkAllFieldsAreNotEmpty() -> Bool {
        if(!(self.textFieldReservationTitle.text?.isEmpty)!) {
            return true
        }
        Utility.showToast(message: "Please add reservation title.", controller: self, position: .center)
        return false
        
    }
    func emptyForm() {
        self.textFieldReservationTitle.text = ""
        self.textFieldSecondaryPhone.text = ""
        self.lableDate.text = "Select Date"
        self.labelNumberOfPeople.text = "Total Number of People"
    }
    
    //MARK:- IBActions
    @IBAction func nameEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validateName(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func phoneEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validatePhone(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.PhoneNumber.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }

    //MARK:- Services
    func processAddReservation() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["date": self.selectedDate,
                                     "secondary_phone_no": self.textFieldSecondaryPhone.text!,
                                     "no_people": self.selectedPeople,
                                     "title": self.textFieldReservationTitle.text!]
        APIManager.sharedInstance.homeAPIManager.addReservationWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            Utility.showToast(message: "Your reservation has been posted successfully", controller: self, position: .center)
            self.emptyForm()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}
