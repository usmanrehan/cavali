//
//  Guest.swift
//  Cavalli
//
//  Created by shardha on 2/1/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Guest: UIViewController {

    //MARK:- Global declarations
    var pageMenu : CAPSPageMenu?
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let controllerInviteGuest = self.storyboard?.instantiateViewController(withIdentifier: "InviteGuest") as! InviteGuest
        let controllerMyReservation = self.storyboard?.instantiateViewController(withIdentifier: "MyReservation") as! MyReservation
        
        controllerInviteGuest.title = "Invite Guest"
        controllerMyReservation.title = "My Guest"
        
        controllerArray.append(controllerInviteGuest)
        controllerArray.append(controllerMyReservation)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        
        let bgColor = UIColor(displayP3Red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(20),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(bgColor),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(UIColor.white),
            .BottomMenuHairlineColor(UIColor.white),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(20),
            .MenuHeight(50)
            
        ]
        
        // Initialize page menu with controller array, frame, and optional parameters
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 80, width: self.view.frame.size.width, height: self.view.frame.height - 80), pageMenuOptions: parameters)
        
        // Lastly add page menu as subview of base view controller view
        // or use pageMenu controller in you view hierachy as desired
        self.view.addSubview(pageMenu!.view)
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
