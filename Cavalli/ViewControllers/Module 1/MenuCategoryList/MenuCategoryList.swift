//
//  MenuCategoryList.swift
//  Cavalli
//
//  Created by shardha on 3/8/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class MenuCategoryList: UIViewController{

    //MARK:- GLobal declarations
    var products = List<Products>()
    var parentNavigationController: UINavigationController?
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "MenuCategoryTVC", bundle: nil), forCellReuseIdentifier: "MenuCategoryTVC")
    }
}

extension MenuCategoryList: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.products.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MenuCategoryTVC") as? MenuCategoryTVC else { return UITableViewCell() }
        cell.configureCell(pro: self.products[indexPath.item])
        return cell
    }
}
extension MenuCategoryList: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  self.view.frame.size.height * 0.25
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "DishDetail") as? DishDetail {
            dvc.product = self.products[indexPath.item]
            self.parentNavigationController?.pushViewController(dvc, animated: true)
            
        }
    }
}
