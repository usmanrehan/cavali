//
//  MyReservation.swift
//  Cavalli
//
//  Created by shardha on 1/31/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
protocol MyReservationProtocol {
    func shouldShowFilter()
    func shouldHideFilter()
}
class MyReservation: UIViewController, FilledReservation {
    
    var isFromEvents : Bool = false

    var selectedEventId : String? = ""

    var isFromReservation : Bool? = false
    
    //MARK:- Global declarations
    var arrayMyReservation = [MyReservationModel]()
    var arrayFilteredMyReservation = [MyReservationModel]()
    var parentNavigationController: UINavigationController?
    var filterData = false
    var delegate: MyReservationProtocol? = nil
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblNoData: UILabel!

    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(selectedEventId!)
        self.resignFirstResponder()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "ReservationTVC", bundle: nil), forCellReuseIdentifier: "ReservationTVC")
        NotificationCenter.default.addObserver(self, selector: #selector(self.filterData(_:)), name: NSNotification.Name(rawValue: "updateMyReservation"), object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.resignFirstResponder()
//        self.view.resignFirstResponder()
        self.delegate?.shouldShowFilter()
        self.processGetReservation()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegate?.shouldHideFilter()
    }
    
    //MARK:- @objc
    @objc func filterData(_ notification: NSNotification) {
        guard let reserved = notification.userInfo?["reserved"] as? Bool else {return}
        guard let pending = notification.userInfo?["pending"] as? Bool else {return}
        guard let rejected = notification.userInfo?["rejected"] as? Bool else {return}
        guard let cancled = notification.userInfo?["cancled"] as? Bool else {return}
        
        if !reserved && !pending && !rejected && !cancled {
            self.filterData = false
            self.tableView.reloadData()
            return
        }
        var arr = [MyReservationModel]()
        for item in self.arrayMyReservation {
            if item.status == "Reserved " && reserved == true {
                arr.append(item)
            }
            if item.status == "Pending" && pending == true {
                arr.append(item)
            }
            if item.status == "Rejected" && rejected == true {
                arr.append(item)
            }
            if item.status == "Canceled " && cancled == true {
                arr.append(item)
            }
        }
        self.arrayFilteredMyReservation = arr
        if self.arrayFilteredMyReservation.count == 0 {
            Utility.showToast(message: "No data found!", controller: self)
        }
        self.filterData = true
        self.tableView.reloadData()
    }
    
    @objc func actionViewDetails(sender: UIButton) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "FilledReservationForm") as? FilledReservationForm {
            dvc.isFromEvent = isFromEvents
            if self.filterData {
                dvc.reservationData = self.arrayFilteredMyReservation[sender.tag]
            } else {
                dvc.reservationData = self.arrayMyReservation[sender.tag]
            }
            dvc.delegate = self
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    //MARK:- Helper Methods
    func didCancle() {
        self.processGetReservation()
    }
    func didCompletedEditing() {
        self.processGetReservation()
        
    }
    func didEndEditing() {
        self.filterData = false
        self.delegate?.shouldShowFilter()
    }    
    
    func processGetReservation() {
        Utility.showLoader()
        let params: [String: Any] = ["event_id": selectedEventId!]
        APIManager.sharedInstance.homeAPIManager.getMyReservationWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            
            var Obj : [MyReservationModel] = [MyReservationModel]()
            for item in responseObject {
                if let newItem = Mapper<MyReservationModel>().map(JSONObject: item){
                    Obj.append(newItem)
                }
            }
            if Obj.count == 0 {
                self.lblNoData.isHidden = false

//                Utility.showToast(message: "No data found!", controller: self)
            } else {
                self.lblNoData.isHidden = true

                self.arrayMyReservation = Obj
                self.tableView.reloadData()
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
    }
}

extension MyReservation: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.filterData {
            return self.arrayFilteredMyReservation.count
        }else {
            return self.arrayMyReservation.count
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ReservationTVC") as? ReservationTVC else { return UITableViewCell() }
        var obj = MyReservationModel()
        if filterData {
            obj = self.arrayFilteredMyReservation[indexPath.item]
        } else {
            obj = self.arrayMyReservation[indexPath.item]
        }
        var strDate : String = ""
        if let date : String = obj.date {
            let formateDate = Utility.customDateFormatter(dateStr: date, dateFormat: "yyyy-MM-dd", formatteddate: "EEEE d, MMM yyyy")
            strDate = formateDate
        }
        cell.configureCell(title: obj.title, date: strDate, status: obj.status, number: obj.noPeople)
        cell.buttonViewDetails.tag = indexPath.item
        cell.buttonViewDetails.addTarget(self, action: #selector(self.actionViewDetails(sender:)), for: .touchUpInside)
        return cell
    }
}

extension MyReservation: UITableViewDelegate{}
