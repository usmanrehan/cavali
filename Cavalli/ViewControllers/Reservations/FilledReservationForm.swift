//
//  FilledReservationForm.swift
//  Cavalli
//
//  Created by shardha on 3/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DateTimePicker
import SwiftDate
import DropDown
import ObjectMapper

protocol FilledReservation {
    func didCompletedEditing()
    func didEndEditing()
    func didCancle()
}
class FilledReservationForm: UIViewController {

    var isFromEvent : Bool? = false
    
    //MARK:- Global declaration
    var reservationData = MyReservationModel()
    
    var delegate: FilledReservation? = nil
    
    @IBOutlet weak var textFieldSelectDate: UITextField?
    
    @IBOutlet weak var labelNumberOfPeople: UILabel!
    @IBOutlet weak var lableTime: UILabel!
    @IBOutlet weak var lableCategory: UILabel!

    private let categoryDropDown = DropDown()
    private let timeDropDown = DropDown()
    private let guestDropDown = DropDown()
    
    var arrayResrvationSlot = [ReservationSlotModel]()

    
    var categories = [String]()//["Dinner & Lounge", "Club"]
    let categoriesBackGround = ["dinner", "club"]

    var timeListLaunchDinner = [String]()//["8:00 pm", "8:30 pm","9:00 pm","9:30 pm", "10:00 pm","10:30 pm", "11:00 pm","11:30 pm"]
    let timeListClub = ["12:00 am", "12:30 am", "01:00 am" , "01:30 am", "02:00 am", "02:30 am" ,"3:00 am"]

    var guestList = Array<String>()
    var selectedCategory: String? = ""
    var selectedPeople = ""
    var selectedDate = ""
    
    //MARK:- IBOutlets
    @IBOutlet weak var textFieldReservationTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldSecondaryPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var lableDate: UILabel!
    
    @IBOutlet weak var btnSaveChenges: UIButton?
    @IBOutlet weak var btnCancelReservation: UIButton?
    @IBOutlet weak var btnSelectTime: UIButton?
    @IBOutlet weak var btnSelectNoOfPersons: UIButton?
    @IBOutlet weak var iconTime: UIImageView!

    @IBOutlet weak var viewCategory: UIView?

    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(reservationData)
        processGetReservationSlots()
        
        if isFromEvent == true {
            textFieldReservationTitle.isUserInteractionEnabled = false
            textFieldSelectDate?.isUserInteractionEnabled = false
            btnSelectTime?.isUserInteractionEnabled = false
            iconTime.isHidden = true
            viewCategory?.isHidden = true
        } else {
            setDatePicker()
            onClickSelectTime()
            onClickSelectCategory()
        }
        
        self.textFieldReservationTitle.text = reservationData.title ?? ""
        self.textFieldFullName.text = AppStateManager.sharedInstance.loggedInUser.userName ?? ""
        self.textFieldEmailAddress.text = AppStateManager.sharedInstance.loggedInUser.email ?? ""
        self.textFieldPhone.text = AppStateManager.sharedInstance.loggedInUser.phone ?? ""
        self.textFieldSecondaryPhone.text = self.reservationData.secondaryPhoneNo
        var selectedDate : String? = ""
        if let date = self.reservationData.date {
            selectedDate = Utility.customDateFormatter(dateStr: date, dateFormat: "yyyy-MM-dd", formatteddate: "dd MMM yyyy")
        }
        self.textFieldSelectDate?.text = selectedDate
        self.labelNumberOfPeople.text = "\(self.reservationData.noPeople ?? "0")"
        self.lableTime.text = self.reservationData.time ?? "00:00"
        
//        if let reserveType = self.reservationData.reservationType {
//            self.selectedCategory = self.reservationData.reservationType
//            if (reserveType.contains("dinner")) {
//                self.lableCategory.text = "Dinner & Lounge"
//            } else {
//                self.lableCategory.text = "Club"
//            }
//        }
        if (reservationData.status?.contains("Reserved"))! {
            textFieldReservationTitle.isUserInteractionEnabled = false
            textFieldSelectDate?.isUserInteractionEnabled = false
            textFieldFullName.isUserInteractionEnabled = false
            textFieldEmailAddress.isUserInteractionEnabled = false
            textFieldPhone.isUserInteractionEnabled = false
            textFieldSecondaryPhone.isUserInteractionEnabled = false
            viewCategory?.isUserInteractionEnabled = false
            textFieldSelectDate?.isUserInteractionEnabled = false
            labelNumberOfPeople.isUserInteractionEnabled = false
            
            btnSaveChenges?.isHidden = true
            btnCancelReservation?.isHidden = true
            btnSelectTime?.isUserInteractionEnabled = false
            btnSelectNoOfPersons?.isUserInteractionEnabled = false
        }
        onClickSelectGuestList()
    }
    
    //MARK: Set Date picker
    func setDatePicker(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: date)
        textFieldSelectDate?.text = result
        selectedDate = date.string(custom: "yyyy-MM-dd HH:mm:ss")

        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = date
        textFieldSelectDate?.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    @objc func handleDatePicker(sender: UIDatePicker) {//2018-05-29
        let date = DateInRegion(absoluteDate: sender.date)
        if date.isToday || date.isInFuture {
            textFieldSelectDate?.text = date.string(custom: "dd MMM yyyy")
            selectedDate = date.string(custom: "yyyy-MM-dd HH:mm:ss")
        }
    }
    
    
    

    // set time DrpDowm
    // set Category
    func onClickSelectTime() {
        //        if lableCategory?.text == "Dinner & Lounge" {
        timeDropDown.anchorView = lableTime
        timeDropDown.direction = .bottom
        timeDropDown.dataSource = timeListLaunchDinner
        
        // Action triggered on selection
        timeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lableTime.text = self.timeListLaunchDinner[index]
        }
        //        } else {
        //            timeDropDown.anchorView = lableTime
        //            timeDropDown.direction = .bottom
        //            timeDropDown.dataSource = timeListClub
        //
        //            // Action triggered on selection
        //            timeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
        //                print("Selected item: \(item) at index: \(index)")
        //                self.lableTime.text = self.timeListClub[index]
        //            }
        //        }
        //        lableTime.text = "Select Time"
    }
    
    // set time DrpDowm
    func onClickSelectCategory() {
        categoryDropDown.anchorView = lableTime
        categoryDropDown.direction = .bottom
        categoryDropDown.dataSource = categories
        // Action triggered on selection
        categoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lableCategory?.text = self.categories[index]
            self.selectedCategory = "\(self.arrayResrvationSlot[index].id)"
            self.timeListLaunchDinner = self.breakSlots(startDate: self.arrayResrvationSlot[index].from!, endDate: self.arrayResrvationSlot[index].to!)
            self.onClickSelectTime()
        }
    }
    // set Guest Drop Down
    func onClickSelectGuestList() {
        guestList.removeAll()
        for index in 1...20 {
            print(index)
            guestList.append("\(index)")
        }
        guestDropDown.anchorView = labelNumberOfPeople
        guestDropDown.direction = .bottom
        guestDropDown.dataSource = guestList
        
        // Action triggered on selection
        guestDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.labelNumberOfPeople.text = item//self.guestList[index]
            self.selectedPeople = "\(item)"
        }
    }
    
    //MARK:- IBActions
    
    //MARK:- IBActions
    @IBAction func actionGuestDropDown(_ sender: UIButton) {
        guestDropDown.show()
    }
    
    @IBAction func actionCategory(_ sender: Any) {
        categoryDropDown.show()
    }
    
    @IBAction func actionTime(_ sender: Any) {
        if lableCategory?.text != "Select Category" {
            onClickSelectTime()
            timeDropDown.show()
        } else {
            Utility.showToast(message: "Please select category first", controller: self)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegate?.didEndEditing()
    }
    @IBAction func actionSaveChanges(_ sender: UIButton) {
        self.validate()
    }
    @IBAction func actionCancel(_ sender: UIButton) {
        self.processCancleReservation()
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    /*@IBAction func actionDropDown(_ sender: UIButton) {
        
        let actionSheetController = UIAlertController(title: "Number Of People", message: "Please select number of people", preferredStyle: .actionSheet)
        let ten = UIAlertAction(title: "10", style: .default) { action -> Void in
            self.selectedPeople = "10"
            self.labelNumberOfPeople.text = "10"
        }
        actionSheetController.addAction(ten)
        
        let twenty = UIAlertAction(title: "20", style: .default) { action -> Void in
            self.selectedPeople = "20"
            self.labelNumberOfPeople.text = "20"
        }
        actionSheetController.addAction(twenty)
        
        let thirty = UIAlertAction(title: "30", style: .default) { action -> Void in
            self.selectedPeople = "30"
            self.labelNumberOfPeople.text = "30"
        }
        actionSheetController.addAction(thirty)
        
        let fifty = UIAlertAction(title: "50", style: .default) { action -> Void in
            self.selectedPeople = "50"
            self.labelNumberOfPeople.text = "50"
        }
        actionSheetController.addAction(fifty)
        
        let cancle = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancle)
        self.present(actionSheetController, animated: true, completion: nil)
    }*/
    
    
    //MARK:- Helper methods
    func validate()
    {
        if !(self.textFieldReservationTitle.text?.isEmpty)! {
            if (self.textFieldReservationTitle.text == self.reservationData.title) && (self.lableDate.text == self.reservationData.date) && (self.labelNumberOfPeople.text == "\(self.reservationData.noPeople)") {
                //
                Utility.showToast(message: "Please edit details first before saving changes.", controller: self)
            }else {
                self.processEditReservation()
            }
        }else {
            Utility.showToast(message: "Reservation title is empty.", controller: self)
        }
    }
    
    func breakSlots(startDate : String, endDate: String) -> Array<String> {
        var array: [String] = []
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "hh:mm a"
        
        let startDate = startDate//"20-08-2018 10:00 AM"
        let endDate = endDate//"20-08-2018 02:30 PM"
        
        let date1 = formatter.date(from: startDate)
        let date2 = formatter.date(from: endDate)
        
        var i = 1
        while true {
            let date = date1?.addingTimeInterval(TimeInterval(i*30*60))
            let string = formatter2.string(from: date!)
            
            if date! >= date2! {
                break;
            }
            
            i += 1
            array.append(string)
        }
        print(array)
        
        return array
    }
    
    func processGetReservationSlots() {
        Utility.showLoader()
        APIManager.sharedInstance.homeAPIManager.getReservationSlotsWith(success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<ReservationSlotModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayResrvationSlot = response
            print(self.arrayResrvationSlot)
            
            for item in self.arrayResrvationSlot {
                self.categories.append(item.name!)
                if item.id == self.reservationData.reservationStatusId {
                    self.lableCategory.text = item.name
                    self.selectedCategory = "\(item.id)"
                }
            }
            self.onClickSelectCategory()
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
    }
    
    //MARK:- Services
    func processCancleReservation() {
        Utility.showLoader()
        let params: [String: Any] = ["reservation_id": self.reservationData.id]
        APIManager.sharedInstance.homeAPIManager.getCancleReservationWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.delegate?.didCancle()
            self.navigationController?.popToRootViewController(animated: true)
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    func processEditReservation() {
        self.view.endEditing(true)
        Utility.showLoader()
        var selectedDate : String? = ""
        if let date = self.textFieldSelectDate?.text {
            selectedDate = Utility.customDateFormatter(dateStr: date, dateFormat: "dd MMM yyyy", formatteddate: "yyyy-MM-dd")
        }
        let params: [String: Any] = ["id":self.reservationData.id,
                                     "date": selectedDate!,
                                     "time": self.lableTime.text!,
                                     "secondary_phone_no": self.textFieldSecondaryPhone.text!,
                                     "no_people": self.labelNumberOfPeople.text!,
                                     "title": self.textFieldReservationTitle.text!,
                                     "reservation_slot_id": selectedCategory!]
        
//        if isFromEvents == false {
//            params.updateValue(selectedCategory!, forKey: "reservation_type")
//        }
        print(params)
        
        APIManager.sharedInstance.homeAPIManager.editReservationWith(params: params, success: {
            (responseObject) in
            
            Utility.hideLoader()
            
            self.delegate?.didCompletedEditing()
            self.navigationController?.popViewController(animated: true)
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}
