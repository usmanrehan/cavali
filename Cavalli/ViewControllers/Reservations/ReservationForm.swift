//
//  ReservationForm.swift
//  Cavalli
//
//  Created by shardha on 1/30/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DateTimePicker
import SwiftDate
import DropDown
import ObjectMapper
class ReservationForm: UIViewController {
    
    var isFromEvents : Bool = true

    var selectedEventId : String? = ""
    //MARK:- Global declarations
    var parentNavigationController: UINavigationController?
    
    var selectedDate: String? = ""
    
    var selectedPeople: String? = ""
    var selectedEventTitle: String? = ""
    var selectedCategory: String? = ""

    private let timeDropDown = DropDown()
    private let guestDropDown = DropDown()
    private let categoryDropDown = DropDown()

    var arrayResrvationSlot = [ReservationSlotModel]()

    //MARK:- IBOutlets
    @IBOutlet weak var textFieldReservationTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldSecondaryPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldSelectDate: UITextField!

    @IBOutlet weak var labelNumberOfPeople: UILabel!
    @IBOutlet weak var lableTime: UILabel!
    @IBOutlet weak var iconTime: UIImageView!

    @IBOutlet weak var btnSelectTime: UIButton?
    @IBOutlet weak var lableCategory: UILabel?
    @IBOutlet weak var viewCategory: UIView?

    var slotInterval = 0
    var categories = [String]()//["Dinner & Lounge", "Club"]
    let categoriesBackGround = ["dinner", "club"]
    var timeListLaunchDinner = [String]()//["8:30 pm","9:00 pm","9:30 pm", "10:00 pm","10:30 pm", "11:00 pm","11:30 pm"]
    let timeListClub = ["11:30 pm","12:00 am", "12:30 am", "01:00 am" , "01:30 am", "02:00 am", "02:30 am"]
    var guestList = Array<String>()
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    
        processGetReservationSlots()
        onClickSelectGuestList()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let user = AppStateManager.sharedInstance.loggedInUser
        self.textFieldEmailAddress.text = user?.email
        self.textFieldPhone.text = user?.phone
        self.textFieldFullName.text = user?.userName
        self.textFieldReservationTitle.text = selectedEventTitle
        
        if isFromEvents == false {
            textFieldSelectDate.isUserInteractionEnabled = true
            setDatePicker()
            
            
        } else {
            btnSelectTime?.isUserInteractionEnabled = false
            textFieldSelectDate.isUserInteractionEnabled = false
            textFieldReservationTitle.isUserInteractionEnabled = false
            textFieldEmailAddress.isUserInteractionEnabled = false
            
            textFieldEmailAddress.textColor = UIColor.lightGray
            textFieldReservationTitle.textColor = UIColor.lightGray
            textFieldPhone.textColor = UIColor.lightGray
            textFieldSelectDate.textColor = UIColor.lightGray
            lableTime.textColor = UIColor.lightGray
            
            //            textFieldSelectDate.textColor = UIColor.lightGray
            iconTime.isHidden = true
            viewCategory?.isHidden = true
            
            self.textFieldSelectDate.text = Utility.customDateFormatter(dateStr: selectedDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "dd MMM yyyy")
            let strTime = Utility.customDateFormatter(dateStr: selectedDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "hh:mm a")
            self.lableTime.text = strTime.lowercased()
            
//            selectedDate = Utility.customDateFormatter(dateStr: selectedDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "yyyy-MM-dd")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         self.view.endEditing(true)
    }
    
    //MARK: Set Date picker
    func setDatePicker(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        let result = formatter.string(from: date)
        textFieldSelectDate.text = result
        selectedDate = date.string(custom: "yyyy-MM-dd HH:mm:ss")

        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        datePickerView.minimumDate = date
        textFieldSelectDate.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    
    @objc func handleDatePicker(sender: UIDatePicker) {//2018-05-29
        let date = DateInRegion(absoluteDate: sender.date)
        if date.isToday || date.isInFuture {
            textFieldSelectDate.text = date.string(custom: "dd MMM yyyy")
            selectedDate = date.string(custom: "yyyy-MM-dd HH:mm:ss")
        }
    }
    
    // set time DrpDowm
    // set Category
    func onClickSelectTime() {
//        if lableCategory?.text == "Dinner & Lounge" {
            timeDropDown.anchorView = lableTime
            timeDropDown.direction = .bottom
            timeDropDown.dataSource = timeListLaunchDinner
            
            // Action triggered on selection
            timeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
                print("Selected item: \(item) at index: \(index)")
                self.lableTime.text = self.timeListLaunchDinner[index]
            }
//        } else {
//            timeDropDown.anchorView = lableTime
//            timeDropDown.direction = .bottom
//            timeDropDown.dataSource = timeListClub
//
//            // Action triggered on selection
//            timeDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
//                print("Selected item: \(item) at index: \(index)")
//                self.lableTime.text = self.timeListClub[index]
//            }
//        }
//        lableTime.text = "Select Time"
    }
    
    // set time DrpDowm
    func onClickSelectCategory() {
        categoryDropDown.anchorView = lableTime
        categoryDropDown.direction = .bottom
        categoryDropDown.dataSource = categories
        // Action triggered on selection
        categoryDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.lableCategory?.text = self.categories[index]
            self.selectedCategory = "\(self.arrayResrvationSlot[index].id)"
            self.slotInterval = (self.arrayResrvationSlot[index].interval)

            self.timeListLaunchDinner = self.breakSlots(startDate: self.arrayResrvationSlot[index].from!, endDate: self.arrayResrvationSlot[index].to!, interval: self.slotInterval)
            self.lableTime.text = "Select Time"
            self.onClickSelectTime()
        }
    }
    
    // set Guest Drop Down
    func onClickSelectGuestList() {
        guestList.removeAll()
        for index in 1...20 {
            print(index)
            guestList.append("\(index)")
        }        
        guestDropDown.anchorView = labelNumberOfPeople
        guestDropDown.direction = .bottom
        guestDropDown.dataSource = guestList
        
        // Action triggered on selection
        guestDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.labelNumberOfPeople.text = item//self.guestList[index]
            self.selectedPeople = "\(item)"
        }
    }
    
    //MARK:- IBActions
    @IBAction func actionGuestDropDown(_ sender: UIButton) {
        guestDropDown.show()
    }
    
    @IBAction func actionCategory(_ sender: Any) {
        categoryDropDown.show()
    }
    
    @IBAction func actionTime(_ sender: Any) {
        if lableCategory?.text != "Select Category" {
            timeDropDown.show()
        } else {
            Utility.showToast(message: "Please select category first", controller: self)
        }
    }
    
    @IBAction func acitonSubmit(_ sender: UIButton) {
        self.validate()
    }
    
    //MARK:- Helper Methods
    func validateName() -> Bool {
        return Validation.validateName(input: self.textFieldFullName.text!)
    }
    func validateEmailAddress() -> Bool {
        if(Validation.isValidEmail(self.textFieldEmailAddress.text!)) {
            return true
        }
        return false
    }
    func validatePhone() -> Bool {
        return Validation.validatePhone(input: self.textFieldPhone.text!)
    }
    
    func validate() {
        if(self.checkAllFieldsAreNotEmpty() && self.validateEmailAddress()) { //&& self.validatePhone()) {
            if self.selectedDate != "" {
                if self.lableTime.text != "Select Time" {
                    if self.selectedPeople != "" {
                        //-
                        self.processAddReservation()
                    } else {
                        Utility.showToast(message: "Please select number of people.", controller: self, duration: 1.0)
                    }
                } else {
                    Utility.showToast(message: "Please select time.", controller: self, duration: 1.0)
                }
            } else {
                Utility.showToast(message: "Please select date.", controller: self, duration: 1.0)
            }
        }
    }
    
    func checkAllFieldsAreNotEmpty() -> Bool {
        if(!(self.textFieldReservationTitle.text?.isEmpty)!) {
            return true
        }
        Utility.showToast(message: "Please add reservation title.", controller: self, position: .center)
        return false
        
    }
    func emptyForm() {
        self.selectedPeople = ""
        self.textFieldReservationTitle.text = ""
        self.textFieldSecondaryPhone.text = ""
        self.lableTime.text = "Select Time"
        self.labelNumberOfPeople.text = "Total Number of People"
    }
    
    //MARK:- IBActions
    @IBAction func nameEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validateName(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func phoneEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validatePhone(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.PhoneNumber.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    
    
    func breakSlots(startDate : String, endDate: String, interval: Int) -> Array<String> {
        var array: [String] = []
        
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        
        let formatter2 = DateFormatter()
        formatter2.dateFormat = "hh:mm a"
        
        let startDate = startDate//"20-08-2018 10:00 AM"
        let endDate = endDate//"20-08-2018 02:30 PM"
        
        let date1 = formatter.date(from: startDate)
        let date2 = formatter.date(from: endDate)
        
        var i = 1
        while true {
            let date = date1?.addingTimeInterval(TimeInterval(i*interval*60))
            let string = formatter2.string(from: date!)
            
            if date! >= date2! {
                break;
            }
            
            i += 1
            array.append(string)
        }
        print(array)
        
        return array
    }
    
    func processGetReservationSlots() {
        Utility.showLoader()
        APIManager.sharedInstance.homeAPIManager.getReservationSlotsWith(success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<ReservationSlotModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayResrvationSlot = response
            print(self.arrayResrvationSlot)
            
            for item in self.arrayResrvationSlot {
                self.categories.append(item.name!)
            }
            
            
            self.onClickSelectCategory()
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
    }
    
    //MARK:- Services
    func processAddReservation() {
        print(selectedEventId!)
        self.view.endEditing(true)
        Utility.showLoader()
        var date = "2018-04-28 19:16:00"
        if let dateSelect = selectedDate {
            date = Utility.customDateFormatter(dateStr: dateSelect , dateFormat: Constants.serverDateFormat, formatteddate: "yyyy-MM-dd")

        }
        //selectedDate = Utility.customDateFormatter(dateStr: selectedDate ?? "2018-04-28 19:16:00", dateFormat: Constants.serverDateFormat, formatteddate: "yyyy-MM-dd")

        let params: [String: Any] = ["date": date,
                                     "time": self.lableTime.text!,
                                     "secondary_phone_no": self.textFieldSecondaryPhone.text!,
                                     "no_people": self.selectedPeople!,
                                     "title": self.textFieldReservationTitle.text!,
                                     "event_id": selectedEventId!,
                                     "reservation_slot_id" : selectedCategory!]
//        if isFromEvents == false {
//            params.updateValue(selectedCategory!, forKey: "reservation_type")
//        }
        APIManager.sharedInstance.homeAPIManager.addReservationWith(params:
            params, success: {
            (responseObject) in
                Utility.hideLoader()
                if self.isFromEvents == false {
                    self.emptyForm()
                } else {
                    self.textFieldSecondaryPhone.text = ""
                    self.labelNumberOfPeople.text = "Total Number of People"
                    self.selectedPeople = ""
                }
                Utility.showToast(message: "Your reservation has been posted successfully", controller: self, position: .center)
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}

