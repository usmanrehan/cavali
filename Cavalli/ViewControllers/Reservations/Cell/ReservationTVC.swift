//
//  ReservationTVC.swift
//  Cavalli
//
//  Created by shardha on 1/31/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class ReservationTVC: UITableViewCell {

    //MARK:- Global declarations
    let colorPending = UIColor(displayP3Red: 255/255.0, green: 152/255.0, blue: 44/255.0, alpha: 1.0)
    let colorReserved = UIColor(displayP3Red: 48/255.0, green: 137/255.0, blue: 41/255.0, alpha: 1.0)
    let colorRejected = UIColor(displayP3Red: 193/255.0, green: 28/255.0, blue: 57/255.0, alpha: 1.0)
    
    //MARK:- IBOutlets
    @IBOutlet weak var lableTotalNumber: UILabel!
    @IBOutlet weak var labelDate: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonViewDetails: UIButton!
    @IBOutlet weak var imageStatus: UIImageView!
    @IBOutlet weak var lableStatus: UILabel!
    
    //MARK:- Nib lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    //255 152 44 orange pending
    //48 137 41 Green reserved
    //193 28 57 Red rejected
    func configureCell(title: String?,date: String?, status:String? , number: String?) {
        self.labelTitle.text = title ?? ""
        self.labelDate.text = date ?? ""
        self.lableStatus.text = status ?? ""
        
        self.lableTotalNumber.text = "\(number ?? "0.0")"
        
        guard let sts = status as? String else { return }
        switch sts {
        case "Pending":
            self.imageStatus.image = #imageLiteral(resourceName: "pending")
            self.lableStatus.textColor = self.colorPending
            self.buttonViewDetails.isHidden = false
            break
        case "Reserved ":
            self.imageStatus.image = #imageLiteral(resourceName: "reserved")
            self.lableStatus.textColor = self.colorReserved
            self.buttonViewDetails.isHidden = false
            break
        case "Rejected":
            self.imageStatus.image = #imageLiteral(resourceName: "rejected")
            self.lableStatus.textColor = self.colorRejected
            self.buttonViewDetails.isHidden = true
            break
        case "Canceled ":
            self.imageStatus.image = #imageLiteral(resourceName: "rejected")
            self.lableStatus.textColor = self.colorRejected
            self.buttonViewDetails.isHidden = true
            break
        default:
            break
        }
    }
}
