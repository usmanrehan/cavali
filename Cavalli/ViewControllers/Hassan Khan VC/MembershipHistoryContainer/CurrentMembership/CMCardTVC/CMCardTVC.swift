//
//  CMCardTVC.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class CMCardTVC: UITableViewCell {

    @IBOutlet weak var lableExpired: UILabel!
    @IBOutlet weak var membershipImage: UIImageView!
    @IBOutlet weak var lableName: UILabel!
    @IBOutlet weak var lableAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
