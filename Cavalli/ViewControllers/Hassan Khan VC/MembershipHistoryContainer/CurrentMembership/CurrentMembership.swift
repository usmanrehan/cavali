//
//  CurrentMembership.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
protocol MembershipHistoryDelegate : NSObjectProtocol {
    func willMoveToMH(controllerMS: MembershipHistory)
    func didMoveToMS()
}
class CurrentMembership: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var parentNavigationController: UINavigationController?
    var arrayCurrentMembership = [MembershipModel]()
    var arrayMembershipHistory = [MembershipModel]()
    weak var delegate: MembershipHistoryDelegate? = nil
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "CMTitleTVC", bundle: nil), forCellReuseIdentifier: "CMTitleTVC")
        self.tableView.register(UINib(nibName: "DDDescriptionTVC", bundle: nil), forCellReuseIdentifier: "DDDescriptionTVC")
        self.tableView.register(UINib(nibName: "CMCardTVC", bundle: nil), forCellReuseIdentifier: "CMCardTVC")
        self.processGetMembershipHistoryData()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
    }
    
    //MARK: - Service
    func processGetMembershipHistoryData() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getMembershipHistoryDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response_object = responseObject as NSDictionary
            let cm = response_object.value(forKey: "current_membership") as! [[String : Any]]
            let mh = response_object.value(forKey: "membership_history") as! [[String : Any]]
            let responseCM = Mapper<MembershipModel>().mapArray(JSONArray: cm)
            let responseMH = Mapper<MembershipModel>().mapArray(JSONArray: mh)
            self.arrayCurrentMembership = responseCM
            self.arrayMembershipHistory = responseMH
            if self.arrayCurrentMembership.count == 0 {
                Utility.showToast(message: "No data found.")
            }
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    
}
extension CurrentMembership: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CMCardTVC") as? CMCardTVC else { return UITableViewCell() }
            if self.arrayCurrentMembership.count > 0 {
                cell.isHidden = false
                cell.lableName.text = self.arrayCurrentMembership[0].title
                cell.lableAmount.text = "AED \(self.arrayCurrentMembership[0].price ?? "0")"
                //cell.membershipImage.sd_setImage(with: URL(string: self.arrayCurrentMembership[0].imageUrl ?? ""))
                cell.membershipImage.sd_setShowActivityIndicatorView(true)
                cell.membershipImage.sd_setIndicatorStyle(.gray)
                cell.membershipImage.sd_setImage(with: URL(string: self.arrayCurrentMembership[0].imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
            }else {
                cell.isHidden = true
            }
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CMTitleTVC") as? CMTitleTVC else { return UITableViewCell() }
            if self.arrayCurrentMembership.count > 0 {
                cell.isHidden = false
                cell.lableTitle.text = "What Includes?"
            }else {
                cell.isHidden = true
            }
            cell.selectionStyle = .none
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDDescriptionTVC") as? DDDescriptionTVC else { return UITableViewCell() }
            if self.arrayCurrentMembership.count > 0 {
                cell.isHidden = false
                cell.lableDesc.text = self.arrayCurrentMembership[0].descriptionValue
            }else {
                cell.isHidden = true
            }
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
            
        }
    }
    
    
}
extension CurrentMembership: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
extension CurrentMembership: MembershipHistoryDelegate {
    func willMoveToMH(controllerMS: MembershipHistory) {
        print("willMoveToMH")
        if self.arrayMembershipHistory.count > 0{
            controllerMS.dataPresent = true
            controllerMS.membershipHistory = self.arrayMembershipHistory[0]
        }else {
            controllerMS.dataPresent = false
            Utility.showToast(message: "No data found.")
        }
    }
    func didMoveToMS() {
        print("didMoveToMS")
        self.processGetMembershipHistoryData()
    }
}
