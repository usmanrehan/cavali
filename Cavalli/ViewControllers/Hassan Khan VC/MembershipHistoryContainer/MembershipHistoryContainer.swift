//
//  MembershipHistoryContainer.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MembershipHistoryContainer: UIViewController {

    
    //MARK:- Global declarations
    var pageMenu : CAPSPageMenu?
    
    weak var msHistoryDelegate: MembershipHistoryDelegate? = nil
    
    var controllerCM = CurrentMembership()
    var controllerMH = MembershipHistory()
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonFilter: UIButton!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let controllerCurrentMembership = self.storyboard?.instantiateViewController(withIdentifier: "CurrentMembership") as! CurrentMembership
        let controllerMembershipHistory = self.storyboard?.instantiateViewController(withIdentifier: "MembershipHistory") as! MembershipHistory
        
        controllerCurrentMembership.parentNavigationController = self.navigationController
        controllerMembershipHistory.parentNavigationController = self.navigationController
        
        controllerCurrentMembership.delegate = controllerCurrentMembership
        self.msHistoryDelegate = controllerCurrentMembership.delegate
        
        controllerCurrentMembership.title = "Current Membership"
        controllerMembershipHistory.title = "History"
        
        controllerArray.append(controllerCurrentMembership)
        controllerArray.append(controllerMembershipHistory)
        
        self.controllerCM = controllerCurrentMembership
        self.controllerMH = controllerMembershipHistory
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        
        //let bgColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(20),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.black),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(UIColor.white),
            .BottomMenuHairlineColor(UIColor.white),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(20),
            .MenuHeight(40)
            
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.height - 80), pageMenuOptions: parameters)
        pageMenu?.delegate = self
        pageMenu?.view.backgroundColor = UIColor.white
        self.view.addSubview(pageMenu!.view)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension MembershipHistoryContainer: CAPSPageMenuDelegate {
    func willMoveToPage(controller: UIViewController, index: Int) {
        if index == 1 {
            self.msHistoryDelegate?.willMoveToMH(controllerMS: self.controllerMH)
        }
    }
    func didMoveToPage(controller: UIViewController, index: Int) {
        if index == 0 {
            self.msHistoryDelegate?.didMoveToMS()
        }
    }
}


