//
//  MembershipHistory.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MembershipHistory: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    var parentNavigationController: UINavigationController?
    var membershipHistory = MembershipModel()
    var dataPresent = false
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        if self.dataPresent {
            self.tableView.dataSource = self
            self.tableView.delegate = self
        }else {
            //Utility.showToast(message: "No data found.")
            return
        }
        self.tableView.estimatedRowHeight = 50
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "CMTitleTVC", bundle: nil), forCellReuseIdentifier: "CMTitleTVC")
        self.tableView.register(UINib(nibName: "DDDescriptionTVC", bundle: nil), forCellReuseIdentifier: "DDDescriptionTVC")
        self.tableView.register(UINib(nibName: "CMCardTVC", bundle: nil), forCellReuseIdentifier: "CMCardTVC")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
    }
    
}
extension MembershipHistory: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CMCardTVC") as? CMCardTVC else { return UITableViewCell() }
            cell.lableExpired.isHidden = false
            cell.lableName.text = self.membershipHistory.title
            cell.lableAmount.text = "AED \(self.membershipHistory.price ?? "0")"
            //cell.membershipImage.sd_setImage(with: URL(string: self.membershipHistory.imageUrl ?? ""))
            cell.membershipImage.sd_setShowActivityIndicatorView(true)
            cell.membershipImage.sd_setIndicatorStyle(.gray)
            cell.membershipImage.sd_setImage(with: URL(string: self.membershipHistory.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
            cell.selectionStyle = .none
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "CMTitleTVC") as? CMTitleTVC else { return UITableViewCell() }
            cell.lableTitle.text = "What Includes?"
            cell.selectionStyle = .none
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "DDDescriptionTVC") as? DDDescriptionTVC else { return UITableViewCell() }
            cell.lableDesc.text = self.membershipHistory.descriptionValue
            cell.selectionStyle = .none
            return cell
        default:
            return UITableViewCell()
            
        }
    }
}
extension MembershipHistory: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}


