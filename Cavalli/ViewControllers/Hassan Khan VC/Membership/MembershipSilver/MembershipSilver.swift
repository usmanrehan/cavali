//
//  MembershipSilver.swift
//  Cavalli
//
//  Created by shardha on 4/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
protocol MembershipSilverDelegate : NSObjectProtocol {
    func willMoveToMG(controllerMS: MembershipGold)
    func willMoveToMP(controllerMS: MembershipPlatinum)
    func didMoveToMS()
}
class MembershipSilver: UIViewController {

    //var memberShipObj = MembershipModel()
    
    var parentNavigationController: UINavigationController?
    var arrayMembership = [MembershipModel]()
    weak var delegate: MembershipSilverDelegate? = nil
    
    var membershipObject = MembershipModel()
    
    @IBOutlet weak var imageMembershipCard: UIImageView?
    @IBOutlet weak var lableTitle: UILabel?
    @IBOutlet weak var lableDesc: UILabel?
    
    @IBOutlet weak var btnMemberShip: UILabel?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.processGetMembershipData()
        // Do any additional setup after loading the view.
        //self.processGetMembershipData()
        
        self.lableTitle?.text = membershipObject.title//self.arrayMembership[0].title
        self.lableTitle?.textColor = UIColor.blue
        self.lableDesc?.text = "AED " +  membershipObject.price!//self.arrayMembership[0].price!//descriptionValue
        self.imageMembershipCard?.sd_setImage(with: URL(string: self.membershipObject.imageUrl ?? ""))
        
//        self.imageMembershipCard?.sd_setShowActivityIndicatorView(true)
//        self.imageMembershipCard?.sd_setIndicatorStyle(.gray)
//        self.imageMembershipCard?.sd_setImage(with: URL(string: memberShip.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func actionMemberShipCard(_ sender: UIButton) {
        
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "MembershipDetails") as? MembershipDetails {
            dvc.membershipData = membershipObject
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
    @IBAction func actionBuynow(_ sender: UIButton) {
        
        self.processBuySubscription()
    }
    
    func updateData(memberShip : MembershipModel) {
        
        self.lableTitle?.text = memberShip.title//self.arrayMembership[0].title
        self.lableTitle?.textColor = UIColor.blue
//        self.lableDesc?.text = "AED " +  memberShip.price!//self.arrayMembership[0].price!//descriptionValue
        //self.imageMembershipCard.sd_setImage(with: URL(string: self.arrayMembership[0].imageUrl ?? ""))
        
//        self.imageMembershipCard?.sd_setShowActivityIndicatorView(true)
//        self.imageMembershipCard?.sd_setIndicatorStyle(.gray)
//        self.imageMembershipCard?.sd_setImage(with: URL(string: memberShip.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
    }
    
    //MARK: - Service
    func processGetMembershipData() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getMembershipDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MembershipModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayMembership = response
//            self.updateData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    func processBuySubscription() {
        Utility.showLoader()
        var params = [String: Any]()
        if let memberId: Int = membershipObject.id {
            params = ["membership_id": memberId]
            APIManager.sharedInstance.homeAPIManager.buySubscriptionWith(params: params, success: {
                (responseObject) in
                Utility.hideLoader()
                let storyboardPayment = UIStoryboard(name: "BarOrdering", bundle: nil) //UIStoryboard.instantiateInitialViewController("BarOrdering")
                if let dvc = storyboardPayment.instantiateViewController(withIdentifier: "PaymentMethode") as? PaymentMethodeViewController {
                    //                dvc.token = "\(tok)"
                    dvc.order_Id = (responseObject["id"] as? Int)!
                    dvc.idTitle = "m_id"
                    dvc.isFromMemberShip = true
                    dvc.strModel = "membership-payment"
                    self.parentNavigationController?.pushViewController(dvc, animated: true)//present(dvc, animated: true, completion: nil)//
                }
            }, failure: {
                (error) in
                Utility.hideLoader()
                Utility.showToast(message: error.localizedFailureReason ?? "")
            })
        }
        else {
            Utility.showToast(message: "Please check yourinternet connection")
        }
    }
    
}

extension MembershipSilver: MembershipSilverDelegate {
    func willMoveToMG(controllerMS: MembershipGold) {
        print("willMoveToMG")
        controllerMS.membership = self.arrayMembership[1]
    }
    func willMoveToMP(controllerMS: MembershipPlatinum) {
         print("willMoveToMP")
        controllerMS.membership = self.arrayMembership[2]
    }
    func didMoveToMS() {
         print("didMoveToMS")
        self.processGetMembershipData()
    }
}
