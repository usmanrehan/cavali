//
//  NotificationVC.swift
//  Cavalli
//
//  Created by shardha on 4/4/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class NotificationVC: UIViewController {

    var arrayNotification = [NotificationsModel]()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "MessagesTVC", bundle: nil), forCellReuseIdentifier: "MessagesTVC")
    }
    
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.processGetNotifications()
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.changeStatusBarColor(clear: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
    }
    
    @IBAction func actionDelete(_ sender: UIButton) {
        print(sender.tag)
        
        let deleteAlert = UIAlertController(title: "Delete Notification", message: "Are you sure you want to detete this notification?", preferredStyle: UIAlertControllerStyle.alert)
        
        deleteAlert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { (action: UIAlertAction!) in
            
            Utility.showLoader()
            self.processDeleteNotifications(notificationId: self.arrayNotification[sender.tag].id)
        }))
        
        deleteAlert.addAction(UIAlertAction(title: "No", style: .cancel, handler: { (action: UIAlertAction!) in
            //self.dismiss(animated: true, completion: nil)
        }))
        self.present(deleteAlert, animated: true, completion: nil)
//        /deleteNotification
    }
   
}
extension NotificationVC: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayNotification.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessagesTVC") as? MessagesTVC else { return UITableViewCell() }
        cell.lblMessageTitle?.isHidden = true
        let obj = self.arrayNotification[indexPath.item]
        cell.labelDesc.text = obj.message
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let odate = dateformatter.date(from: obj.createdAt ?? "2000-01-01 01:01")
        let dFForDate = DateFormatter()
        dFForDate.dateFormat = "dd:MM:yy"
        cell.labelDate.text = dFForDate.string(from: odate!)
        cell.labelTime.text = Utility.UTCToLocal(date: obj.createdAt!, oldFormate: "yyyy-MM-dd HH:mm:ss", newFormate: "h:mm a")
        cell.btnDelete.tag = indexPath.row
        cell.btnDelete.addTarget(self, action: #selector(actionDelete(_:)), for: .touchUpInside)
        return cell
    }
}
extension NotificationVC: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboardHassan = UIStoryboard(name: "Mod3Hassan", bundle: nil)

        let detailController = storyboardHassan.instantiateViewController(withIdentifier: "notificationDetails") as! NotificationDetailsViewController
        detailController.objNotification = arrayNotification[indexPath.row]
        self.navigationController?.pushViewController(detailController, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70//self.view.frame.size.height * 0.22
    }
}
//MARK:- Service
extension NotificationVC{
    //MARK: - Service
    func processGetNotifications() {
        Utility.showLoader()
        let params: [String: Any] = ["": ""]
        APIManager.sharedInstance.homeAPIManager.getNotificationsWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<NotificationsModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayNotification = response
            self.arrayNotification = self.arrayNotification.sorted(by: { $0.createdAt! > $1.createdAt! })
            if self.arrayNotification.count == 0{
                Utility.showToast(message: "No data found.")
            }
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }

    //MARK:- Delete Service
    func processDeleteNotifications(notificationId: Int) {
        Utility.showLoader()
        let params: [String: Any] = ["notification_id": notificationId]
        APIManager.sharedInstance.homeAPIManager.deleteNotificationWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.view.endEditing(true)
            
            self.processGetNotifications()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}
