//
//  NotificationDetailsViewController.swift
//  Cavalli
//
//  Created by shardha kawal on 8/13/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class NotificationDetailsViewController: UIViewController {

    var objNotification = NotificationsModel()
    
    @IBOutlet weak var lblDetails: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var lblDate: UILabel!


    override func viewDidLoad() {
        super.viewDidLoad()

        lblDetails.text = objNotification.message
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let odate = dateformatter.date(from: objNotification.createdAt ?? "2000-01-01 01:01")
        let dFForDate = DateFormatter()
        dFForDate.dateFormat = "dd:MM:yy"
        lblDate.text = dFForDate.string(from: odate!)
        lblTime.text = Utility.UTCToLocal(date: objNotification.createdAt!, oldFormate: "yyyy-MM-dd HH:mm:ss", newFormate: "h:mm a")
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
