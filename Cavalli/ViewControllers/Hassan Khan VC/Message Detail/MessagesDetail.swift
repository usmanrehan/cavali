//
//  MessagesDetail.swift
//  Cavalli
//
//  Created by shardha on 4/4/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import IQKeyboardManagerSwift
class MessagesDetail: UIViewController {
    
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var botConstraint: NSLayoutConstraint!
    @IBOutlet weak var textFieldMessage: UITextView!
    @IBOutlet weak var tableView: UITableView!
    var thread = ThreadModel()
    var chatArray = [ChatModel]()
    var kHight = CGFloat(0)
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        self.textFieldMessage.autocorrectionType = .yes
        self.textFieldMessage.autocorrectionType = .no
        self.textFieldMessage.text = "Write a message..."
        self.textFieldMessage.textColor = UIColor.lightGray
        self.textFieldMessage.delegate = self
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "MessageBubbleTVCSent", bundle: nil), forCellReuseIdentifier: "MessageBubbleTVCSent")
        self.tableView.register(UINib(nibName: "MessageBubbleTVCRecieved", bundle: nil), forCellReuseIdentifier: "MessageBubbleTVCRecieved")
        self.hideKeyboard()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillChangeFrame(notification:)), name: NSNotification.Name.UIKeyboardWillChangeFrame, object: nil)
        self.processGetChatData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
//        NotificationCenter.default.removeObserver(NSNotification.Name.UIKeyboardWillShow)
//        NotificationCenter.default.removeObserver(NSNotification.Name.UIKeyboardWillHide)
    }
    @objc func keyboardWillShow(notification: NSNotification) {
//        if Connectivity.isConnectedToInternet() {
            if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
                self.view.frame.origin.y -= (keyboardSize.height + self.view.frame.origin.y)
                self.tableView.scrollToRow(at: IndexPath(row: self.chatArray.count - 1, section: 0), at: .bottom, animated: true)
            }
//        }
        
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
                self.kHight = 0
                self.view.layoutIfNeeded()
            }
        }
    }
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSend(_ sender: UIButton) {
        if Connectivity.isConnectedToInternet() {
            if self.textFieldMessage.text != "Write a message..." && self.textFieldMessage.text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false{
                self.processAddMessage()
            }
            else{
                self.textFieldMessage.text = self.textFieldMessage.text.trimmingCharacters(in: .whitespacesAndNewlines)
                self.textFieldMessage.resignFirstResponder()
            }
//            let storyBoard = UIStoryboard(name: "Mod3Hassan", bundle: nil)
//            if let dvc = storyBoard.instantiateViewController(withIdentifier: "Membership") as? Membership {
//                self.navigationController?.pushViewController(dvc, animated: true)
//            }
        } else {
            Utility.showToast(message: "Internet connection seems to be offline")
            return
        }
    }
    
    //MARK: - Service
    func processGetChatData() {
        Utility.showLoader()
        let params: [String: Any] = ["thread_id": self.thread.id]
        APIManager.sharedInstance.homeAPIManager.getChatDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<ChatModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.chatArray = response
            if self.chatArray.count == 0 {
                Utility.showToast(message: "No Data Found.")
            }
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath(row: self.chatArray.count - 1, section: 0), at: .bottom, animated: true)
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    func processAddMessage() {
        Utility.showLoader()
        let params: [String: Any] = ["request_id": self.chatArray.first!.requestId,
                                     "receiver_id": "1",
                                     "message": self.textFieldMessage.text!.encodeEmoji]
        APIManager.sharedInstance.homeAPIManager.addMessageWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.view.endEditing(true)
            self.textFieldMessage.text = "Write a message..."
            self.textFieldMessage.textColor = UIColor.lightGray
            self.processGetChatData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}
extension MessagesDetail: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.chatArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let obj = self.chatArray[indexPath.item]
        if obj.senderDetail?.id == 1 { //admin
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageBubbleTVCRecieved") as? MessageBubbleTVCRecieved else { return UITableViewCell() }
            cell.lableBody.text = obj.message?.decodeEmoji
            //cell.imageProfile.sd_setImage(with: URL(string: obj.senderDetail?.imageUrl ?? ""))
            cell.imageProfile.sd_setShowActivityIndicatorView(true)
            cell.imageProfile.sd_setIndicatorStyle(.gray)
            cell.imageProfile.sd_setImage(with: URL(string: obj.senderDetail?.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let odate = dateFormatter.date(from: obj.createdAt ?? "2000-01-01 01:01:01")
            let dFForDate = DateFormatter()
            dFForDate.timeZone = TimeZone.current
            dFForDate.dateFormat = "dd:MM:yy HH:mm:ss"
            cell.lableTimeStamp.text = dFForDate.string(from: odate!)
            
            return cell
        }else { //user
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "MessageBubbleTVCSent") as? MessageBubbleTVCSent else { return UITableViewCell() }
            cell.lableBody.text = obj.message?.decodeEmoji
            //cell.imageProfile.sd_setImage(with: URL(string: obj.senderDetail?.imageUrl ?? ""))
            cell.imageProfile.sd_setShowActivityIndicatorView(true)
            cell.imageProfile.sd_setIndicatorStyle(.gray)
            cell.imageProfile.sd_setImage(with: URL(string: obj.senderDetail?.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
            let odate = dateFormatter.date(from: obj.createdAt ?? "2000-01-01 01:01:01")
            let dFForDate = DateFormatter()
            dFForDate.timeZone = TimeZone.current
            dFForDate.dateFormat = "dd:MM:yy HH:mm:ss"
            cell.lableTimeStamp.text = dFForDate.string(from: odate!)
            return cell
        }
     }
}
extension MessagesDetail: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
}
extension MessagesDetail: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Write a message..."
            textView.textColor = UIColor.lightGray
        }
    }
}
extension String {
    var encodeEmoji: String{
        if let encodeStr = NSString(cString: self.cString(using: .nonLossyASCII)!, encoding: String.Encoding.utf8.rawValue){
            return encodeStr as String
        }
        return self
    }
    var decodeEmoji: String{
        let data = self.data(using: String.Encoding.utf8);
        let decodedStr = NSString(data: data!, encoding: String.Encoding.nonLossyASCII.rawValue)
        if let str = decodedStr{
            return str as String
        }
        return self
    }
}
extension MessagesDetail
{
    func hideKeyboard()
    {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(
            target: self,
            action: #selector(self.dismissKeyboard))
        
        self.tableView.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard()
    {
        view.endEditing(true)
    }
    
    
}

