//
//  BarOrderHistoryContainer.swift
//  Cavalli
//
//  Created by shardha on 4/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class BarOrderHistoryContainer: UIViewController {


    //MARK:- Global declarations
    var pageMenu : CAPSPageMenu?
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonFilter: UIButton!
    
    weak var poDelegate: PendingOrderDelegate? = nil
    
    var controllerOH = OrderHistory()
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let controllerPendingOrders = self.storyboard?.instantiateViewController(withIdentifier: "PendingOrders") as! PendingOrders
        let controllerOrderHistory = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistory") as! OrderHistory
        controllerPendingOrders.delegate = controllerPendingOrders
        self.poDelegate = controllerPendingOrders.delegate
        self.controllerOH = controllerOrderHistory
        
        controllerPendingOrders.parentNavigationController = self.navigationController
        controllerOrderHistory.parentNavigationController = self.navigationController
        
        controllerPendingOrders.title = "Pending Orders"
        
        controllerOrderHistory.title = "Order History"
        
        controllerArray.append(controllerPendingOrders)
        controllerArray.append(controllerOrderHistory)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        
        //let bgColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(20),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.black),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(UIColor.white),
            .BottomMenuHairlineColor(UIColor.white),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(20),
            .MenuHeight(40)
            
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.height - 80), pageMenuOptions: parameters)
        
        pageMenu?.view.backgroundColor = UIColor.white
        pageMenu?.delegate = self
        self.view.addSubview(pageMenu!.view)
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
extension BarOrderHistoryContainer: CAPSPageMenuDelegate {
    func willMoveToPage(controller: UIViewController, index: Int) {
        if index == 1 {
            self.poDelegate?.willMoveToOH(controllerOH: self.controllerOH)
        }
    }
    func didMoveToPage(controller: UIViewController, index: Int) {
        if index == 0 {
            self.poDelegate?.didMoveToPO()
        }
    }
}
