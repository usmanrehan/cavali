//
//  OrderHistory.swift
//  Cavalli
//
//  Created by shardha on 4/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class OrderHistory: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    var parentNavigationController: UINavigationController?
    var arrayOrderHistory = [OrrderHistory]()
    let colorPending = UIColor(displayP3Red: 255/255.0, green: 152/255.0, blue: 44/255.0, alpha: 1.0)
    let colorReserved = UIColor(displayP3Red: 48/255.0, green: 137/255.0, blue: 41/255.0, alpha: 1.0)
    let colorRejected = UIColor(displayP3Red: 193/255.0, green: 28/255.0, blue: 57/255.0, alpha: 1.0)
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "OrderHistoryTVC", bundle: nil), forCellReuseIdentifier: "OrderHistoryTVC")
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.changeStatusBarColor(clear: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
    }
    
}
extension OrderHistory: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOrderHistory.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryTVC") as? OrderHistoryTVC else { return UITableViewCell() }
        let obj = self.arrayOrderHistory[indexPath.item]
        
        cell.lableOrderNumber.text = obj.orderNo
        cell.lablePrice.text = "AED \(obj.totalAmount?.currencyFormatter() ?? "0.0")"
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let odate = dateformatter.date(from: obj.createdAt ?? "2000-01-01 01:01")
        let dFForDate = DateFormatter()
        dFForDate.dateFormat = "dd:MM:yy"
        cell.labelDate.text = dFForDate.string(from: odate!)
        let tFForDate = DateFormatter()
        tFForDate.dateFormat = "HH:mm"
        tFForDate.timeZone = TimeZone.current//(abbreviation: "GMT+:00")//TimeZone.current
        
        cell.lableTime.text = Utility.UTCToLocal(date: obj.createdAt!, oldFormate: "yyyy-MM-dd HH:mm:ss", newFormate: "h:mm a")
        
        //        guard let sts = obj.orderStatusId as? String else { return cell }
        switch obj.orderStatusId  {
        case 1:
            cell.imageStatus.image = #imageLiteral(resourceName: "pending")
            cell.lableStatus.textColor = self.colorPending
            cell.lableStatus.text = "Pending"
            break
        case 2:
            cell.imageStatus.image = #imageLiteral(resourceName: "rejected")
            cell.lableStatus.textColor = self.colorRejected
            cell.lableStatus.text = "Canceled by user"
            break
        case 3:
            cell.imageStatus.image = #imageLiteral(resourceName: "rejected")
            cell.lableStatus.textColor = self.colorRejected
            cell.lableStatus.text = "Rejected"
            break
        case 4:
            cell.imageStatus.image = #imageLiteral(resourceName: "reserved")
            cell.lableStatus.textColor = self.colorReserved
            cell.lableStatus.text = "Completed"
            break
        default:
            break
        }
        return cell
    }
}
extension OrderHistory: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.20
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "OrderHistoryDetail") as? OrderHistoryDetail {
            dvc.parentNavigationController = self.parentNavigationController
            dvc.arrayOrderProducts = self.arrayOrderHistory[indexPath.item].orderProduct
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
    
}

