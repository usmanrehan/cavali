//
//  OrderHistoryDetail.swift
//  Cavalli
//
//  Created by shardha on 4/10/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
class OrderHistoryDetail: UIViewController {

    @IBOutlet weak var lbltitle: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var parentNavigationController: UINavigationController?
    var arrayOrderProducts = List<OrderProduct>()
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 100
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.register(UINib(nibName: "OrderHistoryDetailTVC", bundle: nil), forCellReuseIdentifier: "OrderHistoryDetailTVC")
        
        self.lbltitle.text = "Order Detail"
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.changeStatusBarColor(clear: true)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //appDelegate.changeStatusBarColor(clear: false)
    }
    
    //MARK: - Actions
    @IBAction func actionBack(_ sender: UIButton) {
        self.parentNavigationController?.popViewController(animated: true)
    }
    
}
extension OrderHistoryDetail: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayOrderProducts.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "OrderHistoryDetailTVC") as? OrderHistoryDetailTVC else { return UITableViewCell() }
        let obj = self.arrayOrderProducts[indexPath.item]
        
        cell.labelTitle.text = obj.productDetail?.title
        cell.labelDesc.text = obj.productDetail?.descriptionValue
        cell.labelQty.text = "Qty: \(obj.quantity)"
        cell.lablePrice.text = "AED \((obj.productDetail?.price?.currencyFormatter())! )"
        //cell.imageProduct.sd_setImage(with: URL(string: obj.productDetail?.imageUrl ?? ""))
        cell.imageProduct.sd_setShowActivityIndicatorView(true)
        cell.imageProduct.sd_setIndicatorStyle(.gray)
        cell.imageProduct.sd_setImage(with: URL(string: obj.productDetail?.imageUrl ?? ""), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        
        return cell
    }
}
extension OrderHistoryDetail: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120//UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        //        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "MessagesDetail") as? MessagesDetail {
        //            self.navigationController?.pushViewController(dvc, animated: true)
        //        }
    }
    
}


