//
//  HomeCollectionTVC.swift
//  Cavalli
//
//  Created by shardha on 1/22/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class HomeCollectionTVC: UITableViewCell {

    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var btnReservations: UIButton!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnGuestList: UIButton!
    @IBOutlet weak var btnCompetition: UIButton!
    @IBOutlet weak var btnMusic: UIButton!
    @IBOutlet weak var btnBarOrdering: UIButton!
    @IBOutlet weak var btnCavalliNights: UIButton!
    @IBOutlet weak var btnMembership: UIButton!
    @IBOutlet weak var gridView: UIView!
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
