//
//  HomeSearchModel.swift
//
//  Created by Hamza Hasan on 4/23/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class HomeSearchModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let isActive = "is_active"
    static let descriptionValue = "description"
    static let type = "type"
    static let eventTypeId = "event_type_id"
    static let logoImage = "logo_image"
    static let id = "id"
    static let isMarked = "is_marked"
    static let eventDate = "event_date"
    static let title = "title"
    static let imageUrl = "image_url"
  }

  // MARK: Properties
   @objc dynamic var updatedAt: String? = ""
   @objc dynamic var isActive: String? = ""
   @objc dynamic var descriptionValue: String? = ""
   @objc dynamic var type: String? = ""
   @objc dynamic var eventTypeId: String? = ""
   @objc dynamic var logoImage: String? = ""
   @objc dynamic var id = 0
   @objc dynamic var isMarked = 0
   @objc dynamic var eventDate: String? = ""
   @objc dynamic var title: String? = ""
   @objc dynamic var imageUrl: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    isActive <- map[SerializationKeys.isActive]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    type <- map[SerializationKeys.type]
    eventTypeId <- map[SerializationKeys.eventTypeId]
    logoImage <- map[SerializationKeys.logoImage]
    id <- map[SerializationKeys.id]
    isMarked <- map[SerializationKeys.isMarked]
    eventDate <- map[SerializationKeys.eventDate]
    title <- map[SerializationKeys.title]
    imageUrl <- map[SerializationKeys.imageUrl]
  }


}
