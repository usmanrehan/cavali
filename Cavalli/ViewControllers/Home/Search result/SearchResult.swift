//
//  SearchResult.swift
//  Cavalli
//
//  Created by Usman Bin Rehan on 4/23/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper

class SearchResult: UIViewController {

    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var textFieldSearch: UITextField!

    @IBOutlet weak var tableView: UITableView!
    var arrSearchRecords = [MyEventsModel]()
    var searchedText = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "LatestUpdateTVC", bundle: nil), forCellReuseIdentifier: "LatestUpdateTVC")

        lblNoData.text = AlertMessages.typeToSearch.rawValue
        lblNoData.isHidden = false
        tableView.isHidden = true
        textFieldSearch.becomeFirstResponder()
        textFieldSearch.addTarget(self, action: #selector(actionSearch(_:)), for: .editingChanged)//allEditingEvents)

       // self.tableView.estimatedRowHeight = 100
        // Do any additional setup after loading the view.
    }
    //    override func viewWillAppear(_ animated: Bool) {
    //        super.viewWillAppear(animated)
    //        self.processGetSeatchResults()
    //    }
    
    @IBAction func onBtnBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionSearch(_ sender: Any) {
        if (textFieldSearch.text?.count)! > 2 {
            
            if self.textFieldSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false {
                self.processGetSeatchResults(text: self.textFieldSearch.text ?? "")
            } else{
                self.textFieldSearch.text = self.textFieldSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines)
            }
        }
        else {
            lblNoData.text = self.textFieldSearch.text == "" ? AlertMessages.typeToSearch.rawValue : AlertMessages.noDataFound.rawValue
            lblNoData.isHidden = false
            tableView.isHidden = true
        }
    }
}
extension SearchResult {
    private func pushToSearchResults(){//\_ data: [MyEventsModel] , searchedText : String){
        let storyBoard = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "SearchResult") as? SearchResult {
            //            dvc.arrSearchRecords = data
            //            dvc.searchedText = searchedText
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    private func processGetSeatchResults(text:String){
        let params: [String: Any] = ["search_title": text]
        APIManager.sharedInstance.homeAPIManager.getHomeSearchWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MyEventsModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            if response.count == 0 {
//                let textCount : Int = (self.textFieldSearch.text?.count)!
//                if textFieldSearch.text?.count == 0
//                a != nil ? a! : b
                self.lblNoData.text = self.textFieldSearch.text == "" ? AlertMessages.typeToSearch.rawValue : AlertMessages.noDataFound.rawValue
                self.tableView.isHidden = true
                self.lblNoData.isHidden = false
            } else {
                self.tableView.isHidden = false
                self.lblNoData.isHidden = true
                self.arrSearchRecords = response
                self.tableView.reloadData()
                // self.pushToSearchResults(response, searchedText: self.textFieldSearch.text ?? "")
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
        })
    }
}
extension SearchResult: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrSearchRecords.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LatestUpdateTVC") as? LatestUpdateTVC else { return UITableViewCell() }
        if let imageURL = self.arrSearchRecords[indexPath.row].imageUrl{
            cell.imageL.sd_setShowActivityIndicatorView(true)
            cell.imageL.sd_setIndicatorStyle(.gray)
            cell.imageL.sd_setImage(with: URL(string: imageURL), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        } else {
            cell.imageL.image = #imageLiteral(resourceName: "placeHolder")
        }
        cell.labelTitle.text = self.arrSearchRecords[indexPath.row].title ?? "None"
        cell.labelDescription.text = self.arrSearchRecords[indexPath.row].descriptionValue ?? "None"
        return cell
    }
}
extension SearchResult: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        let search_id = self.arrSearchRecords[indexPath.row].id
        guard let search_type = self.arrSearchRecords[indexPath.row].type else {return}
        switch search_type {
        case "product":
            self.processgetHomeSearchProductDetail(id: search_id, type: search_type)
        case "news":
            print(self.arrSearchRecords[indexPath.row])
            self.pushToEventsDetail(self.arrSearchRecords[indexPath.row])
//            self.processGetLatestUpdates()
        case "event":
            let eventTypeId = self.arrSearchRecords[indexPath.row].eventTypeId ?? "0"
            if eventTypeId == "1"{//music
                self.pushToEventsDetail(self.arrSearchRecords[indexPath.row])
            }
            else{//cavalli
                let event_id = self.arrSearchRecords[indexPath.row].parentId
                self.pushToCavalliNightEventDetail(event_id,self.arrSearchRecords[indexPath.row])
            }
        default:
            break
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  120//UITableViewAutomaticDimension
    }
    //MARK:- Helper Methods
    func pushToEventsDetail(_ selectedEvent: MyEventsModel){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "OurEventDetail") as? OurEventDetail {
            dvc.selectedLatestUpdates = selectedEvent
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    func pushToCavalliNightEventDetail(_ event_id: Int,_ selectedCavalliEvent: MyEventsModel){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "CNEventDetail") as? CNEventDetail {
            dvc.selectedCavaliEvent = selectedCavalliEvent
            dvc.event_id = event_id
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    func pushToLatestUpdates(arrayLatestUpdate: [LatestUpdatesModel]) {
        
//        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "updateDetails") as? UpdateDetailsViewController {
//            let obj : AnyObject = self.arrayLatestUpdates[indexPath.row]
//            print(obj)
//            dvc.selectedLatestUpdates = self.arrayLatestUpdates[indexPath.row]//LatestUpdatesModel(value: obj)
//            self.navigationController?.pushViewController(dvc, animated: true)
//        }
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "LatestUpdates") as? LatestUpdates {
            dvc.arrayLatestUpdate = arrayLatestUpdate
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    func pustToMenuDetail(product : Products){
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "DishDetail") as? DishDetail {
            dvc.product = product
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    func pustToBarDetail(product : Products){
        let storyboard = UIStoryboard(name: "BarOrdering", bundle: nil)
        if let dvc = storyboard.instantiateViewController(withIdentifier: "BarOrderingDetail") as? BarOrderingDetail {
            dvc.product = product
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    private func processGetLatestUpdates() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getLatestUpdatesWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<LatestUpdatesModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.pushToLatestUpdates(arrayLatestUpdate: response)
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
   
    private func processgetHomeSearchProductDetail(id: Int, type : String) {
        Utility.showLoader()
        let params: [String: Any] = ["id": id,"type": type]
        APIManager.sharedInstance.homeAPIManager.getHomeSearchDetailWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = responseObject as NSDictionary
            print(response)
//            let category1 = response.object(forKey: "category")
            guard let category : Int = response["category"] as? Int else {return}
//            let category : Int = response["category"] as! Int// as? String
            switch category{
            case 1:
                let response_Menu = Mapper<Products>().map(JSON: responseObject as [String : Any])
                self.pustToMenuDetail(product: response_Menu!)
            case 2:
                let response_Product = Mapper<Products>().map(JSON: responseObject as [String : Any])
                self.pustToBarDetail(product: response_Product!)
            default:
                break
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
    
    
}
