//
//  ExtentionHomeCollection.swift
//  Cavalli
//
//  Created by shardha on 1/23/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SDWebImage
extension Home: UICollectionViewDelegateFlowLayout {
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2.5, height: collectionView.frame.size.width/2.5)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
}
extension Home: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrayLatestUpdates.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "HomeLatestUpdatesCVC", for: indexPath) as? HomeLatestUpdatesCVC else { return UICollectionViewCell() }
        let lu = self.arrayLatestUpdates[indexPath.item]
        cell.imageBanner.sd_setShowActivityIndicatorView(true)
        cell.imageBanner.sd_setIndicatorStyle(.gray)
        cell.imageBanner.sd_setImage(with: URL(string: lu.imageUrl!), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        cell.lableName.text = lu.title
        return cell
    }
}
extension Home: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       // self.actionSeeAllLatestUpdates(index: indexPath.row)
        
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "updateDetails") as? UpdateDetailsViewController {
            let obj : AnyObject = self.arrayLatestUpdates[indexPath.row]
            print(obj)
            dvc.selectedLatestUpdates = self.arrayLatestUpdates[indexPath.row]//LatestUpdatesModel(value: obj)
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
}
