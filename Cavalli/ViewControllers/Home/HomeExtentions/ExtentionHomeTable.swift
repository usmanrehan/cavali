
//
//  ExtentionHomeTableView.swift
//  Cavalli
//
//  Created by shardha on 1/23/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

extension Home: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section { //categories
        case 0:    // each row height = (self.tableView.frame.size.width/4 + 10)
            // multiplied by two because there are two row of collectionview
            // + 20 is extra padding
            //return ((self.tableView.frame.size.width/4 + 10) * 2) + 20
            return self.tableView.frame.height * 0.63
        default: //latest updates
            return self.tableView.frame.size.width/2
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: HomeHeader = .fromNib()
        header.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        switch section {
        case 0:
            header.labelTitle.text = "CATEGORIES"
            header.buttonSeeAll.isHidden = true
            break
        case 1:
            header.labelTitle.text = "LATEST UPDATES"
            header.buttonSeeAll.isHidden = false
            header.buttonSeeAll.addTarget(self, action: #selector(self.moveToSeeAll), for: .touchUpInside)
            break
        default:
            break
        }
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    @objc func moveToSeeAll() {
        actionSeeAllLatestUpdates(index: 0)
    }
    
    @objc func actionSeeAllLatestUpdates(index : Int)
    {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "LatestUpdates") as? LatestUpdates {
            dvc.arrayLatestUpdate = self.arrayLatestUpdates
            dvc.indexToScrol = index
        self.navigationController?.pushViewController(dvc, animated: true)
    }
}
}

extension Home: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 { //categories
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionTVC") as? HomeCollectionTVC else { return UITableViewCell() }
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.tag = indexPath.section
            cell.collectionView.register(UINib(nibName: "HomeCVC", bundle: nil), forCellWithReuseIdentifier: "HomeCVC")
            cell.collectionView.isScrollEnabled = false
            cell.selectionStyle = .none
            
            cell.btnReservations.addTarget(self, action: #selector(self.onBtnReservations(sender:)), for: .touchUpInside)
            cell.btnMenu.addTarget(self, action: #selector(self.onBtnMenu(sender:)), for: .touchUpInside)
            cell.btnGuestList.addTarget(self, action: #selector(self.onBtnGuestList(sender:)), for: .touchUpInside)
            cell.btnCompetition.addTarget(self, action: #selector(self.onBtnCompetition(sender:)), for: .touchUpInside)
            cell.btnMusic.addTarget(self, action: #selector(self.onBtnMusic(sender:)), for: .touchUpInside)
            cell.btnBarOrdering.addTarget(self, action: #selector(self.onBtnBarOrdering(sender:)), for: .touchUpInside)
            cell.btnCavalliNights.addTarget(self, action: #selector(self.onBtnCavalliNights(sender:)), for: .touchUpInside)
            cell.btnMembership.addTarget(self, action: #selector(self.onBtnMembership(sender:)), for: .touchUpInside)
            
            return cell
        } else { //latest updates
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionTVC") as? HomeCollectionTVC else { return UITableViewCell() }
            cell.gridView.isHidden = true
            cell.collectionView.isHidden = false
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.tag = indexPath.section
            cell.selectionStyle = .none
            let layout = cell.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.scrollDirection = .horizontal
            cell.collectionView.register(UINib(nibName: "HomeLatestUpdatesCVC", bundle: nil), forCellWithReuseIdentifier: "HomeLatestUpdatesCVC")
            cell.collectionView.reloadData()
            return cell
        }
    }
}
//MARK:- Helper Methods
extension Home {
    
    @objc func onBtnReservations(sender: UIButton){
        guard let reservationFilter = self.storyboard?.instantiateViewController(withIdentifier: "ReservationFilter") as? ReservationFilter else { return }
        guard let reservation = self.storyboard?.instantiateViewController(withIdentifier: "Reservations") as? Reservations else { return }
        let nav = ReservationNavigation(menuViewController: UIViewController(), contentViewController: reservation)
        nav.navigationBar.isHidden = true
        nav.sideMenu = ENSideMenu(sourceView: nav.view, menuViewController: reservationFilter, menuPosition:.right)
        nav.sideMenu?.menuWidth = self.view.frame.size.width * 0.60
        self.present(nav, animated: true, completion: nil)
    }
    @objc func onBtnMenu(sender: UIButton){
        sender.setBackgroundImage(#imageLiteral(resourceName: "GoldenBg"), for: .highlighted)// = Global.APP_COLOR//setTitleColor(Global.APP_COLOR, for: .highlighted)
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "Menu") as? Menu {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @objc func onBtnGuestList(sender: UIButton){
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "EventBooking") as? EventBooking {
            dvc.isFromHome = true
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @objc func onBtnCompetition(sender: UIButton){
        let storyBoard = UIStoryboard(name: "BarOrdering", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "Competition") as? Competition {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @objc func onBtnMusic(sender: UIButton){
        
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "MusicPlayer") as? MusicPlayer {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
        
//        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
//        if let dvc = storyBoard.instantiateViewController(withIdentifier: "Music") as? Music {
//            self.navigationController?.pushViewController(dvc, animated: true)
//        }
    }
    @objc func onBtnBarOrdering(sender: UIButton){
        let storyBoard = UIStoryboard(name: "BarOrdering", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "BarOrdering") as? BarOrdering {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @objc func onBtnCavalliNights(sender: UIButton){
        let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "CavalliNights") as? CavalliNights {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    @objc func onBtnMembership(sender: UIButton){
        if Connectivity.isConnectedToInternet() {
            let storyBoard = UIStoryboard(name: "Mod3Hassan", bundle: nil)
            if let dvc = storyBoard.instantiateViewController(withIdentifier: "Membership") as? Membership {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        } else {
            Utility.showToast(message: "Internet connection seems to be offline")
            return
        }
       
    }
}

