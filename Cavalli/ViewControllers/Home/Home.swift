//
//  Home.swift
//  Cavalli
//
//  Created by shardha on 1/20/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import Realm

class Home: UIViewController, ITRAirSideMenuDelegate {

    //MARK:- Global Variables
    var arrayLatestUpdates = [LatestUpdatesModel]()
    
    //MARK:- IBOutlets
    @IBOutlet weak var textFieldSearch: UITextField!
    @IBOutlet weak var tableView: UITableView!
   
    //MARK:- View lifecycle 
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "HomeCollectionTVC", bundle: nil), forCellReuseIdentifier: "HomeCollectionTVC")
        self.processGetLatestUpdates()
        
        textFieldSearch.addTarget(self, action: #selector(actionSearch(_:)), for: .allEditingEvents)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.textFieldSearch.text = ""
    }
    //MARK:- IBActions
    @IBAction func actionSideMenu(_ sender: UIButton) {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.itAirSideMenu?.presentLeftMenuViewController()
    }
    
    @IBAction func actionProfile(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "BarOrdering", bundle: nil)
        if let dvc = storyboard.instantiateViewController(withIdentifier: "MyProfile") as? MyProfile {
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    
    @IBAction func actionSearch(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "SearchResult") as? SearchResult {
            //            dvc.arrSearchRecords = data
            //            dvc.searchedText = searchedText
            self.navigationController?.pushViewController(dvc, animated: true)
        }
        /* if self.textFieldSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty == false {
            self.processGetSeatchResults(text: self.textFieldSearch.text ?? "")
        } else{
            self.textFieldSearch.text = self.textFieldSearch.text?.trimmingCharacters(in: .whitespacesAndNewlines)
        }*/
    }
    //MARK:- Side Menu Controls
    func sideMenu(_ sideMenu: ITRAirSideMenu!, didRecognizePanGesture recognizer: UIPanGestureRecognizer!) {
        //print("didRecognizePanGesture")
    }
    func sideMenu(_ sideMenu: ITRAirSideMenu!, willShowMenuViewController menuViewController: UIViewController!) {
        //print("willShowMenuViewController")
    }
    func sideMenu(_ sideMenu: ITRAirSideMenu!, didShowMenuViewController menuViewController: UIViewController!) {
        //print("didShowMenuViewController")
    }
    func sideMenu(_ sideMenu: ITRAirSideMenu!, willHideMenuViewController menuViewController: UIViewController!) {
//        print("willHideMenuViewController")
    }
    func sideMenu(_ sideMenu: ITRAirSideMenu!, didHideMenuViewController menuViewController: UIViewController!) {
        //print("didHideMenuViewController")
    }
    
    //MARK:- Service
    private func processGetLatestUpdates() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        APIManager.sharedInstance.homeAPIManager.getLatestUpdatesWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<LatestUpdatesModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayLatestUpdates = response
            self.tableView.reloadSections(IndexSet(integer: 1), with: .automatic)
        }, failure: {
            (error) in
            Utility.hideLoader()
        })
    }
    private func pushToSearchResults(){//\_ data: [MyEventsModel] , searchedText : String){
        let storyBoard = UIStoryboard(name: "HomeModule", bundle: nil)
        if let dvc = storyBoard.instantiateViewController(withIdentifier: "SearchResult") as? SearchResult {
//            dvc.arrSearchRecords = data
//            dvc.searchedText = searchedText
            self.navigationController?.pushViewController(dvc, animated: true)
        }
    }
    private func processGetSeatchResults(text:String){
        Utility.showLoader()
        let params: [String: Any] = ["search_title": text]
        self.textFieldSearch.resignFirstResponder()
        APIManager.sharedInstance.homeAPIManager.getHomeSearchWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<MyEventsModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            if response.count == 0 {
                Utility.showToast(message: "No data found.")
            } else {
               // self.pushToSearchResults(response, searchedText: self.textFieldSearch.text ?? "")
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
        })
    }
}


