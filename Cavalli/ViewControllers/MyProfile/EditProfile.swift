//
//  EditProfile.swift
//  Cavalli
//
//  Created by shardha on 3/29/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import ObjectMapper
import DropDown
import SwiftDate

class EditProfile: UIViewController {

    //MARK:- Global declarations
    var imageChanged = false
    var pickedImage = #imageLiteral(resourceName: "image-placeholder")
    let imagePicker = UIImagePickerController()
    
    var strDOB : String = ""

    //MARK:- IBOUTlets
    @IBOutlet weak var textFieldFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var textFieldDOB: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldGender: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var btnGender: UIButton!

    private let genderDropDown = DropDown()
    
    var arrGender = ["Male","Female"]
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let user = AppStateManager.sharedInstance.loggedInUser
        self.textFieldFullName.text = user?.userName
        self.textFieldEmailAddress.text = user?.email
        self.textFieldPhone.text = user?.phone
        self.textFieldGender.text = user?.gender
        if let dob = user?.dateOfBirth {
            if dob != "" {
                self.textFieldDOB.text = Utility.customDateFormatter(dateStr: dob, dateFormat: "yyyy-MM-dd", formatteddate: "dd MMM yyyy")
            }
        }
        
        strDOB = (user?.dateOfBirth)!
        //self.imageProfile.sd_setImage(with: URL(string: (user?.imageUrl ?? "")!))
        
        self.imageProfile.sd_setShowActivityIndicatorView(true)
        self.imageProfile.sd_setIndicatorStyle(.gray)
        self.imageProfile.sd_setImage(with: URL(string: (user?.imageUrl ?? "")!), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        
        self.imagePicker.delegate = self
        
        onClickSelectCategory()
        setDatePicker()
    }
    
    // set Gender Dropdown...
    
    func onClickSelectCategory() {
        genderDropDown.anchorView = btnGender
        genderDropDown.direction = .bottom
        genderDropDown.dataSource = arrGender
        
        // Action triggered on selection
        genderDropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.textFieldGender?.text = self.arrGender[index]
        }
    }
    
    //MARK: Set Date picker
    func setDatePicker(){
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
//        let result = formatter.string(from: date)
//        textFieldDOB.text = result
//        strDOB = date.string(custom: "yyyy-MM-dd HH:mm:ss")
        
        let datePickerView = UIDatePicker()
        // Age of 18.
        
        // Age of 100.
        let MAXIMUM_AGE: Date = Calendar.current.date(byAdding: .year, value: -15, to: Date())!;
        datePickerView.datePickerMode = .date
//        datePickerView.minimumDate = MINIMUM_AGE
        datePickerView.maximumDate = MAXIMUM_AGE//date
        textFieldDOB.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    
    
    @objc func handleDatePicker(sender: UIDatePicker) {//2018-05-29
        let date = DateInRegion(absoluteDate: sender.date)
        //        if date.isToday || date.isInFuture {
        textFieldDOB.text = date.string(custom: "dd MMM yyyy")
        strDOB = date.string(custom: "yyyy-MM-dd")
        //        }
    }
    
    //MARK:- IBActions
    @IBAction func actionGenderDropDown(_ sender: Any) {
        genderDropDown.show()
    }
    

    @IBAction func actionImagePicker(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSaveChanges(_ sender: UIButton) {
        if self.textFieldFullName.text! == "" || self.textFieldPhone.text! == "" || self.textFieldDOB.text! == "" || self.textFieldGender.text! == "" {
            Utility.showToast(message: "Please enter required fields!")
            return
        }
        else {
            if self.validate() {
                self.processSumbitProfileChanges()
            }
        }
        
    }
    
    //MARK:- Helper Methods
    private func validate() -> Bool {
        if Validation.validateStringLength(self.textFieldFullName.text!) {
            if imageChanged {
                return true
            }else {
                //Utility.showToast(message: "Please update image.")
                return true
            }
        }else {
            Utility.showToast(message: "Invalid fullname.")
            return false
        }
    }
    func openCamera() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.camera
        imagePicker.allowsEditing = false
        imagePicker.cameraCaptureMode = .photo
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func openGallary() {
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = false
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    //MARK:- Services
    func processSumbitProfileChanges() {
        if self.pickedImage != #imageLiteral(resourceName: "image-placeholder") {
            Utility.showLoader()
            let image = UIImageJPEGRepresentation(self.pickedImage, 0.5)
            var params: [String: Any] = ["":""]
            params = ["user_name": self.textFieldFullName.text!,
                      "phone": self.textFieldPhone.text!,
                      "email": self.textFieldEmailAddress.text!,
                      "profile_image": image!]
            let strGender = textFieldGender.text?.lowercased()
            params.updateValue(strDOB, forKey: "dob")
            params.updateValue(strGender!, forKey: "gender")
            APIManager.sharedInstance.homeAPIManager.updateProfileWith(params: params, success: {
                (responseObject) in
                let response = Mapper<User>().map(JSON: responseObject as [String : Any])
                let user = User(value: response!)
                Utility.hideLoader()
                Utility.showToast(message: "Profile updated successfully.")
                AppStateManager.sharedInstance.loggedInUser = user
                try! Global.APP_REALM?.write {
                    Global.APP_REALM?.add(user, update: true)
                }
                self.navigationController?.popViewController(animated: true)
            }, failure: {
                (error) in
                Utility.hideLoader()
                Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
            })
        }else {
            Utility.showLoader()
            var params: [String: Any] = ["":""]
            params = ["user_name": self.textFieldFullName.text!,
                      "phone": self.textFieldPhone.text!,
                      "email": self.textFieldEmailAddress.text!]
            let strGender = textFieldGender.text?.lowercased()
            params.updateValue(strDOB, forKey: "dob")
            params.updateValue(strGender!, forKey: "gender")
            APIManager.sharedInstance.homeAPIManager.updateProfileWith(params: params, success: {
                (responseObject) in
                let response = Mapper<User>().map(JSON: responseObject as! [String : Any])
                let user = User(value: response)
                Utility.hideLoader()
                Utility.showToast(message: "Profile updated successfully.")
                AppStateManager.sharedInstance.loggedInUser = user
                try! Global.APP_REALM?.write {
                    Global.APP_REALM?.add(user, update: true)
                }
                self.navigationController?.popViewController(animated: true)
            }, failure: {
                (error) in
                Utility.hideLoader()
                Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
            })
        }
        
    }
}
extension EditProfile: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            self.pickedImage = pickedImage
            self.imageProfile.image = self.pickedImage
            self.imageChanged = true
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
