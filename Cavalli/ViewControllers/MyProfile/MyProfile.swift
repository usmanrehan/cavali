//
//  MyProfile.swift
//  Cavalli
//
//  Created by shardha on 3/29/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class MyProfile: UIViewController {
    
    //MARK:- IBoutlets
    @IBOutlet weak var labelEmail: UILabel!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var imageProfile: UIImageView!
    @IBOutlet weak var imageBanner: UIImageView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let user = AppStateManager.sharedInstance.loggedInUser
        self.labelName.text = user?.userName
        self.labelEmail.text = user?.email
        self.imageBanner.sd_setShowActivityIndicatorView(true)
        self.imageBanner.sd_setIndicatorStyle(.gray)
        self.imageBanner.sd_setImage(with: URL(string: (user?.imageUrl ?? "")!), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        self.imageProfile.sd_setShowActivityIndicatorView(true)
        self.imageProfile.sd_setIndicatorStyle(.gray)
        self.imageProfile.sd_setImage(with: URL(string: (user?.imageUrl ?? "")!), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
    }

    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionCategorySelect(_ sender: UIButton) {
        switch sender.tag {
        case 0:
            let storyBoard = UIStoryboard(name: "Mod3Hassan", bundle: nil)
            if let dvc = storyBoard.instantiateViewController(withIdentifier: "BarOrderHistoryContainer") as? BarOrderHistoryContainer {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
        case 1:
            let storyBoard = UIStoryboard(name: "usman_storyboard", bundle: nil)
            if let dvc = storyBoard.instantiateViewController(withIdentifier: "MyEvents") as? MyEvents {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
        case 2:
            let storyboard = UIStoryboard(name: "Mod4Hassan", bundle: nil)
            guard let reservationFilter = storyboard.instantiateViewController(withIdentifier: "MyGuestFilter") as? MyGuestFilter else { return }
            guard let reservation = storyboard.instantiateViewController(withIdentifier: "MyGuestContainer") as? MyGuestContainer else { return }
            let nav = ReservationNavigation(menuViewController: UIViewController(), contentViewController: reservation)
            nav.navigationBar.isHidden = true
            nav.sideMenu = ENSideMenu(sourceView: nav.view, menuViewController: reservationFilter, menuPosition:.right)
            nav.sideMenu?.menuWidth = self.view.frame.size.width * 0.60
            self.present(nav, animated: true, completion: nil)
            break
        case 3:
            let storyboard = UIStoryboard(name: "HomeModule", bundle: nil)
            guard let reservationFilter = storyboard.instantiateViewController(withIdentifier: "ReservationFilter") as? ReservationFilter else { return }
            guard let reservation = storyboard.instantiateViewController(withIdentifier: "Reservations") as? Reservations else { return }
            let nav = ReservationNavigation(menuViewController: UIViewController(), contentViewController: reservation)
            nav.navigationBar.isHidden = true
            nav.sideMenu = ENSideMenu(sourceView: nav.view, menuViewController: reservationFilter, menuPosition:.right)
            nav.sideMenu?.menuWidth = self.view.frame.size.width * 0.60
            self.present(nav, animated: true, completion: nil)
            break
        case 4:
            let storyBoard = UIStoryboard(name: "Mod3Hassan", bundle: nil)
            if let dvc = storyBoard.instantiateViewController(withIdentifier: "MembershipHistoryContainer") as? MembershipHistoryContainer {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
        case 6:
            let storyBoard = UIStoryboard(name: "BarOrdering", bundle: nil)
            if let dvc = storyBoard.instantiateViewController(withIdentifier: "Competition") as? Competition {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
        case 5:
            let storyboard = UIStoryboard(name: "HomeModule", bundle: nil)
            if let dvc = storyboard.instantiateViewController(withIdentifier: "GuestType") as? GuestListTypeController {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
        case 7:
            let storyBoard = UIStoryboard(name: "Mod3Hassan", bundle: nil)
            if let dvc = storyBoard.instantiateViewController(withIdentifier: "Messages") as? Messages {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
        default:
            break
        }
    }
}

