//
//  SideMenu.swift
//  Cavalli
//
//  Created by shardha on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
protocol ITAirSideMenuDelegate {
    func didTappedInternationRequest()
}
class SideMenu: UIViewController{

    //MARK:- Global declarations
    let arrayData = [("Event Calendar", #imageLiteral(resourceName: "event")), ("Cavalli Social", #imageLiteral(resourceName: "social")), ("Package Information", #imageLiteral(resourceName: "package")), ("International Request", #imageLiteral(resourceName: "international")), ("Gallery", #imageLiteral(resourceName: "gallery")), ("About Us", #imageLiteral(resourceName: "about")), ("Logout", #imageLiteral(resourceName: "logout"))]
    
    var delegate: ITAirSideMenuDelegate? = nil
    
    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)

        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.isScrollEnabled = false
    }
}
extension SideMenu: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC") as? SideMenuTVC else { return UITableViewCell() }
        let (name, image) = self.arrayData[indexPath.item]
        cell.imageIcon.image = image
        cell.labelTitle.text = name
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.074
    }
}
extension SideMenu: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.item {
        case 3:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.itAirSideMenu?.hideViewController()
            self.delegate?.didTappedInternationRequest()
            break
        case 6:
            let appD = Constants.APP_DELEGATE
            appD.showSplash()
        default:
            break
        }
    }
}
