
//
//  ExtentionHomeTableView.swift
//  Cavalli
//
//  Created by shardha on 1/23/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

extension Home: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section { //categories
        case 0:    // each row height = (self.tableView.frame.size.width/4 + 10)
            // multiplied by two because there are two row of collectionview
            // + 20 is extra padding
            return ((self.tableView.frame.size.width/4 + 10) * 2) + 20
        default: //latest updates
            return self.tableView.frame.size.width/2
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header: HomeHeader = .fromNib()
        header.frame = CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 100)
        switch section {
        case 0:
            header.labelTitle.text = "CATEGORIES"
            header.buttonSeeAll.isHidden = true
            break
        case 1:
            header.labelTitle.text = "LATEST UPDATES"
            header.buttonSeeAll.isHidden = false
            header.buttonSeeAll.addTarget(self, action: #selector(self.actionSeeAllLatestUpdates), for: .touchUpInside)
            break
        default:
            break
        }
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    @objc func actionSeeAllLatestUpdates() {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "LatestUpdates") as? LatestUpdates {
            dvc.arrayLatestUpdate = self.arrayLatestUpdates
        self.navigationController?.pushViewController(dvc, animated: true)
    }
    
    
}
}

extension Home: UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 { //categories
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionTVC") as? HomeCollectionTVC else { return UITableViewCell() }
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.tag = indexPath.section
            cell.collectionView.register(UINib(nibName: "HomeCVC", bundle: nil), forCellWithReuseIdentifier: "HomeCVC")
            cell.collectionView.isScrollEnabled = false
            cell.selectionStyle = .none
            return cell
        }else { //latest updates
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCollectionTVC") as? HomeCollectionTVC else { return UITableViewCell() }
            cell.collectionView.delegate = self
            cell.collectionView.dataSource = self
            cell.collectionView.tag = indexPath.section
            cell.selectionStyle = .none
            let layout = cell.collectionView.collectionViewLayout as! UICollectionViewFlowLayout
            layout.scrollDirection = .horizontal
            cell.collectionView.register(UINib(nibName: "HomeLatestUpdatesCVC", bundle: nil), forCellWithReuseIdentifier: "HomeLatestUpdatesCVC")
            cell.collectionView.reloadData()
            return cell
        }
    }
}
