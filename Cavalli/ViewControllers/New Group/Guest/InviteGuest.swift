//
//  InviteGuest.swift
//  Cavalli
//
//  Created by shardha on 2/1/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class InviteGuest: UIViewController{

    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.register(UINib(nibName: "InviteGuestTVC", bundle: nil), forCellReuseIdentifier: "InviteGuestTVC")
    }
}
extension InviteGuest: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "InviteGuestTVC") as? InviteGuestTVC else { return UITableViewCell() }
        return cell
    }
}
extension InviteGuest: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
}
