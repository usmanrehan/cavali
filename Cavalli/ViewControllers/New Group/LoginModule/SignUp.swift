//
//  SignUp.swift
//  Cavalli
//
//  Created by shardha on 1/19/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import FBSDKLoginKit
import Validator
import ObjectMapper


class SignUp: UIViewController{
    
    //MARK:- Global declarations
    private var socialMediaId = ""
    private var socialMediaType = ""
    
    //MARK:- IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var textFieldName: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldEmail: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldPassword: SkyFloatingLabelTextFieldWithIcon!
    @IBOutlet weak var textFieldConfirmPassword: SkyFloatingLabelTextFieldWithIcon!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

         //Do any additional setup after loading the view.
        
//        self.textFieldName.text = "testUser"
//        self.textFieldEmail.text = "bifijorul@uemail99.com"
//        self.textFieldPhone.text = "+923557776237"
//        self.textFieldPassword.text = "123456"
//        self.textFieldConfirmPassword.text = "123456"
        
        
        self.scrollView.delegate = self
        self.textFieldName.delegate = self
        self.textFieldEmail.delegate = self
        self.textFieldPhone.delegate = self
        self.textFieldPassword.delegate = self
        self.textFieldConfirmPassword.delegate = self
    }

   //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionSignUp(_ sender: UIButton) {
        self.validate()
    }
    @IBAction func actionFB(_ sender: UIButton) {
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: ["email"], from: self) { (result, error) in
            if (error == nil){
                let fbloginresult : FBSDKLoginManagerLoginResult = result!
                if fbloginresult.grantedPermissions != nil {
                    if(fbloginresult.grantedPermissions.contains("email"))
                    {
                        self.getFBUserData()
                        fbLoginManager.logOut()
                    }
                }
            }
        }
    }
    @IBAction func actionLogin(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nameEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validateName(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.nameNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func emailEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidEmail(textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.emailNotValid.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func phoneEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validatePhone(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.PhoneNumber.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func passwordEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isValidePassword(value: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.passwordInValidShort.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    @IBAction func confirmPasswordChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.isConfirmPasswordIsEqualToPassword(password: self.textFieldPassword.text!, confirm: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.passwordNotMatchShort.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
   
    //MARK: - HelperMethods
    func checkAllFieldsAreNotEmpty() -> Bool {
        if(!(self.textFieldEmail.text?.isEmpty)! && !(self.textFieldPassword.text?.isEmpty)! && !(self.textFieldPassword.text?.isEmpty)! && !(self.textFieldConfirmPassword.text?.isEmpty)!){
            return true
        }
        
        return false
        
    }
    func validateName() -> Bool {
        return Validation.validateName(input: self.textFieldName.text!)
    }
    func validateEmailAddress() -> Bool {
        if(Validation.isValidEmail(self.textFieldEmail.text!)) {
            return true
        }
        return false
    }
    func validationPassword() -> Bool {
        if(Validation.isValidePassword(value: self.textFieldPassword.text!)) {
            return true
        }
        return false
    }
    func validConfirmPassword() -> Bool {
        if(Validation.isConfirmPasswordIsEqualToPassword(password: self.textFieldPassword.text!, confirm: self.textFieldConfirmPassword.text!)) {
            return true
        }
        
        return false
    }
    func validatePhone() -> Bool {
        return Validation.validatePhone(input: self.textFieldPhone.text!)
    }
    private func validate()                      {
        if(self.checkAllFieldsAreNotEmpty() && self.validateEmailAddress() && self.validationPassword() && self.validConfirmPassword() && self.validatePhone()) {
            if self.socialMediaId == "" {
                self.processRegisterUser(isSocialMedia: false)
            }else {
                self.processRegisterUser(isSocialMedia: true)
            }
        }
    }
    
    //MARK:- Services
    func getFBUserData(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id, name, picture.type(large), email"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
                    var dict = result as! [String : AnyObject]
                    print(dict["id"] as! String)
                    print(dict["name"] as! String)
                    print(dict["email"] as! String)
                    let name = dict["name"] as! String
                    let email = dict["email"] as! String
                    self.socialMediaId = dict["id"] as! String
                    self.socialMediaType = "facebook"
                    self.textFieldName.text = name
                    self.textFieldEmail.text = email
                }
            })
        }
    }
    func processRegisterUser(isSocialMedia: Bool) {
        Utility.showLoader()
        var params: [String: Any] = ["":""]
        if isSocialMedia {
            params = ["user_name": self.textFieldName.text!,
                      "email": self.textFieldEmail.text!,
                      "phone": self.textFieldPhone.text!,
                      "password": self.textFieldPassword.text!,
                      "socialmedia_id": self.socialMediaId,
                      "socialmedia_type": self.socialMediaType,
                      "device_type": "ios",
                      "device_token": "123456789"]
        }else {
            params = ["user_name": self.textFieldName.text!,
                      "email": self.textFieldEmail.text!,
                      "phone": self.textFieldPhone.text!,
                      "password": self.textFieldPassword.text!,
                      "device_type": "ios",
                      "device_token": "123456789"]
        }
        print(params)
        APIManager.sharedInstance.userAPIManager.registerWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            //            let response = responseObject as NSDictionary
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user = User(value: response)
            //            print(responseObject)
            //            print(user)
            AppStateManager.sharedInstance.loggedInUser = user
            try! Global.APP_REALM?.write {
                Global.APP_REALM?.add(user, update: true)
            }
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "Verification") as? Verification {
                dvc.user = user
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
        })
        
    }
}
extension SignUp: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
extension SignUp: UIScrollViewDelegate{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if scrollView.contentOffset.x != 0 {
            scrollView.contentOffset.x = 0
        }
    }
}
