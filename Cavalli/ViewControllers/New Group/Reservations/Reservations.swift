//
//  Reservations.swift
//  Cavalli
//
//  Created by shardha on 1/30/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Reservations: UIViewController, ENSideMenuDelegate, MyReservationProtocol {

    //MARK:- Global declarations
    var pageMenu : CAPSPageMenu?
    
    //MARK:- IBOutlets
    @IBOutlet weak var buttonFilter: UIButton!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        
        self.sideMenuController()?.sideMenu?.delegate = self
        // Do any additional setup after loading the view.
        // Do any additional setup after loading the view.
        // Array to keep track of controllers in page menu
        var controllerArray : [UIViewController] = []
        
        // Create variables for all view controllers you want to put in the
        // page menu, initialize them, and add each to the controller array.
        // (Can be any UIViewController subclass)
        // Make sure the title property of all view controllers is set
        // Example:
        let controllerReservationForm = self.storyboard?.instantiateViewController(withIdentifier: "ReservationForm") as! ReservationForm
        let controllerMyReservation = self.storyboard?.instantiateViewController(withIdentifier: "MyReservation") as! MyReservation
        
        controllerReservationForm.parentNavigationController = self.navigationController
        controllerMyReservation.parentNavigationController = self.navigationController
        
        controllerReservationForm.title = "Reserve Now"
        
        controllerMyReservation.title = "My Reservations"
        controllerMyReservation.delegate = self
        controllerArray.append(controllerReservationForm)
        controllerArray.append(controllerMyReservation)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        
        //let bgColor = UIColor(red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(20),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.black),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(UIColor.white),
            .BottomMenuHairlineColor(UIColor.white),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(20),
            .MenuHeight(40)
            
        ]
        
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.height - 80), pageMenuOptions: parameters)
        
        pageMenu?.view.backgroundColor = UIColor.white
        self.view.addSubview(pageMenu!.view)
    }

   
    
    // MARK: - ENSideMenu Delegate
    func sideMenuWillOpen() {
        //print("sideMenuWillOpen")
    }
    
    func sideMenuWillClose() {
        //print("sideMenuWillClose")
    }
    
    func sideMenuShouldOpenSideMenu() -> Bool {
        //print("sideMenuShouldOpenSideMenu")
        return true
    }
    
    func sideMenuDidClose() {
        //print("sideMenuDidClose")
    }
    
    func sideMenuDidOpen() {
        //print("sideMenuDidOpen")
    }
    

    @IBAction func actionBack(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func acitonFilter(_ sender: UIButton) {
           toggleSideMenuView()
    }
    
    func shouldHideFilter() {
        self.buttonFilter.isHidden = true
    }
    func shouldShowFilter() {
        self.buttonFilter.isHidden = false
    }
    
}
