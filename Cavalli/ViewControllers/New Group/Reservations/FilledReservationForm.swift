//
//  FilledReservationForm.swift
//  Cavalli
//
//  Created by shardha on 3/5/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import DateTimePicker
protocol FilledReservation {
    func didCompletedEditing()
    func didEndEditing()
}
class FilledReservationForm: UIViewController {

    //MARK:- Global declaration
    var reservationData = MyReservationModel()
    var selectedPeople = ""
    var selectedDate = ""
    var delegate: FilledReservation? = nil
    
    //MARK:- IBOutlets
    @IBOutlet weak var textFieldReservationTitle: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldFullName: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldEmailAddress: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var textFieldSecondaryPhone: SkyFloatingLabelTextField!
    @IBOutlet weak var labelNumberOfPeople: UILabel!
    @IBOutlet weak var lableDate: UILabel!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.textFieldReservationTitle.text = reservationData.title ?? ""
        self.textFieldFullName.text = AppStateManager.sharedInstance.loggedInUser.userName ?? ""
        self.textFieldEmailAddress.text = AppStateManager.sharedInstance.loggedInUser.email ?? ""
        self.textFieldPhone.text = AppStateManager.sharedInstance.loggedInUser.phone ?? ""
        self.textFieldSecondaryPhone.text = self.reservationData.secondaryPhoneNo
        self.lableDate.text = self.reservationData.date
        self.labelNumberOfPeople.text = "\(self.reservationData.noPeople ?? "0")"
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.delegate?.didEndEditing()
    }
    
    //MARK:- IBActions
    @IBAction func actionSaveChanges(_ sender: UIButton) {
        self.validate()
    }
    @IBAction func actionCancel(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func actionDropDown(_ sender: UIButton) {
        
        let actionSheetController = UIAlertController(title: "Number Of People", message: "Please select number of people", preferredStyle: .actionSheet)
        let ten = UIAlertAction(title: "10", style: .default) { action -> Void in
            self.selectedPeople = "10"
            self.labelNumberOfPeople.text = "10"
        }
        actionSheetController.addAction(ten)
        
        let twenty = UIAlertAction(title: "20", style: .default) { action -> Void in
            self.selectedPeople = "20"
            self.labelNumberOfPeople.text = "20"
        }
        actionSheetController.addAction(twenty)
        
        let thirty = UIAlertAction(title: "30", style: .default) { action -> Void in
            self.selectedPeople = "30"
            self.labelNumberOfPeople.text = "30"
        }
        actionSheetController.addAction(thirty)
        
        let fifty = UIAlertAction(title: "50", style: .default) { action -> Void in
            self.selectedPeople = "50"
            self.labelNumberOfPeople.text = "50"
        }
        actionSheetController.addAction(fifty)
        
        let cancle = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
        }
        actionSheetController.addAction(cancle)
        self.present(actionSheetController, animated: true, completion: nil)
    }
    @IBAction func actionDate(_ sender: Any) {
        let min = Date()
        //let max = Date().addingTimeInterval(60 * 60 * 24 * 4)
        let picker = DateTimePicker.show(minimumDate: min, maximumDate: nil)
        picker.highlightColor =  Global.APP_COLOR
        picker.darkColor = UIColor.darkGray
        picker.doneButtonTitle = "Done"
        picker.todayButtonTitle = "Today"
        picker.is12HourFormat = true
        picker.dateFormat = "hh:mm aa dd/MM/YYYY"
        //        picker.isDatePickerOnly = true
        picker.completionHandler = { date in
            let formatter = DateFormatter()
            //formatter.dateFormat = "dd-MM-YY"
            formatter.dateFormat = "YYYY-MM-dd HH:mm:ss"
            self.selectedDate = formatter.string(from: date)
            self.lableDate.text = self.selectedDate
            
        }
    }
    
    //MARK:- Helper methods
    func validate() {
        if !(self.textFieldReservationTitle.text?.isEmpty)! {
            if (self.textFieldReservationTitle.text == self.reservationData.title) && (self.lableDate.text == self.reservationData.date) && (self.labelNumberOfPeople.text == "\(self.reservationData.noPeople)") {
                //
                Utility.showToast(message: "Please edit details first before saving changes.", controller: self)
            }else {
                self.processEditReservation()
            }
        }else {
            Utility.showToast(message: "Reservation title is empty.", controller: self)
        }
    }
    
    //MARK:- Services
    func processEditReservation() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["id":self.reservationData.id,
                                     "date": self.lableDate.text!,
                                     "secondary_phone_no": self.textFieldSecondaryPhone.text!,
                                     "no_people": self.labelNumberOfPeople.text!,
                                     "title": self.textFieldReservationTitle.text!]
        APIManager.sharedInstance.homeAPIManager.editReservationWith(params: params, success: {
            (responseObject) in
            
            Utility.hideLoader()
            self.delegate?.didCompletedEditing()
            self.navigationController?.popViewController(animated: true)
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            //print(error.localizedFailureReason ?? "Error")
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}
