//
//  Settings.swift
//  Cavalli
//
//  Created by shardha on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Settings: UIViewController{
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.register(UINib(nibName: "SettingsNotificationTVC", bundle: nil), forCellReuseIdentifier: "SettingsNotificationTVC")
        self.tableView.register(UINib(nibName: "SettingsGeneralTVC", bundle: nil), forCellReuseIdentifier: "SettingsGeneralTVC")
    }
}
extension Settings: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.item {
        case 0:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsNotificationTVC") as? SettingsNotificationTVC else { return UITableViewCell() }
            return cell
        case 1:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
            cell.labelTtile.text = "Change Password"
            return cell
        case 2:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
            cell.labelTtile.text = "Billing Detail"
            return cell
        case 3:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
            cell.labelTtile.text = "Privacy Policy"
            return cell
        case 4:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
            cell.labelTtile.text = "Terms & Conditions"
            return cell
        default:
            break
        }
        return UITableViewCell()
    }
}
extension Settings: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.item {
        case 1:
            let story = UIStoryboard(name: "LoginModule", bundle: nil)
            if let dvc = story.instantiateViewController(withIdentifier: "ChangePassword") as? ChangePassword {
                self.navigationController?.pushViewController(dvc, animated: true)
            }
        case 3:
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "TCandPP") as? TCandPP {
                dvc.isPrivacy = true
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
        case 4:
            if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "TCandPP") as? TCandPP {
                dvc.isPrivacy = false
                self.navigationController?.pushViewController(dvc, animated: true)
            }
            break
            
        default:
            break
        }
    }
}
