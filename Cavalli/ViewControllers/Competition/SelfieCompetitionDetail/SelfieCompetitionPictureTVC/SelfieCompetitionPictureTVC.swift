//
//  SelfieCompetitionPictureTVC.swift
//  Cavalli
//
//  Created by shardha on 3/28/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class SelfieCompetitionPictureTVC: UITableViewCell {

    @IBOutlet weak var imageSelfie: UIImageView!
    @IBOutlet weak var buttonCamera: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
