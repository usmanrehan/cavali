//
//  Competition.swift
//  Cavalli
//
//  Created by shardha on 3/27/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Competition: UIViewController {

    //MARK:- Global declaration
    var pageMenu : CAPSPageMenu?
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: true)
        
        var controllerArray : [UIViewController] = []
        
        let controllerReservationForm = self.storyboard?.instantiateViewController(withIdentifier: "LatestCompetition") as! LatestCompetition
        let controllerMyReservation = self.storyboard?.instantiateViewController(withIdentifier: "CompetitionHistory") as! CompetitionHistory
        controllerReservationForm.parentNavigationController = self.navigationController
        controllerMyReservation.parentNavigationController = self.navigationController
        controllerReservationForm.title = "Latest Competitions"//"latest competitions"
        controllerMyReservation.title = "History"
        controllerArray.append(controllerReservationForm)
        controllerArray.append(controllerMyReservation)
        
        // Customize page menu to your liking (optional) or use default settings by sending nil for 'options' in the init
        // Example:
        
        //let bgColor = UIColor(displayP3Red: 0/255.0, green: 0/255.0, blue: 0/255.0, alpha: 0.5)
        
        let goldenColor = UIColor(displayP3Red: 162/255.0, green: 146/255.0, blue: 100/255.0, alpha: 1.0)
        
        let parameters: [CAPSPageMenuOption] = [
            .MenuItemSeparatorWidth(20),
            .UseMenuLikeSegmentedControl(true),
            .MenuItemSeparatorPercentageHeight(0.0),
            .ScrollMenuBackgroundColor(UIColor.black),
            .SelectionIndicatorColor(goldenColor),
            .SelectedMenuItemLabelColor(goldenColor),
            .UnselectedMenuItemLabelColor(UIColor.white),
            .BottomMenuHairlineColor(UIColor.white),
            .AddBottomMenuHairline(false),
            .EnableHorizontalBounce(false),
            .MenuMargin(20),
            .MenuHeight(40)
        ]
        pageMenu = CAPSPageMenu(viewControllers: controllerArray, frame: CGRect(x: 0, y: 100, width: self.view.frame.size.width, height: self.view.frame.height - 80), pageMenuOptions: parameters)
        pageMenu?.view.backgroundColor = UIColor.white
        self.view.addSubview(pageMenu!.view)
    }
    
    //MARK:- IBActions
    @IBAction func actionBack(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
