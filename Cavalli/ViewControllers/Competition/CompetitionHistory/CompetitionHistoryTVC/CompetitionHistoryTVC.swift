//
//  CompetitionHistoryTVC.swift
//  Cavalli
//
//  Created by shardha on 3/28/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class CompetitionHistoryTVC: UITableViewCell {
    
    @IBOutlet weak var labelDesc: UILabel!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageProduct: UIImageView!
    @IBOutlet weak var lableStatus: UILabel!
    @IBOutlet weak var imageStatus: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
