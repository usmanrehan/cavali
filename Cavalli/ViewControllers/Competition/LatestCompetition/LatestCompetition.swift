//
//  LatestCompetition.swift
//  Cavalli
//
//  Created by shardha on 3/28/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
class LatestCompetition: UIViewController{
    
    //MARK:- Global declaration
    var parentNavigationController: UINavigationController?
    var arrayCompetitionModel = [CompetitionModel]()
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.tableView.dataSource = self
        self.tableView.delegate = self
//        self.tableView.rowHeight = UITableViewAutomaticDimension
//        self.tableView.estimatedRowHeight = 100
        self.tableView.register(UINib(nibName: "LatestCompetitionTVC", bundle: nil), forCellReuseIdentifier: "LatestCompetitionTVC")
        self.processGetCompetitionData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.processGetCompetitionData()
    }
    
    //MARK: - Service
    func processGetCompetitionData() {
        Utility.showLoader()
        let params: [String: Any] = ["":""]
        print(params)
        APIManager.sharedInstance.homeAPIManager.getCompetitionDataWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<CompetitionModel>().mapArray(JSONArray: responseObject as! [[String : Any]])
            self.arrayCompetitionModel = response
            try! Global.APP_REALM?.write {
                Global.APP_REALM?.add(response, update: true)
            }
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "")
        })
    }
}
extension LatestCompetition : UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayCompetitionModel.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "LatestCompetitionTVC") as? LatestCompetitionTVC else { return UITableViewCell() }
        
        cell.labelTitle.text = self.arrayCompetitionModel[indexPath.item].title
        cell.labelDesc.text = self.arrayCompetitionModel[indexPath.item].descriptionValue
        cell.labelSerial.text = "Serial: " + (self.arrayCompetitionModel[indexPath.item].serialNo ?? "")
        //MMMM yyyy
        let formatter = DateFormatter()
        formatter.dateFormat = "YYYY-MM-dd"
        let dt = formatter.date(from: self.arrayCompetitionModel[indexPath.item].validDate ?? "2018/01/01")
        formatter.dateFormat = "MMMM yyyy"
        cell.labelExpiry.text = "Valid to \(formatter.string(from: dt!))"
        //cell.imageProduct.sd_setImage(with: URL(string: self.arrayCompetitionModel[indexPath.item].imageUrl!))
        cell.imageProduct.sd_setShowActivityIndicatorView(true)
        cell.imageProduct.sd_setIndicatorStyle(.gray)
        cell.imageProduct.sd_setImage(with: URL(string: self.arrayCompetitionModel[indexPath.item].imageUrl!), placeholderImage: #imageLiteral(resourceName: "image-placeholder"), options: .refreshCached, progress: nil, completed: nil)
        return cell
    }
}
extension LatestCompetition : UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "SelfieCompetitionDetail") as? SelfieCompetitionDetail {
            dvc.competitionModel = self.arrayCompetitionModel[indexPath.item]
            self.parentNavigationController?.pushViewController(dvc, animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130//UITableViewAutomaticDimension
    }
}
