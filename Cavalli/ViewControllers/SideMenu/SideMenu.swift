//
//  SideMenu.swift
//  Cavalli
//
//  Created by shardha on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
protocol ITAirSideMenuDelegate {
    func didTappedInternationRequest()
    func didTappedTermsAndCondittions()
    func didTappedCavalliSocial()
    func didTappedGallery()
    func didTapPrivacyPolicy()
    func didTappedAboutUs()
}
class SideMenu: UIViewController{
    var window: UIWindow?
    //MARK:- Global declarations ("Event Calendar", #imageLiteral(resourceName: "event")),
    let arrayData = [("Cavalli Social", #imageLiteral(resourceName: "social")), ("International Request", #imageLiteral(resourceName: "international")), ("Gallery", #imageLiteral(resourceName: "gallery")), ("About Us", #imageLiteral(resourceName: "about")), ("Privacy Policy", #imageLiteral(resourceName: "privacy")), ("Terms & Conditions", #imageLiteral(resourceName: "terms")), ("Logout", #imageLiteral(resourceName: "logout"))]
    let LogoutAlertView = LogoutPopUpView.instanceFromNib() as! LogoutPopUpView
    var delegate: ITAirSideMenuDelegate? = nil
    
    //MARK:- IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LogoutAlertView.btnYes.addTarget(self, action: #selector(processLogoutUser), for: .touchUpInside)
        LogoutAlertView.btnNo.addTarget(self, action: #selector(onBtnNo), for: .touchUpInside)
        
        // Do any additional setup after loading the view.
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.changeStatusBarColor(clear: false)
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.isScrollEnabled = false
    }
    @IBAction func ActionMoveNext(_ sender: Any) {
        print("Tapped")
    }
    @objc func onBtnYes(){
        
        AppStateManager.sharedInstance.logOutUser()
        let appD = Constants.APP_DELEGATE
        appD.showSplash()
        let window = UIApplication.shared.keyWindow!
        for item in window.subviews{
            if item.tag == 101{
                item.removeFromSuperview()
            }
        }
    }
    @objc func onBtnNo(){
        let window = UIApplication.shared.keyWindow!
        for item in window.subviews{
            if item.tag == 101{
                item.removeFromSuperview()
            }
        }
        LogoutAlertView.removeAnimate()
    }
}
extension SideMenu: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrayData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "SideMenuTVC") as? SideMenuTVC else { return UITableViewCell() }
        let (name, image) = self.arrayData[indexPath.item]
        cell.imageIcon.image = image
        cell.labelTitle.text = name
//        cell.backgroundColor = UIColor.red
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.view.frame.size.height * 0.074
    }
}
extension SideMenu: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.item {
//        case 0:
//            let appDelegate = UIApplication.shared.delegate as! AppDelegate
//            appDelegate.itAirSideMenu?.hideViewController()
//            self.delegate?.didTappedEventCalendar()
//            break
        case 0:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.itAirSideMenu?.hideViewController()
            self.delegate?.didTappedCavalliSocial()
        case 1:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.itAirSideMenu?.hideViewController()
            self.delegate?.didTappedInternationRequest()
            break
        case 2:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.itAirSideMenu?.hideViewController()
            self.delegate?.didTappedGallery()
        case 4:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.itAirSideMenu?.hideViewController()
            self.delegate?.didTapPrivacyPolicy()
        case 5:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.itAirSideMenu?.hideViewController()
            self.delegate?.didTappedTermsAndCondittions()
        case 3:
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.itAirSideMenu?.hideViewController()
            self.delegate?.didTappedAboutUs()
        case 6:
            self.LogoutAlertView.frame = self.view.frame
            self.LogoutAlertView.showAnimate()
            self.LogoutAlertView.tag = 101
            let window = UIApplication.shared.keyWindow!
            window.addSubview(self.LogoutAlertView)
        default:
            break
        }
    }
}

extension SideMenu {
    //MARK:- Services
    @objc func processLogoutUser() {
        self.view.endEditing(true)
        Utility.showLoader()
        let params: [String: Any] = ["device_token": Constants.device_token]
        APIManager.sharedInstance.userAPIManager.logOuWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.onBtnYes()
//            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
//            let user = User(value: response!)
//            if user.active == "0" {
//                let appDelegate = Constants.APP_DELEGATE
//                appDelegate.showVerification(user: user)
//            }else {
//                try! Global.APP_REALM?.write {
//                    AppStateManager.sharedInstance.loggedInUser = user
//                    Global.APP_REALM?.add(user, update: true)
//                }
//                let appDelegate = Constants.APP_DELEGATE
//                appDelegate.showHome()
//            }
//            print(user)
            
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: (error.localizedFailureReason ?? "Error"), controller: self)
        })
    }
}

