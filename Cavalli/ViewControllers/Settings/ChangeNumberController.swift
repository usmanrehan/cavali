//
//  ChangeNumberController.swift
//  Cavalli
//
//  Created by shardha kawal on 10/19/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField
import ObjectMapper
import CountryPickerView

class ChangeNumberController: UIViewController {

    @IBOutlet weak var textNumber: SkyFloatingLabelTextFieldWithIcon?
    
    @IBOutlet weak var lblPlaceHolder: UILabel!
    weak var cpvTextField: CountryPickerView!

    var isFromVerifcation : Bool? = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if isFromVerifcation == true {
            textNumber?.placeholder = "Enter New Number"
        }
        else {
            textNumber?.placeholder = "Enter Old Number"
        }
        lblPlaceHolder?.text = textNumber?.placeholder
        // Do any additional setup after loading the view.
        
        let cp = CountryPickerView(frame: CGRect(x: 0, y: 5, width: 90, height: (textNumber?.frame.size.height)!))
        cp.flagImageView.translatesAutoresizingMaskIntoConstraints = true
        cp.countryDetailsLabel.translatesAutoresizingMaskIntoConstraints = true
        cp.flagImageView.frame = CGRect(x: 0, y: 12, width: 30, height: (textNumber?.frame.size.height)! - 10)
        cp.countryDetailsLabel.frame = CGRect(x: 35, y: 12, width: 100, height: (textNumber?.frame.size.height)! - 10)
        cp.countryDetailsLabel.font = UIFont.init(name: "Poppins-Regular", size: 17)!
        textNumber?.leftView = cp
        
        textNumber?.textColor = UIColor.black
        textNumber?.leftViewMode = .always
        
        self.cpvTextField = cp
        self.cpvTextField.countryDetailsLabel.textColor = UIColor.black
        self.cpvTextField.flagImageView.image = #imageLiteral(resourceName: "phone")
        textNumber?.showDoneButtonOnKeyboard()
        
        self.cpvTextField.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func validatePhone() -> Bool {
        return Validation.validatePhone(input: self.textNumber!.text!)
    }
    
    @IBAction func phoneEditingChanged(_ textfield: SkyFloatingLabelTextField) {
        if(!Validation.validatePhone(input: textfield.text!)) {
            textfield.errorColor = UIColor.red
            textfield.errorMessage = AlertMessages.PhoneNumber.rawValue
        }
        else {
            textfield.errorMessage = ""
        }
    }
    
    @IBAction func actionChangeNumber(_ sender: Any) {
        if textNumber?.text?.count != 0  {
            if self.validatePhone() {
                requestVerifyNumber()
            }
        } else {
            Utility.main.showAlert(message: "Please \((textNumber?.placeholder)!)", title: "Message")
        }
    }
    
    @IBAction func actionBack(_ sender: UIButton) {
        self.view.endEditing(true)
        self.navigationController?.popViewController(animated: true)
    }
    
    func requestUpdateNumber() {
        Utility.showLoader()
        let params: [String: Any] = ["phone": (textNumber?.text)!]
        APIManager.sharedInstance.homeAPIManager.updatePhoneNumberWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            self.navigationController?.popToRootViewController(animated: true)
        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
    }
    
    
    func requestVerifyNumber() {
        var params: [String: Any]
        Utility.showLoader()
        let phoneNum = self.cpvTextField.countryDetailsLabel.text! + (textNumber?.text)!

        if isFromVerifcation == true {
            params = ["phone": phoneNum, "type": "new"]
        } else {
            params = ["phone": phoneNum, "type": "old"]
        }
        APIManager.sharedInstance.homeAPIManager.verifyPhoneNumberWith(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let response = Mapper<User>().map(JSON: responseObject as [String : Any])
            let user = User(value: response!)

            
                if self.isFromVerifcation == true {
                    if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "Verification") as? Verification {
                        dvc.user = user
                        let phoneNum = self.cpvTextField.countryDetailsLabel.text! + (self.textNumber?.text)!
                        dvc.strPhoneNum = phoneNum
                        dvc.isMobileNumVerified = true
                        Utility.showToast(message: "Code has been sent to your phone number")
                        self.navigationController?.pushViewController(dvc, animated: true)
                    }
                }
                else {
                    if let dvc = self.storyboard?.instantiateViewController(withIdentifier: "ChangeNumber") as? ChangeNumberController {
                        dvc.isFromVerifcation = true
                        self.navigationController?.pushViewController(dvc, animated: true)
                    }
                    //dvc.isFromMobileNumVerification = true
                }
               
//            }
            Utility.hideLoader()

        }, failure: {
            (error) in
            Utility.hideLoader()
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ChangeNumberController: CountryPickerViewDelegate{
    
    
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country)
        self.view.endEditing(true)
        self.cpvTextField.countryDetailsLabel.text = country.phoneCode// "(\(country.code)) \(country.phoneCode)"
        self.cpvTextField.flagImageView.image = #imageLiteral(resourceName: "phone")
    }
}
