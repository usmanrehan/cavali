//
//  Settings.swift
//  Cavalli
//
//  Created by shardha on 1/24/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit

class Settings: UIViewController{
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        self.tableView.estimatedRowHeight = 50
        self.tableView.register(UINib(nibName: "SettingsNotificationTVC", bundle: nil), forCellReuseIdentifier: "SettingsNotificationTVC")
        self.tableView.register(UINib(nibName: "SettingsGeneralTVC", bundle: nil), forCellReuseIdentifier: "SettingsGeneralTVC")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIApplication.shared.isStatusBarHidden = false
    }
    override var prefersStatusBarHidden: Bool {
        return false
    }
    
    @objc func onBtnChangeNotification(sender: UISwitch) {
        print(sender.isOn)
        let status = (sender.isOn == false) ? "0" :  "1"
//        print(sender.hashValue)
        processRequestPushOnOff(notify_status: status)
    }
}
extension Settings {
    func processRequestPushOnOff(notify_status: String) {
        Utility.showLoader()
        let params: [String: Any] = ["notify_status":notify_status]
        print(params)
        APIManager.sharedInstance.homeAPIManager.methodePushOnOff(params: params, success: {
            (responseObject) in
            Utility.hideLoader()
            let user = User()
            user.updatedAt = AppStateManager.sharedInstance.loggedInUser.createdAt
            user.email = AppStateManager.sharedInstance.loggedInUser.email
            user.token = AppStateManager.sharedInstance.loggedInUser.token
            user.active = AppStateManager.sharedInstance.loggedInUser.active
            user.isNotify = notify_status
            user.verificationCode = AppStateManager.sharedInstance.loggedInUser.verificationCode
            user.status = AppStateManager.sharedInstance.loggedInUser.status
            user.id = AppStateManager.sharedInstance.loggedInUser.id
            user.createdAt = AppStateManager.sharedInstance.loggedInUser.createdAt
            user.phone = AppStateManager.sharedInstance.loggedInUser.phone
            user.userName = AppStateManager.sharedInstance.loggedInUser.userName
            user.imageUrl = AppStateManager.sharedInstance.loggedInUser.imageUrl
            AppStateManager.sharedInstance.loggedInUser = user
            print(AppStateManager.sharedInstance.loggedInUser)
            
            try! Global.APP_REALM?.write {
                AppStateManager.sharedInstance.loggedInUser = user
                Global.APP_REALM?.add(user, update: true)
            }
            self.tableView.reloadData()
        }, failure: {
            (error) in
            Utility.hideLoader()
            //Utility.showAlert(message: error.localizedFailureReason ?? "", title: Utility.NSLocalizedString("Alert"))
            Utility.showToast(message: error.localizedFailureReason ?? "", controller: self)
        })
    }
}
extension Settings: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if AppStateManager.sharedInstance.loggedInUser.socialMediaId == "" {
            return 5
        } else {
            return 4
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if AppStateManager.sharedInstance.loggedInUser.socialMediaId == "" {
            switch indexPath.item {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsNotificationTVC") as? SettingsNotificationTVC else { return UITableViewCell()
                }
                cell.pushOnOff?.addTarget(self, action: #selector(onBtnChangeNotification(sender:)), for: .valueChanged)
                if AppStateManager.sharedInstance.loggedInUser.isNotify == "0" {
                    cell.pushOnOff?.setOn(false, animated: true)
                } else {
                    cell.pushOnOff?.setOn(true, animated: true)
                }
                return cell
            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
                cell.labelTtile.text = "Change Password"
                return cell
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
                cell.labelTtile.text = "Edit Profile"
                return cell
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
                cell.labelTtile.text = "Change Number"
                return cell
            case 4:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
                cell.labelTtile.text = "Version 1.0"
                return cell
            default:
                break
            }
        }
        else {
            switch indexPath.item {
            case 0:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsNotificationTVC") as? SettingsNotificationTVC else { return UITableViewCell()
                }
                cell.pushOnOff?.addTarget(self, action: #selector(onBtnChangeNotification(sender:)), for: .valueChanged)
                if AppStateManager.sharedInstance.loggedInUser.isNotify == "0" {
                    cell.pushOnOff?.setOn(false, animated: true)
                } else {
                    cell.pushOnOff?.setOn(true, animated: true)
                }
                return cell
            case 2:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
                cell.labelTtile.text = "Change Number"
                return cell
            case 3:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
                cell.labelTtile.text = "Version 1.0"
                return cell
            case 1:
                guard let cell = tableView.dequeueReusableCell(withIdentifier: "SettingsGeneralTVC") as? SettingsGeneralTVC else { return UITableViewCell() }
                cell.labelTtile.text = "Edit Profile"
                return cell
            default:
                break
            }
        }
        return UITableViewCell()
    }
}
extension Settings: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return  UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if AppStateManager.sharedInstance.loggedInUser.socialMediaId == "" {
            switch indexPath.item {
            case 2:
                let story = UIStoryboard(name: "LoginModule", bundle: nil)
                if let dvc = story.instantiateViewController(withIdentifier: "ChangePassword") as? ChangePassword {
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
            case 1:
                let storyBoard = UIStoryboard(name: "BarOrdering", bundle: nil)
                if let dvc = storyBoard.instantiateViewController(withIdentifier: "EditProfile") as? EditProfile {
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
                break
            case 3:
                let story = UIStoryboard(name: "LoginModule", bundle: nil)
                if let dvc = story.instantiateViewController(withIdentifier: "ChangeNumber") as? ChangeNumberController {
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
                break
            default:
                break
            }
        } else {
            switch indexPath.item {
            case 1:
                let storyBoard = UIStoryboard(name: "BarOrdering", bundle: nil)
                if let dvc = storyBoard.instantiateViewController(withIdentifier: "EditProfile") as? EditProfile {
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
                break
            case 2:
                let story = UIStoryboard(name: "LoginModule", bundle: nil)
                if let dvc = story.instantiateViewController(withIdentifier: "ChangeNumber") as? ChangeNumberController {
                    self.navigationController?.pushViewController(dvc, animated: true)
                }
                break
            default:
                break
            }
        }
    }
}
