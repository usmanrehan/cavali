import UIKit
import RealmSwift
class AppStateManager: NSObject {

    
    static let sharedInstance = AppStateManager()
    var loggedInUser: User!
    var realm: Realm!
    var shouldShowFilter = false

    override init() {
       
        super.init()
        
        if(!(realm != nil)){
            realm = try! Realm()
        }
        
        loggedInUser = realm.objects(User.self).first

    }
    
    func isUserLoggedIn() -> Bool{
        
        if (self.loggedInUser) != nil {
            
            if self.loggedInUser.isInvalidated {
                return false
            }
            
            return true
        }
        
        return false
    }

    
    func logOutUser()   {
        if(!(realm != nil)){
            realm = try! Realm()
        }
        try! realm.write {
            realm.delete(loggedInUser)
        }
        self.loggedInUser = nil
    }
    
    func updateUser(user: User)   {
        self.loggedInUser = user
        try! Global.APP_REALM?.write {
            realm.add(self.loggedInUser, update: true)
        }
    }

}
