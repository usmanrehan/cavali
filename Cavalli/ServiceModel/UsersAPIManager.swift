    //
//  UsersAPIManager.swift
//  Mahalati
//
//  Created by Hassan Khan on 4/20/17.
//  Copyright © 2017 Muzamil Hassan. All rights reserved.
//

import UIKit
import Alamofire

class UserAPIManager: APIManagerBase {
//    //MARK: - /Register
    func registerWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.Register)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    
    func loginWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.Login)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    
    func logOuWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.Logout)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func socialLoginWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.SocialLogin)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    func forgotPasswordWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.ForgotPassword)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    func verificationCodeWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.Verification)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    func addInternationalRequestWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.AddInternationalRequest)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func addInquiryWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.AddInquiry)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func changePasswordWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.ChangePassword)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func setNewPasswordWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.ResetPassword)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    
//    //MARK: - /Lang
//    func changeLanguageWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
//        let route: URL = POSTURLforRoute(route: Constants.lang)!
//        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
//    }
//    func logOutWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
//        let route: URL = POSTURLforRoute(route: Constants.logout)!
//        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
//    }
//    //MARK: - /Verify Code
//    func verifyCodeWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
//        let route: URL = POSTURLforRoute(route: Constants.verifyCode)!
//        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
//    }
//    //MARK: - /UpdateProfile
//    func updateProfileWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
//        let route: URL = POSTURLforRoute(route: Constants.updateProfile)!
//        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure)
//    }
}
