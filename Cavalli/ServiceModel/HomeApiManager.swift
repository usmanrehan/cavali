//
//  TechnicianApiManager.swift
//  Yellow Cap
//
//  Created by Hassan Khan on 6/5/17.
//  Copyright © 2017 Hassan Khan. All rights reserved.
//
import UIKit
import Alamofire

class HomeApiManager: APIManagerBase {
    func getLatestUpdatesWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.LatestUpdates)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func methodePushOnOff(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.PushOnOff)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func addReservationWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.AddReservation)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getMyReservationWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.MyReservation)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func getReservationSlotsWith(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.ReservationSlots)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: [:], success: success, failure: failure, withHeaders: true)
    }
    
    func getCavalliSocialOptions(success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.getSocialLinks)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: [:], success: success, failure: failure, withHeaders: true)
    }
    
    func editReservationWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.EditReservation)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getMenuCategoriesWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.MenuCategories)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getMenuProductWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetMenuProducts)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getCMSWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.CMS)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
    }
    func getBarDataWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.BarData)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getCompetitionDataWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.CompetitionData)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getCompetitionHistoryWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.CompetitionHistory)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func submitCompetitionWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.PaticipateCompetition)!
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure)
    }
    func updateProfileWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.UpdateProfile)!
        print(route)
        self.postRequestWithMultipart(route: route, parameters: params, success: success, failure: failure)
    }
    func addOrderWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.AddOrder)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getTaxWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.Tax)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getMarkedEventsWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.MarkedEvents)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func markUnMarkEventWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.MarkUnMarkEvent)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getEventsWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetEvents)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getLiveMusicInfo(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetLiveMusicInfo)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //MARK: - Module3
    func getThreadDataWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetThreads)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func getChatDataWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetChat)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func addMessageWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.AddMessage)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func deleteNotificationWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.DeleteNotifications)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func getOrderHistoryDataWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetOrderHistoy)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func getMembershipDataWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetMembership)!
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func buySubscriptionWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.BuySubscription)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func getMembershipHistoryDataWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetMembershipHistory)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getNotificationsWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetNotifications)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getMyEventsWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetMyEvents)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getGalleryWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetGallery)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func getCategoryWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetGalleryCategories)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
//    func getCategoryWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
//        let route: URL = GETURLfor(route: Constants.GetGalleryCategories)!
//        print(route)
//        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
//    }
    func getHomeSearchWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.HomeSearch)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func getCMSDatahWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GET_CMS)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: false)
       // self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    //Module 4
    //MARK: - Module 4
    func getGuestListCategoriesWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GuestListCategories)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getGuestListWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GuestList)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func addGuestListWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.AddGuestList)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
        
    }
    func editGuestListWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.EditGuestList)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getMembersWith(params: Parameters, success: @escaping DefaultArrayResultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GuestList)!
        print(route)
        self.getRequestArrayWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func addGuestMemberWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.AddGuestListMember)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
        
    }
    func editGuestWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.EditGuest)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func deleteGuestWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.DeleteGuest)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getGuestHistoryDataWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetGuestHistoy)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getHomeSearchDetailWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.HomeSearchDetail)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func checkMarkStatusWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.CheckMarkStatus)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func updateDeviceTokenWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.UpdateDeviceToken)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    func getCancleReservationWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = GETURLfor(route: Constants.GetCancleReservation)!
        print(route)
        self.getRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func updatePhoneNumberWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.updatePhoneNo)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
    
    func verifyPhoneNumberWith(params: Parameters, success: @escaping DefaultAPISuccessClosure, failure: @escaping DefaultAPIFailureClosure) {
        let route: URL = POSTURLforRoute(route: Constants.checkPhoneNo)!
        print(route)
        self.postRequestWith(route: route, parameters: params, success: success, failure: failure, withHeaders: true)
    }
}
