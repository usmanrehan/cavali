//
//  AppDelegate.swift
//  Cavalli
//
//  Created by shardha on 1/16/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import Hero
import IQKeyboardManagerSwift
import AMXFontAutoScale
import FBSDKLoginKit
import RealmSwift
import UserNotifications

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var itAirSideMenu: ITRAirSideMenu?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        var controller = UIViewController()
        application.statusBarView?.isHidden = false
        controller.setNeedsStatusBarAppearanceUpdate()

        UIApplication.shared.applicationIconBadgeNumber = 0
        self.pushRequest(application: application)
        self.processRealmMigration()
        // Override point for customization after application launch.
        IQKeyboardManager.sharedManager().disabledToolbarClasses = [MessagesDetail.self] //of type UIViewController
        IQKeyboardManager.sharedManager().disabledTouchResignedClasses = [MessagesDetail.self]
        IQKeyboardManager.sharedManager().disabledDistanceHandlingClasses = [MessagesDetail.self]
        
        IQKeyboardManager.sharedManager().enable = true
        UILabel.amx_autoScaleFont(forReferenceScreenSize: .size5p5Inch)
        UIApplication.shared.statusBarStyle = .lightContent
        self.changeStatusBarColor(clear: false)
        //self.showBar()
        self.checkUser()
        
        return FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
    
    }
    
    
    func applicationWillResignActive(_ application: UIApplication) {
        FBSDKAppEvents.activateApp()
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    func checkUser() {
        if AppStateManager.sharedInstance.isUserLoggedIn() {
            if AppStateManager.sharedInstance.loggedInUser.active == "0" {
                self.showSplash()
            }else {
                self.showHome()
            }
        }else {
            self.showSplash()
        }
    }
    func showVerification(user: User) {
        let storyboard = UIStoryboard(name: "LoginModule", bundle: nil)
        let root = storyboard.instantiateViewController(withIdentifier: "Verification") as! Verification
        root.user = user
        let navigationController = UINavigationController(rootViewController: root)
        navigationController.navigationBar.isHidden = true
        navigationController.isHeroEnabled = true
        navigationController.heroNavigationAnimationType = .none
        self.window?.rootViewController = navigationController
        
    }
    func showSplash()  {
        let storyboard = UIStoryboard(name: "LoginModule", bundle: nil)
        let root = storyboard.instantiateViewController(withIdentifier: "Splash") as! Splash
        let navigationController = UINavigationController(rootViewController: root)
        navigationController.navigationBar.isHidden = true
        navigationController.isHeroEnabled = true
        navigationController.heroNavigationAnimationType = .none
        self.window?.rootViewController = navigationController
    }
    
    //BarOrdering
    func showBar()  {
        let storyboard = UIStoryboard(name: "BarOrdering", bundle: nil)
        let root = storyboard.instantiateViewController(withIdentifier: "BarOrdering") as! BarOrdering
        let navigationController = UINavigationController(rootViewController: root)
        navigationController.navigationBar.isHidden = true
        navigationController.isHeroEnabled = true
        navigationController.heroNavigationAnimationType = .none
        self.window?.rootViewController = navigationController
    }
    
    func showHome() {
        let storyboard = UIStoryboard(name: "HomeModule", bundle: nil)
        let home = storyboard.instantiateViewController(withIdentifier: "MainContainer") as! MainContainer
        let sideMenu = storyboard.instantiateViewController(withIdentifier: "SideMenu") as! SideMenu
        sideMenu.delegate = home
        
        let navigationController = UINavigationController(rootViewController: home)
        navigationController.navigationBar.isHidden = true
        let itairSideMenu = ITRAirSideMenu(contentViewController: navigationController, leftMenuViewController: sideMenu)
        
        //optional delegate to receive menu view status
        //itairSideMenu?.delegate = home
        
        //content view shadow properties
        itairSideMenu?.contentViewShadowColor = UIColor.blue
        itairSideMenu?.contentViewShadowOffset = CGSize(width: 0, height: 0)
        itairSideMenu?.contentViewShadowOpacity = 0.6
        itairSideMenu?.contentViewShadowRadius = 12
        itairSideMenu?.contentViewShadowEnabled = false
        itairSideMenu?.panGestureEnabled = false
        itairSideMenu?.panFromEdge = false
        
        //content view animation properties
        itairSideMenu?.contentViewScaleValue = 1.0
        itairSideMenu?.contentViewRotatingAngle = 15.0
        itairSideMenu?.contentViewTranslateX = 200.0
        
        //menu view properties
        itairSideMenu?.menuViewRotatingAngle = 20.0
        itairSideMenu?.menuViewTranslateX = 130.0
        
        self.itAirSideMenu = itairSideMenu
        
        self.window?.backgroundColor = UIColor(displayP3Red: 27/255.0, green: 27/255.0, blue: 31/255.0, alpha: 0.0)
        
        UIView.transition(with: self.window!, duration: 0.7, options: UIViewAnimationOptions.transitionFlipFromBottom, animations: {
            self.window?.rootViewController = itairSideMenu
        }, completion: nil)
    
    }

    func changeStatusBarColor(clear: Bool) {
        if clear {
            UIApplication.shared.statusBarView?.backgroundColor = UIColor(displayP3Red: 27/255.0, green: 27/255.0, blue: 31/255.0, alpha: 0.0)
        }else {
            UIApplication.shared.statusBarView?.backgroundColor = UIColor(displayP3Red: 27/255.0, green: 27/255.0, blue: 31/255.0, alpha: 1.0)
        }
    }
    func processRealmMigration() {
        var config = Realm.Configuration()
        config.deleteRealmIfMigrationNeeded = true
        
        Realm.Configuration.defaultConfiguration = config
    }
}

extension AppDelegate: UNUserNotificationCenterDelegate{
    func pushRequest(application:UIApplication){
        let center  = UNUserNotificationCenter.current()
        center.delegate = self
        // set the type as sound or badge
        center.requestAuthorization(options: [.sound,.alert,.badge]) { (granted, error) in
            // Enable or disable features based on authorization
            if granted{
                print("access granted")
            } else {
                Utility.showToast(message: "Please grant access for push notification") 
            }
        }
        application.registerForRemoteNotifications()
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        var token: String = ""
        for i in 0..<deviceToken.count {
            token += String(format: "%02.2hhx", deviceToken[i] as CVarArg)
        }
        Constants.device_token = token
        if AppStateManager.sharedInstance.isUserLoggedIn(){
            self.processUpdateDeviceToken()
        }
    }
    func processUpdateDeviceToken(){
        let params: [String: Any] = ["device_token": Constants.device_token,
                                     "device_type": "ios"]
        print(params)
        APIManager.sharedInstance.homeAPIManager.updateDeviceTokenWith(params: params, success: {
            (responseObject) in
            print(Constants.device_token)
        }, failure: {
            (error) in
            print(error.localizedDescription)
        })
    }
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("fail to register notification")
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,  willPresent notification: UNNotification, withCompletionHandler   completionHandler: @escaping (_ options:   UNNotificationPresentationOptions) -> Void) {
        completionHandler([.badge,.alert,.sound])
        print("Handle push from foreground")
        // custom code to handle push while app is in the foreground
        print("\(notification.request.content.userInfo)")
        let userInfo = notification.request.content.userInfo
        let aps = userInfo["aps"] as? NSDictionary
        let notifyObject = aps!["alert"] as AnyObject
        print(notifyObject)
        if let type : String = (notifyObject["type"])! as? String {
            if type == "payment_success" {
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
            }
            else {
                NotificationCenter.default.post(name: Notification.Name("NotificationIdentifierFail"), object: nil)
            }

        }
        
        
        
      
    }
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Handle push from background or closed")
        // if you set a member variable in didReceiveRemoteNotification, you  will know if this is from closed or background
        //let notificationData = response.notification.request.content.userInfo["aps"] as! NSDictionary
        let userInfo = response.notification.request.content.userInfo
        let aps = userInfo["aps"] as? NSDictionary
        print(aps)
    }
}

extension UIApplication {
    var statusBarView: UIView? {
        return value(forKey: "statusBar") as? UIView
    }
}



extension String {
    
    func currencyFormatter() -> String {
        

//        let largeNumber = 31908551587
        let str = Double(self)
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = NumberFormatter.Style.decimal
        let formattedNumber = numberFormatter.string(from: NSNumber(value:str!))
        print(formattedNumber)
        return formattedNumber!
    }
    
}
