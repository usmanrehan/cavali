
//
//  ValidationError.swift
//  CatchMapp
//
//  Created by Hassan Khan on 10/25/17.
//  Copyright © 2017 Hassan Khan. All rights reserved.
//

import Foundation
enum ValidationErrors: String, Error {
    case invalidName = "Invalid name."
    case invalidPassword = "Password should be atleast six characters long."
    case invalidEmail = "Invalid email."
    case passwordDontMatch = "Password and confirm password don't match."
    case invalidPhone = "Phone number should consist of eight digits."
    case invalidDoB = "Date of birth should not be empty."
    case myprofile = "Do you want to save Changes?"
    case location = "Please allow CatchMapps to access your location."
    case terms = "Please agree to terms and conditions."
    case hiddenProfile = "You cannot see the user's profile which is hidden."
    var message: String { return self.rawValue }
}
