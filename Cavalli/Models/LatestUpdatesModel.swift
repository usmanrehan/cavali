//
//  LatestUpdates.swift
//
//  Created by Hamza Hasan on 3/13/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper_Realm
import ObjectMapper
import RealmSwift
import Realm

public class LatestUpdatesModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let isActive = "is_active"
    static let descriptionValue = "description"
    static let title = "title"
    static let imageUrl = "image_url"
    static let deletedAt = "deleted_at"
    static let logoImage = "logo_image"
    static let newsImages = "news_images"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isActive: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var deletedAt: String? = ""
  @objc dynamic var logoImage: String? = ""
    var newsImages = List<LatestImageModel>()

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
        self.init()
    }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    isActive <- map[SerializationKeys.isActive]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    title <- map[SerializationKeys.title]
    imageUrl <- map[SerializationKeys.imageUrl]
    deletedAt <- map[SerializationKeys.deletedAt]
    logoImage <- map[SerializationKeys.logoImage]
    
    newsImages <- (map[SerializationKeys.newsImages], ListTransform<LatestImageModel>())
  }


}
