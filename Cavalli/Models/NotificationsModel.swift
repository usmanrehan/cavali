//
//  NotificationsModel.swift
//
//  Created by Hamza Hasan on 4/12/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class NotificationsModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let isRead = "is_read"
    static let isActive = "is_active"
    static let createdAt = "created_at"
    static let message = "message"
    static let userId = "user_id"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isRead: String? = ""
  @objc dynamic var isActive: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var message: String? = ""
  @objc dynamic var userId: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    isRead <- map[SerializationKeys.isRead]
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
    message <- map[SerializationKeys.message]
    userId <- map[SerializationKeys.userId]
  }
}


public class CavalliSocialModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let key = "key"
        static let value = "value"
        static let createdAt = "created_at"
        static let deletedAt = "deleted_at"
        static let updatedAt = "updated_at"
        
        static let image = "image"
        static let image_url = "image_url"
        static let is_social = "is_social"

    }
 
    
    // MARK: Properties
    @objc dynamic var id = 0
    @objc dynamic var key: String? = ""
    @objc dynamic var value: String? = ""
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var deletedAt: String? = ""
    @objc dynamic var updatedAt: String? = ""

    @objc dynamic var image_url: String? = ""
    @objc dynamic var image: String? = ""
    @objc dynamic var is_social = 0
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        updatedAt <- map[SerializationKeys.updatedAt]
        id <- map[SerializationKeys.id]
        key <- map[SerializationKeys.key]
        value <- map[SerializationKeys.value]
        createdAt <- map[SerializationKeys.createdAt]
        deletedAt <- map[SerializationKeys.deletedAt]
        image <- map[SerializationKeys.image]
        image_url <- map[SerializationKeys.image_url]
        is_social <- map[SerializationKeys.is_social]
    }
}

