//
//  MyReservationModel.swift
//
//  Created by Hamza Hasan on 3/13/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift

public class MyReservationModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let reservationStatusId = "reservation_status_id"
        static let status = "status"
        static let updatedAt = "updated_at"
        static let id = "id"
        static let noPeople = "no_people"
        static let isActive = "is_active"
        static let createdAt = "created_at"
        static let date = "date"
        static let title = "title"
        static let userId = "user_id"
        static let secondaryPhoneNo = "secondary_phone_no"
        static let time = "time"
        static let reservation_slot_id = "reservation_slot_id"

        static let day = "day"
        static let eventId = "event_id"
        static let reservationType = "reservation_type"


    }

    // MARK: Properties
    @objc dynamic var reservation_slot_id = 0

    @objc dynamic var reservationStatusId = 0
    @objc dynamic var status: String? = ""
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var noPeople: String? = ""
    @objc dynamic var isActive: String? = ""
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var date: String? = ""
    @objc dynamic var title: String? = ""
    @objc dynamic var userId = 0
    @objc dynamic var secondaryPhoneNo: String? = ""
    @objc dynamic var time: String? = ""
    
    @objc dynamic var day : String? = ""
    @objc dynamic var eventId = 0
    @objc dynamic var reservationType: String? = ""

    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        reservation_slot_id <- map[SerializationKeys.reservation_slot_id]
        reservationStatusId <- map[SerializationKeys.reservationStatusId]
        status <- map[SerializationKeys.status]
        updatedAt <- map[SerializationKeys.updatedAt]
        id <- map[SerializationKeys.id]
        noPeople <- map[SerializationKeys.noPeople]
        isActive <- map[SerializationKeys.isActive]
        createdAt <- map[SerializationKeys.createdAt]
        date <- map[SerializationKeys.date]
        title <- map[SerializationKeys.title]
        userId <- map[SerializationKeys.userId]
        secondaryPhoneNo <- map[SerializationKeys.secondaryPhoneNo]
        time <- map[SerializationKeys.time]
        
        day <- map[SerializationKeys.day]
        reservationType <- map[SerializationKeys.reservationType]
        eventId <- map[SerializationKeys.eventId]

    }
    
    
}

public class ReservationSlotModel: Object, Mappable {

    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let name = "name"
        static let from = "from"
        static let to = "to"
        static let interval = "interval"
        static let created_at = "created_at"
        static let updated_at = "updated_at"
        static let deleted_at = "deleted_at"
    }
    
    // MARK: Properties
    
    @objc dynamic var id = 0
    @objc dynamic var name: String? = ""
    @objc dynamic var from: String? = ""
    @objc dynamic var to: String? = ""
    @objc dynamic var interval = 0
    @objc dynamic var created_at: String? = ""
    @objc dynamic var updated_at: String = ""
    @objc dynamic var deleted_at: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map[SerializationKeys.id]
        name <- map[SerializationKeys.name]
        from <- map[SerializationKeys.from]
        to <- map[SerializationKeys.to]
        interval <- map[SerializationKeys.interval]
        created_at <- map[SerializationKeys.created_at]
        updated_at <- map[SerializationKeys.updated_at]
        deleted_at <- map[SerializationKeys.deleted_at]
        
    }
    
    
}
