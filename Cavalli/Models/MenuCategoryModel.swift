//
//  MenuCategoryModel.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift

public class MenuCategoryModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let name = "name"
        static let parentId = "parent_id"
        static let categoryTypeId = "category_type_id"
        static let categoryProduct = "category_product"

        static let id = "id"
        static let image = "image"
        static let isActive = "is_active"
        static let updatedAt = "updated_at"
        static let categoryImage = "category_image"
        static let deletedAt = "deleted_at"
    }
    
    // MARK: Properties "category_product" = detail;
    @objc dynamic var name: String? = ""
    @objc dynamic var parentId = 0
    @objc dynamic var categoryTypeId = 0
    @objc dynamic var id = 0
    @objc dynamic var image: String? = ""
    @objc dynamic var isActive: String? = ""
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var categoryImage: String? = ""
    @objc dynamic var deletedAt: String? = ""
    @objc dynamic var categoryProduct: String? = ""

  // MARK: ObjectMapper Initializers
  // Map a JSON object to this class using ObjectMapper.
  //
  // - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
        self.init()
    }

    override public class func primaryKey() -> String? {
        return "id"
    }

  // Map a JSON object to this class using ObjectMapper.
  //
  // - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        name <- map[SerializationKeys.name]
        parentId <- map[SerializationKeys.parentId]
        categoryTypeId <- map[SerializationKeys.categoryTypeId]
        id <- map[SerializationKeys.id]
        image <- map[SerializationKeys.image]
        isActive <- map[SerializationKeys.isActive]
        updatedAt <- map[SerializationKeys.updatedAt]
        categoryImage <- map[SerializationKeys.categoryImage]
        deletedAt <- map[SerializationKeys.deletedAt]
        categoryProduct <- map[SerializationKeys.categoryProduct]

    }
}
