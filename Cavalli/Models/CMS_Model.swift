//
//  CMS_Model.swift
//  Cavalli
//
//  Created by shardha kawal on 8/7/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class CMS_Model: Object, Mappable {
   
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let cmsTypeId = "cms_type_id"
        static let descriptionValue = "description"
        static let title = "title"
        static let isActive = "is_active"
        static let type = "type"
        static let createdAt = "created_at"
        static let deletedAt = "deleted_at"
        static let updatedAt = "updated_at"
       
        
    }
    
    
    // MARK: Properties
    @objc dynamic var id = 0
    @objc dynamic var isActive: String? = ""
    @objc dynamic var cmsTypeId = 0

    @objc dynamic var descriptionValue: String? = ""
    
    @objc dynamic var title: String? = ""
    
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var deletedAt: String? = ""
    @objc dynamic var createdAt: String? = ""

    @objc dynamic var type: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        id <- map[SerializationKeys.id]
        isActive <- map[SerializationKeys.isActive]
        descriptionValue <- map[SerializationKeys.descriptionValue]
        title <- map[SerializationKeys.title]
        cmsTypeId <- map[SerializationKeys.cmsTypeId]
        type <- map[SerializationKeys.type]

        createdAt <- map[SerializationKeys.createdAt]
        deletedAt <- map[SerializationKeys.deletedAt]
        updatedAt <- map[SerializationKeys.updatedAt]
    }

}

