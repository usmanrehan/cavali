//
//  SenderDetail.swift
//
//  Created by Hamza Hasan on 4/9/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class SenderDetail: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let email = "email"
    static let profileImage = "profile_image"
    static let active = "active"
    static let isNotify = "is_notify"
    static let verificationCode = "verification_code"
    static let status = "status"
    static let id = "id"
    static let createdAt = "created_at"
    static let phone = "phone"
    static let imageUrl = "image_url"
    static let userName = "user_name"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var profileImage: String? = ""
  @objc dynamic var active: String? = ""
  @objc dynamic var isNotify: String? = ""
  @objc dynamic var verificationCode: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var phone: String? = ""
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var userName: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    email <- map[SerializationKeys.email]
    profileImage <- map[SerializationKeys.profileImage]
    active <- map[SerializationKeys.active]
    isNotify <- map[SerializationKeys.isNotify]
    verificationCode <- map[SerializationKeys.verificationCode]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
    phone <- map[SerializationKeys.phone]
    imageUrl <- map[SerializationKeys.imageUrl]
    userName <- map[SerializationKeys.userName]
  }


}
