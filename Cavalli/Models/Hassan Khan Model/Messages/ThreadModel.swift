//
//  ThreadModel.swift
//
//  Created by Hamza Hasan on 4/9/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ThreadModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let requestTypeId = "request_type_id"
    static let isActive = "is_active"
    static let receiverId = "receiver_id"
    static let senderId = "sender_id"
    static let id = "id"
    static let isRead = "is_read"
    static let createdAt = "created_at"
    static let title = "title"
    static let message = "message"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var requestTypeId: String? = ""
  @objc dynamic var isActive: String? = ""
  @objc dynamic var receiverId: String? = ""
  @objc dynamic var senderId: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isRead: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var message: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    requestTypeId <- map[SerializationKeys.requestTypeId]
    isActive <- map[SerializationKeys.isActive]
    receiverId <- map[SerializationKeys.receiverId]
    senderId <- map[SerializationKeys.senderId]
    id <- map[SerializationKeys.id]
    isRead <- map[SerializationKeys.isRead]
    createdAt <- map[SerializationKeys.createdAt]
    title <- map[SerializationKeys.title]
    message <- map[SerializationKeys.message]
  }


}
