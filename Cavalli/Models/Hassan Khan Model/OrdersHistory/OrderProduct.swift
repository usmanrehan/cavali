//
//  OrderProduct.swift
//
//  Created by Hamza Hasan on 4/7/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class OrderProduct: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let productId = "product_id"
    static let orderId = "order_id"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let isActive = "is_active"
    static let createdAt = "created_at"
    static let productDetail = "product_detail"
    static let quantity = "quantity"
    static let price = "price"
  }

  // MARK: Properties
  @objc dynamic var productId = 0
  @objc dynamic var orderId = 0
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isActive: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var productDetail: ProductDetail?
  @objc dynamic var quantity = 0
  @objc dynamic var price = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    productId <- map[SerializationKeys.productId]
    orderId <- map[SerializationKeys.orderId]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
    productDetail <- map[SerializationKeys.productDetail]
    quantity <- map[SerializationKeys.quantity]
    price <- map[SerializationKeys.price]
  }


}
