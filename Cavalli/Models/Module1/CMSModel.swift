//
//  CMSModel.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CMSModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let isActive = "is_active"
    static let descriptionValue = "description"
    static let title = "title"
    static let type = "type"
    static let cmsTypeId = "cms_type_id"
    static let deletedAt = "deleted_at"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isActive: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var type: String? = ""
  @objc dynamic var cmsTypeId = 0
  @objc dynamic var deletedAt: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    isActive <- map[SerializationKeys.isActive]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    title <- map[SerializationKeys.title]
    type <- map[SerializationKeys.type]
    cmsTypeId <- map[SerializationKeys.cmsTypeId]
    deletedAt <- map[SerializationKeys.deletedAt]
  }


}
