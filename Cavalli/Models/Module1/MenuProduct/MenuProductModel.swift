//
//  MenuProductModel.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm
public class MenuProductModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let name = "name"
    static let parentId = "parent_id"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let isActive = "is_active"
    static let categoryTypeId = "category_type_id"
    static let products = "products"
    static let deletedAt = "deleted_at"
  }

  // MARK: Properties
  @objc dynamic var name: String? = ""
  @objc dynamic var parentId = 0
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isActive: String? = ""
  @objc dynamic var categoryTypeId = 0
  var products = List<Products>()
  @objc dynamic var deletedAt: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    name <- map[SerializationKeys.name]
    parentId <- map[SerializationKeys.parentId]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    isActive <- map[SerializationKeys.isActive]
    categoryTypeId <- map[SerializationKeys.categoryTypeId]
    products <- (map[SerializationKeys.products], ListTransform<Products>())
    deletedAt <- map[SerializationKeys.deletedAt]
  }


}
