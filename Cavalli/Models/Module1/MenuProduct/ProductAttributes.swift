//
//  ProductAttributes.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm
public class ProductAttributes: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let productId = "product_id"
    static let value = "value"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let key = "key"
    static let isActive = "is_active"
    static let createdAt = "created_at"
  }

  // MARK: Properties
  @objc dynamic var productId = 0
  @objc dynamic var value: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var key: String? = ""
  @objc dynamic var isActive: String? = ""
  @objc dynamic var createdAt: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    productId <- map[SerializationKeys.productId]
    value <- map[SerializationKeys.value]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    key <- map[SerializationKeys.key]
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
  }


}
