//
//  Products.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm
public class Products: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let isActive = "is_active"
    static let descriptionValue = "description"
    static let categoryId = "category_id"
    static let price = "price"
    static let logoImage = "logo_image"
    static let productAttributes = "product_attributes"
    static let id = "id"
    static let productImages = "product_images"
    static let createdAt = "created_at"
    static let title = "title"
    static let imageUrl = "image_url"
    static let deletedAt = "deleted_at"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var isActive: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var categoryId = 0
  @objc dynamic var price: String? = ""
  @objc dynamic var logoImage: String? = ""
  var productAttributes = List<ProductAttributes>()
  @objc dynamic var id = 0
  var productImages = List<ProductImages>()
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var deletedAt: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    isActive <- map[SerializationKeys.isActive]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    categoryId <- map[SerializationKeys.categoryId]
    price <- map[SerializationKeys.price]
    logoImage <- map[SerializationKeys.logoImage]
    productAttributes <- (map[SerializationKeys.productAttributes], ListTransform<ProductAttributes>())
    id <- map[SerializationKeys.id]
    productImages <- (map[SerializationKeys.productImages], ListTransform<ProductImages>())
    createdAt <- map[SerializationKeys.createdAt]
    title <- map[SerializationKeys.title]
    imageUrl <- map[SerializationKeys.imageUrl]
    deletedAt <- map[SerializationKeys.deletedAt]
  }


}
