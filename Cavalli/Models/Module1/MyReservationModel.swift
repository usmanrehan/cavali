//
//  MyReservationModel.swift
//
//  Created by Hamza Hasan on 3/13/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift

public class MyReservationModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let reservationStatusId = "reservation_status_id"
        static let status = "status"
        static let updatedAt = "updated_at"
        static let id = "id"
        static let noPeople = "no_people"
        static let isActive = "is_active"
        static let createdAt = "created_at"
        static let date = "date"
        static let title = "title"
        static let userId = "user_id"
        static let secondaryPhoneNo = "secondary_phone_no"
    }
    
    // MARK: Properties
    @objc dynamic var reservationStatusId = 0
    @objc dynamic var status: String? = ""
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var noPeople: String? = ""
    @objc dynamic var isActive: String? = ""
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var date: String? = ""
    @objc dynamic var title: String? = ""
    @objc dynamic var userId = 0
    @objc dynamic var secondaryPhoneNo: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        reservationStatusId <- map[SerializationKeys.reservationStatusId]
        status <- map[SerializationKeys.status]
        updatedAt <- map[SerializationKeys.updatedAt]
        id <- map[SerializationKeys.id]
        noPeople <- map[SerializationKeys.noPeople]
        isActive <- map[SerializationKeys.isActive]
        createdAt <- map[SerializationKeys.createdAt]
        date <- map[SerializationKeys.date]
        title <- map[SerializationKeys.title]
        userId <- map[SerializationKeys.userId]
        secondaryPhoneNo <- map[SerializationKeys.secondaryPhoneNo]
    }
    
    
}
