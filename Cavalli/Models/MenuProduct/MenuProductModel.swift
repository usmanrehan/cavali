//
//  MenuProductModel.swift
//
//  Created by Hamza Hasan on 3/14/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm
public class MenuProductModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {    
    static let products = "products"
    static let categoryTypeId = "category_type_id"
    static let categoryImage = "category_image"
    static let categoryProduct = "category_product"
    static let deletedAt = "deleted_at"
    static let createdAt = "created_at"
    static let id = "id"
    static let image = "image"
    static let isActive = "is_active"
    static let name = "name"
    static let parentId = "parent_id"
    static let positionId = "position_id"
  }
    
  // MARK: Properties
    var products = List<Products>()
    @objc dynamic var categoryTypeId = 0
    @objc dynamic var categoryImage: String? = ""
    @objc dynamic var categoryProduct: String? = ""
    @objc dynamic var deletedAt: String? = ""
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var image: String? = ""
    @objc dynamic var isActive = 0
    @objc dynamic var name: String? = ""
    @objc dynamic var parentId = 0
    @objc dynamic var positionId = 0
  

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        products <- (map[SerializationKeys.products], ListTransform<Products>())
        categoryTypeId <- map[SerializationKeys.categoryTypeId]
        categoryImage <- map[SerializationKeys.categoryImage]
        categoryProduct <- map[SerializationKeys.categoryProduct]
        deletedAt <- map[SerializationKeys.deletedAt]
        createdAt <- map[SerializationKeys.createdAt]
        id <- map[SerializationKeys.id]
        image <- map[SerializationKeys.image]
        isActive <- map[SerializationKeys.isActive]
        name <- map[SerializationKeys.name]
        parentId <- map[SerializationKeys.parentId]
        positionId <- map[SerializationKeys.positionId]
    
    }


}
