//
//  TaxModel.swift
//
//  Created by Hamza Hasan on 3/31/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class TaxModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let value = "value"
    static let updatedAt = "updated_at"
    static let key = "key"
  }

  // MARK: Properties
  @objc dynamic var value: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var key: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

   

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    value <- map[SerializationKeys.value]
    updatedAt <- map[SerializationKeys.updatedAt]
    key <- map[SerializationKeys.key]
  }


}
