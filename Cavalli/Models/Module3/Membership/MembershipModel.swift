//
//  MembershipModel.swift
//
//  Created by Hamza Hasan on 4/11/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class MembershipModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let memberShipImages = "member_ship_images"
    static let isActive = "is_active"
    static let descriptionValue = "description"
    static let period = "period"
    static let price = "price"
    static let logoImage = "logo_image"
    static let membershipType = "membership_type"
    static let id = "id"
    static let title = "title"
    static let imageUrl = "image_url"
    static let days = "days"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  var memberShipImages = List<MemberShipImages>()
  @objc dynamic var isActive: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var period: String? = ""
  @objc dynamic var price: String? = ""
  @objc dynamic var logoImage: String? = ""
  @objc dynamic var membershipType: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var title: String? = ""
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var days: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    memberShipImages <- (map[SerializationKeys.memberShipImages], ListTransform<MemberShipImages>())
    isActive <- map[SerializationKeys.isActive]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    period <- map[SerializationKeys.period]
    price <- map[SerializationKeys.price]
    logoImage <- map[SerializationKeys.logoImage]
    membershipType <- map[SerializationKeys.membershipType]
    id <- map[SerializationKeys.id]
    title <- map[SerializationKeys.title]
    imageUrl <- map[SerializationKeys.imageUrl]
    days <- map[SerializationKeys.days]
  }


}
