//
//  ChatModel.swift
//
//  Created by Hamza Hasan on 4/9/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ChatModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let senderId = "sender_id"
    static let requestId = "request_id"
    static let id = "id"
    static let updatedAt = "updated_at"
    static let isActive = "is_active"
    static let createdAt = "created_at"
    static let receiverId = "receiver_id"
    static let receiverDetail = "receiver_detail"
    static let senderDetail = "sender_detail"
    static let message = "message"
  }

  // MARK: Properties
  @objc dynamic var senderId: String? = ""
  @objc dynamic var requestId: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var isActive: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var receiverId: String? = ""
  @objc dynamic var receiverDetail: ReceiverDetail?
  @objc dynamic var senderDetail: SenderDetail?
  @objc dynamic var message: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    senderId <- map[SerializationKeys.senderId]
    requestId <- map[SerializationKeys.requestId]
    id <- map[SerializationKeys.id]
    updatedAt <- map[SerializationKeys.updatedAt]
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
    receiverId <- map[SerializationKeys.receiverId]
    receiverDetail <- map[SerializationKeys.receiverDetail]
    senderDetail <- map[SerializationKeys.senderDetail]
    message <- map[SerializationKeys.message]
  }


}
