//
//  ReceiverDetail.swift
//
//  Created by Hamza Hasan on 4/9/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ReceiverDetail: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let email = "email"
    static let fullName = "full_name"
    static let profileImage = "profile_image"
    static let active = "active"
    static let isNotify = "is_notify"
    static let status = "status"
    static let lastName = "last_name"
    static let id = "id"
    static let firstName = "first_name"
    static let createdAt = "created_at"
    static let imageUrl = "image_url"
    static let userName = "user_name"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var email: String? = ""
  @objc dynamic var fullName: String? = ""
  @objc dynamic var profileImage: String? = ""
  @objc dynamic var active: String? = ""
  @objc dynamic var isNotify: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var lastName: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var firstName: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var userName: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    email <- map[SerializationKeys.email]
    fullName <- map[SerializationKeys.fullName]
    profileImage <- map[SerializationKeys.profileImage]
    active <- map[SerializationKeys.active]
    isNotify <- map[SerializationKeys.isNotify]
    status <- map[SerializationKeys.status]
    lastName <- map[SerializationKeys.lastName]
    id <- map[SerializationKeys.id]
    firstName <- map[SerializationKeys.firstName]
    createdAt <- map[SerializationKeys.createdAt]
    imageUrl <- map[SerializationKeys.imageUrl]
    userName <- map[SerializationKeys.userName]
  }


}
