//
//  ProductDetail.swift
//
//  Created by Hamza Hasan on 4/7/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class ProductDetail: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let isActive = "is_active"
    static let createdAt = "created_at"
    static let descriptionValue = "description"
    static let categoryId = "category_id"
    static let imageUrl = "image_url"
    static let title = "title"
    static let price = "price"
    static let logoImage = "logo_image"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isActive: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  @objc dynamic var categoryId: String? = ""
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var price: String? = ""
  @objc dynamic var logoImage: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    categoryId <- map[SerializationKeys.categoryId]
    imageUrl <- map[SerializationKeys.imageUrl]
    title <- map[SerializationKeys.title]
    price <- map[SerializationKeys.price]
    logoImage <- map[SerializationKeys.logoImage]
  }


}
