//
//  OrderHistory.swift
//
//  Created by Hamza Hasan on 4/7/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class OrrderHistory: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let tip = "tip"
    static let isActive = "is_active"
    static let orderNo = "order_no"
    static let orderStatusId = "order_status_id"
    static let paymentTypeId = "payment_type_id"
    static let totalAmount = "total_amount"
    static let orderProduct = "order_product"
    static let tax = "tax"
    static let id = "id"
    static let subTotal = "sub_total"
    static let createdAt = "created_at"
    static let userId = "user_id"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var tip: String? = ""
  @objc dynamic var isActive: String? = ""
  @objc dynamic var orderNo: String? = ""
  @objc dynamic var orderStatusId: String? = ""
  @objc dynamic var paymentTypeId: String? = ""
  @objc dynamic var totalAmount: String? = ""
  var orderProduct = List<OrderProduct>()
  @objc dynamic var tax: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var subTotal: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var userId: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    tip <- map[SerializationKeys.tip]
    isActive <- map[SerializationKeys.isActive]
    orderNo <- map[SerializationKeys.orderNo]
    orderStatusId <- map[SerializationKeys.orderStatusId]
    paymentTypeId <- map[SerializationKeys.paymentTypeId]
    totalAmount <- map[SerializationKeys.totalAmount]
    orderProduct <- (map[SerializationKeys.orderProduct], ListTransform<OrderProduct>())
    tax <- map[SerializationKeys.tax]
    id <- map[SerializationKeys.id]
    subTotal <- map[SerializationKeys.subTotal]
    createdAt <- map[SerializationKeys.createdAt]
    userId <- map[SerializationKeys.userId]
  }


}
