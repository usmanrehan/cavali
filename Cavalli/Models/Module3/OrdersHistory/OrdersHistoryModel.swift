//
//  OrdersHistoryModel.swift
//
//  Created by Hamza Hasan on 4/7/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class OrdersHistoryModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let pendingOrder = "pending_order"
    static let orderHistory = "order_history"
  }

  // MARK: Properties
  var pendingOrder = List<PendingOrder>()
  var orderHistory = List<OrrderHistory>()

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    pendingOrder <- (map[SerializationKeys.pendingOrder], ListTransform<PendingOrder>())
    orderHistory <- (map[SerializationKeys.orderHistory], ListTransform<OrrderHistory>())
  }


}
