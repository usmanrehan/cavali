//
//  MarkedEventsModel.swift
//
//  Created by Hamza Hasan on 4/10/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class MarkedEventsModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let isActive = "is_active"
    static let descriptionValue = "description"
    static let eventTypeId = "event_type_id"
    static let eventImages = "event_images"
    static let latitude = "latitude"
    static let logoImage = "logo_image"
    static let location = "location"
    static let id = "id"
    static let isMarked = "is_marked"
    static let isGrand = "is_grand"

    static let createdAt = "created_at"
    static let eventDate = "event_date"
    static let title = "title"
    static let imageUrl = "image_url"
    static let longitude = "longitude"
    static let parentId = "parent_id"

    
  }

  // MARK: Properties
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var isActive: String? = ""
    @objc dynamic var descriptionValue: String? = ""
    @objc dynamic var eventTypeId: String? = ""
    var eventImages = List<EventImages>()
    @objc dynamic var latitude: String? = ""
    @objc dynamic var logoImage: String? = ""
    @objc dynamic var location: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var isMarked = 0
    @objc dynamic var isGrand = 0
    
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var eventDate: String? = ""
    @objc dynamic var title: String? = ""
    @objc dynamic var imageUrl: String? = ""
    @objc dynamic var longitude: String? = ""
    @objc dynamic var parentId = 0

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        updatedAt <- map[SerializationKeys.updatedAt]
        isActive <- map[SerializationKeys.isActive]
        descriptionValue <- map[SerializationKeys.descriptionValue]
        eventTypeId <- map[SerializationKeys.eventTypeId]
        eventImages <- (map[SerializationKeys.eventImages], ListTransform<EventImages>())
        latitude <- map[SerializationKeys.latitude]
        logoImage <- map[SerializationKeys.logoImage]
        location <- map[SerializationKeys.location]
        id <- map[SerializationKeys.id]
        isMarked <- map[SerializationKeys.isMarked]
        createdAt <- map[SerializationKeys.createdAt]
        eventDate <- map[SerializationKeys.eventDate]
        title <- map[SerializationKeys.title]
        imageUrl <- map[SerializationKeys.imageUrl]
        longitude <- map[SerializationKeys.longitude]
        parentId <- map[SerializationKeys.parentId]
        isGrand <- map[SerializationKeys.isGrand]
    }
}
