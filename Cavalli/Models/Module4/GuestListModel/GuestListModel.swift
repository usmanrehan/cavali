//
//  GuestListModel.swift
//
//  Created by Hamza Hasan on 4/19/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class GuestListModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let guestListMember = "guest_list_member"
    
    static let isActive = "is_active"
    static let createdAt = "created_at"
    static let guestStatusId = "guest_status_id"
    static let title = "title"
    static let userId = "user_id"
    
    static let guestCategoryId = "guest_category_id"

  }

    
  // MARK: Properties
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var id = 0
    var guestListMember = List<GuestListMember>()
    @objc dynamic var isActive: String? = ""
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var guestStatusId: String? = ""
    @objc dynamic var title: String? = ""
    @objc dynamic var userId: String? = ""
    @objc dynamic var guestCategoryId = 0

    
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    guestListMember <- (map[SerializationKeys.guestListMember], ListTransform<GuestListMember>())
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
    guestStatusId <- map[SerializationKeys.guestStatusId]
    title <- map[SerializationKeys.title]
    userId <- map[SerializationKeys.userId]
    guestCategoryId <- map[SerializationKeys.guestCategoryId]
  }


}
