//
//  GuestListMember.swift
//
//  Created by Hamza Hasan on 4/19/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class GuestListMember: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let deletedAt = "deleted_at"
    static let createdAt = "created_at"
    static let id = "id"
    static let fullName = "full_name"
    static let isActive = "is_active"
    static let guestId = "guest_id"
    static let emailAddress = "email_address"
    static let mobileNumber = "mobile_no"
    static let nameTitle = "name_title"
    static let date = "date"
  }

    
  // MARK: Properties
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var deletedAt: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var fullName: String? = ""
    @objc dynamic var isActive: String? = ""
    @objc dynamic var guestId: String? = ""
    @objc dynamic var emailAddress: String? = ""
    @objc dynamic var mobileNumber: String? = ""
    @objc dynamic var nameTitle: String? = ""
    @objc dynamic var date: String? = ""
  

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    deletedAt <- map[SerializationKeys.deletedAt]
    createdAt <- map[SerializationKeys.createdAt]

    id <- map[SerializationKeys.id]
    fullName <- map[SerializationKeys.fullName]
    isActive <- map[SerializationKeys.isActive]
    guestId <- map[SerializationKeys.guestId]
    emailAddress <- map[SerializationKeys.emailAddress]
    mobileNumber <- map[SerializationKeys.mobileNumber]
    nameTitle <- map[SerializationKeys.nameTitle]
    date <- map[SerializationKeys.date]

  }


}
