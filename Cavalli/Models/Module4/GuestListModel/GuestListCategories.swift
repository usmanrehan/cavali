//
//  GuestListCategories.swift
//  Cavalli
//
//  Created by shardha kawal on 6/26/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

class GuestListCategories: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let updatedAt = "updated_at"
        static let id = "id"
        static let guestTypeDescription = "description"
        static let reason = "reason"
        static let createdAt = "created_at"
        static let status = "status"
        static let title = "title"
    }
    
    // MARK: Properties
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var guestTypeDescription: String? = ""
    @objc dynamic var reason: String? = ""
    @objc dynamic var status = 0
    @objc dynamic var title: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        updatedAt <- map[SerializationKeys.updatedAt]
        id <- map[SerializationKeys.id]
        guestTypeDescription <- map[SerializationKeys.guestTypeDescription]
        reason <- map[SerializationKeys.reason]
        createdAt <- map[SerializationKeys.createdAt]
        status <- map[SerializationKeys.status]
        title <- map[SerializationKeys.title]
        
    }

}

