//
//  MyGuestHistoryModel.swift
//
//  Created by Hamza Hasan on 4/23/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class MyGuestHistoryModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let history = "history"
    static let upcoming = "upcoming"
  }

  // MARK: Properties
  var history = List<History>()
  var upcoming = List<Upcoming>()

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

//    override public class func primaryKey() -> String? {
//    return "id"
//  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    history <- (map[SerializationKeys.history], ListTransform<History>())
    upcoming <- (map[SerializationKeys.upcoming], ListTransform<Upcoming>())
  }


}
