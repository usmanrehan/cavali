//
//  Upcoming.swift
//
//  Created by Hamza Hasan on 4/23/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class Upcoming: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let guestStatusId = "guest_status_id"
    static let id = "id"
    static let guestListCategory = "guest_list_category"
    static let guestListMember = "guest_list_member"
    static let isActive = "is_active"
    static let createdAt = "created_at"
    static let title = "title"
    static let userId = "user_id"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var guestStatusId = 0
  @objc dynamic var id = 0
    @objc dynamic var guestListCategory : GuestListCategories?
  var guestListMember = List<GuestListMember>()
  @objc dynamic var isActive: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var userId: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    guestStatusId <- map[SerializationKeys.guestStatusId]
    id <- map[SerializationKeys.id]
    guestListMember <- (map[SerializationKeys.guestListMember], ListTransform<GuestListMember>())
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
    title <- map[SerializationKeys.title]
    userId <- map[SerializationKeys.userId]
    guestListCategory <- map[SerializationKeys.guestListCategory]

  }


}
