//
//  CompetitionImages.swift
//
//  Created by Hamza Hasan on 3/30/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CompetitionImages: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let id = "id"
    static let image = "image"
    static let isActive = "is_active"
    static let createdAt = "created_at"
    static let competitionId = "competition_id"
    static let imageUrl = "image_url"
  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var image: String? = ""
  @objc dynamic var isActive: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var competitionId: String? = ""
  @objc dynamic var imageUrl: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    image <- map[SerializationKeys.image]
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
    competitionId <- map[SerializationKeys.competitionId]
    imageUrl <- map[SerializationKeys.imageUrl]
  }


}
