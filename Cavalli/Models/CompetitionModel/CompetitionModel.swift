//
//  CompetitionModel.swift
//
//  Created by Hamza Hasan on 3/30/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CompetitionModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let logoImage = "logo_image"
    static let updatedAt = "updated_at"
    static let id = "id"
    static let isActive = "is_active"
    static let createdAt = "created_at"
    static let descriptionValue = "description"
    static let competitionImages = "competition_images"
    static let validDate = "valid_date"
    static let imageUrl = "image_url"
    static let title = "title"
    static let serialNo = "serial_no"
    static let type = "type"

    
  }

  // MARK: Properties
  @objc dynamic var logoImage: String? = ""
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var isActive: String? = ""
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  var competitionImages = List<CompetitionImages>()
  @objc dynamic var validDate: String? = ""
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var title: String? = ""
    @objc dynamic var serialNo: String? = ""
    @objc dynamic var type: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    logoImage <- map[SerializationKeys.logoImage]
    updatedAt <- map[SerializationKeys.updatedAt]
    id <- map[SerializationKeys.id]
    isActive <- map[SerializationKeys.isActive]
    createdAt <- map[SerializationKeys.createdAt]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    competitionImages <- (map[SerializationKeys.competitionImages], ListTransform<CompetitionImages>())
    validDate <- map[SerializationKeys.validDate]
    imageUrl <- map[SerializationKeys.imageUrl]
    title <- map[SerializationKeys.title]
    serialNo <- map[SerializationKeys.serialNo]
    type <- map[SerializationKeys.type]
  }


}
