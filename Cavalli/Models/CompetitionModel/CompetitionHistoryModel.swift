//
//  CompetitionHistoryModel.swift
//
//  Created by Hamza Hasan on 3/30/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class CompetitionHistoryModel: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
  private struct SerializationKeys {
    static let updatedAt = "updated_at"
    static let isActive = "is_active"
    static let descriptionValue = "description"
    static let competitionImages = "competition_images"
    static let logoImage = "logo_image"
    static let status = "status"
    static let id = "id"
    static let createdAt = "created_at"
    static let title = "title"
    static let validDate = "valid_date"
    static let imageUrl = "image_url"
    static let serialNo = "serial_no"
    static let type = "type"

  }

  // MARK: Properties
  @objc dynamic var updatedAt: String? = ""
  @objc dynamic var isActive: String? = ""
  @objc dynamic var descriptionValue: String? = ""
  var competitionImages = List<CompetitionImages>()
  @objc dynamic var logoImage: String? = ""
  @objc dynamic var status: String? = ""
  @objc dynamic var id = 0
  @objc dynamic var createdAt: String? = ""
  @objc dynamic var title: String? = ""
  @objc dynamic var validDate: String? = ""
  @objc dynamic var imageUrl: String? = ""
  @objc dynamic var serialNo: String? = ""
    @objc dynamic var type: String? = ""

  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
  }

    override public class func primaryKey() -> String? {
    return "id"
  }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  public func mapping(map: Map) {
    updatedAt <- map[SerializationKeys.updatedAt]
    isActive <- map[SerializationKeys.isActive]
    descriptionValue <- map[SerializationKeys.descriptionValue]
    competitionImages <- (map[SerializationKeys.competitionImages], ListTransform<CompetitionImages>())
    logoImage <- map[SerializationKeys.logoImage]
    status <- map[SerializationKeys.status]
    id <- map[SerializationKeys.id]
    createdAt <- map[SerializationKeys.createdAt]
    title <- map[SerializationKeys.title]
    validDate <- map[SerializationKeys.validDate]
    imageUrl <- map[SerializationKeys.imageUrl]
    serialNo <- map[SerializationKeys.serialNo]
    type <- map[SerializationKeys.type]

  }


}
