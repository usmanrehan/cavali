//
//  LatestImageModel.swift
//  Cavalli
//
//  Created by shardha kawal on 8/6/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import UIKit
import ObjectMapper
import RealmSwift
import ObjectMapper_Realm

public class LatestImageModel: Object, Mappable {
    
    // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let id = "id"
        static let newsId = "news_id"
        static let image = "image"
        static let isActive = "is_active"
        static let createdAt = "created_at"
        static let deletedAt = "deleted_at"
        static let updatedAt = "updated_at"
        static let imageUrl = "image_url"
    }
    
    
    // MARK: Properties
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var deletedAt: String? = ""

    @objc dynamic var id = 0
    @objc dynamic var image: String? = ""
    @objc dynamic var isActive: String? = ""
    @objc dynamic var newsId: String? = ""
    @objc dynamic var imageUrl: String? = ""
    
    // MARK: ObjectMapper Initializers
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    
    required convenience public init?(map : Map){
        self.init()
    }
    
    override public class func primaryKey() -> String? {
        return "id"
    }
    
    /// Map a JSON object to this class using ObjectMapper.
    ///
    /// - parameter map: A mapping from ObjectMapper.
    public func mapping(map: Map) {
        updatedAt <- map[SerializationKeys.updatedAt]
        createdAt <- map[SerializationKeys.createdAt]
        deletedAt <- map[SerializationKeys.deletedAt]

        id <- map[SerializationKeys.id]
        image <- map[SerializationKeys.image]
        isActive <- map[SerializationKeys.isActive]
        newsId <- map[SerializationKeys.newsId]
        imageUrl <- map[SerializationKeys.imageUrl]
    }
    
    
}

