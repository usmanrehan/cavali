//
//  User.swift
//
//  Created by Hamza Hasan on 3/10/18
//  Copyright (c) . All rights reserved.
//


import ObjectMapper
import RealmSwift
import Realm

public class User: Object, Mappable {

  // MARK: Declaration for string constants to be used to decode and also serialize.
    private struct SerializationKeys {
        static let updatedAt = "updated_at"
        static let email = "email"
        static let token = "token"
        static let active = "active"
        static let isNotify = "is_notify"
        static let verificationCode = "verification_code"
        static let status = "status"
        static let id = "id"
        static let createdAt = "created_at"
        static let phone = "phone"
        static let userName = "user_name"
        static let imageUrl = "image_url"
        static let dateOfBirth = "dob"
        static let gender = "gender"
        static let socialMediaType = "socialmedia_type"
        static let socialMediaId = "socialmedia_id"
    }
    
    // MARK: Properties
    @objc dynamic var updatedAt: String? = ""
    @objc dynamic var email: String? = ""
    @objc dynamic var token: String? = ""
    @objc dynamic var active: String? = ""
    @objc dynamic var isNotify: String? = ""
    @objc dynamic var verificationCode = 0
    @objc dynamic var status: String? = ""
    @objc dynamic var id = 0
    @objc dynamic var createdAt: String? = ""
    @objc dynamic var phone: String? = ""
    @objc dynamic var userName: String? = ""
    @objc dynamic var imageUrl: String? = ""
    @objc dynamic var dateOfBirth: String? = ""
    @objc dynamic var gender: String? = ""
    @objc dynamic var socialMediaType: String? = ""
    @objc dynamic var socialMediaId: String? = ""
  // MARK: ObjectMapper Initializers
  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.

    required convenience public init?(map : Map){
    self.init()
    }

    override public class func primaryKey() -> String? {
    return "id"
    }

  /// Map a JSON object to this class using ObjectMapper.
  ///
  /// - parameter map: A mapping from ObjectMapper.
  
    public func mapping(map: Map) {
        updatedAt <- map[SerializationKeys.updatedAt]
        email <- map[SerializationKeys.email]
        token <- map[SerializationKeys.token]
        active <- map[SerializationKeys.active]
        isNotify <- map[SerializationKeys.isNotify]
        verificationCode <- map[SerializationKeys.verificationCode]
        status <- map[SerializationKeys.status]
        id <- map[SerializationKeys.id]
        createdAt <- map[SerializationKeys.createdAt]
        phone <- map[SerializationKeys.phone]
        userName <- map[SerializationKeys.userName]
        imageUrl <- map[SerializationKeys.imageUrl]
        dateOfBirth <- map[SerializationKeys.dateOfBirth]
        gender <- map[SerializationKeys.gender]
        socialMediaId <- map[SerializationKeys.socialMediaId]
        socialMediaType <- map[SerializationKeys.socialMediaType]
    }


}
