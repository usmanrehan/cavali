//
//  BookCartModel.swift
//  Auditix
//
//  Created by Ingic Development Team on 1/12/18.
//  Copyright © 2018 Ingic. All rights reserved.
//

import Foundation
import RealmSwift

class Cart: Object {
    
    @objc dynamic var id = 0
    @objc dynamic var userId = 0
    @objc dynamic var product: Products!
    @objc dynamic var count = 1
    
    override public class func primaryKey() -> String? {
        return "id"
    }
}
